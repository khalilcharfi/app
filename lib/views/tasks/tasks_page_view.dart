/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'package:famedly/components/default_floating_action_button.dart';
import 'package:famedly/components/draggable_scrollbar.dart';
import 'package:famedly/components/nav_scaffold.dart';
import 'package:famedly/components/task_list_tile.dart';
import 'package:famedly/styles/colors.dart';
import 'package:famedly/views/tasks/tasks_page.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:famedly/styles/famedly_icons_icons.dart';

class TasksPageView extends StatelessWidget {
  final TasksPageController controller;

  const TasksPageView(this.controller, {Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return NavScaffold(
      key: Key('tasks_page'),
      activePage: MainPage.tasks,
      floatingActionButton: DefaultFloatingActionButton(
        scrollController: controller.scrollController,
      ),
      appBar: AppBar(
        automaticallyImplyLeading: false,
        centerTitle: true,
        title: Text(
          L10n.of(context).myTasks,
        ),
        actions: <Widget>[
          PopupMenuButton(
            key: Key('PopupMenu'),
            onSelected: (_) => controller.clearCompletedTasks(),
            icon: Icon(FamedlyIcons.menu),
            itemBuilder: (BuildContext context) => <PopupMenuEntry>[
              PopupMenuItem(
                key: Key('ClearPopupMenuItem'),
                value: 'clear',
                child: Text(L10n.of(context).clearCompletedTasks),
              )
            ],
          ),
        ],
      ),
      body: Builder(builder: (context) {
        controller.sortTasks();
        if (controller.tasks.isEmpty) {
          return Center(
            child: Text(L10n.of(context).noTasks),
          );
        }
        return FamedlyDraggableScrollbar(
          isAlwaysShown: kIsWeb,
          controller: controller.scrollController,
          child: ListView.builder(
            controller: controller.scrollController,
            itemCount:
                controller.tasks.length + (controller.doneCount != 0 ? 1 : 0),
            itemBuilder: (context, index) {
              // render the Done seperator, if there are any done tasks
              if (controller.doneCount != 0 &&
                  index == controller.tasks.length - controller.doneCount) {
                final dividerColor =
                    Theme.of(context).brightness == Brightness.light
                        ? FamedlyColors.paleGrey
                        : FamedlyColors.slate;
                return Column(
                  children: <Widget>[
                    Divider(thickness: 1, height: 1, color: dividerColor),
                    SizedBox(height: 10),
                    ListTile(
                      key: Key('DoneListTile'),
                      title: Text(
                          '${L10n.of(context).done} (${controller.doneCount})'),
                      trailing: Icon(controller.showDone
                          ? Icons.keyboard_arrow_up
                          : Icons.keyboard_arrow_down),
                      onTap: () {
                        controller.showDoneTasks();
                      },
                    )
                  ],
                );
              }
              // After rendering the virtual done list tile we need to shift
              // our lookups one index down
              if (controller.doneCount != 0 &&
                  index > controller.tasks.length - controller.doneCount) {
                index -= 1;
              }
              final task = controller.tasks[index];
              if (!controller.showDone && task.done) {
                return Container();
              }
              return TaskListTile(task: task);
            },
          ),
        );
      }),
    );
  }
}
