/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'package:famedly/styles/famedly_icons_icons.dart';
import 'package:flutter/material.dart';
import 'package:fhir/r4.dart' as fhir;
import 'package:flutter_gen/gen_l10n/l10n.dart';

class QuestionnaireItemsBuilder extends StatelessWidget {
  final List<fhir.QuestionnaireItem> items;
  final void Function(String, dynamic) onChanged;

  const QuestionnaireItemsBuilder({
    Key key,
    @required this.items,
    @required this.onChanged,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      physics: NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      itemCount: items?.length ?? 0,
      itemBuilder: (BuildContext context, int i) {
        return _ItemBuilderWithChildren(item: items[i], onChanged: onChanged);
      },
    );
  }
}

class _ItemBuilderWithChildren extends StatelessWidget {
  final fhir.QuestionnaireItem item;
  final void Function(String, dynamic) onChanged;

  const _ItemBuilderWithChildren({Key key, this.item, this.onChanged})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final itemWidget = _ItemBuilder(item: item, onChanged: onChanged);
    if (item.item?.isNotEmpty ?? false) {
      return Padding(
        padding: const EdgeInsets.only(bottom: 16.0),
        child: Material(
          elevation: 1,
          borderRadius: BorderRadius.circular(4),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                ListTile(
                  title: Text(
                    '${item.text}:',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  contentPadding: EdgeInsets.zero,
                ),
                itemWidget,
                Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    for (var item in item.item)
                      _ItemBuilderWithChildren(
                          item: item, onChanged: onChanged),
                  ],
                ),
              ],
            ),
          ),
        ),
      );
    }
    return itemWidget;
  }
}

class _ItemBuilder extends StatefulWidget {
  final fhir.QuestionnaireItem item;
  final void Function(String, dynamic) onChanged;

  const _ItemBuilder({Key key, @required this.item, @required this.onChanged})
      : super(key: key);

  @override
  __ItemBuilderState createState() => __ItemBuilderState();
}

class __ItemBuilderState extends State<_ItemBuilder> {
  var _currentValue;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 16.0),
      child: Builder(
        builder: (context) {
          switch (widget.item.type) {
            case fhir.QuestionnaireItemType.time:
              return ListTile(
                leading: CircleAvatar(
                  child: Icon(FamedlyIcons.clock),
                ),
                title: Text(widget.item.text),
                subtitle: Text(_currentValue ?? L10n.of(context).pleaseChoose),
                onTap: () async {
                  final timeOfDay = await showTimePicker(
                    context: context,
                    initialTime: TimeOfDay.now(),
                  );
                  if (timeOfDay != null) {
                    setState(() => _currentValue =
                        '${timeOfDay.hour.toString().padLeft(2, '0')}:${timeOfDay.minute.toString().padLeft(2, '0')} Uhr');
                    widget.onChanged(widget.item.text, _currentValue);
                  }
                },
              );
            case fhir.QuestionnaireItemType.date:
              return ListTile(
                leading: CircleAvatar(
                  child: Icon(Icons.calendar_today_outlined),
                ),
                title: Text(widget.item.text),
                subtitle: Text(_currentValue ?? L10n.of(context).pleaseChoose),
                onTap: () async {
                  final date = await showDatePicker(
                    firstDate: DateTime.parse('1900-01-01'),
                    lastDate: DateTime.parse('2100-01-01'),
                    context: context,
                    initialDate: DateTime.now(),
                  );
                  if (date != null) {
                    setState(() => _currentValue =
                        '${date.month.toString().padLeft(2, '0')}.${date.day.toString().padLeft(2, '0')}.${date.year.toString()}');
                    widget.onChanged(widget.item.text, _currentValue);
                  }
                },
              );
            case fhir.QuestionnaireItemType.datetime:
              return ListTile(
                leading: CircleAvatar(
                  child: Icon(Icons.calendar_today_outlined),
                ),
                title: Text(widget.item.text),
                subtitle: Text(_currentValue ?? L10n.of(context).pleaseChoose),
                onTap: () async {
                  final date = await showDatePicker(
                    firstDate: DateTime.parse('1900-01-01'),
                    lastDate: DateTime.parse('2100-01-01'),
                    context: context,
                    initialDate: DateTime.now(),
                  );
                  final timeOfDay = await showTimePicker(
                    context: context,
                    initialTime: TimeOfDay.now(),
                  );
                  if (date != null) {
                    setState(() => _currentValue =
                        '${timeOfDay.hour.toString().padLeft(2, '0')}:${timeOfDay.minute.toString().padLeft(2, '0')} Uhr - ${date.month.toString().padLeft(2, '0')}.${date.day.toString().padLeft(2, '0')}.${date.year.toString()}');
                    widget.onChanged(widget.item.text, _currentValue);
                  }
                },
              );
            case fhir.QuestionnaireItemType.boolean:
              return CheckboxListTile(
                value: _currentValue ?? false,
                title: Text(widget.item.text),
                onChanged: (b) {
                  if (b != null) {
                    setState(() => _currentValue = b);
                    widget.onChanged(widget.item.text, b);
                  }
                },
              );
            case fhir.QuestionnaireItemType.display:
            case fhir.QuestionnaireItemType.group:
              return Text(widget.item.text);
            case fhir.QuestionnaireItemType.choice:
              return DropdownButtonFormField<String>(
                onChanged: (text) => text != null
                    ? widget.onChanged(widget.item.text, text)
                    : null,
                value: '',
                items: widget.item.answerOption
                    .map(
                      (answerOption) => DropdownMenuItem<String>(
                        value: answerOption.value,
                        child: Text(
                          answerOption.value,
                          overflow: TextOverflow.ellipsis,
                          maxLines: 1,
                        ),
                      ),
                    )
                    .toList()
                      ..insert(
                        0,
                        DropdownMenuItem<String>(
                          value: '',
                          child: Opacity(
                            opacity: 0.5,
                            child: Text(widget.item.text),
                          ),
                        ),
                      ),
              );
            case fhir.QuestionnaireItemType.text:
            case fhir.QuestionnaireItemType.string:
            case fhir.QuestionnaireItemType.decimal:
            case fhir.QuestionnaireItemType.integer:
            default:
              return TextFormField(
                keyboardType: {
                  fhir.QuestionnaireItemType.decimal,
                  fhir.QuestionnaireItemType.integer,
                }.contains(widget.item.type)
                    ? TextInputType.number
                    : widget.item.type == fhir.QuestionnaireItemType.text
                        ? TextInputType.multiline
                        : null,
                minLines:
                    widget.item.type == fhir.QuestionnaireItemType.text ? 3 : 1,
                maxLines:
                    widget.item.type == fhir.QuestionnaireItemType.text ? 3 : 1,
                onChanged: (text) => widget.onChanged(widget.item.text, text),
                decoration: InputDecoration(
                  labelText: widget.item.text,
                  border: OutlineInputBorder(),
                ),
                validator: (text) {
                  if (widget.item.required_?.toString() == 'true' &&
                      text.isEmpty) {
                    return L10n.of(context).thisFieldIsRequired;
                  }
                  return null;
                },
              );
          }
        },
      ),
    );
  }
}

extension on fhir.QuestionnaireAnswerOption {
  String get value =>
      valueCoding?.display ?? valueString ?? valueInteger.toString();
}
