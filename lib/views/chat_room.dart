/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'dart:async';
import 'dart:io';

import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:adaptive_page_layout/adaptive_page_layout.dart';
import 'package:famedly/components/FamedlyPopupMenuButton.dart';
import 'package:famedly/config/forms.dart';
import 'package:famedly/styles/famedly_icons_v2_icons.dart';
import 'package:famedly/utils/famedlysdk_store.dart';
import 'package:famedly/utils/matrix_locals.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:flutter_dropzone/flutter_dropzone.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:future_loading_dialog/future_loading_dialog.dart';
import 'package:keep_keyboard_popup_menu/keep_keyboard_popup_menu.dart';
import 'package:pedantic/pedantic.dart';

import '../components/avatar.dart';
import '../components/chat_room/input_field.dart';
import '../components/connection_status_header.dart';
import '../components/matrix_event_list.dart';
import '../models/famedly.dart';
import '../models/patients/room_patients_extension.dart';
import '../models/requests/request.dart';
import '../models/requests/room_extension.dart';
import '../models/room_type_extension.dart';
import '../styles/colors.dart';
import '../utils/date_time_extension.dart';
import '../utils/room_archive_dialog_extension.dart';
import '../utils/room_send_file_extension.dart';
import '../utils/room_status_extension.dart';

class ChatRoom extends StatefulWidget {
  final String id;
  final Room room;
  final String routeName;

  ChatRoom({
    Key key,
    @required this.id,
    this.room,
    @required this.routeName,
  }) : super(key: key ?? Key('chatroom-$id'));

  @override
  ChatRoomState createState() => ChatRoomState();
}

class ChatRoomState extends State<ChatRoom> {
  Room room;
  Event _replyTo;
  Famedly famedly;

  DropzoneViewController _dropzoneViewController;

  bool _dropZoneHovered = false;

  bool directChatLeftDialogVisible = false;

  bool showAcceptDeny = false;
  final GlobalKey<InputFieldState> focusKey = GlobalKey<InputFieldState>();

  @override
  void dispose() {
    famedly.activeRoomID = null;
    famedly.roomsPlugin.onLeaveRoom = null;
    super.dispose();
  }

  Future<void> _acceptRequest(BuildContext context) async {
    final req = Request(room);
    final bundle = Request.getRequestBundle(room.client, req.content.requestId);

    if (!req.isAuthor() ||
        (bundle.broadcastRoom == null && bundle.rooms.length == 1)) {
      final success = await showFutureLoadingDialog(
        context: context,
        future: () => req.accept(),
      );
      if (success.error == null) {
        setState(() {
          room = famedly.client.getRoomById(widget.id);
        });
      }
    } else {
      if (OkCancelResult.ok ==
          await showOkCancelAlertDialog(
            context: context,
            title: L10n.of(context).acceptBroadcastNotice,
            okLabel: L10n.of(context).yes,
            cancelLabel: L10n.of(context).cancel,
            useRootNavigator: false,
          )) {
        await showFutureLoadingDialog(
          context: context,
          future: () => Request.closeBroadcast(bundle, room),
        );
        setState(() {
          room = famedly.client.getRoomById(widget.id);
        });
      }
    }
  }

  void _dropFileAction(ev) async {
    setState(() => _dropZoneHovered = false);
    final name = await _dropzoneViewController.getFilename(ev);
    final bytes = await _dropzoneViewController.getFileData(ev);
    final matrixFile = MatrixFile(
      bytes: bytes,
      name: name,
    );
    await showFutureLoadingDialog(
      context: context,
      future: () => room
          .sendFileEventWithThumbnail(matrixFile.mimeType.startsWith('image')
              ? MatrixImageFile(
                  bytes: bytes,
                  name: name,
                )
              : matrixFile),
    );
  }

  Future<void> _rejectRequest(BuildContext context) async {
    final req = Request(room);
    await showFutureLoadingDialog(
        context: context,
        future: () async {
          await req.reject();
          await room.leave();
        });
    setState(() {
      room = famedly.client.getRoomById(widget.id);
    });
  }

  void _goToChatDetails() {
    if (!room.isDirectChat && room.membership != Membership.leave) {
      AdaptivePageLayout.of(context).pushNamed('/room/${widget.id}/settings');
    } else if (room.isDirectChat && room.membership != Membership.leave) {
      AdaptivePageLayout.of(context)
          .pushNamed('/room/${widget.id}/member/${room.directChatMatrixID}');
    }
  }

  List<KeepKeyboardPopupMenuItem> getPopupList(
      BuildContext context, ClosePopupFn closePopup, bool isRequestRoom) {
    final list = <KeepKeyboardPopupMenuItem>[];
    if (!isRequestRoom) {
      list.add(KeepKeyboardPopupMenuItem(
        key: Key('ChatDetailsPopupMenuItem'),
        onTap: () async {
          unawaited(closePopup());
          if (room.isDirectChat) {
            await AdaptivePageLayout.of(context).pushNamed(
                '/room/${widget.id}/member/${room.directChatMatrixID}');
          } else {
            await AdaptivePageLayout.of(context)
                .pushNamed('/room/${widget.id}/settings');
          }
        },
        child: Text(L10n.of(context).chatDetails),
      ));
    }
    if (room.membership != Membership.leave) {
      list.add(KeepKeyboardPopupMenuItem(
        key: Key('InvitePopupMenuItem'),
        onTap: () async {
          unawaited(closePopup());
          await AdaptivePageLayout.of(context)
              .pushNamed('${widget.routeName}/invite');
        },
        child: Text(L10n.of(context).addParticipant),
      ));
      if (room.encrypted || room.client.encryptionEnabled) {
        list.add(KeepKeyboardPopupMenuItem(
          key: Key('EncryptionSettingsPopupMenuItem'),
          onTap: () async {
            unawaited(closePopup());
            if (room.encrypted) {
              await AdaptivePageLayout.of(context)
                  .pushNamed('${widget.routeName}/encryption');
            } else {
              if (OkCancelResult.ok ==
                  await showOkCancelAlertDialog(
                    context: context,
                    title:
                        L10n.of(context).enableExperimentalEndToEndEncryption,
                    okLabel: L10n.of(context).yes,
                    cancelLabel: L10n.of(context).cancel,
                    message: L10n.of(context).encryptionErrorMessage,
                    useRootNavigator: false,
                  )) {
                await showFutureLoadingDialog(
                  context: context,
                  future: () => room.enableEncryption(),
                );
              }
            }
          },
          child: Text(L10n.of(context).encryption),
        ));
      }
      list.add(KeepKeyboardPopupMenuItem(
        key: Key('ArchivePopupMenuItem'),
        onTap: () async {
          unawaited(closePopup());
          await room.showArchiveDialog(context);
        },
        child: Text(
            isRequestRoom
                ? L10n.of(context).leaveRequest
                : L10n.of(context).archiveAction,
            style: TextStyle(color: FamedlyColors.grapefruit)),
      ));
      if (showAcceptDeny) {
        list.add(KeepKeyboardPopupMenuItem(
          key: Key('AcceptRequestPopupMenuItem'),
          onTap: () async {
            unawaited(closePopup());
            await _acceptRequest(context);
          },
          child: Text(L10n.of(context).acceptInquiry),
        ));
        list.add(KeepKeyboardPopupMenuItem(
          key: Key('RejectRequestPopupMenuItem'),
          onTap: () async {
            unawaited(closePopup());
            await _rejectRequest(context);
          },
          child: Text(L10n.of(context).rejectInquiry),
        ));
      }
    }

    list.add(
      KeepKeyboardPopupMenuItem(
        key: Key('SearchPopupMenuItem'),
        onTap: () async {
          unawaited(closePopup());
          await AdaptivePageLayout.of(context)
              .pushNamed('/room/${widget.id}/search');
        },
        child: Text(L10n.of(context).search),
      ),
    );

    return list;
  }

  void loadRoom() {
    room ??= famedly.client.getRoomById(widget.id);
    if (room == null) {
      // It is possible that we join a room which is not yet synced
      famedly.client.onEvent.stream
          .firstWhere((EventUpdate r) => r.roomID == widget.id)
          .then((EventUpdate r) {
        room = famedly.client.getRoomById(widget.id);
        if (room != null && mounted) {
          setState(() {});
        }
      });
    }
    room ??= widget.room ??
        Room(
            id: widget.id,
            client: famedly.client,
            membership: Membership.leave);
  }

  void showDirectChatLeftDialog(BuildContext context) async {
    if (directChatLeftDialogVisible) return;
    directChatLeftDialogVisible = true;
    final directChatId = room.directChatMatrixID;
    final directChatName = room
        .getUserByMXIDSync(directChatId)
        .calcDisplayname(mxidLocalPartFallback: false);
    await showFutureLoadingDialog(context: context, future: () => room.leave());
    setState(() {
      room.membership = Membership.leave;
    });
    if (OkCancelResult.ok ==
        await showOkCancelAlertDialog(
          context: context,
          title: L10n.of(context).hasArchivedConversation(directChatName),
          okLabel: L10n.of(context).startNewConversation,
          cancelLabel: L10n.of(context).back,
          useRootNavigator: false,
        )) {
      final roomID = await User(directChatId, room: room).startDirectChat();
      if (roomID != null) {
        await AdaptivePageLayout.of(context)
            .pushNamedAndRemoveUntilIsFirst('/room/$roomID');
      }
    }
  }

  void _onSendText(String message) {
    room.sendTextEvent(message, inReplyTo: _replyTo);
    // now we check if we need to ask for information, if we are a broadcast request room
    if (room.isBroadcastRequest) {
      final req = Request(room);
      if (!req.isAuthor() &&
          !req.youAccepted() &&
          !req.youNeedInfo() &&
          !req.youRejected()) {
        req.needInfo();
      }
    }
  }

  Future<void> _onSendAudio(String path) async {
    final file = File(path);
    final bytes = await file.readAsBytes();
    final audioFile = MatrixAudioFile(
        bytes: bytes, name: '$path.${InputField.defaultAudioFileType}');

    try {
      final uploadResp =
          await room.sendFileEvent(audioFile, inReplyTo: _replyTo);

      final mxcUrl = uploadResp;
      final url = Uri.parse(mxcUrl).getDownloadLink(Famedly.of(context).client);

      // Put file in cache
      await DefaultCacheManager().putFile(url.toString(), bytes);
    } catch (e) {
      AdaptivePageLayout.of(context).showSnackBar(
        SnackBar(
          content: Text(e.toLocalizedString(context)),
        ),
      );
      return;
    }
  }

  @override
  Widget build(BuildContext context) {
    famedly = Famedly.of(context);

    famedly.roomsPlugin.onLeaveRoom = (String roomID) {
      if (roomID == room.id) {
        setState(() {
          room.membership = Membership.leave;
        });
      }
    };

    loadRoom();

    return StreamBuilder<String>(
        stream: room.onUpdate.stream,
        builder: (context, snapshot) {
          final isRequestRoom = room.type == roomTypeRequest;

          var displayname =
              room.getLocalizedDisplayname(MatrixLocals(L10n.of(context)));
          if (isRequestRoom) {
            final req = Request(room);
            displayname = req.description;
            final subname = req.isAuthor()
                ? req.organisation
                : room.requestContent.requestingOrganisation
                    .replaceAll('.famedly.de', '');
            if (subname != null && subname.isNotEmpty) {
              displayname += ' / ' + subname;
            }
            showAcceptDeny = room.isBroadcastRequest &&
                req.content.requestType !=
                    'com.famedly.request.${Forms.defaultForm}' &&
                ((req.isAuthor() && req.remoteAccepted()) || !req.isAuthor()) &&
                !req.youAccepted() &&
                !req.youRejected();
          }
          if (room.type == roomTypePatient) {
            displayname = room
                .getLocalizedFamedlyDisplayname(MatrixLocals(L10n.of(context)));
          }

          if (room.isDirectChat &&
              room.getUserByMXIDSync(room.directChatMatrixID).membership ==
                  Membership.leave) {
            Timer(Duration(milliseconds: 500),
                () => showDirectChatLeftDialog(context));
          }

          final showArchiveButton = isRequestRoom &&
              room.mJoinedMemberCount + room.mInvitedMemberCount == 1;

          return Scaffold(
            key: Key('chat_room'),
            appBar: PreferredSize(
              preferredSize: const Size.fromHeight(56.0),
              child: AppBar(
                centerTitle: false,
                key: Key('ChatRoomAppBar'),
                title: room.patientData != null
                    ? ListTile(
                        key: Key('GoToPatientsSettings'),
                        contentPadding:
                            AdaptivePageLayout.of(context).columnMode(context)
                                ? null
                                : EdgeInsets.zero,
                        onTap: () => AdaptivePageLayout.of(context)
                            .pushNamed('/patients/${room.id}'),
                        leading:
                            Avatar(room.avatar, name: room.patientData.name),
                        title: Text(
                          '${room.patientData.name} ${room.patientData.birthday == null ? "" : "(" + room.patientData.birthday.age.toString() + ")"}',
                          style: TextStyle(fontSize: 16),
                        ),
                        subtitle: Text('Fall-ID: ${room.patientData.caseId}',
                            style: TextStyle(
                              color: FamedlyColors.blueyGrey,
                              fontSize: 12,
                            )),
                      )
                    : ListTile(
                        key: Key('GoToChatDetails'),
                        onTap: () {
                          if (!isRequestRoom) _goToChatDetails();
                        },
                        contentPadding: EdgeInsets.zero,
                        title: Text(
                          displayname,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                            fontSize: FamedlyStyles.columnMode ? 16 : null,
                            color: Theme.of(context).textTheme.bodyText1.color,
                          ),
                        ),
                        subtitle: Text(
                          room.getLocalizedStatus(context),
                          maxLines: 1,
                          style: TextStyle(
                            color: FamedlyColors.slateGrey,
                            fontSize: 12,
                          ),
                        ),
                      ),
                actions: <Widget>[
                  room.membership == Membership.leave
                      ? Container()
                      : FamedlyPopupMenuButton(
                          key: Key('PopupMenu'),
                          icon: Icon(FamedlyIconsV2.more),
                          menuItemBuilder: (context, closePopup) =>
                              getPopupList(context, closePopup, isRequestRoom),
                        ),
                ],
              ),
            ),
            body: SafeArea(
              child: Stack(
                children: [
                  FutureBuilder<Object>(
                      future: Store().getItem(Famedly.wallpaperNamespace),
                      builder: (context, snapshot) {
                        var wallpaper = 0;
                        if (snapshot.data != null) {
                          if (snapshot.data is int) {
                            wallpaper = snapshot.data;
                          } else if (snapshot.data is String) {
                            wallpaper = int.parse(snapshot.data);
                          }
                        }
                        if (wallpaper < 1 ||
                            wallpaper > Famedly.wallpaperCount) {
                          wallpaper = 0;
                        }
                        final darkmode =
                            Theme.of(context).brightness == Brightness.dark &&
                                wallpaper == 0;
                        return Image.asset(
                          'assets/images/wallpaper/$wallpaper.png',
                          fit: BoxFit.cover,
                          width: double.infinity,
                          height: double.infinity,
                          color: darkmode
                              ? Theme.of(context).scaffoldBackgroundColor
                              : null,
                          colorBlendMode: darkmode ? BlendMode.darken : null,
                        );
                      }),
                  if (kIsWeb)
                    DropzoneView(
                      operation: DragOperation.copy,
                      onCreated: (ctrl) => _dropzoneViewController = ctrl,
                      onError: (String error) =>
                          AdaptivePageLayout.of(context).showSnackBar(
                        SnackBar(
                          content: Text(error),
                        ),
                      ),
                      onHover: () => _dropZoneHovered
                          ? null
                          : setState(() => _dropZoneHovered = true),
                      onDrop: _dropFileAction,
                      onLeave: () => !_dropZoneHovered
                          ? null
                          : setState(() => _dropZoneHovered = false),
                    ),
                  Column(
                    children: <Widget>[
                      if (showAcceptDeny || showArchiveButton)
                        Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Container(
                              height: 90,
                              color: Theme.of(context).selectedRowColor,
                              child: Center(
                                child: showArchiveButton
                                    ? ElevatedButton(
                                        onPressed: () =>
                                            room.showArchiveDialog(context),
                                        style: ElevatedButton.styleFrom(
                                          primary:
                                              Theme.of(context).backgroundColor,
                                          onPrimary: Theme.of(context)
                                              .textTheme
                                              .bodyText1
                                              .color,
                                        ),
                                        child: Text(
                                            L10n.of(context).archiveAction),
                                      )
                                    : Row(
                                        mainAxisSize: MainAxisSize.min,
                                        children: <Widget>[
                                          ElevatedButton(
                                            onPressed: () =>
                                                _rejectRequest(context),
                                            style: ElevatedButton.styleFrom(
                                              primary: Theme.of(context)
                                                  .backgroundColor,
                                              onPrimary:
                                                  FamedlyColors.grapefruit,
                                            ),
                                            child: Text(
                                              L10n.of(context).decline,
                                            ),
                                          ),
                                          SizedBox(width: 23),
                                          ElevatedButton(
                                            onPressed: () =>
                                                _acceptRequest(context),
                                            child:
                                                Text(L10n.of(context).accept),
                                          ),
                                        ],
                                      ),
                              ),
                            ),
                            Divider(height: 1),
                          ],
                        ),
                      Expanded(
                        child: Column(
                          children: [
                            ConnectionStatusHeader(
                              hide: AdaptivePageLayout.of(context)
                                  .columnMode(context),
                            ),
                            Expanded(
                              child: MatrixEventList(
                                room,
                                onReply: (event) =>
                                    setState(() => _replyTo = event),
                              ),
                            ),
                          ],
                        ),
                      ),
                      if (room.membership != Membership.leave &&
                          !showArchiveButton)
                        InputField(
                          room,
                          onSendText: _onSendText,
                          onSendAudio: _onSendAudio,
                          replyTo: _replyTo,
                          key: focusKey,
                          onCloseReply: () => setState(() => _replyTo = null),
                        ),
                    ],
                  ),
                  if (kIsWeb && _dropZoneHovered)
                    Opacity(
                      opacity: 0.9,
                      child: Container(
                        color: Theme.of(context).backgroundColor,
                        alignment: Alignment.center,
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(FamedlyIconsV2.up_1, size: 40),
                            Text(
                              L10n.of(context).putFilesHereToSend,
                              style: TextStyle(
                                fontSize: 20,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                ],
              ),
            ),
          );
        });
  }
}
