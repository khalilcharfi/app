/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'package:famedly/styles/colors.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:url_launcher/url_launcher.dart';

class QrScannerPage extends StatefulWidget {
  final Function(String code) onQrCode;

  const QrScannerPage({this.onQrCode, Key key}) : super(key: key);

  static void scan(BuildContext context,
      {Function(String code) onQrCode}) async {
    final status = Permission.camera.request();
    if (await status.isGranted) {
      await Navigator.of(context, rootNavigator: false).push(
        MaterialPageRoute(
          builder: (context) => QrScannerPage(onQrCode: onQrCode),
        ),
      );
    }
  }

  @override
  _QrScannerPageState createState() => _QrScannerPageState();
}

class _QrScannerPageState extends State<QrScannerPage> {
  QRViewController controller;
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
  @override
  BuildContext context;

  bool _scanned = false;

  void _onQRViewCreated(QRViewController controller) {
    this.controller = controller;
    controller.scannedDataStream.listen((scanData) async {
      if (_scanned) return;
      _scanned = true;
      Logs().d('QR Code scanned: ${scanData.code}');
      if (widget.onQrCode != null) {
        widget.onQrCode(scanData.code);
      } else {
        await launch(scanData.code);
      }
    });
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    this.context = context;
    return Scaffold(
      appBar: AppBar(
        title: ListTile(
          contentPadding: EdgeInsets.zero,
          trailing: Image.asset(
            'assets/images/qr.png',
            width: 32,
            height: 32,
          ),
          title: Text(L10n.of(context).scanTheQrCode),
        ),
      ),
      body: Padding(
        padding: EdgeInsets.zero,
        child: QRView(
          formatsAllowed: [BarcodeFormat.qrcode],
          overlay: QrScannerOverlayShape(
            borderColor: FamedlyColors.azul,
            borderRadius: 10,
            borderLength: 30,
            borderWidth: 8,
            cutOutSize: MediaQuery.of(context).size.width <
                    MediaQuery.of(context).size.height
                ? MediaQuery.of(context).size.width * .8 // portrait
                : MediaQuery.of(context).size.height *
                    .6, //landscape (there's less space here due to the AppBar)
          ),
          key: qrKey,
          onQRViewCreated: _onQRViewCreated,
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          FloatingActionButton(
            heroTag: null,
            elevation: 10,
            onPressed: () => controller?.toggleFlash(),
            child: Icon(Icons.flash_on),
          ),
          SizedBox(width: 16),
          FloatingActionButton(
            heroTag: null,
            elevation: 10,
            onPressed: () => controller?.flipCamera(),
            child: Icon(Icons.switch_camera),
          ),
        ],
      ),
    );
  }
}
