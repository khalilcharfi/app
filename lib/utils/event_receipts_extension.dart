/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'package:famedlysdk/famedlysdk.dart';

extension EventReceiptsExtension on Event {
  /// Returns a list of [Receipt] instances for this event.
  List<Receipt> get readSoFar {
    if (!(room.roomAccountData.containsKey('m.receipt'))) return [];
    final receiptsList = <Receipt>[];
    for (final entry in room.roomAccountData['m.receipt'].content.entries) {
      // This is not 100% accurate because the origin_server_ts isn't fully safe.
      // But until we have Timeline and Room merged to get all events synchroniously
      // this is hopefully the best solution.
      if (entry.value['ts'] >= originServerTs.millisecondsSinceEpoch) {
        receiptsList.add(Receipt(room.getUserByMXIDSync(entry.key),
            DateTime.fromMillisecondsSinceEpoch(entry.value['ts'])));
      }
    }
    return receiptsList;
  }
}
