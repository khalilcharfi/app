import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:famedly/styles/famedly_icons_v2_icons.dart';
import 'package:famedly/utils/platform_extension.dart';
import 'package:famedlysdk/encryption.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:future_loading_dialog/future_loading_dialog.dart';

import '../../utils/beautify_string_extension.dart';
import 'adaptive_flat_button.dart';

class KeyVerificationDialog extends StatefulWidget {
  Future<bool> show(BuildContext context) => isCupertinoStyle
      ? showCupertinoDialog<bool>(
          context: context,
          builder: (context) => this,
          useRootNavigator: false,
        )
      : showDialog<bool>(
          context: context,
          builder: (context) => this,
          useRootNavigator: false,
        );

  final KeyVerification request;
  final BuildContext context;

  KeyVerificationDialog({
    @required this.request,
    @required this.context,
  });

  @override
  _KeyVerificationPageState createState() => _KeyVerificationPageState();
}

class _KeyVerificationPageState extends State<KeyVerificationDialog> {
  void Function() originalOnUpdate;
  final _scrollController = ScrollController();

  @override
  void initState() {
    originalOnUpdate = widget.request.onUpdate;
    widget.request.onUpdate = () {
      if (originalOnUpdate != null) {
        originalOnUpdate();
      }
      setState(() => null);
    };
    widget.request.client.getProfileFromUserId(widget.request.userId).then((p) {
      profile = p;
      setState(() => null);
    });
    super.initState();
  }

  @override
  void dispose() {
    widget.request.onUpdate =
        originalOnUpdate; // don't want to get updates anymore
    if (![KeyVerificationState.error, KeyVerificationState.done]
        .contains(widget.request.state)) {
      widget.request.cancel('m.user');
    }
    _scrollController.dispose();
    super.dispose();
  }

  Profile profile;

  @override
  Widget build(BuildContext context) {
    Widget body;
    final buttons = <Widget>[];
    switch (widget.request.state) {
      case KeyVerificationState.askSSSS:
        // prompt the user for their ssss passphrase / key
        final textEditingController = TextEditingController();
        String input;
        final checkInput = () async {
          if (input == null) {
            return;
          }
          final valid = await showFutureLoadingDialog(
              context: context,
              future: () async {
                // make sure the loading spinner shows before we test the keys
                await Future.delayed(Duration(milliseconds: 100));
                var valid = false;
                try {
                  await widget.request.openSSSS(recoveryKey: input);
                  valid = true;
                } catch (_) {
                  try {
                    await widget.request.openSSSS(passphrase: input);
                    valid = true;
                  } catch (_) {
                    valid = false;
                  }
                }
                return valid;
              });
          if (valid.result == false) {
            await showOkAlertDialog(
              context: context,
              message: L10n.of(context).invalidPassword,
              useRootNavigator: false,
            );
          }
        };
        body = Container(
          margin: EdgeInsets.only(left: 8.0, right: 8.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(L10n.of(context).askSSSSSign,
                  style: TextStyle(fontSize: 20)),
              Container(height: 10),
              TextField(
                controller: textEditingController,
                autofocus: false,
                autocorrect: false,
                onSubmitted: (s) {
                  input = s;
                  checkInput();
                },
                minLines: 1,
                maxLines: 1,
                obscureText: true,
                decoration: InputDecoration(
                  hintText: L10n.of(context).passphraseOrKey,
                  prefixStyle: TextStyle(color: Theme.of(context).primaryColor),
                  suffixStyle: TextStyle(color: Theme.of(context).primaryColor),
                  border: OutlineInputBorder(),
                ),
              ),
            ],
          ),
        );
        buttons.add(AdaptiveTextButton(
          onPressed: () {
            input = textEditingController.text;
            checkInput();
          },
          child: Text(L10n.of(context).next),
        ));
        buttons.add(AdaptiveTextButton(
          onPressed: () => widget.request.openSSSS(skip: true),
          child: Text(L10n.of(context).skip),
        ));
        break;
      case KeyVerificationState.askAccept:
        body = Container(
          margin: EdgeInsets.only(left: 8.0, right: 8.0),
          child: Text(
              L10n.of(context).askVerificationRequest(profile.displayname),
              style: TextStyle(fontSize: 20)),
        );
        buttons.add(AdaptiveTextButton(
          onPressed: () => widget.request.acceptVerification(),
          child: Text(L10n.of(context).accept),
        ));
        buttons.add(AdaptiveTextButton(
          onPressed: () {
            widget.request.rejectVerification().then((_) {
              Navigator.of(context, rootNavigator: false).pop<bool>(false);
            });
          },
          child: Text(L10n.of(context).decline),
        ));
        break;
      case KeyVerificationState.waitingAccept:
        body = Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            isCupertinoStyle
                ? CupertinoActivityIndicator()
                : CircularProgressIndicator(),
            SizedBox(height: 10),
            Text(
              L10n.of(context).waitingPartnerAcceptRequest,
              textAlign: TextAlign.center,
            ),
          ],
        );
        if (widget.request.deviceId != null) {
          final key = widget
              .request
              .client
              .userDeviceKeys[widget.request.userId]
              .deviceKeys[widget.request.deviceId];
          if (key != null) {
            buttons.add(
              AdaptiveTextButton(
                onPressed: () async {
                  final result = await showOkCancelAlertDialog(
                    context: widget.context,
                    title: L10n.of(context).verifyManual,
                    message: key.ed25519Key.beautified,
                    useRootNavigator: false,
                  );
                  if (result == OkCancelResult.ok) {
                    await key.setVerified(true);
                  }
                  await widget.request.cancel();
                  Navigator.of(context, rootNavigator: false)
                      .pop<bool>(result == OkCancelResult.ok);
                },
                child: Text(L10n.of(context).verifyManual),
              ),
            );
          }
        }
        break;
      case KeyVerificationState.askSas:
        TextSpan compareWidget;
        // maybe add a button to switch between the two and only determine default
        // view for if "emoji" is a present sasType or not?
        String compareText;
        if (widget.request.sasTypes.contains('emoji')) {
          compareText = L10n.of(context).compareEmojiMatch;
          compareWidget = TextSpan(
            children: widget.request.sasEmojis
                .map((e) => WidgetSpan(child: _Emoji(e)))
                .toList(),
          );
        } else {
          compareText = L10n.of(context).compareNumbersMatch;
          final numbers = widget.request.sasNumbers;
          final numbstr = '${numbers[0]}-${numbers[1]}-${numbers[2]}';
          compareWidget =
              TextSpan(text: numbstr, style: TextStyle(fontSize: 40));
        }
        body = Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Center(
              child: Text(
                compareText,
                style: TextStyle(fontSize: 16),
                textAlign: TextAlign.center,
              ),
            ),
            SizedBox(height: 10),
            Text.rich(
              compareWidget,
              textAlign: TextAlign.center,
            ),
          ],
        );
        buttons.add(AdaptiveTextButton(
          textColor: Colors.red,
          onPressed: () => widget.request.rejectSas(),
          child: Text(L10n.of(context).theyDontMatch),
        ));
        buttons.add(AdaptiveTextButton(
          onPressed: () => widget.request.acceptSas(),
          child: Text(L10n.of(context).theyMatch),
        ));
        break;
      case KeyVerificationState.waitingSas:
        final acceptText = widget.request.sasTypes.contains('emoji')
            ? L10n.of(context).waitingPartnerEmoji
            : L10n.of(context).waitingPartnerNumbers;
        body = Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            isCupertinoStyle
                ? CupertinoActivityIndicator()
                : CircularProgressIndicator(),
            SizedBox(height: 10),
            Text(
              acceptText,
              textAlign: TextAlign.center,
            ),
          ],
        );
        break;
      case KeyVerificationState.done:
        body = Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Icon(
              Icons.security_outlined,
              color: Colors.green,
              size: 200.0,
            ),
            SizedBox(height: 10),
            Text(
              L10n.of(context).verifySuccess,
              textAlign: TextAlign.center,
            ),
          ],
        );
        buttons.add(AdaptiveTextButton(
          onPressed: () =>
              Navigator.of(context, rootNavigator: false).pop<bool>(true),
          child: Text(L10n.of(context).close),
        ));
        break;
      case KeyVerificationState.error:
        body = Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Icon(FamedlyIconsV2.close, color: Colors.red, size: 200.0),
            SizedBox(height: 10),
            Text(
              L10n.of(context).oopsSomethingWentWrong,
              textAlign: TextAlign.center,
            ),
          ],
        );
        buttons.add(AdaptiveTextButton(
          onPressed: () =>
              Navigator.of(context, rootNavigator: false).pop<bool>(false),
          child: Text(L10n.of(context).close),
        ));
        break;
    }
    body ??= Text('ERROR: Unknown state ' + widget.request.state.toString());
    final otherName = profile?.displayname ?? widget.request.userId;
    var bottom;
    if (widget.request.deviceId != null && widget.request.deviceId != '*') {
      final deviceName = widget
              .request
              .client
              .userDeviceKeys[widget.request.userId]
              ?.deviceKeys[widget.request.deviceId]
              ?.deviceDisplayName ??
          '';
      bottom = Container(
        alignment: Alignment.center,
        padding: EdgeInsets.all(16.0),
        child: Text(
          '$deviceName (${widget.request.deviceId})',
          textAlign: TextAlign.center,
          style: TextStyle(color: Theme.of(context).textTheme.caption.color),
        ),
      );
    }
    final userNameTitle = Row(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          otherName,
          maxLines: 1,
          style: TextStyle(
            fontWeight: FontWeight.bold,
          ),
        ),
      ],
    );
    final title = Text(
      L10n.of(context).verifyTitle,
      textAlign: TextAlign.center,
    );
    final content = Scrollbar(
      isAlwaysShown: true,
      controller: _scrollController,
      child: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        controller: _scrollController,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            if (isCupertinoStyle) ...[
              SizedBox(height: 8),
              Center(child: userNameTitle),
              SizedBox(height: 12),
            ],
            body,
            if (bottom != null) bottom,
          ],
        ),
      ),
    );
    if (isCupertinoStyle) {
      return CupertinoAlertDialog(
        title: title,
        content: content,
        actions: buttons,
      );
    }
    return AlertDialog(
      title: title,
      content: content,
      actions: buttons,
    );
  }
}

class _Emoji extends StatelessWidget {
  final KeyVerificationEmoji emoji;

  _Emoji(this.emoji);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Text(emoji.emoji, style: TextStyle(fontSize: 50)),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 4),
          child: Text(emoji.name),
        ),
        Container(height: 10, width: 5),
      ],
    );
  }
}
