/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:adaptive_page_layout/adaptive_page_layout.dart';
import 'package:famedly/components/draggable_scrollbar.dart';
import 'package:famedly/config/forms.dart';
import 'package:famedly/styles/famedly_icons_v2_icons.dart';
import 'package:famedly/utils/error_reporter.dart';
import 'package:famedly/utils/form_manager.dart';
import 'package:famedly_directory_client_sdk/famedly_directory_client_sdk.dart';
import 'package:fhir/r4.dart' as fhir;
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';

import '../components/search_bar.dart';
import '../models/famedly.dart';
import '../styles/colors.dart';

class NewBroadcastPage extends StatefulWidget {
  final ScrollController _scrollController = ScrollController();

  @override
  NewBroadcastPageState createState() => NewBroadcastPageState();
}

class NewBroadcastPageState extends State<NewBroadcastPage> {
  Set<String> selectedContacts = {};
  String filterText;
  fhir.Questionnaire _selectedForm;
  final FormManager formManager = FormManager();
  List<Contact> contacts = [];

  final TextEditingController _filterTextController = TextEditingController();

  void _setBroadcastType(BuildContext context) async {
    final allForms = await formManager.getAllForms();
    var name = await showConfirmationDialog<String>(
      barrierDismissible: false,
      context: context,
      title: L10n.of(context).pleaseChoose,
      useRootNavigator: false,
      actions: allForms
          .map(
            (form) => AlertDialogAction(
              label: form.title ?? form.name,
              key: form.name,
            ),
          )
          .toList(),
    );
    if (name == null && _selectedForm == null) {
      // Select the default form which should be the first.
      name = Forms.defaultForm;
    }
    if (name == null) return;
    final form = name.isEmpty ? null : await formManager.loadForm(name);
    setState(() {
      selectedContacts.clear();
      _selectedForm = form;
    });
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(
      (_) => _setBroadcastType(context),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        leading: IconButton(
          icon: Icon(FamedlyIconsV2.close),
          onPressed: AdaptivePageLayout.of(context).pop,
        ),
        titleSpacing:
            AdaptivePageLayout.of(context).columnMode(context) ? 12 : 0,
        title: Text(_selectedForm?.title ?? ''),
        bottom: PreferredSize(
          preferredSize: Size.fromHeight(50),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: SearchBar(
              filterTextController: _filterTextController,
              onTextChanged: (text) => setState(() => filterText = text),
            ),
          ),
        ),
      ),
      floatingActionButton: selectedContacts.isEmpty
          ? null
          : FloatingActionButton(
              onPressed: () {
                final contacts = <Contact>[];
                for (final c in this.contacts) {
                  if (selectedContacts.contains(c.id)) {
                    contacts.add(c);
                  }
                }
                if (contacts.isEmpty) {
                  return; // nothing is selected
                }
                AdaptivePageLayout.of(context).pushNamed(
                  '/requestCompositing/${_selectedForm.name}',
                  arguments: contacts,
                );
              },
              child: Icon(FamedlyIconsV2.right),
            ),
      body: FutureBuilder<List<Organisation>>(
          future: Famedly.of(context)
              .getOrganizationDirectoryClient()
              .then((directory) => directory.search().catchError((e, s) {
                    ErrorReporter.reportError(e, s);
                  })),
          builder: (context, snapshot) {
            if (!snapshot.hasData) {
              return LinearProgressIndicator();
            }
            contacts.clear();
            for (final organisation in snapshot.data) {
              for (final contact in organisation.contacts) {
                if (contact.matrixId != null) {
                  contacts.add(contact);
                }
              }
            }
            return FutureBuilder<List<Tag>>(
                future: Famedly.of(context)
                    .getOrganizationDirectoryClient()
                    .then((directory) => directory.getTags().catchError((e, s) {
                          ErrorReporter.reportError(e, s);
                        })),
                builder: (context, snapshot) {
                  if (!snapshot.hasData) {
                    return CircularProgressIndicator();
                  }
                  final tags = snapshot.data;

                  final famedly = Famedly.of(context);

                  final contacts = List<Contact>.from(this.contacts)
                      .where(
                        (c) => c.matrixId != famedly.client.userID,
                      )
                      .toList(growable: true);
                  if (_selectedForm != null &&
                      _selectedForm.name != 'default') {
                    final selectedTagId = tags
                        .firstWhere(
                            (tag) =>
                                tag.description == _selectedForm.tagDescription,
                            orElse: () => null)
                        ?.id;
                    if (selectedTagId != null) {
                      contacts.removeWhere(
                        (c) => !c.tags.contains(selectedTagId),
                      );
                    }
                  }
                  if (filterText?.isNotEmpty ?? false) {
                    contacts.removeWhere((contact) => !(contact.description
                            .toLowerCase()
                            .contains(filterText.toLowerCase()) ||
                        contact.organisation.name
                            .toLowerCase()
                            .contains(filterText.toLowerCase())));
                  }

                  if (contacts.isEmpty) {
                    return Center(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          L10n.of(context).noContactPointsVisibleToYou,
                          textAlign: TextAlign.center,
                        ),
                      ),
                    );
                  }
                  return FamedlyDraggableScrollbar(
                    controller: widget._scrollController,
                    isAlwaysShown: kIsWeb,
                    child: ListView.separated(
                      controller: widget._scrollController,
                      separatorBuilder: (BuildContext context, int index) {
                        return Divider(
                            color: FamedlyColors.paleGrey, indent: 15);
                      },
                      itemCount: contacts.length,
                      itemBuilder: (BuildContext context, int index) =>
                          CheckboxListTile(
                        value: selectedContacts.contains(contacts[index].id),
                        onChanged: (select) {
                          setState(() {
                            if (select == true) {
                              if (selectedContacts.length >= 20) {
                                AdaptivePageLayout.of(context).showSnackBar(
                                  SnackBar(
                                    content: Text(L10n.of(context)
                                        .moreContactPointsCantBeSelected),
                                  ),
                                );
                                return;
                              }
                              selectedContacts.add(contacts[index].id);
                            } else {
                              selectedContacts.remove(contacts[index].id);
                            }
                          });
                        },
                        title: Text(contacts[index].organisation.name,
                            style: TextStyle(
                              fontSize: 15,
                              color: FamedlyColors.slate,
                              fontWeight: FontWeight.w600,
                            )),
                        subtitle: Text(
                          contacts[index].description,
                          style: TextStyle(
                            fontSize: 13,
                            color: FamedlyColors.slate,
                          ),
                        ),
                      ),
                    ),
                  );
                });
          }),
    );
  }
}
