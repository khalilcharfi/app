/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'package:flutter/material.dart';

class MouseOverBuilder extends StatefulWidget {
  final Function(BuildContext, bool) builder;

  const MouseOverBuilder({Key key, this.builder}) : super(key: key);
  @override
  _MouseOverBuilderState createState() => _MouseOverBuilderState();
}

class _MouseOverBuilderState extends State<MouseOverBuilder> {
  bool _hover = false;

  void _toggleHover(bool hover) {
    if (_hover != hover) {
      setState(() => _hover = hover);
    }
  }

  @override
  Widget build(BuildContext context) {
    return MouseRegion(
      onEnter: (_) => _toggleHover(true),
      onExit: (_) => _toggleHover(false),
      child: widget.builder != null ? widget.builder(context, _hover) : null,
    );
  }
}
