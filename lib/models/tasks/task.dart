import 'dart:async';

import 'subtask.dart';
import 'tasklist.dart';

class Task {
  String _title;
  String _description;
  DateTime _date;
  bool _done;
  List<Subtask> _subtask;
  TaskList _tasklist;

  static const mNamespace = 'com.famedly.talk.tasks';

  Task(
      {String title,
      String description,
      DateTime date,
      bool done = false,
      TaskList taskList,
      List<Subtask> subtask}) {
    _title = title;
    _description = description;
    _date = date;
    _done = done;
    _subtask = subtask;
    _tasklist;
    _subtask = subtask ?? [];
  }

  /// Returns the (required) title of this task.
  String get title => _title;

  /// Returns the description of this task if given.
  String get description => _description;

  /// Returns the date duo this task should be finished if given.
  DateTime get date => _date;

  /// Wheither this task is done or not. At beginning this is false.
  bool get done => _done;

  /// Returns the list of subtasks for this task.
  List<Subtask> get subtask => _subtask;

  Future<bool> save() => _tasklist.save();

  int get id => _tasklist.tasks.indexOf(this);

  Task.fromJson(Map<String, dynamic> json, TaskList taskList) {
    _title = json['title'];
    _description = json['description'];
    _date = json['date'] != null && json['date'] is int
        ? DateTime.fromMillisecondsSinceEpoch(json['date'])
        : null;
    _done = json['done'];
    _tasklist = taskList;
    if (json['subtask'] != null) {
      _subtask = <Subtask>[];
      json['subtask'].forEach((v) {
        _subtask.add(Subtask.fromJson(v, this));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['title'] = _title;
    data['description'] = _description;
    data['date'] = _date?.millisecondsSinceEpoch;
    data['done'] = _done;
    if (_subtask != null) {
      data['subtask'] = _subtask.map((v) => v.toJson()).toList();
    }
    return data;
  }

  /// Removes this task from the tasklist and the matrix homeserver.
  Future<bool> remove() async {
    final currentList = List<Task>.from(_tasklist.tasks);
    currentList.removeWhere(
        (Task testTask) => testTask.toJson().toString() == toJson().toString());
    await _tasklist.updateTaskList(currentList);
    return true;
  }

  Future<bool> setTitle(String newTitle) async {
    final oldTitle = _title;
    _title = newTitle;
    final saved = await save();
    if (saved != true) _title = oldTitle;
    return saved;
  }

  Future<bool> setDescription(String newDescription) async {
    final oldDescription = _description;
    _description = newDescription;
    try {
      await save();
    } catch (_) {
      _description = oldDescription;
      rethrow;
    }
    return true;
  }

  Future<bool> setDate(DateTime newDate) async {
    final oldDate = _date;
    _date = newDate;
    try {
      await save();
    } catch (_) {
      _date = oldDate;
      rethrow;
    }
    return true;
  }

  Future<bool> setDone(bool newDone) async {
    final oldDone = _done;
    _done = newDone;
    try {
      await save();
    } catch (_) {
      _done = oldDone;
      rethrow;
    }
    return true;
  }

  Future<bool> addSubtask(String title) async {
    final oldSubtask = _subtask;
    _subtask.add(Subtask(title: title, parent: this));
    try {
      await save();
    } catch (_) {
      _subtask = oldSubtask;
      rethrow;
    }

    return true;
  }
}
