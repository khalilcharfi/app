import 'package:adaptive_page_layout/adaptive_page_layout.dart';
import 'package:famedly/models/famedly.dart';
import 'package:famedly/styles/colors.dart';
import 'package:famedly/utils/famedlysdk_store.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:provider/provider.dart';

import 'fake_matrix_api.dart';

class MockNavigatorObserver extends Mock implements NavigatorObserver {}

class Utils {
  static String fromRichTextToPlainText(final Widget widget) {
    if (widget is RichText) {
      if (widget.text is TextSpan) {
        final buffer = StringBuffer();
        (widget.text as TextSpan).computeToPlainText(buffer);
        return buffer.toString();
      }
    }
    return null;
  }

  static void printWidgets(WidgetTester tester) {
    debugPrint(tester.allWidgets.toList().join('\n').toString());
  }

  static Future<Client> get defaultClient async {
    final client = Client('testclient', httpClient: FakeMatrixApi());
    await client.init(
      newHomeserver: Uri.parse('https://fakeServer.notExisting'),
      newDeviceID: 'GHTYAJCE',
      newToken: 'abc123',
      newUserID: '@test:fakeServer.notExisting',
      newDeviceName: 'Famedly Widget Tests',
    );

    // Make sure we had a sync
    await client.oneShotSync();

    return client;
  }

  static Future<Widget> getWidgetWrapper(
      Widget child, NavigatorObserver routeObserver,
      {Client client}) async {
    client ??= await Utils.defaultClient;
    Store.isWidgetTest = true;
    return Provider<Famedly>(
      create: (context) => Famedly(
        matrixClient: client,
        context: context,
      ),
      child: MaterialApp(
        title: 'Famedly Talk',
        theme: Themes.famedlyLightTheme,
        localizationsDelegates: L10n.localizationsDelegates,
        supportedLocales: L10n.supportedLocales,
        navigatorObservers: [routeObserver],
        home: AdaptivePageLayout(
          onGenerateRoute: (_) => ViewData(mainView: (_) => child),
        ),
      ),
    );
  }

  static Future<Null> tapItem(WidgetTester tester, Key key) async {
    /// Tap the button which should open the PopupMenu.
    /// By calling tester.pumpAndSettle(), we ensure that all animations
    /// have completed before we continue further.
    await tester.tap(find.byKey(key));
    await tester.pumpAndSettle();
  }

  static Future<Null> tapItemByType(WidgetTester tester, Type type) async {
    /// Tap the button which should open the PopupMenu.
    /// By calling tester.pumpAndSettle(), we ensure that all animations
    /// have completed before we continue further.
    await tester.tap(find.byType(type));
    await tester.pumpAndSettle();
  }
}
