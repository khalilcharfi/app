The current specification is more a MVP and may be improved in the near future.

A room is a patient room if it has a state event of the type: `com.famedly.app.patient_room`. As an additional identifier all patient rooms SHOULD have a `m.room.name` with the same name as the type to identify patient rooms where the state events are not yet visible.

The state event contains informations about a patient:

### Patient State
| Name   | Type | Description |
| ------ | ------ | --------- |
| case_id | String | The iternal case identifier of the patient for the organisation |
| name | String | The given and family name of the patient. |
| birthday | String | The birthday in the format `yyyy-MM-dd`.|
| sex | Enum | One of [male, female, unknown, other] |
