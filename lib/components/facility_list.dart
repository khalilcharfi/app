/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'dart:async';

import 'package:adaptive_page_layout/adaptive_page_layout.dart';
import 'package:famedly_directory_client_sdk/famedly_directory_client_sdk.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';

import '../models/famedly.dart';
import '../styles/colors.dart';
import '../views/invite_page.dart';
import 'avatar.dart';

class FacilityList extends StatefulWidget {
  final TextEditingController filterTextGetter;
  final String tag;
  final List<Subtype> subtypeList;
  final OnInviteCallback onInvite;

  FacilityList(
    this.filterTextGetter, {
    this.tag,
    this.subtypeList,
    this.onInvite,
    Key key,
  }) : super(key: key);

  @override
  FacilityListState createState() => FacilityListState();
}

class FacilityListState extends State<FacilityList> {
  String filterText;
  List<Organisation> orgaList;

  void handleFilter() {
    if (widget.filterTextGetter == null) return;
    setState(() {
      filterText = widget.filterTextGetter.text;
    });
  }

  @override
  void initState() {
    super.initState();
    if (widget.filterTextGetter != null) {
      widget.filterTextGetter.addListener(handleFilter);
    }
  }

  Future<List<Organisation>> getOrganisations(BuildContext context) async {
    if (orgaList != null) return orgaList;
    try {
      final directory =
          await Famedly.of(context).getOrganizationDirectoryClient();
      return await directory.search();
    } catch (exception, stacktrace) {
      Logs().e(exception.toString());
      Logs().e(stacktrace.toString());
      return [];
    }
  }

  @override
  void dispose() {
    if (widget.filterTextGetter != null) {
      widget.filterTextGetter.removeListener(handleFilter);
    }
    super.dispose();
  }

  String getTagDescriptionFromID(String tagID) {
    if (widget.subtypeList == null) return '...';
    final desc = widget.subtypeList
        .singleWhere((Subtype t) => t.id == tagID)
        .description;
    return desc;
  }

  bool filter(Organisation organisation) =>
      (widget.tag.isNotEmpty && organisation.orgType != widget.tag) ||
      (filterText != null &&
          filterText != '' &&
          !(organisation.name +
                  (organisation.location?.country ?? '') +
                  (organisation.location?.state ?? '') +
                  (organisation.location?.street ?? ''))
              .toLowerCase()
              .contains(filterText.toLowerCase()));

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<Organisation>>(
        future: getOrganisations(context),
        builder:
            (BuildContext context, AsyncSnapshot<List<Organisation>> snapshot) {
          if (!snapshot.hasData) {
            return Center(child: CircularProgressIndicator());
          }
          if (snapshot.data == null) {
            return Center(
                child: Text(L10n.of(context).oopsSomethingWentWrong +
                    ' ' +
                    L10n.of(context).organisationServerIsNotResponding));
          }
          orgaList = snapshot.data;

          orgaList.sort((a, b) => a.name.compareTo(b.name));

          if (orgaList.isEmpty) {
            return Center(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  L10n.of(context).noFacilitiesWereFoundForThisContactPoint,
                  textAlign: TextAlign.center,
                ),
              ),
            );
          }

          return ListView.separated(
            separatorBuilder: (BuildContext context, int index) =>
                filter(orgaList[index])
                    ? Container()
                    : Divider(
                        color: Theme.of(context).brightness == Brightness.light
                            ? FamedlyColors.paleGrey
                            : FamedlyColors.slate,
                        indent: 78,
                        thickness: 1,
                      ),
            itemCount: orgaList.length,
            itemBuilder: (BuildContext context, int index) {
              final organisation = orgaList[index];
              if (filter(organisation)) return Container();
              return ListTile(
                  onTap: () {
                    AdaptivePageLayout.of(context).pushNamed(
                        '/directory/${organisation.id}',
                        arguments: widget.onInvite);
                  },
                  leading: Avatar(null, name: organisation.name),
                  title: Text(organisation.name),
                  subtitle: Wrap(
                    children: <Widget>[
                      if (Directory.subtypeCache[organisation.orgType] != null)
                        Container(
                            margin: EdgeInsets.only(right: 5),
                            decoration: BoxDecoration(
                                color: FamedlyColors.paleGrey,
                                borderRadius: BorderRadius.circular(19)),
                            padding: EdgeInsets.symmetric(
                                horizontal: 8, vertical: 5),
                            child: Text(
                                Directory.subtypeCache[organisation.orgType]
                                    .description,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                    color: FamedlyColors.metallicBlue,
                                    fontSize: 10))),
                      Text(organisation.location?.city ?? ''),
                    ],
                  ));
            },
          );
        });
  }
}

class ContactPoint {
  final Organisation organisation;
  final int contactPointID;

  const ContactPoint({this.organisation, this.contactPointID});
}
