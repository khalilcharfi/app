import 'package:famedlysdk/famedlysdk.dart';

import '../../models/requests/request.dart';
import '../room_type_extension.dart';

class RequestFilters {
  static bool isRequestRoomFilter(Room room) =>
      room.membership != Membership.leave &&
      room.membership != Membership.ban &&
      room.type == roomTypeRequest;

  static bool isOwnBroadcastBundleFilter(RequestBundle bundle) =>
      bundle.isOwnBroadcast();

  static bool isAdminBundleFilter(RequestBundle bundle) =>
      bundle.isAdmin() ||
      bundle.broadcastRoom?.membership == Membership.invite ||
      bundle.rooms.first?.membership == Membership.invite;
}
