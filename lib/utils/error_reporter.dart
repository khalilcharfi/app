/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'dart:async';
import 'dart:io';
import 'dart:math';

import 'package:device_info/device_info.dart';
import 'package:famedly/config/current_commit_hash.dart';
import 'package:famedlysdk/famedlysdk.dart' as sdk;
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:localstorage/localstorage.dart';
import 'package:sentry/sentry.dart';
import 'package:universal_html/html.dart' as html;

abstract class ErrorReporter {
  /// Reports the [exception] and optionally its [stackTrace] to Sentry.io.
  ///
  /// Optionally allows specifying a [stackFrameFilter] that receives the
  /// list of stack frames just before sending to allow modifying it.
  static Future<SentryId> captureException({
    @required dynamic exception,
    dynamic stackTrace,
  }) async {
    String deviceDisplayName;
    String deviceVendor;
    if (kIsWeb) {
      deviceVendor = html.window.navigator.userAgent;
      deviceDisplayName = 'Famedly Web';
    } else if (Platform.isAndroid) {
      final androidInfo = await DeviceInfoPlugin().androidInfo;
      deviceDisplayName = androidInfo.device;
      deviceVendor = androidInfo.brand;
    } else if (Platform.isIOS) {
      final iosInfo = await DeviceInfoPlugin().iosInfo;
      deviceDisplayName = iosInfo.model;
      deviceVendor = 'Apple';
    } else {
      deviceVendor = 'Desktop';
      deviceDisplayName = 'Famedly Desktop';
    }
    final event = SentryEvent(
      environment: 'production',
      user: SentryUser(
        id: Random.secure().nextInt(400000000).toString(),
        // Full random to be sure that we cant recognize a person by its id.
        extras: {
          'version': CurrentCommitHash.hash,
          'vendor': deviceVendor,
          'device': deviceDisplayName,
          if (kDebugMode) 'debugMode': true,
        },
      ),
      release: CurrentCommitHash.hash,
      throwable: exception,
    );

    return Sentry.captureEvent(event, stackTrace: stackTrace);
  }

  static const Set<Object> exceptionsToIgnore = {
    SocketException,
    TimeoutException,
    ClientException,
    HandshakeException,
    sdk.MatrixConnectionException,
  };

  static bool filterException(Object error) {
    if (exceptionsToIgnore.contains(error.runtimeType) ||
        error.toString().startsWith('minified:')) {
      return true;
    }
    return false;
  }

  static Future<void> reportError(Object error, StackTrace stackTrace) async {
    final storage = LocalStorage('FamedlyLocalStorage');
    await storage.ready;
    final bool sentryOptIn = storage.getItem('sentry') ?? false;
    sdk.Logs().e('Capture exception', error, stackTrace);

    if (sentryOptIn && !filterException(error)) {
      // Send the Exception and Stacktrace to Sentry in Production mode
      await captureException(
        exception: error,
        stackTrace: stackTrace,
      );
    }
  }
}
