/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:famedly/components/unread_bubble.dart';
import 'package:flutter/material.dart';
import 'package:adaptive_page_layout/adaptive_page_layout.dart';

import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:future_loading_dialog/future_loading_dialog.dart';
import '../models/roles/role_entity.dart';
import '../styles/colors.dart';
import '../views/invite_page.dart';
import 'avatar.dart';
import 'default_modal_bottom_sheet.dart';

import 'mouse_over_builder.dart';

enum RoleListContextualAction { optIn, optOut }

class RoleListTile extends StatefulWidget {
  final RoleEntity role;
  final bool allowOptOut;
  final bool detailsButton;
  final OnInviteCallback onInvite;

  const RoleListTile({
    Key key,
    this.role,
    this.allowOptOut = true,
    this.detailsButton = false,
    this.onInvite,
  }) : super(key: key);

  @override
  RoleListTileState createState() => RoleListTileState(role);
}

class RoleListTileState extends State<RoleListTile> {
  RoleEntity role;
  RoleListTileState(this.role);

  List<PopupMenuItem<RoleListContextualAction>> getPopupMenuItems(
          BuildContext context) =>
      [
        PopupMenuItem<RoleListContextualAction>(
          value: RoleListContextualAction.optIn,
          child: Text(L10n.of(context).optInButton),
        ),
        PopupMenuItem<RoleListContextualAction>(
          value: RoleListContextualAction.optOut,
          child: Text(L10n.of(context).optOutButton),
        ),
      ];

  void _showModalBottomSheetAction(BuildContext outerContext) {
    showModalBottomSheet(
      context: outerContext,
      backgroundColor: Colors.transparent,
      builder: (context) => DefaultModalBottomSheet(
        child: ListView(
          shrinkWrap: true,
          children: getPopupMenuItems(context)
              .map((item) => ListTile(
                    title: item.child,
                    onTap: () {
                      Navigator.of(context, rootNavigator: false).pop();
                      _contextualAction(outerContext, item.value);
                    },
                  ))
              .toList(),
        ),
      ),
    );
  }

  void _contextualAction(
      BuildContext context, RoleListContextualAction action) async {
    switch (action) {
      case RoleListContextualAction.optIn:
        _optInAction(context);
        break;
      case RoleListContextualAction.optOut:
        _optOutAction(context);
        break;
    }
  }

  void _optInAction(BuildContext context) async {
    if (OkCancelResult.ok ==
        await showOkCancelAlertDialog(
          context: context,
          title: L10n.of(context).optInNotice,
          okLabel: L10n.of(context).yes,
          cancelLabel: L10n.of(context).cancel,
          useRootNavigator: false,
        )) {
      await showFutureLoadingDialog(
        context: context,
        future: () => role.userOptOut(out: false),
      );
      setState(() {
        role.userAvailable = true;
      });
    }
  }

  void _optOutAction(BuildContext context) async {
    if (OkCancelResult.ok ==
        await showOkCancelAlertDialog(
          context: context,
          title: L10n.of(context).optOutNotice,
          okLabel: L10n.of(context).yes,
          cancelLabel: L10n.of(context).cancel,
          useRootNavigator: false,
        )) {
      await showFutureLoadingDialog(
        context: context,
        future: () => role.userOptOut(),
      );
      setState(() {
        role.userAvailable = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return MouseOverBuilder(
      builder: (context, hover) => ListTile(
        onLongPress: widget.allowOptOut
            ? null
            : () => _showModalBottomSheetAction(context),
        onTap: () async {
          if (widget.detailsButton) {
            if (widget.onInvite != null) {
              widget.onInvite(InviteType.role, role.mxid);
            } else {
              role.startRequest(context);
            }
          } else {
            await AdaptivePageLayout.of(context)
                .pushNamed('/role/${role.id}', arguments: role);
          }
        },
        leading: Avatar(
          role.avatarUrl,
          size: 40,
          name: role.name,
        ),
        title: Row(
          children: [
            Text(
              role.name,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                  color: role.userAvailable ? FamedlyColors.aquaMarine : null),
            ),
            Spacer(),
            if (widget.allowOptOut)
              Opacity(
                // We need to hide it this way because otherwise the button would stop existing
                // when the user stop hovers it to select a PopupMenuButtonItem.
                opacity: hover ? 1 : 0,
                child: Container(
                  height: UnreadBubble.size,
                  margin: hover
                      ? const EdgeInsets.only(left: 8.0)
                      : EdgeInsets.zero,
                  width: hover ? 20 : 0,
                  child: PopupMenuButton<RoleListContextualAction>(
                    padding: EdgeInsets.zero,
                    itemBuilder: (context) => getPopupMenuItems(context),
                    onSelected: (action) => _contextualAction(context, action),
                  ),
                ),
              ),
          ],
        ),
        subtitle: Text(
          L10n.of(context).numMembers(role.users.length),
          style: TextStyle(
            color: FamedlyColors.blueyGrey,
            fontSize: 14,
            fontWeight: FontWeight.w400,
            fontStyle: FontStyle.normal,
            letterSpacing: 0,
          ),
          overflow: TextOverflow.ellipsis,
        ),
        trailing: widget.detailsButton
            ? ElevatedButton(
                onPressed: () async {
                  await AdaptivePageLayout.of(context)
                      .pushNamed('/role/${role.id}', arguments: role);
                },
                style: ElevatedButton.styleFrom(
                  padding: EdgeInsets.all(8),
                ),
                child: Text(L10n.of(context).details),
              )
            : null,
      ),
    );
  }
}
