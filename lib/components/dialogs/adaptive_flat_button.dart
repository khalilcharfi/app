import 'package:famedly/utils/platform_extension.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AdaptiveTextButton extends StatelessWidget {
  final Widget child;
  final Color textColor;
  final Function onPressed;

  const AdaptiveTextButton({
    Key key,
    this.child,
    this.textColor,
    this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (isCupertinoStyle) {
      return CupertinoDialogAction(
        onPressed: onPressed,
        textStyle: textColor != null ? TextStyle(color: textColor) : null,
        child: child,
      );
    }
    return TextButton(
      style: TextButton.styleFrom(
        textStyle: TextStyle(color: textColor),
      ),
      onPressed: onPressed,
      child: child,
    );
  }
}
