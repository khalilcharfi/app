import 'dart:async';

import 'package:famedly/utils/error_reporter.dart';
import 'package:famedlysdk/encryption.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/material.dart';
import 'package:adaptive_page_layout/adaptive_page_layout.dart';

import 'package:flutter/cupertino.dart';

import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'famedly_plugin.dart';

class MatrixErrorPlugin extends FamedlyPlugin {
  MatrixErrorPlugin(matrix) : super(matrix) {
    olmErrorSub ??= client.onEncryptionError.stream.listen(handleOlmError);
    syncErrorSub ??= client.onSyncError.stream.listen(handleSdkError);
    databaseErrorSub ??=
        client.database?.onError?.stream?.listen(handleSdkError);
  }

  StreamSubscription<SdkError> olmErrorSub;
  StreamSubscription<SdkError> syncErrorSub;
  StreamSubscription<SdkError> databaseErrorSub;

  L10n get localizations => L10n.of(famedly.context);

  void showError(String error, {String title}) => (String title, String error,
          BuildContext localContext) =>
      AdaptivePageLayout.of(famedly.context).showSnackBar(
        SnackBar(
          content:
              Text('${title ?? localizations.oopsSomethingWentWrong}\n$error'),
        ),
      );

  void showInfo(String info) =>
      (String title, String error, BuildContext localContext) =>
          AdaptivePageLayout.of(famedly.context).showSnackBar(
            SnackBar(
              content: Text(info),
            ),
          );

  void handleSdkError(SdkError error) {
    // If the app has no connection to the server, this is shown
    // by the ConnectionStatusHeader.
    if (error.exception is MatrixConnectionException) {
      return;
    }
    if (error.exception is MatrixException) {
      handleError(error.exception);
    }
    ErrorReporter.reportError(
      error.exception,
      error.stackTrace,
    );
    return;
  }

  void handleOlmError(SdkError error) {
    if (error.exception is DecryptException &&
        {
          DecryptException.isntSentForThisDevice,
          DecryptException.unableToDecryptWithAnyOlmSession
        }.contains(error.exception.toString())) {
      return;
    }
    ErrorReporter.reportError(
      error.exception,
      error.stackTrace,
    );
  }

  void handleError(MatrixException error) {
    if (client.homeserver.toString() == 'https://example.com') return;
    String errorMsg;
    Logs().w('[Matrix] [${error.errcode}] ${error.error}');
    switch (error.error) {
      case MatrixError.M_LIMIT_EXCEEDED:
        errorMsg = localizations.tooManyRequests;
        break;
      case MatrixError.M_UNKNOWN_TOKEN:
        errorMsg = localizations.yourSessionIsNoLongerValid;
        break;
      case MatrixError.M_FORBIDDEN:
        if (error.errorMessage.startsWith(
            "You don't have permission to post that to the room.")) {
          errorMsg = localizations.noPermissionForThisAction;
        } else if (error.errorMessage.startsWith('You cannot kick user')) {
          errorMsg = localizations.noPermissionForThisAction;
        } else {
          errorMsg = localizations.invalidUsernameOrPassword;
        }
        break;
      case MatrixError.M_UNSUPPORTED_ROOM_VERSION:
        errorMsg = localizations.unsupportedRoomVersion;
        break;
      default:
        errorMsg =
            error.errorMessage.isEmpty ? error.errcode : error.errorMessage;
        break;
    }
    showError(errorMsg);
  }
}
