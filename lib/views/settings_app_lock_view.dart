/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'package:famedly/models/famedly.dart';
import 'package:famedly/components/settings_list_group.dart';
import 'package:famedly/styles/colors.dart';

import 'package:flutter/services.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:flutter/material.dart';
import 'package:local_auth/local_auth.dart';
import 'package:flutter_app_lock/flutter_app_lock.dart';
import 'enter_pass_code_view.dart';
import '../utils/famedlysdk_store.dart';
import 'package:adaptive_page_layout/adaptive_page_layout.dart';

class SettingsAppLockView extends StatefulWidget {
  @override
  _SettingsAppLockViewState createState() => _SettingsAppLockViewState();
}

class _SettingsAppLockViewState extends State<SettingsAppLockView> {
  final storage = Store();

  final TextEditingController controller = TextEditingController();

  final LocalAuthentication localAuthentication = LocalAuthentication();

  void _setPassCode(String passCode, BuildContext context) async {
    Famedly.of(context).analyticsPlugin.trackEventWithName(
        'App-Lock Screen', 'set passcode button', 'clicked');
    if (passCode.length != 6) {
      AdaptivePageLayout.of(context).showSnackBar(
        SnackBar(
          content: Text(L10n.of(context).pleaseEnter6Digits),
        ),
      );
      return;
    }
    await AppLock.of(context).showLockScreen();
    await storage.setItem(EnterPassCodeView.appLockNamespace, passCode);
    AppLock.of(context).enable();

    setState(() {});
  }

  void _disableAppLock(BuildContext context) async {
    await AppLock.of(context).showLockScreen();
    await storage.setItem(EnterPassCodeView.appLockNamespace, null);
    await storage.setItem(
        EnterPassCodeView.appLockBiometricsNamespace, 'false');
    AppLock.of(context).disable();
    controller.clear();
    setState(() => null);
    AdaptivePageLayout.of(context).showSnackBar(
      SnackBar(
        content: Text(L10n.of(context).appLockRemoved),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: BackButton(),
        title: Text(L10n.of(context).appLock),
      ),
      backgroundColor: Theme.of(context).secondaryHeaderColor,
      body: ListView(
        padding: FamedlyStyles.getTwoColumnListViewPadding(context),
        children: [
          FutureBuilder(
              future: storage.getItem(EnterPassCodeView.appLockNamespace),
              builder: (context, snapshot) {
                final active = snapshot.data != null;
                return SettingsListGroup(
                  children: [
                    if (active)
                      ListTile(
                        title: Text(L10n.of(context).appLockActive),
                        trailing: Icon(
                          Icons.delete_forever,
                          color: Colors.red,
                        ),
                        onTap: active ? () => _disableAppLock(context) : null,
                      ),
                    if (!active)
                      ListTile(
                          title: Text(L10n.of(context).pleaseSetAPasscode)),
                    Divider(height: 1),
                    Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: TextField(
                        controller: controller,
                        obscureText: true,
                        maxLength: 6,
                        keyboardType: TextInputType.number,
                        textInputAction: TextInputAction.done,
                        inputFormatters: <TextInputFormatter>[
                          FilteringTextInputFormatter.digitsOnly
                        ],
                        onSubmitted: (String text) =>
                            _setPassCode(text, context),
                        decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          labelText: L10n.of(context).setPasscode,
                          hintText: '******',
                          suffixIcon: IconButton(
                            icon: Icon(Icons.send),
                            onPressed: () =>
                                _setPassCode(controller.text, context),
                          ),
                        ),
                      ),
                    ),
                    FutureBuilder<bool>(
                      future: localAuthentication.canCheckBiometrics,
                      builder: (BuildContext context, AsyncSnapshot snapshot) {
                        if (!snapshot.hasData || snapshot.data == false) {
                          return Container();
                        }
                        return ListTile(
                          title: Text(L10n.of(context)
                              .allowUnlockingWithFingerprintOrFaceId),
                          trailing: FutureBuilder(
                              future: storage.getItem(
                                  EnterPassCodeView.appLockBiometricsNamespace),
                              builder: (context, snapshot) {
                                return Switch(
                                  value: snapshot.hasData
                                      ? snapshot.data == 'true'
                                          ? true
                                          : false
                                      : false,
                                  onChanged: !active
                                      ? null
                                      : (bool b) async {
                                          await AppLock.of(context)
                                              .showLockScreen();
                                          await storage.setItem(
                                              EnterPassCodeView
                                                  .appLockBiometricsNamespace,
                                              b ? 'true' : 'false');
                                          setState(() => null);
                                        },
                                );
                              }),
                        );
                      },
                    ),
                  ],
                );
              }),
        ],
      ),
    );
  }
}
