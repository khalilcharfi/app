import 'package:flutter_gen/gen_l10n/l10n_en.dart';
import 'package:famedly/views/archives_pages.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'utils.dart';

void main() {
  final localization = L10nEn();

  /// All Tests related to the RoomsPage
  group('ArchivesPage', () {
    NavigatorObserver mockObserver;

    setUp(() {
      mockObserver = MockNavigatorObserver();
    });

    /// Check if Page has title
    testWidgets('Check if Page has title', (WidgetTester tester) async {
      await tester.runAsync(() async {
        await tester.pumpWidget(
          await Utils.getWidgetWrapper(
            ArchivesPage(),
            mockObserver,
          ),
        );

        await tester.pump(Duration.zero);

        expect(find.text(localization.archive), findsOneWidget); // First Item
      });
    });
/*
  // FIXME the `list = await matrix.client.archive;` stuff in matrix_chat_list doesn't seem to work in tests.
    testWidgets('Check if Rooms get listed', (WidgetTester tester) async {
      await tester.runAsync(() async {
        await tester.pumpWidget(
          Utils.getWidgetWrapper(
            ArchivesPage(),
            mockObserver,
          ),
        );

        await tester.pumpAndSettle(
            Duration(seconds: 10), EnginePhase.build, Duration(minutes: 1));

        Utils.printWidgets(tester);
      });
    });

    /// Check the remove dialog
    testWidgets('Check the remove dialog', (WidgetTester tester) async {
      await tester.runAsync(() async {
        await tester.pumpWidget(
          Utils.getWidgetWrapper(
            ArchivesPage(),
            mockObserver,
          ),
        );

        await tester.pump(Duration.zero);

        await Utils.tapItem(tester, Key('PopupMenu'));
        expect(find.text('Clear archive'), findsOneWidget);
        await Utils.tapItem(tester, Key('Clear'));

        await tester.pump(Duration.zero);

        // Tap on clear archive and check the dialog box
        expect(find.text('The deletion can not be undone. Delete anyway?'),
            findsOneWidget);
        expect(find.text('Back'.toUpperCase()), findsOneWidget);
        expect(find.text('Clear'.toUpperCase()), findsOneWidget);

        // Close the dialog
        await Utils.tapItem(tester, Key('BackButtonInClearDialog'));
        expect(find.text('The deletion can not be undone. Delete anyway?'),
            findsNothing);
      });
    });*/
  });
}
