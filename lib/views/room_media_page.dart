/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/material.dart';

import '../models/famedly.dart';
import '../components/matrix_event_list.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';

class RoomMediaPage extends StatelessWidget {
  final String roomId;

  final List<String> messageTypes;

  const RoomMediaPage(this.roomId, this.messageTypes, {Key key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final room = Famedly.of(context).client.getRoomById(roomId);

    if (room == null) {
      return Scaffold(
        appBar: AppBar(
          leading: BackButton(),
          elevation: 0,
        ),
        body: Center(
          child: Text(L10n.of(context).noMessagesFoundYet),
        ),
      );
    }

    return Scaffold(
      appBar: AppBar(
        leading: BackButton(),
        elevation: 0,
      ),
      body: MatrixEventList(
        room,
        padding: EdgeInsets.zero,
        filter: (Event event) =>
            ![
              EventTypes.Message,
              EventTypes.Sticker,
            ].contains(event.type) ||
            !messageTypes.contains(event.messageType),
      ),
    );
  }
}
