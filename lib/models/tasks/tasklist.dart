import 'dart:async';

import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'task.dart';

typedef onTaskListUpdate = void Function();

class TaskList {
  final MethodChannel platform = MethodChannel('famedly.com/famedly');

  final Client client;

  List<Task> tasks;

  List<Task> _loadTasks() {
    final tasks = <Task>[];
    if (client.accountData[mNamespace] != null &&
        client.accountData[mNamespace].content['tasks'] is List<dynamic>) {
      final List<dynamic> jsonTaskList =
          client.accountData[mNamespace].content['tasks'];
      for (var i = 0; i < jsonTaskList.length; i++) {
        tasks.add(Task.fromJson(jsonTaskList[i], this));
      }
    }
    return tasks;
  }

  onTaskListUpdate onUpdate;

  static const mNamespace = 'com.famedly.talk.tasks';

  StreamSubscription<BasicEvent> userEventSub;

  TaskList(this.client, {this.onUpdate}) {
    tasks = _loadTasks();

    userEventSub ??= client.onAccountData.stream
        .where((BasicEvent u) => u.type == mNamespace)
        .listen((BasicEvent u) {
      tasks = _loadTasks();
      if (onUpdate != null) onUpdate();
    });
  }

  Future<void> createNotification(
      {@required String notificationTitle,
      String description,
      @required DateTime date}) async {}

  /// Creates a new task and publishes it in the account data on the matrix homeserver.
  /// Returns the newly created task or null if something went wrong.
  Future<Task> create(
      {@required String title,
      @required String notificationTitle,
      String description,
      DateTime date}) async {
    if (date != null && notificationTitle != null) {
      await createNotification(
          notificationTitle: notificationTitle,
          description: description,
          date: date);
    }

    final newTask = Task(
        taskList: this, title: title, description: description, date: date);
    final taskList = List<Task>.from(tasks);
    taskList.add(newTask);
    await updateTaskList(taskList);
    return newTask;
  }

  Future<bool> save() async {
    await updateTaskList(tasks);
    return true;
  }

  /// Updates the task list. Throws MatrixException or other exceptions on error.
  Future<void> updateTaskList(List<Task> taskList) async {
    final jsonCurrentList = <dynamic>[];
    for (var i = 0; i < taskList.length; i++) {
      jsonCurrentList.add(taskList[i].toJson());
    }
    final data = <String, dynamic>{'tasks': jsonCurrentList};
    return await client.request(RequestType.PUT,
        '/client/r0/user/${client.userID}/account_data/$mNamespace',
        data: data);
  }
}
