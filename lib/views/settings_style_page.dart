/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'package:adaptive_theme/adaptive_theme.dart';
import 'package:famedly/components/settings_list_group.dart';
import 'package:famedly/styles/colors.dart' as famedly_colors;
import 'package:famedly/utils/error_reporter.dart';
import 'package:famedly/utils/famedlysdk_store.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';

import '../models/famedly.dart';

class SettingsStylePage extends StatefulWidget {
  @override
  _SettingsStylePageState createState() => _SettingsStylePageState();
}

class _SettingsStylePageState extends State<SettingsStylePage> {
  AdaptiveThemeMode _currentTheme;
  final storage = Store();
  Future<dynamic> _currentWallpaperFuture;

  void _switchTheme(AdaptiveThemeMode newTheme, BuildContext context) {
    Famedly.of(context)
        .analyticsPlugin
        .trackEventWithName('Style/theme', 'theme', 'changed to $newTheme');
    switch (newTheme) {
      case AdaptiveThemeMode.light:
        AdaptiveTheme.of(context).setLight();
        break;
      case AdaptiveThemeMode.dark:
        AdaptiveTheme.of(context).setDark();
        break;
      case AdaptiveThemeMode.system:
        AdaptiveTheme.of(context).setSystem();
        break;
    }
    setState(() => _currentTheme = newTheme);
  }

  void _switchWallpaper(int wallpaper, BuildContext context) async {
    await storage.setItem(Famedly.wallpaperNamespace, wallpaper.toString());
    setState(() => _currentWallpaperFuture = null);
  }

  @override
  Widget build(BuildContext context) {
    _currentTheme ??= AdaptiveTheme.of(context).mode;
    _currentWallpaperFuture ??= storage.getItem(Famedly.wallpaperNamespace);
    return Scaffold(
      appBar: AppBar(
        leading: BackButton(),
        title: Text('Style'),
      ),
      backgroundColor: Theme.of(context).secondaryHeaderColor,
      body: ListView(
        padding:
            famedly_colors.FamedlyStyles.getTwoColumnListViewPadding(context),
        children: [
          SettingsListGroup(
            title: L10n.of(context).theme,
            children: [
              RadioListTile<AdaptiveThemeMode>(
                groupValue: _currentTheme,
                key: Key('System'),
                value: AdaptiveThemeMode.system,
                title: Text(L10n.of(context).system),
                onChanged: (t) => _switchTheme(t, context),
              ),
              RadioListTile<AdaptiveThemeMode>(
                groupValue: _currentTheme,
                key: Key('Light'),
                value: AdaptiveThemeMode.light,
                title: Text(L10n.of(context).light),
                onChanged: (t) => _switchTheme(t, context),
              ),
              RadioListTile<AdaptiveThemeMode>(
                groupValue: _currentTheme,
                key: Key('Dark'),
                value: AdaptiveThemeMode.dark,
                title: Text(L10n.of(context).dark),
                onChanged: (t) => _switchTheme(t, context),
              ),
            ],
          ),
          FutureBuilder<dynamic>(
              future: _currentWallpaperFuture,
              builder: (context, snapshot) {
                if (snapshot.hasError) {
                  ErrorReporter.captureException(exception: snapshot.error);
                  return Text(L10n.of(context).oopsSomethingWentWrong);
                }
                final currentWallpaper = snapshot.data is String
                    ? int.parse(snapshot.data)
                    : snapshot.data;
                return SettingsListGroup(
                  title: L10n.of(context).wallpaper,
                  children: [
                    for (var i = 0; i < Famedly.wallpaperCount; i++)
                      RadioListTile<int>(
                        groupValue: currentWallpaper,
                        value: i,
                        key: Key('background_' + i.toString()),
                        title: Image.asset(
                          'assets/images/wallpaper/$i.png',
                          height: 50,
                          fit: BoxFit.cover,
                        ),
                        onChanged: (wallpaper) =>
                            _switchWallpaper(wallpaper, context),
                      ),
                  ],
                );
              }),
        ],
      ),
    );
  }
}
