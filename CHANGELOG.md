# Changelog Famedly App
Multiplatform Application for internal communication and intersectoral exchange for the german healthcare sector.

## Unreleased

### Feature
* Three column requests design [384c953c]

### Fixes
* Filter contacts who can be invited [99dcdcdf]
* Capitalization in message menu in english locale [d8726bd8]
* Read Receipt dialog in column mode [6cf2ae61]
* Room routing [a2af922f]
* iOS key verification dialog [28e9c0a7]
* Display error message if password was wrong in UIA [9669f49f]
* Missing padding for emoji text [9c67c65d]
* Display support instead of internal team to user [a5e62bdc]
* Dialogs using wrong Navigator [e9dcccc1]
* Minor userviewer fixes [85149acc]
* Push test order [79f333c7]

### Docs
* Add missing windows download links [6654fb5e]
* Update steps to release [c284fadf]

### Refactor
* Requests page [922278f6]

## v0.32.1 - 2021-02-22

### Chore
* Bump version [0a0460ac]
* Update SDK [fcc2ea28]

### Feature
* Add windows msix build [8784b18d]

### Fixes
* Broadcasts on iOS [d7f53081]
* Invite from user directory [178f8ad4]
* Leave request [cb9dd4ec]
* Minor desktop task lists bugs [dbb6edb9]
* Windows null exception on webHasFocus [3e91d905]
* User directory if role manager isnt responding [8c9dee71]
* Clear applock on logout [80bd148f]

## v0.32.0 - 2021-02-16

### Chore
* Bump version [84003623]
* update flutter_olm (olm 3.2.1) [dffaf604]
* Switch to file picker cross [85b1fb42]
* Hide state events sent or about botty [0d4d0c00]

### Feature
* Implement new bootstrap GUI [9673b22c]
* Play call sound on start and end call [236f4730]
* Add autofillhints [fb93a0d5]

### Fixes
* Update tab title [c8adf73b]
* Web tab notification counter [92c1d086]
* Room Media Page [d09cc826]
* Clear applock on reinstall iOS [fbfafa8c]
* First login set avatar [00252d1b]
* Dark mode call fab colors [64f58790]
* Calls breaking rooms [18a021e4]
* Web invite notification content [d39e2ab7]
* ImageViewer design [ea7c01a5]
* Hide checkoutPhoneSettings in web [0c85ebed]
* Start chat dialog [11dceaa7]

### Refactor
* use fcm_shared_isolate for ios notifications [99777e30]
* Remove unnecessary var and add missing null check [930c2fd3]
* Speed up app start [5a1e7b1f]

## v0.31.2 - 2021-01-25

### Chore
* Bump version [473a82e4]
* Update SDK [e3564b81]
* Update SDK [c9ffb316]

### Feature
* Confirmation dialog before starting a new chat [137acfa4]

### Fixes
* applock file picker [adcac76f]
* Avatar upload with transparent avatars [f98690e9]
* Web notifications on blur [265a3d6b]
* Dont check local url on debug [06bebffa]
* Requests subtitle [8439ecef]
* Join room error reports [91a64508]
* Key verification dialog button order [6a5d2539]
* Bad encryption design [253592fd]

### Refactor
* Desktop notifications [002183c4]

## v0.31.1 - 2021-01-20

### Chore
* Bump version [ee8837c9]

### Fixes
* Invite push for rooms without avatar [c3832c00]
* User Directory Search [aa8c88e4]
* Facilitypage if there is no role manager [bb35490e]
* Crazy unread marker [1df5a6cb]

### Refactor
* remove unneeded duplicates [c5d3779a]

## v0.31.0 - 2021-01-17

### Chore
* Bump version [3c3b301e]
* Update flutter_matrix_html [ad1ea3f4]

### Feature
* New wallpaper and default adaptive wallpaper [1301d654]
* Redesign search view [70a6abe2]

### Fixes
* Hide fallback room names [a35a4269]
* Lookup archived rooms [f941e03e]
* iOS push url [0107ebde]
* Incoming broadcast dialog [0e41cf4b]
* Internal requests routing [f4acc6b5]
* Minor fix in facility page [e9390899]
* Request history [a6ff77d5]
* Logout in web [4e2855d2]
* Remove Gemfile.lock [ca5c2776]
* Switching room bugs [4513f6a8]
* use branch ref because the branch gets rebased [18aad0a9]
* Olm Exception handling [ff8adbfb]
* Uncaught MatrixConnectionException [edfd9130]

## v0.30.1 - 2021-01-07

### Chore
* Bump version [e916da09]
* Update SDK [3379c6e9]
* Add allowed server [488f2c82]
* Update SDK [0978c9c0]
* Add license headers [55fc0dbb]

### Feature
* Implement config file for web [ada54707]
* Update dependencies and improve sentry [6e9cf282]
* Implement bootstrap technical preview [0c27ba6f]
* Verify own devices in device settings [907feddf]

### Fixes
* Flickering chat list [29989a60]
* debugPrint analyzer error in ios patch [0baf8373]
* Typo in english translation [55b0ae19]
* Routing on logout [4f105208]
* Broadcast search field [4b77dc6b]

### Refactor
* Check role manager at organisation picker [502bb60d]
* Famedly [2f6c3c12]

## v0.30.0 - 2021-01-04

### Chore
* Add tests for device management [b80633b2]
* Bump version [53ecc8ee]
* Switch to pub matomo [c4f3f761]
* update sdk [4ebf4bbb]
* update sdk [de8e8a22]
* Update SDK [ca17f90a]
* Update sdk [288ade8c]
* Update SDK [3cb60005]
* remove downloaded files [c4cf5466]
* Translate tutorial [c8b941e8]

### Feature
* Bring back manual verification [1fa13847]
* Update sdk [3206c324]
* Show typing indicator in chat list [789123d0]
* Implement emoji verification [4c2fbb31]
* Implement nice logviewer [b01b8404]

### Fixes
* Archive room [89af0f50]
* Reset layout after logout [b997d73d]
* Multiple sentry warnings by updating matomo [19ba0ac8]
* Only send read marker for valid matrix IDs [5f200e72]
* properly strip reply fallback [a04b7f79]
* Switch theme without app restart [19a01a93]
* Send read marker [f624186a]
* Set passphrase [ab48d543]
* Room item context menu [6e5ba72f]
* Missing catch [6cef5159]

### Refactor
* Place Matrix widget below MaterialApp [8debefb8]
* Implement adaptive loading dialog [2b9ac480]
* Implement adaptivedialogs [5db5615a]

## v0.29.0 - 2020-12-18

### Chore
* Update SDK [1b20ac79]
* Next version [7ccc2e3b]
* update fcm_shared_isolate (optional firebase) [7828e45b]
* Update dependencies [0aef627c]
* Upgrade flutter_sound [5097cf56]
* Update SDK [e5f0eb64]

### Feature
* Implement new adaptive page layout [a4f7fd15]
* presence depending on AppLifecycleState [89e35ad2]
* Implement fhir questionaire broadcasts [6aa087d7]
* Implement FamedlyIconsV2 [e2886ded]
* Implement notification settings [c9d2dadc]

### Fixes
* Magic link login [398b21f2]
* Update APL [1303691e]
* APL regressions [f5f14263]
* detect untranslated messages in code_analyze.sh [5bfec8ea]
* Minor dark mode color fixes [4a2aea82]
* Use flutter_sound_lite to decrease install size [02184f97]
* iOS voice recording [ca6da592]
* Android invite link [193bb636]
* Download files [c6dc7c1f]
* Abort recording when requesting permissions [142d2d00]
* Questionnaire nested items [0f65aaf1]
* Analyzer job [fcb3343e]
* no more multicolored progressbars [3e364cb4]
* Highlight room list tile [f99286be]
* Wrong color in contact list tile [4c01535b]
* play and pause of audio messages [f43e795c]
* build warning on iOS [bc621bd4]
* Error flushbar on cancel image editor [d5731397]
* Add sentry on login exception [2fc2a198]
* Input text field size on macOS browser [62634f58]
* Missing localization [63015c23]

### Docs
* Add more manual tests [a11a8e8a]
* Update release documentation [41b769c7]

### Refactor
* remove pushMain [bfe04e45]
* Update SDK [a06bc57c]
* Use EdgeInsets.zero [e56591f4]
* remove globalActiveRoomID [36eb4eb1]
* fcm_shared_isolate [f47b62be]
* do not create BackgroundPushPlugin if kIsWeb [eac7f75f]
* Remove widget function antipattern [6fc13075]
* Broadcast picker [8ef76084]

## v0.28.1 - 2020-12-02

### Chore
* Update flutter_intro [c422e9f0]
* automated dependency update [6789a0c9]

### Fixes
* Hide username in matomo [c9b447b2]
* Tutorial after password change [05404c98]
* Tutorial not working on QR code login [7085416d]
* Missing null check [62288836]

## v0.28.0 - 2020-12-01

### Chore
* move checkout phone dialog to settings info [99012257]
* Change e2ee warning message [ef7c854c]
* Update dependencies [376e06d4]
* update flutter_matrix_html [b7b22e8f]
* add read receipts and search page to integration tests [7d399669]
* Update dependencies [8e8ca937]
* Switch from asuka to flushbar [30a05471]
* Go back to upstream matomo [36e4ecb7]
* big localization overhaul [0f339f1c]

### Feature
* DatePicker for patient birthday [95dc8424]
* mark rooms as unread [4ef7c8fb]
* Make images fit to their max aspect ratio [de0da8ba]
* Fit images inside the allowed space if possible [43f8992c]
* Show replied to image [6b17d4c3]
* go to room on notification tap [95d0a9cf]
* show toast how to send voice messages [24860d48]
* android: prevent screenshots being taken [a32f0d37]
* show notifications for invites [b15a8f60]
* Implement search for roles [e51dae2f]
* Add participant button in chat [6618f18d]
* Automatically load history on first room enter [64afb832]

### Fixes
* Clear forward content on pop and remove dead code [5aac3bc8]
* user directory settings [ba587208]
* QR code login [b7c9d5c3]
* Login exception handling [da57ef82]
* Move connectivity permission dialog to settings [8989ef65]
* Missing null checks [663e17fc]
* mentions not insertable in the middle of a message [97accd49]
* unset activeRoomID based on AppLifecycleState [ccc0b78f]
* Guard against empty filename in file messages [3baab4b8]
* sent read receipts only once [79e48cd3]
* Web doesnt start [19e362ef]
* document viewer in multi-column layout when not logged in [b58157f5]
* search page l10n lookup [04aabff9]
* localize 'unread messages' [02edd935]
* detect language for notifications [44a45163]
* Minor fixes from sentry [ccc04ad7]
* Show error on failed call [f16ae961]
* Dont send connection problems to sentry [61e7061b]
* Error localizations [82b50e05]
* Minor buildcontext dialog bug [eea66a05]
* read receipt dialog [2df078c3]
* Tutorial [5331af8e]
* don't autofocus chat input field on mobile [1a1eaf15]
* Loading mode pop [8ff65a7b]
* smaller redact bubbles [16cca182]
* Keyboard visibility on web warning [5838ae76]
* Missing mounted check [5ec0ebde]

### Refactor
* Minor client init fixes and refactoring [04ce4238]
* Prints and debug handling [1ebe446f]

## v0.27.0 - 2020-11-16

### Chore
* Bump version [c9a9ab87]
* Update dependencies [d335396b]

### Feature
* New user directory GUI [bd8d84dd]
* New message bubble design [bfbe69ab]
* Better push service error handling [6bafc802]
* Enable matomo for web [8340491e]

### Fixes
* Ugly workaround for push in open rooms [8fe59b9c]
* Missing null check in global user directory [0be595f2]
* Missing null check [2a44311e]
* Sentry on web [0659779e]
* Thumbnail creation [e960b784]
* Push test message [8688fab0]
* Automute contact discovery rooms [97878e14]
* Dark mode text color [965db74c]
* User directory follow up fix [af425d33]
* Display organisation name instead of domain [2d8a27d1]
* Push map room id to int [29a79ae4]
* Dropzone [75e6a4fc]
* Downloading files [d758e560]
* Change style [1c32e372]
* First enter user directory settings [82f32f23]
* Special characters in user directory [06d15a6d]
* iOS foreground push [e2437cc1]

### Refactor
* Event widget [8dc7bd5c]

## v0.26.1 - 2020-11-09

### Feature
* Implement welcome message [5c6abdad]

### Fixes
* Change password on first login [44653384]

## v0.26.0 - 2020-11-04

### Chore
* Update howto release docs [4ed7114a]
* Next version [486e3db3]
* Use random mxid localparts as matomo id [b00012a0]
* Remove broken build iOS job from CI [67b9c2e4]

### Feature
* Add mxid to survey urls [50b7e99f]

### Fixes
* Hide mxid in matomo [5d35ae61]
* Automatic leave broken invites [95bd059d]
* Intro scroll view [fe4de789]

## v0.25.2 - 2020-11-05

### Chore
* Update version [79c27cf8]

### Fixes
* Hotfix join broken rooms [684d1d07]
* windows build [16688fa3]

## v0.25.1 - 2020-11-05

### Chore
* Update version [72a06153]

### Fixes
* Switch to dart matomo [f7797952]
* Chat design fixes [47cffae0]
* Wrong wallpaper path [44dca659]
* Matomo [a72828fa]

## v0.25.0 - 2020-11-03

### Chore
* Bump version [c53a8c7f]
* Update dependencies [045ad09e]
* Update contributing [9e3d09cd]
* use german text for notifications [a35e792b]
* Rename my requests [10f781bb]
* Remove fade transition [131ecde4]
* Update dependencies [ebd74763]
* remove unused file [c8b73474]
* Update sdk [4b3dae18]
* update matrix_link_text [b179ac77]
* Use ci variables for integration tests [4ddd51c2]

### Feature
* Implement custom wallpaper [34858e10]
* indicate not-yet-loaded events [300e3d5f]
* Add initial desktop support [1efeaecc]
* Implement first login tutorial [72af8336]

### Fixes
* Loading dialog [54bda679]
* iOS build [42e806b8]
* UserList [85f1b72d]
* Search page [4992022a]
* disable LocalNotificationPlugin on web [2abaacb8]
* Missing nullcheck on taskpage [1ce74be4]
* Ignore matrix connection exceptions [214d820a]
* Invisible unread badges [c4008203]
* Archive room from rooms list [c9d322f5]
* don't call setupClient twice [f608c022]
* non-annoying push [24a7daf5]
* Download on Android [1a9c5d94]
* tutorial bugs [4b191321]
* Duplicated globalkeys [3c497297]
* Non-clickable links in web [4db8e178]

### Docs
* document how to build a release version of the android app [f0153657]

### Refactor
* Sentry [843dce1f]
* remove custom Application class [b6de7f31]
* Update SDK [7399a44d]

## v0.24.1 - 2020-10-21

### Chore
* Bump version [931a3529]
* update native_imaging [12464c57]

### Feature
* Use HTML rendering [3d700431]
* Swithc to matrix_link_text [1cf447e5]
* Redesign slideables [943c2299]
* Add privacy policy to app [d7c89475]
* Implement new FAB [8e7112cd]
* Enter text before start request [1da47a9b]
* Make FAB extended [3faf0a1d]

### Fixes
* Unread requests bubble color [0b5705b5]
* Remove broken video event [e9b0e3a6]
* Outside logout [cfc0abc2]
* Auto update notifications muted header [416a049f]
* Dropdown button shadow [6c3ce091]
* Version comparison by semver package [90f62a79]
* Minor design and localization fixes [65ea4375]
* Native imaging [c12ca003]
* Clean up loading dialog [5ab4327e]
* Send thumbnails [d7f80006]
* Change password view [0d396b3f]
* Disable android backup [7005184a]
* Send files on iOS [4b68aebd]

### Refactor
* Blue color for request unread bubbles [bd19e9c4]

## v0.24.0 - 2020-10-08

### Chore
* Bump sdk and version [f0354245]
* Update secure storage [b3501382]
* use latest javascript olm [3760653a]
* update sdk [afd06824]
* Upgrade to flutter 1.22 [8e32b0a2]
* Update SDK [02bac561]
* Fix formatting errors [c7b12f0c]
* Update SDK [dd9958f0]

### Feature
* Implement newest file picker [0dd90adc]
* Add Notification 15m before TODO ending date [a6245a96]
* Implement new group name textfield [e08daca2]
* Implement web scroll bars everywhere [6cce87d4]
* Implement license page [f485e9d2]

### Fixes
* iOS settings [f935daf1]
* overgo issues with flutter_secure_storage [d0cd446f]
* Don't send rr when room is fully read [a0c2bbd6]
* Typo integration tests [bcf7404d]
* Catch errors in setupClient [19c06748]
* Clean up test push on test servers [7a505295]
* LinearGradients [c515bed1]
* Disable connection status header in integration tests [52bc04d6]
* Filter textfield focus [8ddc6449]
* Local notifications by downgrading [429580a9]
* Event list scroll behaviour [db88bfed]
* login error elided the actual error [cc81bf66]
* Integration tests on buildserver [baaebe89]
* Integration test restart emulator [f14e3146]
* iPad camera [43890386]
* Integration tests [648e92f1]
* Make sure the translations are updated [e8d6c24b]
* Passcode screen localizations [eaf63c28]
* Add lastbuildid from iOS to gitignore [a6cda901]
* Missing translations [ac7cbe56]
* Some messages incorrectly being displayed with a large font [90e3a13a]
* Removes user from the member list in case of leaving the group [ba2105d6]
* Cocoapods [2957bcf7]
* Update SDK and refactoring [948bd40f]
* Download files [54009110]
* Remove covid bot from iOS [52944164]
* Enable e2ee recovery for things like corrupted olm session recovery [80509280]
* Post archive routing [12287d60]

### Refactor
* Emoji text messages [c9208204]

### Test
* Add SettingsPage Basic Test [2a42f334]
* Add SharePage Test [43f99c42]
* Add InvitePage Test [86210e2f]
* Add event_search_results_page and image_view tests which both are unsolvable at this point [64aefb77]
* Add enter_pass_code_view Test [c9ac4a87]
* Add empty_page Test [b62e9bc9]
* Add device_settings_page Test [05a31b12]

## v0.23.7 - 2020-09-09

### Chore
* Update version number and changelog [31bf6235]
* Update sdk [a4bb83ef]

### Fixes
* Show patient transfer formular on contact points with this tag [d8d781d4]
* Key requests from unknown users [87727e09]
* Requests sorted wrong [aa45163c]
* Request page is not updated automatically [8f4f0764]
* Incoming key request breaks fingerprint scanner [8c21687b]
* Minor sentry bug [a9c91e85]
* Remove device dialog [20c9a106]
* App lock view visible for 1s even when disabled [c9921df1]
* Web desktop routes [5eeb5c83]

## v0.23.6 - 2020-09-03

### Chore
* Update version [769f3658]

### Fixes
* All dialogs [bbcb07a7]
* Allow key sharing with other users [17221d25]
* Dialogs [78a89e38]

## v0.23.5 - 2020-09-01

### Chore
* Update version number [b7b14954]

### Fixes
* Hotfix for loading dialog [e43022e8]

### Refactor
* reformat files [a9e63ed6]

## v0.23.4 - 2020-08-31

### Feature
* mark forwards [c3f896c0]

### Fixes
* Disable backgroundpush and enable sentry by default in point release [dec51e38]
* Hotfix web [bc04bad1]
* iOS build [2b3adbcd]
* Darkmode button color on tasks view [47416926]

## v0.23.1 - 2020-08-26

### Chore
* switch from keep_screen_on to wakelock [094fdea4]
* upgrade to embeddings v2 [473101be]
* do not use deprecated moor_ffi package [3175f49b]

### Feature
* new version [96a61b2a]
* Enable background push [8d9937e1]

### Fixes
* Send database errors to sentry [b21e2415]
* web [f685f4ec]
* Web applock [b3789db9]
* App lock [5466357a]
* Send read receipt on web only when focus [772762d7]

### Refactor
* remove empty MainActivity [5328936f]
* omit bool variable tryUseCache [a185c3b3]

## v0.23.0 - 2020-08-21

### Chore
* Update SDK [cb327604]

### Feature
* Display emoji messages bigger [96306724]
* make Sentry opt in to be GDPR compliant [e517889e]

### Fixes
* Disable background push on android [507a54e2]
* improve stability of integration test [72ac92ab]
* display fingerprint key instead of identity key on settings info page [25ac61bd]

### Style
* Change package:famedly imports to relative imports [43303e0c]

### Refactor
* Matrix Chat List [2e355248]


This CHANGELOG.md was generated with [**Changelog for Dart**](https://pub.dartlang.org/packages/changelog)
