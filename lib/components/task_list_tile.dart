/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'package:famedly/models/tasks/task.dart';
import 'package:famedly/styles/colors.dart';

import 'package:flutter/material.dart';
import 'package:adaptive_page_layout/adaptive_page_layout.dart';
import 'package:future_loading_dialog/future_loading_dialog.dart';

import 'circular_check_box.dart';
import 'date_display.dart';

class TaskListTile extends StatelessWidget {
  final Task task;

  const TaskListTile({Key key, @required this.task}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        ListTile(
            key: Key('TaskListTile'),
            title: Text(
              task.title,
              style: TextStyle(
                  decoration: task.done
                      ? TextDecoration.lineThrough
                      : TextDecoration.none),
            ),
            subtitle: task.description.isEmpty &&
                    task.date == null &&
                    task.subtask.isEmpty
                ? null
                : Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                        if (task.description.isNotEmpty)
                          Padding(
                              padding: EdgeInsets.only(top: 5, bottom: 5),
                              child: Text(task.description)),
                        if (task.date != null) DateDisplay(task.date),
                        if (task.subtask.isNotEmpty) ...{
                          SizedBox(height: 20),
                          Divider(
                            height: 1,
                            thickness: 1,
                            color:
                                Theme.of(context).brightness == Brightness.light
                                    ? FamedlyColors.paleGrey
                                    : FamedlyColors.slate,
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width - 60,
                            height: task.subtask.length * 55.0,
                            child: ListView.builder(
                              physics: const NeverScrollableScrollPhysics(),
                              itemCount: task.subtask.length,
                              itemBuilder: (BuildContext context, int index) {
                                final subtask = task.subtask[index];
                                return ListTile(
                                    leading: CircularCheckBox(
                                      value: subtask.done,
                                      activeColor: FamedlyColors.azul,
                                      onChanged: (bool newChecked) =>
                                          showFutureLoadingDialog(
                                              context: context,
                                              future: () =>
                                                  subtask.setDone(newChecked)),
                                    ),
                                    title: Text(
                                      subtask.title,
                                      style: TextStyle(
                                          decoration: subtask.done
                                              ? TextDecoration.lineThrough
                                              : TextDecoration.none),
                                    ));
                              },
                            ),
                          ),
                        }
                      ]),
            leading: CircularCheckBox(
              key: Key('TagListCheckBox'),
              value: task.done,
              activeColor: FamedlyColors.azul,
              onChanged: (bool newChecked) => showFutureLoadingDialog(
                  context: context, future: () => task.setDone(newChecked)),
            ),
            onTap: () {
              AdaptivePageLayout.of(context).pushNamed('/tasks/${task.id}');
            }),
        SizedBox(
          height: 0.0 +
              (task.description.isEmpty ? 0 : 5) +
              (task.date == null ? 0 : 5),
        ),
        Divider(
          indent: 80,
          height: 1,
          thickness: 1,
          color: Theme.of(context).brightness == Brightness.light
              ? FamedlyColors.paleGrey
              : FamedlyColors.slate,
        ),
      ],
    );
  }
}
