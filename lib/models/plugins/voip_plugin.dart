import 'dart:async';
import 'dart:core';

import 'package:connectivity/connectivity.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter_webrtc/flutter_webrtc.dart';
import 'package:sdp_transform/sdp_transform.dart' as sdp_transform;

import 'famedly_plugin.dart';
import '../../utils/voip/user_media_manager.dart';

enum CallState { kConnecting, kConnected, kMuted, kHeld, kHangup }

enum CallType { kVoice, kVideo, kData }

enum Direction { kIncoming, kOutgoing }

typedef MediaStreamCallback = void Function(MediaStream stream);

typedef CallStateCallback = void Function(CallState state);

typedef CallSessionCallback = void Function(CallSession session);

class CallSession {
  CallSession(this.voip, this.roomId, this.callId, this.dir, this.displayname,
      this.type,
      [this.remoteSdp]);
  VoipPlugin voip;
  RTCPeerConnection pc;
  String callId;
  String roomId;
  String displayname;

  /// Call ID
  CallType type;
  Direction dir;

  List<RTCIceCandidate> localCandidates = <RTCIceCandidate>[];

  /// For incoming call.
  RTCSessionDescription remoteSdp;
  List<RTCIceCandidate> remoteCandidates = <RTCIceCandidate>[];

  CallState state = CallState.kConnecting;
  MediaStream localStream, remoteStream;

  MediaStreamCallback onLocalStream;
  MediaStreamCallback onRemoveStream;

  bool iceGatheringFinished = false;

  final _callStateController =
      StreamController<CallState>.broadcast(sync: true);
  Stream<CallState> get onCallStateChanged => _callStateController.stream;

  bool get isOutgoing => dir == Direction.kOutgoing;
  bool _answeredByUs = false;

  bool speakerOn = false;
  bool micMuted = false;
  bool camMuted = false;

  bool get voiceonly =>
      (localStream == null || localStream.getVideoTracks().isEmpty) &&
      (remoteStream == null || remoteStream.getVideoTracks().isEmpty);

  Future<void> initCall(CallType type) async {
    final stream = await voip.createLocalStream(type);
    addLocalStream(stream);
  }

  void addRemoveStream(MediaStream stream) {
    remoteStream = stream;
    onRemoveStream?.call(stream);
  }

  void addLocalStream(MediaStream stream) {
    localStream = stream;
    speakerOn = type == CallType.kVideo;
    final audioTrack = localStream.getAudioTracks()[0];
    audioTrack.enableSpeakerphone(speakerOn);
    onLocalStream?.call(stream);
  }

  void setCallState(CallState newState) {
    state = newState;
    _callStateController.add(newState);
  }

  void muteMic({bool muted}) {
    if (muted != null) {
      micMuted = muted;
    } else {
      micMuted = !micMuted;
    }
    setCallState(micMuted ? CallState.kMuted : CallState.kConnected);
    if (localStream != null) {
      final audioTrack = localStream.getAudioTracks()[0];
      audioTrack.enabled = !micMuted;
    }
  }

  void muteCamera() {
    camMuted = !camMuted;
    if (localStream != null) {
      final videoTrack = localStream.getVideoTracks()[0];
      videoTrack.enabled = !camMuted;
    }
  }

  void setSpeakerOn() {
    speakerOn = !speakerOn;
  }

  void switchCamera() {
    if (localStream != null) {
      localStream.getVideoTracks()[0].switchCamera();
    }
  }

  void answer() async {
    await voip.answerCall(roomId, callId);
    setCallState(CallState.kConnected);
  }

  void hangup() async {
    await voip.hangupCall(roomId, callId);
    setCallState(CallState.kHangup);
    cleanUp();
  }

  void hold() {
    // Not yet implemented.
  }

  void unhold() {
    // Not yet implemented.
  }

  void sendDTMF(String tones) {
    // Not yet implemented.
  }

  void cleanUp() async {
    if (localStream != null) {
      localStream.getTracks().forEach((element) async {
        await element.dispose();
      });
      await localStream.dispose();
      localStream = null;
    }
    onLocalStream = null;
    onRemoveStream = null;
  }
}

class VoipPlugin extends FamedlyPlugin {
  TurnServerCredentials _turnServerCredentials;
  Map<String, CallSession> sessions = <String, CallSession>{};
  String currentCID;
  ConnectivityResult _currentConnectivity;
  CallSessionCallback onIncomingCall;

  VoipPlugin(matrix) : super(matrix) {
    client.onCallInvite.stream.listen((Event event) {
      if (event.senderId == client.userID) {
        // Ignore messages to yourself.
        return;
      }

      Logs().v(
          '[VOIP] onCallInvite ${event.senderId} => ${client.userID}, \ncontent => ${event.content.toString()}');

      final String callId = event.content['call_id'];

      if (currentCID != null) {
        // Only one session at a time.
        Logs().v('[VOIP] onCallInvite: There is already a session.');
        event.room.hangupCall(callId);
        return;
      }

      final displayName = event.room.displayname;
      if (sessions[callId] != null) {
        // Session already exist.
        Logs().v('[VOIP] onCallInvite: Session [$callId] already exist.');
        return;
      }

      final callType = getCallType(event.content['offer']['sdp']);

      final session = CallSession(
          this,
          event.room.id,
          callId,
          Direction.kIncoming,
          displayName,
          callType,
          RTCSessionDescription(
            event.content['offer']['sdp'],
            event.content['offer']['type'],
          ));
      sessions[callId] = session;
      currentCID = callId;
      session.initCall(callType).then((_) {
        // Popup CallingPage for incoming call.
        famedly.adaptivePageLayout.currentState.pushNamed(
            '/call/${event.room.id}/${callTypeToString(callType)}/incoming');
      });
      // Handle incoming call for callkeep plugin.
      onIncomingCall?.call(session);
      // Play ringtone
      try {
        UserMediaManager().startRinginTone();
      } catch (_) {}
    });

    client.onCallAnswer.stream.listen((Event event) async {
      Logs().v('[VOIP] onCallAnswer => ${event.content.toString()}');
      final String callId = event.content['call_id'];
      final session = sessions[callId];
      if (session != null) {
        if (event.senderId == client.userID) {
          // Ignore messages to yourself.
          if (!session._answeredByUs) {
            try {
              await UserMediaManager().stopRingingTone();
            } catch (_) {}
            session.setCallState(CallState.kHangup);
            sessionDispose(callId);
            session.cleanUp();
            currentCID = null;
          }
          return;
        }

        if (session.dir == Direction.kOutgoing) {
          await session.pc.setRemoteDescription(RTCSessionDescription(
              event.content['answer']['sdp'], event.content['answer']['type']));
          session.setCallState(CallState.kConnected);
        }
      } else {
        Logs().v('[VOIP] onCallAnswer: Session [$callId] not found!');
      }
    });

    client.onCallCandidates.stream.listen((Event event) async {
      if (event.senderId == client.userID) {
        // Ignore messages to yourself.
        return;
      }
      Logs().v('[VOIP] onCallCandidates => ${event.content.toString()}');
      final String callId = event.content['call_id'];
      final session = sessions[callId];
      if (session != null) {
        final candidates = event.content['candidates'];
        candidates.forEach((json) async {
          final candidate = RTCIceCandidate(
            json['candidate'],
            json['sdpMid'],
            json['sdpMLineIndex']?.round() ?? 0,
          );

          if (session.pc != null) {
            await session.pc.addCandidate(candidate);
          } else {
            session.remoteCandidates.add(candidate);
          }
        });

        if (session.pc != null &&
            session.pc.iceConnectionState ==
                RTCIceConnectionState.RTCIceConnectionStateDisconnected) {
          _restartIce();
        }
      } else {
        Logs().v('[VOIP] onCallCandidates: Session [$callId] not found!');
      }
    });

    client.onCallHangup.stream.listen((Event event) async {
      // stop play ringtone, if this is an incoming call
      try {
        await UserMediaManager().stopRingingTone();
      } catch (_) {}
      Logs().v('[VOIP] onCallHangup => ${event.content.toString()}');
      final String callId = event.content['call_id'];
      final session = sessions[callId];
      if (session != null) {
        // hangup in any case, either if the other party hung up or we did on another device
        session.setCallState(CallState.kHangup);
        sessionDispose(callId);
        session.cleanUp();
      } else {
        Logs().v('[VOIP] onCallHangup: Session [$callId] not found!');
      }
      currentCID = null;
    });

    Connectivity().onConnectivityChanged.listen(_handleNetworkChanged);
    Connectivity()
        .checkConnectivity()
        .then((result) => _currentConnectivity = result)
        .catchError((e) => _currentConnectivity = ConnectivityResult.none);
  }

  CallType getCallType(String sdp) {
    final session = sdp_transform.parse(sdp);
    if (session['media'].indexWhere((e) => e['type'] == 'video') != -1) {
      return CallType.kVideo;
    }
    return CallType.kVoice;
  }

  Map<String, dynamic> getOfferAnswerConstraints(CallType type,
      {bool iceRestart = false}) {
    return {
      'mandatory': {
        'OfferToReceiveAudio': true,
        'OfferToReceiveVideo': type == CallType.kVideo,
        if (iceRestart) 'IceRestart': true
      },
      'optional': [],
    };
  }

  Future<Map<String, dynamic>> getIceSevers() async {
    if (_turnServerCredentials == null) {
      try {
        _turnServerCredentials = await client.getTurnServer();
      } catch (e) {
        Logs().v(
            '[VOIP] client.getTurnServerCredentials error => ${e.toString()}');
      }
    }

    /*if (_turnServerCredentials == null) {
      return {'url': 'stun:stun.l.google.com:19302'};
    }*/

    return {
      'username': _turnServerCredentials.username,
      'credential': _turnServerCredentials.password,
      'url': _turnServerCredentials.uris[0]
    };
  }

  void _handleNetworkChanged(ConnectivityResult result) async {
    // Got a new connectivity status!
    if (_currentConnectivity != result) {
      _restartIce();
    }
    _currentConnectivity = result;
  }

  void _restartIce() async {
    if (currentCID != null) {
      Logs().v('[VOIP] iceRestart.');
      // Needs restart ice on session.pc and renegotiation.
      final session = sessions[currentCID];
      session.iceGatheringFinished = false;
      final desc = await session.pc.createOffer(
          getOfferAnswerConstraints(session.type, iceRestart: true));
      await session.pc.setLocalDescription(desc);
      session.localCandidates.clear();
    }
  }

  Future<CallSession> inviteToCall(String roomId, CallType type) async {
    final room = famedly.client.getRoomById(roomId);
    if (room == null) {
      Logs().v('[VOIP] Invalid room id [$roomId].');
      return null;
    }

    final callId = 'cid${DateTime.now().millisecondsSinceEpoch}';
    final session = CallSession(
        this, roomId, callId, Direction.kOutgoing, room.displayname, type);
    currentCID = callId;
    sessions[callId] = session;
    await session.initCall(type);

    try {
      final pc = await _createPeerConnection(session);
      session.pc = pc;
      await pc.addStream(session.localStream);

      pc.onIceCandidate = (RTCIceCandidate candidate) async {
        //Logs().v('[VOIP] onIceCandidate => ${candidate.toMap().toString()}');
        session.localCandidates.add(candidate);
      };
      pc.onIceGatheringState = (RTCIceGatheringState state) async {
        Logs().v('[VOIP] IceGatheringState => ${state.toString()}');

        if (state == RTCIceGatheringState.RTCIceGatheringStateGathering) {
          Timer(Duration(milliseconds: 3000), () async {
            if (!session.iceGatheringFinished) {
              session.iceGatheringFinished = true;
              await candidateReady(room, session);
            }
          });
        }

        if (state == RTCIceGatheringState.RTCIceGatheringStateComplete) {
          if (!session.iceGatheringFinished) {
            session.iceGatheringFinished = true;
            await candidateReady(room, session);
          }
        }
      };

      pc.onIceConnectionState = (RTCIceConnectionState state) {
        Logs().v('[VOIP] RTCIceConnectionState => ${state.toString()}');
        if (state == RTCIceConnectionState.RTCIceConnectionStateConnected) {
          session.localCandidates.clear();
          session.remoteCandidates.clear();
        }
      };

      final desc = await pc.createOffer(getOfferAnswerConstraints(type));
      final res = await room.inviteToCall(callId, 10 * 1000, desc.sdp);
      Logs().v('[VOIP] room.inviteToCall res => $res');
      await pc.setLocalDescription(desc);
      currentCID = callId;
    } catch (e) {
      Logs().v('[VOIP] room.inviteToCall error => ${e.toString()}');
    }

    await famedly.adaptivePageLayout.currentState
        .pushNamed('/call/$roomId/video/outgoing');
    return session;
  }

  Future<void> candidateReady(room, session) async {
    /*
    Currently, trickle-ice is not supported, so it will take a
    long time to wait to collect all the canidates, set the
    timeout for collection canidates to speed up the connection.
    */
    try {
      final candidates = <Map<String, dynamic>>[];
      session.localCandidates.forEach((element) {
        candidates.add(element.toMap());
      });
      final res = await room.sendCallCandidates(session.callId, candidates);
      Logs().v('[VOIP] sendCallCandidates res => $res');
    } catch (e) {
      Logs().v('[VOIP] sendCallCandidates e => ${e.toString()}');
    }
  }

  Future<String> answerCall(String roomId, String callId) async {
    // stop play ringtone
    try {
      await UserMediaManager().stopRingingTone();
    } catch (_) {}
    final room = famedly.client.getRoomById(roomId);
    final session = sessions[callId];
    session._answeredByUs = true;
    try {
      final pc = await _createPeerConnection(session);
      session.pc = pc;
      await pc.addStream(session.localStream);

      pc.onIceCandidate = (RTCIceCandidate candidate) async {
        Logs().v(
            '[VOIP] answerCall:onIceCandidate => ${candidate.toMap().toString()}');
        session.localCandidates.add(candidate);
      };

      pc.onIceGatheringState = (RTCIceGatheringState state) async {
        Logs().v('[VOIP] IceGatheringState => ${state.toString()}');
        if (state == RTCIceGatheringState.RTCIceGatheringStateGathering) {
          Timer(Duration(milliseconds: 3000), () async {
            if (!session.iceGatheringFinished) {
              session.iceGatheringFinished = true;
              await candidateReady(room, session);
            }
          });
        }

        if (state == RTCIceGatheringState.RTCIceGatheringStateComplete) {
          if (!session.iceGatheringFinished) {
            session.iceGatheringFinished = true;
            await candidateReady(room, session);
          }
        }
      };

      await pc.setRemoteDescription(session.remoteSdp);
      final desc =
          await pc.createAnswer(getOfferAnswerConstraints(session.type));
      session.remoteCandidates
          .forEach((candidate) => pc.addCandidate(candidate));
      final res = await room.answerCall(session.callId, desc.sdp);
      Logs().v('[VOIP] room.answerCall res => $res');
      await pc.setLocalDescription(desc);
      return res;
    } catch (e) {
      Logs().v('[VOIP] room.answerCall error => ${e.toString()}');
    }
    return null;
  }

  Future<void> hangupCall(String roomId, String callId) async {
    // stop play ringtone
    try {
      await UserMediaManager().stopRingingTone();
    } catch (_) {}
    final room = famedly.client.getRoomById(roomId);
    if (room != null) {
      try {
        final res = await room.hangupCall(callId);
        Logs().v('[VOIP] room.hangupCall res => $res');
        sessionDispose(callId);
      } catch (e) {
        Logs().v('[VOIP] room.hangupCall error => ${e.toString()}');
      }
      currentCID = null;
    }
  }

  void sessionDispose(String callId) async {
    final session = sessions.remove(callId);
    if (session != null && session.pc != null) {
      await session.pc.close();
      await session.pc.dispose();
    }
  }

  Future<MediaStream> createLocalStream(CallType type) async {
    final mediaConstraints = {
      'audio': true,
      'video': type == CallType.kVideo
          ? {
              'mandatory': {
                'minWidth': '640',
                'minHeight': '480',
                'minFrameRate': '30',
              },
              'facingMode': 'user',
              'optional': [],
            }
          : false,
    };
    return await navigator.mediaDevices.getUserMedia(mediaConstraints);
  }

  Future<RTCPeerConnection> _createPeerConnection(CallSession session) async {
    final constraints = {
      'mandatory': {},
      'optional': [
        {'DtlsSrtpKeyAgreement': true},
      ],
    };

    final configuration = <String, dynamic>{
      'iceServers': [await getIceSevers()]
    };

    final pc = await createPeerConnection(configuration, constraints);

    pc.onAddStream = (MediaStream stream) {
      session?.addRemoveStream(stream);
    };

    return pc;
  }
}

CallType stringToCallType(String enumName) {
  switch (enumName) {
    case 'voice':
      return CallType.kVoice;
    case 'video':
      return CallType.kVideo;
    case 'data':
      return CallType.kData;
    default:
      throw Exception('Invalid Type => $enumName');
      break;
  }
}

String callTypeToString(CallType type) {
  switch (type) {
    case CallType.kVoice:
      return 'voice';
    case CallType.kVideo:
      return 'video';
    case CallType.kData:
      return 'data';
    default:
      throw Exception('Invalid Type => $type');
      break;
  }
}

String callStateToString(CallState state) {
  switch (state) {
    case CallState.kConnecting:
      return 'Connecting';
    case CallState.kConnected:
      return 'Connected';
    case CallState.kHangup:
      return 'Hangup';
    default:
      return state.toString();
      break;
  }
}

Direction stringToDirection(String enumName) {
  switch (enumName) {
    case 'incoming':
      return Direction.kIncoming;
    case 'outgoing':
      return Direction.kOutgoing;
    default:
      throw Exception('Invalid Type => $enumName');
      break;
  }
}
