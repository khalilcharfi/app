#!/bin/sh -ve
flutter format lib/ test/ test_driver/ --set-exit-if-changed
flutter analyze
flutter gen-l10n --template-arb-file intl_en.arb --untranslated-messages-file untranslated.json
# Exit with an error, if there are untranslated messages
if [ -f untranslated.json ] && jq -e 'select(values | flatten | . != [])' untranslated.json; then exit 1; fi
