import 'package:famedly/views/contact_page.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_test/flutter_test.dart';

import 'utils.dart';

void main() {
  group('ContactsPage', () {
    NavigatorObserver mockObserver;

    setUp(() {
      mockObserver = MockNavigatorObserver();
    });

    testWidgets(
      'has "new" buttons',
      (WidgetTester tester) async {
        await tester.runAsync(() async {
          // Create widget
          await tester.pumpWidget(
            await Utils.getWidgetWrapper(
              ContactsPage(),
              mockObserver,
            ),
          );

          await tester.pumpAndSettle();

          expect(find.byKey(Key('NewContactButton')), findsOneWidget);
          expect(find.byKey(Key('NewGroupButton')), findsOneWidget);
        });
      },
    );
  });
}
