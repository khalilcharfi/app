import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:famedly/styles/colors.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:intl/intl.dart';

import '../../famedly_icon_button.dart';

class EventTitle extends StatelessWidget {
  final String displayname;
  final Event event;
  final bool display;
  final DateTime sentDateTime;

  static const String _emailPrefix = '_email_';

  const EventTitle({
    Key key,
    this.displayname,
    @required this.event,
    this.display,
    this.sentDateTime,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final displayname = this.displayname ??
        event.sender.calcDisplayname(mxidLocalPartFallback: false);
    if (!display) {
      return Container();
    } else {
      Widget decoration = Container();
      if (event.sender.id.startsWith('@' + _emailPrefix)) {
        decoration = FamedlyIconButton(
          iconData: Icons.alternate_email,
          onTap: () {
            showOkAlertDialog(
              context: context,
              title: L10n.of(context).contactIsEmail,
              okLabel: L10n.of(context).next,
              useRootNavigator: false,
            );
          },
          iconSize: 14,
          size: 16,
        );
      }
      return Row(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(bottom: 5.0),
            child: Row(
              children: <Widget>[
                decoration,
                Text(
                  displayname +
                      (sentDateTime != null
                          ? ' ' +
                              DateFormat(L10n.of(context).sentDateTimeFormat)
                                  .format(sentDateTime)
                          : ''),
                  style:
                      TextStyle(fontSize: 12.0, color: FamedlyColors.slateGrey),
                ),
              ],
            ),
          ),
        ],
      );
    }
  }
}
