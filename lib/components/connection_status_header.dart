/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'dart:async';
import 'package:flutter/material.dart';

import '../models/famedly.dart';

class ConnectionStatusHeader extends StatefulWidget {
  final bool hide;

  const ConnectionStatusHeader({this.hide = false, Key key}) : super(key: key);
  @override
  _ConnectionStatusHeaderState createState() => _ConnectionStatusHeaderState();
}

class _ConnectionStatusHeaderState extends State<ConnectionStatusHeader> {
  StreamSubscription _onSyncSub;
  StreamSubscription _onSyncErrorSub;

  /// Before we get any sync we display the connection status header. But we
  /// don't do this in integration tests, because animations can make the driver
  /// waiting forever.
  static bool _connected = true;

  set connected(bool connected) {
    if (mounted) {
      setState(() => _connected = connected);
    }
  }

  @override
  void dispose() {
    _onSyncSub?.cancel();
    _onSyncErrorSub?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _onSyncSub ??= Famedly.of(context).client.onSync.stream.listen(
          (_) => connected = true,
        );
    _onSyncErrorSub ??= Famedly.of(context).client.onSyncError.stream.listen(
          (_) => connected = false,
        );

    final display = !widget.hide &&
        !_connected &&
        !Famedly.of(context).loginPlugin.isIntegrationTest;

    return AnimatedContainer(
      duration: Duration(milliseconds: 500),
      height: display ? 5 : 0,
      child: display ? LinearProgressIndicator() : null,
    );
  }
}
