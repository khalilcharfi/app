/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import '../models/room_type_extension.dart';
import '../styles/colors.dart';
import 'draggable_scrollbar.dart';
import '../models/famedly.dart';
import 'rooms_list/item.dart';
import 'rooms_list/request_item.dart';

typedef RoomFilter = bool Function(Room room);

class MatrixChatList extends StatefulWidget {
  final RoomFilter filter;
  final onlyLeft;
  final TextEditingController filterTextGetter;
  final String activeChatID;
  final ScrollController scrollController;

  /// Widget shown when there are no
  /// chats to list.
  final Widget emptyListPlaceholder;

  MatrixChatList(
    this.filterTextGetter, {
    this.onlyLeft = false,
    this.filter,
    this.activeChatID,
    this.emptyListPlaceholder,
    ScrollController scrollController,
    Key key,
  })  : scrollController = scrollController ?? ScrollController(),
        super(key: key);

  @override
  MatrixChatListState createState() => MatrixChatListState();
}

class MatrixChatListState extends State<MatrixChatList> {
  Famedly famedly;
  String filterText;

  void handleFilter() {
    if (widget.filterTextGetter == null) return;
    setState(() {
      filterText = widget.filterTextGetter.text;
    });
  }

  List<Room> filter(List<Room> roomList) {
    return roomList
        ?.where(
          (room) => !(room.id == null ||
              (widget.filter != null && !widget.filter(room)) ||
              (filterText != null &&
                  filterText != '' &&
                  !room.displayname
                      .toLowerCase()
                      .contains(filterText.toLowerCase()))),
        )
        ?.toList();
  }

  @override
  void initState() {
    super.initState();
    if (widget.filterTextGetter != null) {
      widget.filterTextGetter.addListener(handleFilter);
    }
  }

  Widget _buildItem(List<Room> rooms, BuildContext context, int index) {
    if (rooms[index].type == roomTypeRequest) {
      return RequestItem(
        key: Key('room_$index'),
        room: rooms[index],
        type: 'requests/conversations',
      );
    } else {
      return RoomItem(
        key: Key('room_$index'),
        item: rooms[index],
        activeChat: widget.activeChatID == rooms[index].id,
      );
    }
  }

  @override
  void dispose() {
    if (widget.filterTextGetter != null) {
      widget.filterTextGetter.removeListener(handleFilter);
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    famedly = Famedly.of(context);

    return StreamBuilder<Object>(
        stream: famedly.client.onSync.stream,
        builder: (context, snapshot) {
          final filtered = filter(famedly.client.rooms);

          if (filtered == null || filtered.isEmpty) {
            return widget.emptyListPlaceholder ?? Container();
          }

          return FamedlyDraggableScrollbar(
            controller: widget.scrollController,
            isAlwaysShown: kIsWeb,
            child: ListView.separated(
              controller: widget.scrollController,
              addAutomaticKeepAlives: true,
              physics: const AlwaysScrollableScrollPhysics(),
              itemCount: filtered.length,
              separatorBuilder: (context, i) => Divider(
                indent: 70,
                height: 1,
                thickness: 1,
                color: Theme.of(context).brightness == Brightness.light
                    ? FamedlyColors.paleGrey
                    : FamedlyColors.slate,
              ),
              itemBuilder: (context, index) => _buildItem(
                filtered,
                context,
                index,
              ),
            ),
          );
        });
  }
}
