import 'package:famedly/components/chat_room/event_widgets/image_event_content.dart';
import 'package:famedly/styles/famedly_icons_v2_icons.dart';
import 'package:famedly/utils/matrix_locals.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:matrix_link_text/link_text.dart';

import '../../../styles/colors.dart';
import '../../../utils/event_forwarded_extension.dart';
import '../../../utils/url_launcher.dart';
import '../../famedly_icon_button.dart';
import '../../html_message.dart';
import 'event_widget.dart';

class TextEventContent extends StatelessWidget {
  final Event textEvent;

  final Event prevEvent;
  final Event nextEvent;
  final Timeline timeline;

  TextEventContent(
    this.textEvent, {
    @required this.prevEvent,
    @required this.nextEvent,
    @required this.timeline,
    Key key,
  }) : super(key: key);

  String stripReply() {
    final lines = textEvent.text.split('\n');
    lines.removeWhere((String line) => line.startsWith('>'));

    return lines.where((String line) => line.isNotEmpty).join('\n');
  }

  @override
  Widget build(BuildContext context) {
    final ownMessage = textEvent.isMine;
    final textColor =
        ownMessage ? Colors.white : Theme.of(context).textTheme.bodyText1.color;
    final linkColor =
        ownMessage ? FamedlyColors.lightLinkColor : FamedlyColors.darkLinkColor;
    final bigText = !textEvent.redacted &&
        textEvent.onlyEmotes &&
        textEvent.numberEmotes > 0 &&
        textEvent.numberEmotes <= 10;
    if (textEvent.redacted) {
      String redactedBecause = textEvent.redactedBecause.content['reason'];
      if (redactedBecause == null || redactedBecause.isEmpty) {
        redactedBecause = L10n.of(context).messageHasBeenRemoved;
      }
      return Padding(
        padding: EventWidget.bubblePadding,
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Icon(Icons.not_interested, color: textColor, size: 14),
            SizedBox(width: 8),
            _Linkify(
                text: redactedBecause,
                textColor: textColor,
                linkColor: linkColor),
          ],
        ),
      );
    } else if (textEvent.relationshipType == RelationshipTypes.reply) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          FutureBuilder<Event>(
            future: textEvent.getReplyEvent(timeline),
            builder: (context, snapshot) {
              final event = snapshot.data ??
                  Event(
                    content: {
                      'msgtype': 'm.text',
                      'body': '...',
                    },
                    type: 'm.room.message',
                    eventId: 'loadingevent',
                    originServerTs: textEvent.originServerTs,
                    senderId: textEvent.senderId,
                    room: textEvent.room,
                  );

              return TextReplyEventContent(
                repliedEvent: event,
                prevToReplyEvent: prevEvent,
                replyEvent: textEvent,
                nextToReplyEvent: nextEvent,
                textColor: ownMessage ? FamedlyColors.blueyGrey : textColor,
                linkColor: linkColor,
                backgroundColor: ownMessage
                    ? FamedlyColors.darkerGrey
                    : Theme.of(context).secondaryHeaderColor,
              );
            },
          ),
          Padding(
            padding: EventWidget.bubblePadding.copyWith(),
            child: textEvent.isRichMessage
                ? HtmlMessage(
                    html: textEvent.content['formatted_body'],
                    defaultTextStyle: TextStyle(
                      color: textColor,
                      fontSize: bigText ? 45 : 15,
                    ),
                    linkStyle: TextStyle(
                        fontSize: bigText ? 45 : 15,
                        color: linkColor,
                        decoration: TextDecoration.underline),
                    room: textEvent.room,
                    emoteSize: bigText ? 45 : 22,
                  )
                : _Linkify(
                    text: stripReply(),
                    bigText: bigText,
                    textColor: textColor,
                    linkColor: linkColor,
                  ),
          ),
        ],
      );
    } else {
      return Padding(
        padding: EventWidget.bubblePadding,
        child: textEvent.isRichMessage
            ? HtmlMessage(
                html: textEvent.content['formatted_body'],
                defaultTextStyle: TextStyle(
                  color: textColor,
                  fontSize: bigText ? 45 : 15,
                ),
                linkStyle: TextStyle(
                    fontSize: bigText ? 45 : 15,
                    color: linkColor,
                    decoration: TextDecoration.underline),
                room: textEvent.room,
                emoteSize: bigText ? 45 : 22,
              )
            : _Linkify(
                text: textEvent.getLocalizedBody(
                  MatrixLocals(L10n.of(context)),
                  hideReply: true,
                ),
                textColor: textColor,
                linkColor: linkColor,
                bigText: bigText),
      );
    }
  }
}

class TextReplyEventContent extends StatelessWidget {
  final Event repliedEvent;

  // Note these are previous and next to the event that is *replying* to
  // `repliedEvent`, not `repliedEvent` itself.
  final Event prevToReplyEvent;
  final Event replyEvent;
  final Event nextToReplyEvent;
  final Color textColor;
  final Color linkColor;
  final Color backgroundColor;

  final double elevation;

  final VoidCallback onTapClose;

  TextReplyEventContent({
    Key key,
    @required repliedEvent,
    this.prevToReplyEvent,
    this.replyEvent,
    this.nextToReplyEvent,
    this.elevation = 0,
    this.backgroundColor,
    this.textColor,
    this.linkColor,
    this.onTapClose,
  })  : repliedEvent = (repliedEvent.messageType == MessageTypes.Text ||
                repliedEvent.messageType == MessageTypes.Image)
            ? repliedEvent
            : Event(
                eventId: repliedEvent.eventId,
                status: repliedEvent.status,
                type: repliedEvent.type,
                room: repliedEvent.room,
                roomId: repliedEvent.roomId,
                senderId: repliedEvent.senderId,
                originServerTs: repliedEvent.time,
                content: {
                    'msgType': 'm.text',
                    'body': repliedEvent.content['body'] ??
                        // Here is no context so this can't be localized.
                        // Normally this should never appear in matrix.
                        'Message',
                  }),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    BorderRadius radius;

    if (replyEvent != null) {
      radius = BorderRadius.circular(4);
    } else {
      radius = BorderRadius.circular(EventWidget.noneRadius);
    }

    return Padding(
      padding: EdgeInsets.only(
        top: 3,
        left: 3,
        right: 3,
      ),
      child: Container(
        width: double.infinity,
        child: Material(
          color: backgroundColor,
          borderRadius: radius,
          elevation: elevation,
          child: CustomPaint(
            foregroundPainter: ReplyBorderPainter(
              radius,
            ),
            child: Stack(
              children: <Widget>[
                Padding(
                  padding: EventWidget.bubblePadding.copyWith(
                    left: 14,
                    right: 14 - ReplyBorderPainter.lineWidth,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        repliedEvent.sender.calcDisplayname(),
                        style: TextStyle(
                          fontSize: 14,
                          color: FamedlyColors.aquaMarine,
                        ),
                      ),
                      SizedBox(height: 4),
                      repliedEvent.messageType == MessageTypes.Image
                          ? ImageEventContent(repliedEvent, isReply: true)
                          : repliedEvent.isRichMessage
                              ? HtmlMessage(
                                  html: repliedEvent.content['formatted_body'],
                                  defaultTextStyle: TextStyle(
                                    color: textColor,
                                    fontSize: 15,
                                  ),
                                  linkStyle: TextStyle(
                                      fontSize: 15,
                                      color: linkColor,
                                      decoration: TextDecoration.underline),
                                  room: repliedEvent.room,
                                  emoteSize: 22,
                                  maxLines: 1,
                                )
                              : Text(
                                  repliedEvent.getLocalizedBody(
                                    MatrixLocals(L10n.of(context)),
                                    hideReply: true,
                                  ),
                                  maxLines: 1,
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(
                                    color: textColor,
                                    fontStyle: FontStyle.italic,
                                  ),
                                ),
                    ],
                  ),
                ),
                if (onTapClose != null)
                  Positioned(
                    top: 4,
                    right: 4,
                    child: FamedlyIconButton(
                      iconData: FamedlyIconsV2.close,
                      size: 18,
                      iconSize: 16,
                      elevation: 0,
                      onTap: onTapClose,
                    ),
                  ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class _Linkify extends StatelessWidget {
  final String text;
  final Color textColor;
  final Color linkColor;
  final bool bigText;

  const _Linkify(
      {Key key,
      this.text,
      this.textColor,
      this.linkColor,
      this.bigText = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final textColor = this.textColor ??
        (Theme.of(context).brightness == Brightness.light
            ? FamedlyColors.slate
            : Colors.white);
    return LinkText(
      onLinkTap: (url) => UrlLauncher(context, url).launchUrl(),
      linkStyle: TextStyle(
          fontSize: bigText ? 45 : 15,
          color: linkColor,
          decoration: TextDecoration.underline),
      text: text,
      textStyle: TextStyle(
        fontSize: bigText ? 45 : 15,
        color: textColor,
      ),
    );
  }
}

class ReplyBorderPainter extends CustomPainter {
  static const lineWidth = 4.0;

  final BorderRadius radius;

  ReplyBorderPainter(this.radius);

  @override
  void paint(Canvas canvas, Size size) {
    canvas.clipRRect(
      RRect.fromRectAndCorners(
        Rect.fromPoints(Offset(0, 0), Offset(size.width, size.height)),
        topLeft: radius.topLeft,
        bottomLeft: radius.bottomLeft,
      ),
    );
    canvas.drawRect(
      Rect.fromPoints(Offset(0, 0), Offset(lineWidth, size.height)),
      Paint()..color = FamedlyColors.aquaMarine,
    );
  }

  @override
  bool shouldRepaint(ReplyBorderPainter oldDelegate) =>
      radius != oldDelegate.radius;
}
