import 'package:intl/intl.dart';

class PatientData {
  String name;
  DateTime birthday;
  Sex sex;
  String caseId;

  PatientData({this.name, this.birthday, this.sex, this.caseId});

  PatientData.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    birthday =
        json['birthday'] == null ? null : DateTime.parse(json['birthday']);
    sex = Sex.values
        .firstWhere((s) => s.toString().split('.').last == json['sex']);
    caseId = json['case_id'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['name'] = name;
    data['birthday'] =
        birthday == null ? null : DateFormat('yyyy-MM-dd').format(birthday);
    data['sex'] = sex.toString().split('.').last;
    data['case_id'] = caseId;
    return data;
  }
}

enum Sex { male, female, unknown, other }
