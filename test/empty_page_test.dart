import 'package:famedly/views/empty_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'utils.dart';

void main() {
  /// All Tests related to the EmptyPage
  /// This test only exists for the coverage value
  group('EmptyPage', () {
    NavigatorObserver mockObserver;

    setUp(() {
      mockObserver = MockNavigatorObserver();
    });

    testWidgets('should have a image', (WidgetTester tester) async {
      await tester.runAsync(() async {
        await tester.pumpWidget(
          await Utils.getWidgetWrapper(
            EmptyPage(),
            mockObserver,
          ),
        );

        await tester.pumpAndSettle();

        expect(find.byKey(Key('famedly_logo')), findsOneWidget);
      });
    });
  });
}
