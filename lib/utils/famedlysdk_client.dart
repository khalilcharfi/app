/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'package:famedlysdk/encryption/utils/key_verification.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/foundation.dart';

import '../models/patients/client_patients_extension.dart';
import '../models/requests/request.dart';
import 'famedlysdk_store.dart';

Client getClient([String name, bool important = false]) {
  _important |= important;
  if (name != null) {
    _clientName = name;
  }
  name ??= getClientName();
  if (isClientCached()) {
    Logs().v('[Matrix] Using cached client');
    return _client;
  }
  Logs().v('[Matrix] Generating new client');
  _client = Client(name,
      databaseBuilder: (Client client) => getDatabase(client, _important),
      databaseDestroyer: destroyDatabase,
      enableE2eeRecovery: true,
      verificationMethods: {
        KeyVerificationMethod.numbers,
        if (!kIsWeb) KeyVerificationMethod.emoji,
      },
      importantStateEvents: {
        'm.room.type',
        EventTypes.RoomCreate, // until soru fixes the SDK
        patientRoomNamespace,
        Request.requestNameSpace,
        Request.broadcastNameSpace,
        Request.requestStateNameSpace,
        Request.usersNameSpace,
        EventTypes.RoomPowerLevels,
      })
    ..init();
  _loginState = null;
  _client.onLoginStateChanged.stream.listen((state) {
    _loginState = state;
  });
  return _client;
}

bool isClientCached() {
  return _client != null;
}

String getClientName() {
  return _clientName ?? 'Famedly';
}

LoginState getCurrentLoginState() {
  return _loginState;
}

bool isClientImportant() {
  return _important;
}

LoginState _loginState;
String _clientName;
Client _client;
bool _important = false;
