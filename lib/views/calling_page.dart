/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'dart:async';

import 'package:assets_audio_player/assets_audio_player.dart';
import 'package:famedly/styles/famedly_icons_v2_icons.dart';
import 'package:famedly/utils/platform_extension.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_webrtc/flutter_webrtc.dart';
import 'package:wakelock/wakelock.dart';
import 'package:pedantic/pedantic.dart';
import 'package:adaptive_page_layout/adaptive_page_layout.dart';
import 'package:universal_html/html.dart' as darthtml;

import '../models/famedly.dart';
import '../models/plugins/voip_plugin.dart';

class Calling extends StatefulWidget {
  final BuildContext context;
  VoipPlugin get voip => Famedly.of(context).voipPlugin;
  Client get client => Famedly.of(context).client;
  Calling(this.context, {Key key}) : super(key: key);
  @override
  _MyCallingPage createState() => _MyCallingPage();
}

class _MyCallingPage extends State<Calling> {
  VoipPlugin get voip => widget.voip;
  Room get room => widget.client.getRoomById(session?.roomId);
  String get displayName => room?.displayname ?? '';
  String get callId => voip.currentCID;
  CallSession get session => voip.sessions[callId];
  MediaStream get localStream => session?.localStream;
  MediaStream get remoteStream => session?.remoteStream;
  bool get speakerOn => session?.speakerOn ?? false;
  bool get micMuted => session?.micMuted ?? false;
  bool get camMuted => session?.camMuted ?? false;
  bool get voiceonly => session?.voiceonly ?? false;

  final RTCVideoRenderer _localRenderer = RTCVideoRenderer();
  final RTCVideoRenderer _remoteRenderer = RTCVideoRenderer();
  double _localVideoHeight;
  double _localVideoWidth;
  EdgeInsetsGeometry _localVideoMargin;
  CallState _state;

  void _playCallSound() async {
    final path = 'assets/sounds/call.wav';
    if (kIsWeb) {
      darthtml.AudioElement()
        ..src = 'assets/$path'
        ..autoplay = true
        ..load();
    } else if (kIsMobile) {
      await AssetsAudioPlayer.newPlayer().open(Audio(path));
    } else {
      Logs().w('Playing sound not implemented for this platform!');
    }
  }

  @override
  void initState() {
    super.initState();
    initialize();
    _playCallSound();
  }

  void initialize() async {
    await _localRenderer.initialize();
    await _remoteRenderer.initialize();

    if (session == null) return;

    session.onCallStateChanged.listen(_handleCallState);
    session.onLocalStream = _handleLocalStream;
    session.onRemoveStream = _handleRemoteStream;
    _state = session.state;

    if (session.type == CallType.kVideo) {
      try {
        // Enable wakelock (keep screen on)
        unawaited(Wakelock.enable());
      } catch (_) {}
    }
    if (session.localStream != null) {
      _handleLocalStream(session.localStream);
    }
    if (session.remoteStream != null) {
      _handleRemoteStream(session.remoteStream);
    }
  }

  void cleanUp() {
    Timer(Duration(seconds: 2), () {
      AdaptivePageLayout.of(context).pop();
    });
    if (session != null && session.type == CallType.kVideo) {
      try {
        unawaited(Wakelock.disable());
      } catch (_) {}

      _localRenderer.srcObject = null;
      _remoteRenderer.srcObject = null;
      _localRenderer.dispose();
      _remoteRenderer.dispose();
    }
  }

  @override
  void deactivate() {
    super.deactivate();
  }

  void _resizeLocalVideo() {
    _localVideoMargin = remoteStream != null
        ? EdgeInsets.only(top: 20.0, left: 20.0)
        : EdgeInsets.zero;
    _localVideoWidth = remoteStream != null
        ? MediaQuery.of(context).size.width / 4
        : MediaQuery.of(context).size.width;
    _localVideoHeight = remoteStream != null
        ? MediaQuery.of(context).size.height / 4
        : MediaQuery.of(context).size.height;
  }

  void _handleCallState(CallState state) {
    Logs().v('CallingPage::handleCallState: ${state.toString()}');
    if (mounted) {
      setState(() {
        _state = state;
        if (_state == CallState.kHangup) cleanUp();
      });
    }
  }

  void _handleLocalStream(MediaStream stream) {
    if (mounted) {
      setState(() {
        _localRenderer.srcObject = stream;
        _resizeLocalVideo();
      });
    }
  }

  void _handleRemoteStream(MediaStream stream) {
    if (mounted) {
      setState(() {
        _remoteRenderer.srcObject = stream;
        _resizeLocalVideo();
      });
    }
  }

  void _answerCall() {
    setState(() {
      session?.answer();
    });
  }

  void _hangUp() {
    _playCallSound();
    setState(() {
      session?.hangup();
    });
  }

  void _muteMic() {
    setState(() {
      session?.muteMic();
    });
  }

  void _muteCamera() {
    setState(() {
      session?.muteCamera();
    });
  }

  void _switchCamera() => session?.switchCamera();

  /*
  void _switchSpeaker() {
    setState(() {
      session.setSpeakerOn();
    });
  }
  */
  List<Widget> _buildActionButtons() {
    final switchCameraButton = FloatingActionButton(
      heroTag: 'switchCamera',
      onPressed: _switchCamera,
      foregroundColor: Theme.of(context).textTheme.bodyText1.color,
      backgroundColor: Theme.of(context).backgroundColor,
      child: const Icon(Icons.switch_camera),
    );
    /*
    var switchSpeakerButton = FloatingActionButton(
      heroTag: 'switchSpeaker',
      child: Icon(_speakerOn ? Icons.volume_up : Icons.volume_off),
      onPressed: _switchSpeaker,
      foregroundColor: Colors.black54,
      backgroundColor: Theme.of(context).backgroundColor,
    );
    */
    final hangupButton = FloatingActionButton(
      heroTag: 'hangup',
      onPressed: _hangUp,
      tooltip: 'Hangup',
      foregroundColor: Theme.of(context).textTheme.bodyText1.color,
      backgroundColor: _state == CallState.kHangup
          ? Theme.of(context).backgroundColor
          : Colors.red,
      child: Icon(Icons.call_end),
    );

    final answerButton = FloatingActionButton(
      heroTag: 'answer',
      onPressed: _answerCall,
      tooltip: 'Answer',
      backgroundColor: Colors.green,
      child: Icon(FamedlyIconsV2.phone),
    );

    final muteMicButton = FloatingActionButton(
      heroTag: 'muteMic',
      onPressed: _muteMic,
      foregroundColor:
          micMuted ? Colors.red : Theme.of(context).textTheme.bodyText1.color,
      backgroundColor: Theme.of(context).backgroundColor,
      child: Icon(micMuted ? Icons.mic_off : Icons.mic),
    );

    final muteCameraButton = FloatingActionButton(
      heroTag: 'muteCam',
      onPressed: _muteCamera,
      foregroundColor:
          camMuted ? Colors.red : Theme.of(context).textTheme.bodyText1.color,
      backgroundColor: Theme.of(context).backgroundColor,
      child: Icon(camMuted ? Icons.videocam_off : Icons.videocam),
    );

    switch (_state) {
      case CallState.kConnecting:
        if (session.isOutgoing) {
          return <Widget>[
            hangupButton,
          ];
        } else {
          return <Widget>[
            answerButton,
            hangupButton,
          ];
        }
        break;
      case CallState.kConnected:
      case CallState.kMuted:
      case CallState.kHeld:
        return <Widget>[
          muteMicButton,
          //switchSpeakerButton,
          switchCameraButton,
          muteCameraButton,
          hangupButton,
        ];
        break;
      case CallState.kHangup:
        return <Widget>[
          hangupButton,
        ];
        break;
    }

    return <Widget>[];
  }

  List<Widget> _buildContent(Orientation orientation) {
    final stackWidgets = <Widget>[];
    if (!voiceonly && remoteStream != null) {
      stackWidgets.add(Center(
        child: RTCVideoView(_remoteRenderer,
            objectFit: RTCVideoViewObjectFit.RTCVideoViewObjectFitCover),
      ));
    }
    if (!voiceonly && localStream != null) {
      stackWidgets.add(Container(
        alignment: Alignment.topLeft,
        child: AnimatedContainer(
          width: (orientation == Orientation.portrait)
              ? _localVideoWidth
              : _localVideoHeight,
          height: (orientation == Orientation.portrait)
              ? _localVideoHeight
              : _localVideoWidth,
          alignment: Alignment.topLeft,
          duration: Duration(milliseconds: 300),
          margin: _localVideoMargin,
          child: RTCVideoView(_localRenderer,
              mirror: true,
              objectFit: RTCVideoViewObjectFit.RTCVideoViewObjectFitCover),
        ),
      ));
    }
    return stackWidgets;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('$displayName: ${callStateToString(_state)}'),
          actions: <Widget>[],
          leading: Container(),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
        floatingActionButton: SizedBox(
            width: 320.0,
            height: 150.0,
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: _buildActionButtons())),
        body: OrientationBuilder(
            builder: (BuildContext context, Orientation orientation) {
          return Container(child: Stack(children: _buildContent(orientation)));
        }));
  }
}
