/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'dart:async';

import 'package:adaptive_page_layout/adaptive_page_layout.dart';
import 'package:famedly/components/draggable_scrollbar.dart';
import 'package:famedly/components/user_directory_login_builder.dart';
import 'package:famedly/styles/colors.dart';
import 'package:famedly_user_directory_client_sdk/famedly_user_directory_client_sdk.dart'
    as user_directory;
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';

import '../models/famedly.dart';
import '../models/roles/client_extension.dart';
import 'users_list/item.dart';

class UserList extends StatefulWidget {
  final Widget header;
  final void Function(Profile user, {String organisationName}) onTap;
  final String roomId;
  final TextEditingController searchController;

  const UserList({
    Key key,
    this.header,
    this.searchController,
    this.onTap,
    this.roomId,
  }) : super(key: key);

  @override
  _UserListState createState() => _UserListState();
}

class _UserListState extends State<UserList> {
  Timer _searchControllerCooldown;
  Future<List<user_directory.User>> _searchUsersFuture;

  String ownOrganisationName;
  bool hideUsersFromOtherServers = false;

  void _onTapUser(BuildContext context, Profile contact,
      {String organisationName}) async {
    if (widget.onTap != null) {
      return widget.onTap(contact, organisationName: organisationName);
    }
    if (organisationName != null) {
      await AdaptivePageLayout.of(context).pushNamed(
        '/organisation/$organisationName/contacts/${contact.userId}',
        arguments: contact,
      );
      return;
    }
    await AdaptivePageLayout.of(context).pushNamed(
      '/contacts/${contact.userId}',
      arguments: contact,
    );
  }

  void _searchControllerListener() {
    _searchControllerCooldown?.cancel();
    _searchControllerCooldown = Timer(
        Duration(milliseconds: 500),
        () => mounted
            ? setState(() {
                _searchControllerCooldown = null;
                _searchUsersFuture = null;
              })
            : null);
  }

  Future<List<user_directory.User>> _searchUsers(BuildContext context) async {
    final userDirectoryClient =
        await Famedly.of(context).getUserDirectoryClient();
    ownOrganisationName ??= (await Famedly.of(context)
            .client
            .getOwnOrganisation(
                Famedly.of(context).getOrganizationDirectoryClient())
            .catchError((e, s) {
      Logs().w('Unable to fetch own organisation name', e, s);
    }))
        ?.name;
    if (widget.searchController.text.isEmpty) {
      return userDirectoryClient.requestAllUsers();
    }
    return userDirectoryClient.searchUsers(widget.searchController.text);
  }

  @override
  void initState() {
    super.initState();
    widget.searchController?.addListener(_searchControllerListener);
  }

  @override
  void dispose() {
    widget.searchController?.removeListener(_searchControllerListener);
    super.dispose();
  }

  Widget _listBuilder(
      BuildContext context, List<user_directory.User> userDirectoryUsers) {
    return FutureBuilder<List<User>>(
      future: Famedly.of(context).contactDiscoveryPlugin.loadContacts(
            roomId: widget.roomId,
            query: widget.searchController.text,
          ),
      builder: (BuildContext context, AsyncSnapshot<List<User>> snapshot) {
        if (snapshot.data == null &&
            snapshot.connectionState != ConnectionState.done) {
          return Center(child: CircularProgressIndicator());
        }

        final contactList = (snapshot.data ?? <User>[])
            .map((u) => Profile(
                u.calcDisplayname(mxidLocalPartFallback: false), u.avatarUrl)
              ..userId = u.id)
            .toList();
        contactList.sort(
          (a, b) => a.displayname
              .toUpperCase()
              .compareTo(b.displayname.toUpperCase()),
        );
        final _scrollController = ScrollController();
        final listViewLength =
            contactList.length + userDirectoryUsers.length + 1;
        return FamedlyDraggableScrollbar(
          isAlwaysShown: kIsWeb,
          controller: _scrollController,
          child: ListView.separated(
            separatorBuilder: (_, i) => i == 0
                ? _ListHeader(title: L10n.of(context).contacts)
                : i == contactList.length
                    ? _ListHeader(title: L10n.of(context).otherUsers)
                    : Container(),
            controller: _scrollController,
            key: Key('UsersListView'),
            itemCount: listViewLength,
            itemBuilder: (context, index) {
              if (index == 0) {
                return Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    widget.header ?? Container(),
                    Divider(height: 1, color: FamedlyColors.blueyGrey),
                    Material(
                      color: Theme.of(context).secondaryHeaderColor,
                      child: SwitchListTile(
                        title: Text(L10n.of(context).displayOnlyFrom(
                            ownOrganisationName ??
                                Famedly.of(context).client.userID.domain)),
                        value: hideUsersFromOtherServers,
                        onChanged: (b) =>
                            setState(() => hideUsersFromOtherServers = b),
                      ),
                    ),
                  ],
                );
              }
              index--;

              if (index < contactList.length) {
                final item = contactList[index];
                if (item.displayname
                        .contains(widget.searchController?.text ?? '') &&
                    (!hideUsersFromOtherServers ||
                        item.userId.domain ==
                            Famedly.of(context).client.userID.domain)) {
                  return UserItem(
                    contact: item,
                    onTap: () => _onTapUser(context, item),
                  );
                }
                return Container();
              }
              index -= contactList.length;
              final item = userDirectoryUsers[index];
              if (hideUsersFromOtherServers &&
                  item.organisation != ownOrganisationName) {
                return Container();
              }
              final profile =
                  Profile(item.displayname, Uri.parse(item.avatarUrl ?? ''))
                    ..userId = item.id;
              return UserItem(
                contact: profile,
                subtitle: item.organisation,
                onTap: () => _onTapUser(context, profile,
                    organisationName: item.organisation),
              );
            },
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return UserDirectoryLoginBuilder(
      builder: (context) {
        _searchUsersFuture ??= _searchUsers(context);
        return FutureBuilder<List<user_directory.User>>(
          future: _searchUsersFuture,
          builder: (context, snapshot) {
            if (snapshot.data == null &&
                snapshot.connectionState != ConnectionState.done) {
              return Center(child: CircularProgressIndicator());
            }
            final userDirectoryUsers = snapshot.data ?? <user_directory.User>[];
            return _listBuilder(context, userDirectoryUsers);
          },
        );
      },
      errorBuilder: (context) => _listBuilder(context, []),
    );
  }
}

class _ListHeader extends StatelessWidget {
  final String title;

  const _ListHeader({Key key, @required this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Divider(height: 1, color: FamedlyColors.blueyGrey),
        Container(
          width: double.infinity,
          color: Theme.of(context).secondaryHeaderColor,
          padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
          child: Text(title),
        ),
        Divider(height: 1, color: FamedlyColors.blueyGrey),
      ],
    );
  }
}
