/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:famedly/components/settings_list_group.dart';
import 'package:famedly/styles/famedly_icons_v2_icons.dart';
import 'package:flutter/material.dart';
import 'package:future_loading_dialog/future_loading_dialog.dart';

import '../models/famedly.dart';
import '../components/profile_scaffold.dart';
import '../components/users_list/profile_item.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import '../models/roles/client_extension.dart';
import '../models/roles/role_availability.dart';
import '../models/roles/role_entity.dart';
import '../styles/colors.dart';

class RolePage extends StatefulWidget {
  final int id;
  final RoleEntity role;

  const RolePage(this.id, {Key key, this.role}) : super(key: key);

  @override
  RolePageState createState() => RolePageState(id, role);
}

class RolePageState extends State<RolePage> {
  final int id;
  RoleEntity role;

  RolePageState(this.id, this.role);

  Future<RoleEntity> loadedFuture;
  Future<RoleAvailability> loadedAvailabilityFuture;

  @override
  Widget build(BuildContext context) {
    final famedly = Famedly.of(context);
    if (loadedFuture == null) {
      if (role != null && role.id == id) {
        loadedFuture = Future<RoleEntity>.value(role);
      } else {
        loadedFuture = famedly.client.getRole(id);
      }
    }
    loadedAvailabilityFuture ??= famedly.client.getRoleAvailability(id);
    return FutureBuilder<RoleEntity>(
        future: loadedFuture,
        builder: (BuildContext context, AsyncSnapshot<RoleEntity> snapshot) {
          if (snapshot.hasError) {
            return Center(child: Text(snapshot.error.toString()));
          }
          if (!snapshot.hasData) {
            return Container(
              color: Theme.of(context).backgroundColor,
              child: Center(
                child: CircularProgressIndicator(),
              ),
            );
          }
          final role = snapshot.data;
          return FutureBuilder<RoleAvailability>(
            future: loadedAvailabilityFuture,
            builder: (BuildContext context,
                AsyncSnapshot<RoleAvailability> snapshot) {
              if (snapshot.hasError) {
                return Center(child: Text(snapshot.error.toString()));
              }
              if (!snapshot.hasData) {
                return Container(
                  color: Theme.of(context).backgroundColor,
                  child: Center(
                    child: CircularProgressIndicator(),
                  ),
                );
              }
              final availability = snapshot.data;
              // checks if we are in the role
              final isOwn = role.users.contains(famedly.client.userID);
              role.userAvailable =
                  availability.users.contains(famedly.client.userID);
              // okay, we are fully loaded and have both "role" and "availability" available for our pleasure to use
              final peopleInShift =
                  availability.available && availability.users.isNotEmpty
                      ? ListView.separated(
                          shrinkWrap: true,
                          physics: const NeverScrollableScrollPhysics(),
                          itemCount: availability.users.length,
                          separatorBuilder: (context, i) => Divider(
                            indent: 70,
                            height: 1,
                            thickness: 1,
                            color:
                                Theme.of(context).brightness == Brightness.light
                                    ? FamedlyColors.paleGrey
                                    : FamedlyColors.slate,
                          ),
                          itemBuilder: (context, index) => ProfileItem(
                            mxid: availability.users[index],
                            owner: availability.users[index] == role.owner,
                          ),
                        )
                      : ListTile(title: Text(L10n.of(context).nobody));
              final allPeople = ListView.separated(
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                itemCount: role.users.length,
                separatorBuilder: (context, i) => Divider(
                  indent: 70,
                  height: 1,
                  thickness: 1,
                  color: Theme.of(context).brightness == Brightness.light
                      ? FamedlyColors.paleGrey
                      : FamedlyColors.slate,
                ),
                itemBuilder: (context, index) => ProfileItem(
                  mxid: role.users[index],
                  owner: role.users[index] == role.owner,
                ),
              );
              return ProfileScaffold(
                title: Text(role.name),
                profileName: role.name,
                profileContent: role.avatarUrl,
                buttons: role.privateInvite && availability.available
                    ? [
                        ProfileScaffoldButton(
                          iconData: FamedlyIconsV2.chats,
                          onTap: () => role.startRequest(context),
                        ),
                        ProfileScaffoldButton(
                          iconData: FamedlyIconsV2.phone,
                          onTap: () => showOkAlertDialog(
                            context: context,
                            title: L10n.of(context)
                                .thisFeatureHasNotBeenImplementedYet,
                            okLabel: L10n.of(context).next,
                            useRootNavigator: false,
                          ),
                        ),
                      ]
                    : [],
                body: Column(
                  children: <Widget>[
                    if (availability.available)
                      SettingsListGroup(
                        title: L10n.of(context).inOffice,
                        children: [peopleInShift],
                      ),
                    SettingsListGroup(
                      title: L10n.of(context).allMembers,
                      children: [allPeople],
                    ),
                    if (isOwn && availability.available)
                      Padding(
                        padding: EdgeInsets.only(top: 20, bottom: 20),
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            primary: FamedlyColors.grapefruit,
                          ),
                          onPressed: () async {
                            if (role.userAvailable) {
                              // opt out
                              if (OkCancelResult.ok ==
                                  await showOkCancelAlertDialog(
                                    context: context,
                                    title: L10n.of(context).optOutNotice,
                                    okLabel: L10n.of(context).yes,
                                    cancelLabel: L10n.of(context).cancel,
                                    useRootNavigator: false,
                                  )) {
                                await showFutureLoadingDialog(
                                  context: context,
                                  future: () => role.userOptOut(),
                                );
                                setState(() {
                                  role.userAvailable = false;
                                  availability.users
                                      .remove(famedly.client.userID);
                                });
                              }
                            } else {
                              // opt in
                              if (OkCancelResult.ok ==
                                  await showOkCancelAlertDialog(
                                    context: context,
                                    title: L10n.of(context).optInNotice,
                                    okLabel: L10n.of(context).yes,
                                    cancelLabel: L10n.of(context).cancel,
                                    useRootNavigator: false,
                                  )) {
                                await showFutureLoadingDialog(
                                  context: context,
                                  future: () => role.userOptOut(out: false),
                                );
                                setState(() {
                                  role.userAvailable = true;
                                  availability.users.add(famedly.client.userID);
                                });
                              }
                            }
                          },
                          child: Text(
                            role.userAvailable
                                ? L10n.of(context).optOutButton
                                : L10n.of(context).optInButton,
                          ),
                        ),
                      ),
                  ],
                ),
              );
            },
          );
        });
  }
}
