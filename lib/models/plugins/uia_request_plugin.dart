import 'dart:async';

import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:famedly/models/plugins/famedly_plugin.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:url_launcher/url_launcher.dart';

class UiaRequestPlugin extends FamedlyPlugin {
  StreamSubscription<UiaRequest> onUiaRequest;

  UiaRequestPlugin(matrix) : super(matrix) {
    onUiaRequest ??= client.onUiaRequest.stream.listen(_onUiaRequest);
  }

  String _lastStage;

  void _onUiaRequest(UiaRequest uiaRequest) async {
    if (uiaRequest.state == UiaRequestState.done) {
      _lastStage = null;
      return null;
    }
    if (uiaRequest.state != UiaRequestState.waitForUser ||
        uiaRequest.nextStages.isEmpty) return;
    final stage = uiaRequest.nextStages.first;
    switch (stage) {
      case AuthenticationTypes.password:
        final password = famedly.loginPlugin.cachedPassword ??
            (await showTextInputDialog(
                    title: L10n.of(famedly.context).pleaseEnterYourPassword,
                    message: stage == _lastStage
                        ? L10n.of(famedly.context).wrongPasswordPleaseTryAgain
                        : null,
                    context: famedly.context,
                    okLabel: L10n.of(famedly.context).next,
                    cancelLabel: L10n.of(famedly.context).cancel,
                    useRootNavigator: false,
                    textFields: [
                  DialogTextField(
                    minLines: 1,
                    maxLines: 1,
                    obscureText: true,
                    hintText: '******',
                  )
                ]))
                ?.single;
        if (password?.isEmpty ?? true) {
          uiaRequest.cancel();
          return;
        }
        Logs().v('[UIA Request] Complete stage $stage');
        _lastStage = stage;
        return uiaRequest.completeStage(
          AuthenticationPassword(
            session: uiaRequest.session,
            user: client.userID,
            password: password,
            identifier: AuthenticationUserIdentifier(user: client.userID),
          ),
        );
      case 'com.famedly.login.token':
        await showOkAlertDialog(
          context: famedly.context,
          title: L10n.of(famedly.context).oopsSomethingWentWrong,
          message: L10n.of(famedly.context).uiaConfigFailed,
          okLabel: L10n.of(famedly.context).close,
          useRootNavigator: false,
        );
        uiaRequest.cancel(Exception('Can not handle stage $stage'));
        _lastStage = null;
        return;
      default:
        await launch(
          famedly.client.homeserver.toString() +
              '/_matrix/client/r0/auth/$stage/fallback/web?session=${uiaRequest.session}',
        );
        if (OkCancelResult.ok ==
            await showOkCancelAlertDialog(
              message: L10n.of(famedly.context).pleaseFollowInstructionsOnWeb,
              context: famedly.context,
              okLabel: L10n.of(famedly.context).continueText,
              cancelLabel: L10n.of(famedly.context).cancel,
              useRootNavigator: false,
            )) {
          _lastStage = stage;
          return uiaRequest.completeStage(
            AuthenticationData(session: uiaRequest.session),
          );
        } else {
          uiaRequest.cancel();
          return;
        }
    }
  }
}
