const String _matrixUrlSchemeMask = 'https://matrix.to/#/';

class BroadcastContactsContent {
  final String requestedOrganisation;
  final String organisationId;
  final String organisationName;
  final String contactId;
  final String contactDescription;
  final String contactUri;

  String _matrixId;
  String get matrixId {
    if (_matrixId == null && contactUri.startsWith(_matrixUrlSchemeMask)) {
      _matrixId = contactUri.replaceFirst(_matrixUrlSchemeMask, '');
    }
    return _matrixId;
  }

  BroadcastContactsContent(
      {this.requestedOrganisation,
      this.organisationId,
      this.organisationName,
      this.contactId,
      this.contactDescription,
      this.contactUri});

  static List<BroadcastContactsContent> listFromJson(List<dynamic> jsonList) {
    final contacts = <BroadcastContactsContent>[];
    jsonList.forEach((v) {
      contacts.add(BroadcastContactsContent.fromJson(v));
    });
    return contacts;
  }

  BroadcastContactsContent.fromJson(Map<String, dynamic> json)
      : requestedOrganisation = json['requested_organisation'],
        organisationId = json['organisation_id'],
        organisationName = json['organisation_name'],
        contactId = json['contact_id'],
        contactDescription = json['contact_description'],
        contactUri = json['contact_uri'];

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['requested_organisation'] = requestedOrganisation;
    data['organisation_id'] = organisationId;
    data['organisation_name'] = organisationName;
    data['contact_id'] = contactId;
    data['contact_description'] = contactDescription;
    data['contact_uri'] = contactUri;
    return data;
  }
}

class BroadcastContent {
  final String title;
  final List<BroadcastContactsContent> contacts;

  BroadcastContent({this.title, this.contacts});

  BroadcastContent.fromJson(Map<String, dynamic> json)
      : title = json['title'],
        contacts = BroadcastContactsContent.listFromJson(json['contacts']);

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['title'] = title;
    data['contacts'] = [];
    for (final contact in contacts) {
      data['contacts'].add(contact.toJson());
    }
    return data;
  }
}
