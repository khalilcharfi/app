/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'package:famedly/components/nav_scaffold.dart';
import 'package:famedly/components/request_list.dart';
import 'package:famedly/models/requests/filters.dart';
import 'package:famedly/models/requests/request.dart';
import 'package:famedly/models/roles/role_entity.dart';
import 'package:famedly/views/requests/requests_list_page_role.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';

class RequestsListPageRoleView extends StatelessWidget {
  final RequestsListPageRoleController controller;

  const RequestsListPageRoleView(this.controller, {Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<RoleEntity>(
      future: controller.getLoadedFuture(),
      builder: (BuildContext context, AsyncSnapshot<RoleEntity> snapshot) {
        if (snapshot.hasError) {
          return Center(child: Text(snapshot.error.toString()));
        }
        if (!snapshot.hasData) {
          return Container(
            color: Theme.of(context).backgroundColor,
            child: Center(
              child: CircularProgressIndicator(),
            ),
          );
        }
        final role = snapshot.data;
        return NavScaffold(
          key: Key('RequestsPage'),
          activePage: MainPage.requests,
          appBar: AppBar(
            title: Text(role.name),
          ),
          filterTextController: controller.filterTextController,
          body: FutureBuilder<bool>(
            future: controller.waitForFirstSync(),
            builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
              if (snapshot.hasData) {
                return RequestsList(
                  controller.filterTextController,
                  activeChatId: controller.widget.activeChatId,
                  emptyListPlaceholder: Center(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        L10n.of(context).noMoreRequests,
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 20),
                      ),
                    ),
                  ),
                  filter: RequestFilters.isRequestRoomFilter,
                  requestBundleFilter: (RequestBundle bundle) =>
                      controller.requestBundleFilter(bundle, role),
                  type: 'requests/role/${role.id}',
                );
              } else {
                return Container(
                  color: Theme.of(context).backgroundColor,
                  child: Center(
                    child: CircularProgressIndicator(),
                  ),
                );
              }
            },
          ),
        );
      },
    );
  }
}
