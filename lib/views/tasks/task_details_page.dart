/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'package:adaptive_page_layout/adaptive_page_layout.dart';
import 'package:famedly/models/famedly.dart';
import 'package:famedly/models/tasks/task.dart';
import 'package:famedly/models/tasks/tasklist.dart';
import 'package:famedly/views/tasks/task_details_page_view.dart';
import 'package:flutter/material.dart';
import 'package:future_loading_dialog/future_loading_dialog.dart';

class TaskDetailsPage extends StatefulWidget {
  final String id;

  const TaskDetailsPage({Key key, this.id}) : super(key: key);
  @override
  TaskDetailsPageController createState() => TaskDetailsPageController();
}

class TaskDetailsPageController extends State<TaskDetailsPage> {
  TaskList taskList;
  Task task;
  final TextEditingController descController = TextEditingController();
  final TextEditingController subTaskController = TextEditingController();

  @override
  void setState(fn) {
    if (!mounted) return;
    super.setState(fn);
  }

  @override
  void initState() {
    if (taskList == null) {
      taskList = TaskList(Famedly.of(context).client);
      taskList.onUpdate = () {
        setState(() {
          if (int.parse(widget.id) < taskList.tasks.length) {
            // When a item is deleted flutter rebuilds due to the onUpdate setState
            // error on that page before adaptivelayout.pop pops the page.
            task = taskList.tasks[int.parse(widget.id)];
          }
        });
      };
      task = taskList.tasks[int.parse(widget.id)];
    }
    super.initState();
  }

  @override
  void dispose() {
    if (descController.text.isNotEmpty &&
        descController.text != task.description) {
      task.setDescription(descController.text);
    }
    if (subTaskController.text.isNotEmpty) {
      task.addSubtask(subTaskController.text);
    }
    super.dispose();
  }

  void deleteTask() async {
    final success = await showFutureLoadingDialog(
        context: context, future: () => task.remove());
    if (success.error == null) AdaptivePageLayout.of(context).pop();
  }

  void editingComplete() {
    showFutureLoadingDialog(
      context: context,
      future: () => task.setDescription(descController.text),
    );
  }

  void deleteDate() {
    showFutureLoadingDialog(
      context: context,
      future: () => (task.setDate(null)),
    );
  }

  void addSubtask(String text) {
    showFutureLoadingDialog(
        context: context, future: () => task.addSubtask(text));
    subTaskController.clear();
  }

  void comepleteSubtask(int index, bool check) {
    showFutureLoadingDialog(
      context: context,
      future: () => task.subtask[index].setDone(check),
    );
  }

  void changeDate() async {
    final date = await showDatePicker(
        context: context,
        firstDate: DateTime.now(),
        lastDate: DateTime.fromMillisecondsSinceEpoch(
            DateTime.now().millisecondsSinceEpoch +
                (1000 * 60 * 60 * 24 * 365 * 10)),
        initialDate: DateTime.now());
    if (date == null) return;

    final timeOfDay = await showTimePicker(
      context: context,
      initialTime: TimeOfDay.now(),
    );
    if (timeOfDay == null) return;

    await showFutureLoadingDialog(
      context: context,
      future: () => task.setDate(
        DateTime(date.year, date.month, date.day, timeOfDay.hour,
            timeOfDay.minute, 0, 0, 0),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return TaskDetailsPageView(this);
  }
}
