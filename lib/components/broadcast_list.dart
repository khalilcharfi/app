/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import '../models/requests/broadcast_content.dart';
import '../models/requests/request.dart';
import '../models/requests/request_content.dart';
import '../models/room_type_extension.dart';
import '../models/famedly.dart';
import 'rooms_list/request_item.dart';

typedef SetTitle = void Function(String t);

class BroadcastList extends StatefulWidget {
  final SetTitle setTitle;
  final TextEditingController filterTextGetter;
  final String requestId;
  final String activeChatId;

  /// Widget shown when there are no
  /// chats to list.
  final Widget emptyListPlaceholder;

  BroadcastList(
    this.filterTextGetter,
    this.requestId, {
    this.setTitle,
    this.emptyListPlaceholder,
    this.activeChatId,
    Key key,
  }) : super(key: key);

  @override
  BroadcastListState createState() => BroadcastListState();
}

class BroadcastListState extends State<BroadcastList> {
  List<Room> get roomList => famedly.client.rooms;
  List<Room> rooms;
  Room broadcastRoom;
  BroadcastContent broadcastContent;

  Famedly famedly;
  String filterText;

  void handleFilter() {
    if (widget.filterTextGetter == null) return;
    setState(() {
      filterText = widget.filterTextGetter.text;
    });
  }

  void rebuildView() {
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    if (widget.filterTextGetter != null) {
      widget.filterTextGetter.addListener(handleFilter);
    }
  }

  @override
  void dispose() {
    if (widget.filterTextGetter != null) {
      widget.filterTextGetter.removeListener(handleFilter);
    }
    super.dispose();
  }

  List<BroadcastContactsContent> filterContacts() {
    return broadcastContent?.contacts?.where((contact) {
      if (filterText == null || filterText == '') return true;
      return contact.organisationName
          .toLowerCase()
          .contains(filterText.toLowerCase());
    })?.toList();
  }

  List<Room> filterRooms() {
    return rooms?.where((room) {
      if (filterText == null || filterText == '') return true;
      return Request(room)
          .organisation
          .toLowerCase()
          .contains(filterText.toLowerCase());
    })?.toList();
  }

  Request _getRequestFromContact(BroadcastContactsContent contact) {
    for (final room in rooms) {
      final r = Request(room);
      if (r.content.contactId == contact.contactId) {
        return r;
      }
    }
    return Request(broadcastRoom, contact: contact);
  }

  int _getRsortNumber(Request req) {
    if (req.remoteAccepted()) return 3;
    if (req.remoteNeedInfo()) return 2;
    if (req.remoteRejected()) return 1;
    return 0;
  }

  int _compareRequests(Request req1, Request req2) {
    final r1sort = _getRsortNumber(req1);
    final r2sort = _getRsortNumber(req2);
    if (r1sort != r2sort) return r2sort - r1sort;
    if (req1.room.timeCreated != req2.room.timeCreated) {
      return req1.room.timeCreated.millisecondsSinceEpoch <
              req2.room.timeCreated.millisecondsSinceEpoch
          ? 1
          : -1;
    }
    return 0;
  }

  @override
  Widget build(BuildContext context) {
    famedly = Famedly.of(context);

    return StreamBuilder<Object>(
        stream: famedly.client.onSync.stream,
        builder: (context, snapshot) {
          rooms = [];
          broadcastRoom = null;
          for (final room in roomList) {
            if (room.type != roomTypeRequest) continue;
            if (room.states[Request.requestNameSpace] == null) continue;
            final content = RequestContent.fromJson(
                room.getState(Request.requestNameSpace).content);
            if (content.requestId != widget.requestId) continue;
            if (room.states[Request.broadcastNameSpace] != null) {
              // we have a broadcast room
              broadcastRoom = room;
              broadcastContent = BroadcastContent.fromJson(
                  room.getState(Request.broadcastNameSpace).content);
              widget.setTitle(broadcastContent.title);
            } else {
              // we have a normal room
              rooms.add(room);
              widget.setTitle(content.requestTitle);
            }
          }
          if (broadcastRoom != null) {
            final contacts = filterContacts();

            if (contacts == null || contacts.isEmpty) {
              return widget.emptyListPlaceholder ?? Container();
            }
            contacts.sort((BroadcastContactsContent c1,
                    BroadcastContactsContent c2) =>
                _compareRequests(
                    _getRequestFromContact(c1), _getRequestFromContact(c2)));

            final _scrollController = ScrollController();

            return Scrollbar(
              isAlwaysShown: kIsWeb,
              controller: _scrollController,
              child: ListView.builder(
                controller: _scrollController,
                shrinkWrap: true,
                itemCount: contacts.length,
                itemBuilder: (context, index) {
                  if (index >= contacts.length) return Container();
                  final contact = contacts[index];
                  for (final room in rooms) {
                    final r = Request(room);
                    if (r.content.contactId == contact.contactId) {
                      return RequestItem(
                        key: Key('request_$index'),
                        room: room,
                        colorBar: true,
                        type: 'broadcast/${widget.requestId}',
                        activeChat: widget.activeChatId == room.id,
                      );
                    }
                  }
                  return RequestItem(
                    key: Key('request_$index'),
                    room: broadcastRoom,
                    contacts: [contact],
                    colorBar: true,
                    type: 'broadcast/${widget.requestId}',
                  );
                },
              ),
            );
          } else {
            // fallback, no broadcast room
            final roomsFiltered = filterRooms();

            if (roomsFiltered == null || roomsFiltered.isEmpty) {
              return widget.emptyListPlaceholder ?? Container();
            }
            roomsFiltered.sort((Room r1, Room r2) =>
                _compareRequests(Request(r1), Request(r2)));
            final _scrollController = ScrollController();
            return Scrollbar(
              isAlwaysShown: kIsWeb,
              controller: _scrollController,
              child: ListView.builder(
                controller: _scrollController,
                shrinkWrap: true,
                itemCount: roomsFiltered.length,
                itemBuilder: (context, index) {
                  if (index >= roomsFiltered.length) return Container();
                  return RequestItem(
                    key: Key('request_$index'),
                    room: roomsFiltered[index],
                    colorBar: true,
                    type: 'broadcast/${widget.requestId}',
                    activeChat: widget.activeChatId == roomsFiltered[index].id,
                  );
                },
              ),
            );
          }
        });
  }
}
