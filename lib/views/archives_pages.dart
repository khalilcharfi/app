/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:famedly/models/famedly.dart';
import 'package:famedly/components/rooms_list/item.dart';
import 'package:famedly/styles/famedly_icons_v2_icons.dart';
import 'package:famedly/utils/error_reporter.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:future_loading_dialog/future_loading_dialog.dart';
import '../styles/colors.dart';

/// This page displays a list of archived chats.
class ArchivesPage extends StatefulWidget {
  @override
  _ArchivesPageState createState() => _ArchivesPageState();
}

class _ArchivesPageState extends State<ArchivesPage> {
  final matrixChatList = GlobalKey();
  void _clearArchiveAction(BuildContext context) async {
    if (_archive == null) return;
    if (OkCancelResult.ok ==
        await showOkCancelAlertDialog(
          context: context,
          message: L10n.of(context).clearWarningText,
          okLabel: L10n.of(context).clear,
          cancelLabel: L10n.of(context).cancel,
          useRootNavigator: false,
        )) {
      await showFutureLoadingDialog(
          context: context,
          future: () async {
            for (var i = 0; i < _archive.length; i++) {
              await _archive[i].forget();
            }
          });
      setState(() => _archive = null);
      await showOkAlertDialog(
        context: context,
        message: L10n.of(context).archiveCleared,
        okLabel: L10n.of(context).next,
        useRootNavigator: false,
      );
    }
  }

  List<Room> _archive;

  Future<List<Room>> _loadArchive(BuildContext context) async {
    _archive ??= await Famedly.of(context).client.archive;
    return _archive;
  }

  final ScrollController _scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: Key('ArchivesPage'),
      appBar: AppBar(
        leading: BackButton(),
        title: Text(L10n.of(context).archive),
        actions: <Widget>[
          PopupMenuButton(
            key: Key('PopupMenu'),
            onSelected: (result) {
              switch (result) {
                case 'clear':
                  _clearArchiveAction(context);
              }
            },
            icon: Icon(FamedlyIconsV2.more),
            itemBuilder: (BuildContext context) => <PopupMenuEntry>[
              PopupMenuItem(
                key: Key('Clear'),
                value: 'clear',
                child: Text(L10n.of(context).clearArchive),
              ),
            ],
          ),
        ],
      ),
      body: FutureBuilder<List<Room>>(
          future: _loadArchive(context),
          builder: (context, snapshot) {
            if (snapshot.hasError) {
              ErrorReporter.captureException(exception: snapshot.error);
              return Center(child: Text(snapshot.error.toString()));
            }
            if (!snapshot.hasData) {
              return Center(child: CircularProgressIndicator());
            }
            final rooms = snapshot.data;
            return Scrollbar(
              isAlwaysShown: kIsWeb,
              controller: _scrollController,
              child: ListView.separated(
                controller: _scrollController,
                shrinkWrap: true,
                addAutomaticKeepAlives: true,
                physics: const AlwaysScrollableScrollPhysics(),
                itemCount: rooms.length,
                separatorBuilder: (context, i) => Divider(
                  indent: 70,
                  height: 1,
                  thickness: 1,
                  color: Theme.of(context).brightness == Brightness.light
                      ? FamedlyColors.paleGrey
                      : FamedlyColors.slate,
                ),
                itemBuilder: (context, index) => RoomItem(
                  key: Key('room_$index'),
                  item: rooms[index],
                ),
              ),
            );
          }),
    );
  }
}
