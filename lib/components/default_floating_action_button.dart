/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'package:adaptive_page_layout/adaptive_page_layout.dart';
import 'package:famedly/models/tasks/tasklist.dart';
import 'package:famedly/styles/colors.dart';
import 'package:famedly/styles/famedly_icons_v2_icons.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';

import '../models/famedly.dart';
import 'add_task_modal.dart';
import 'default_modal_bottom_sheet.dart';

class DefaultFloatingActionButton extends StatefulWidget {
  final ScrollController scrollController;

  const DefaultFloatingActionButton({
    this.scrollController,
    Key key,
  }) : super(key: key);

  @override
  _DefaultFloatingActionButtonState createState() =>
      _DefaultFloatingActionButtonState();
}

class _DefaultFloatingActionButtonState
    extends State<DefaultFloatingActionButton> {
  bool _minify = false;

  void _scrollControllerListener() {
    if (widget.scrollController.position.pixels > 0 && !_minify) {
      setState(() => _minify = true);
    }
    if (widget.scrollController.position.pixels <= 0 && _minify) {
      setState(() => _minify = false);
    }
  }

  void onPressed(BuildContext context) {
    Famedly.of(context)
        .analyticsPlugin
        .trackEventWithName('Main Page', 'FAB button', 'clicked');
    showModalBottomSheet(
      isScrollControlled: true,
      context: AdaptivePageLayout.of(context).context,
      backgroundColor: Colors.transparent,
      builder: (innerContext) => DefaultModalBottomSheet(
        child: ListView(
          physics: NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          children: [
            _DefaultBottomSheetListTile(
              iconData: FamedlyIconsV2.chats,
              title: L10n.of(context).startNewConversation,
              onTap: () => {
                Navigator.of(innerContext).pop(),
                Famedly.of(context).analyticsPlugin.trackEventWithName(
                    'Main Page', 'Start new conversation', 'clicked'),
                AdaptivePageLayout.of(context).pushNamedAndRemoveUntilIsFirst(
                  '/contacts',
                )
              },
              key: Key('start_new_conversation_button'),
            ),
            _DefaultBottomSheetListTile(
              iconData: FamedlyIconsV2.patient,
              title: L10n.of(context).newPatient,
              onTap: () => {
                Navigator.of(innerContext).pop(),
                Famedly.of(context)
                    .analyticsPlugin
                    .trackEventWithName('Main Page', 'New patient', 'clicked'),
                AdaptivePageLayout.of(context).pushNamedAndRemoveUntilIsFirst(
                  '/patients/new',
                )
              },
              key: Key('create_new_patient_button'),
            ),
            _DefaultBottomSheetListTile(
              iconData: FamedlyIconsV2.integration,
              title: L10n.of(context).addIntegration,
              onTap: () => {
                Navigator.of(innerContext).pop(),
                Famedly.of(context).analyticsPlugin.trackEventWithName(
                    'Main Page', 'Add integration', 'clicked'),
                AdaptivePageLayout.of(context).pushNamedAndRemoveUntilIsFirst(
                  '/directory/integrations',
                )
              },
              key: Key('add_integration_button'),
            ),
            _DefaultBottomSheetListTile(
              iconData: FamedlyIconsV2.requests,
              title: L10n.of(context).startRequest,
              onTap: () => {
                Navigator.of(innerContext).pop(),
                Famedly.of(context).analyticsPlugin.trackEventWithName(
                    'Main Page', 'Start Request', 'clicked'),
                AdaptivePageLayout.of(context).pushNamedAndRemoveUntilIsFirst(
                  '/directory',
                )
              },
              key: Key('start_request_button'),
            ),
            _DefaultBottomSheetListTile(
              iconData: Icons.speaker_phone,
              title: L10n.of(context).newBroadcast,
              key: Key('start_broadcast_button'),
              onTap: () async {
                Navigator.of(innerContext).pop();
                Famedly.of(context).analyticsPlugin.trackEventWithName(
                    'Main Page', 'New broadcast', 'clicked');
                await AdaptivePageLayout.of(context)
                    .pushNamedAndRemoveUntilIsFirst(
                  '/newBroadcast/verify',
                );
              },
            ),
            _DefaultBottomSheetListTile(
              iconData: FamedlyIconsV2.tasks,
              title: L10n.of(context).addTodo,
              onTap: () {
                Navigator.of(innerContext).pop();
                Famedly.of(context)
                    .analyticsPlugin
                    .trackEventWithName('Main Page', 'Add Todo', 'clicked');
                _showAddTaskModal();
              },
              key: Key('add_task_button'),
            ),
          ],
        ),
      ),
    );
  }

  void _showAddTaskModal() => showModalBottomSheet<void>(
        isScrollControlled: true,
        context: context,
        backgroundColor: Colors.transparent,
        builder: (_) => AddTaskModal(
          TaskList(Famedly.of(context).client),
        ),
      );

  @override
  void initState() {
    widget.scrollController?.addListener(_scrollControllerListener);
    super.initState();
  }

  @override
  void dispose() {
    widget.scrollController?.removeListener(_scrollControllerListener);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final icon = Icon(FamedlyIconsV2.add);
    final key = Key('default_floating_action_button');
    return _minify
        ? FloatingActionButton(
            onPressed: () => onPressed(context),
            heroTag: 'default_floating_action_button',
            key: key,
            child: icon,
          )
        : FloatingActionButton.extended(
            onPressed: () => onPressed(context),
            label: Text(L10n.of(context).newWord),
            icon: icon,
            heroTag: 'default_floating_action_button',
            key: key,
          );
  }
}

class _DefaultBottomSheetListTile extends StatelessWidget {
  final String title;
  final IconData iconData;
  final Function onTap;

  const _DefaultBottomSheetListTile({
    Key key,
    @required this.title,
    @required this.iconData,
    @required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: ListTile(
        leading: CircleAvatar(
          backgroundColor: Theme.of(context).backgroundColor,
          foregroundColor: FamedlyColors.azul,
          child: Icon(iconData),
        ),
        title: Text(
          title,
          style: TextStyle(color: Theme.of(context).textTheme.bodyText1.color),
        ),
        onTap: onTap,
      ),
    );
  }
}
