import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:package_info/package_info.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:pub_semver/pub_semver.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'famedly_plugin.dart';

class AppOutdatedPlugin extends FamedlyPlugin {
  AppOutdatedPlugin(matrix) : super(matrix);

  static Uri outdatedVersionApi = Uri.parse(
      'https://gitlab.com/famedly/services/app-min-update-version/-/raw/master/min-update-versions.json');

  static const String androidUpdateUrl =
      'https://play.google.com/store/apps/details?id=com.famedly.talk&hl=de';
  static const String iosUpdateUrl =
      'https://apps.apple.com/de/app/famedly/id1459847644';

  bool checkedThisSession = false;

  void checkIfOutdated() async {
    if (checkedThisSession || kIsWeb) return;
    checkedThisSession = true;

    if ((Platform.isAndroid || Platform.isIOS) && await isOutdated()) {
      if (OkCancelResult.ok ==
          await showOkCancelAlertDialog(
            context: famedly.context,
            title: L10n.of(famedly.context).pleaseUpdateDescription,
            okLabel: L10n.of(famedly.context).update,
            cancelLabel: L10n.of(famedly.context).back,
            useRootNavigator: false,
          )) {
        if (Platform.isAndroid) {
          await launch(androidUpdateUrl);
        } else if (Platform.isIOS) {
          await launch(iosUpdateUrl);
        }
      }
    }
  }

  Future<bool> isOutdated() async {
    final packageInfo = await PackageInfo.fromPlatform();
    final currentSemverNum = Version.parse(packageInfo.version);

    Map<String, dynamic> versions;
    try {
      final response = await http.get(outdatedVersionApi);
      versions = json.decode(response.body);
    } on SocketException catch (_) {
      return false;
    }
    VersionConstraint minimalRequiredVersion;
    if (Platform.isAndroid) {
      minimalRequiredVersion = VersionConstraint.parse(versions['android']);
      return currentSemverNum < minimalRequiredVersion;
    } else if (Platform.isIOS) {
      minimalRequiredVersion = VersionConstraint.parse(versions['ios']);
      return currentSemverNum < minimalRequiredVersion;
    } else if (!Platform.isIOS && !Platform.isAndroid) {
      minimalRequiredVersion = VersionConstraint.parse(versions['web']);
      return currentSemverNum < minimalRequiredVersion;
    }
    return false;
  }
}
