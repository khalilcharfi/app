/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'dart:io';
import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:future_loading_dialog/future_loading_dialog.dart';
import 'package:open_file/open_file.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:universal_html/html.dart' as html;
import 'package:android_path_provider/android_path_provider.dart';
import 'package:adaptive_page_layout/adaptive_page_layout.dart';

import 'package:flutter_gen/gen_l10n/l10n.dart';

class Downloader {
  final BuildContext context;

  const Downloader(this.context);

  static const String fileDownloadMessageType = 'com.famedly.app.file_download';
  static const String isRedactableNamespace = 'com.famedly.app.redactable';

  Future<void> start(Event fileEvent,
      {String fileName = 'famedly_downloaded_file',
      String mimeType = 'text/plain'}) async {
    final confirmed = await showOkCancelAlertDialog(
      context: context,
      title: L10n.of(context).decryptionMessage,
      okLabel: L10n.of(context).yes,
      cancelLabel: L10n.of(context).cancel,
      useRootNavigator: false,
    );
    if (confirmed == OkCancelResult.ok) {
      if (kIsWeb) {
        _webDownload(fileEvent, fileName, mimeType);
      } else {
        _appDownload(fileEvent, fileName, mimeType);
      }
      await fileEvent.room.sendEvent(<String, dynamic>{
        'msgtype': fileDownloadMessageType,
        'body': L10n.of(context).downloadedAttachment(
            fileEvent.room
                .getUserByMXIDSync(fileEvent.room.client.userID)
                .calcDisplayname(mxidLocalPartFallback: false),
            fileName),
        '$isRedactableNamespace': false,
      });
    }
    return null;
  }

  void _webDownload(Event fileEvent, String fileName, String mimeType) async {
    final file = await showFutureLoadingDialog(
        context: context,
        future: () => fileEvent.downloadAndDecryptAttachment());

    final element = html.document.createElement('a');
    element.setAttribute('download', fileName);
    element.setAttribute('href',
        html.Url.createObjectUrlFromBlob(html.Blob([file.result.bytes])));
    element.setAttribute('target', '_blank');
    element.setAttribute('rel', 'noopener');
    element.setAttribute('type', mimeType);
    element.style.display = 'none';
    html.document.body.append(element);

    element.click();

    element.remove();
  }

  void _appDownload(Event fileEvent, String fileName, String mimeType) async {
    if (!(await Permission.storage.request()).isGranted) return;
    final path = await showFutureLoadingDialog(
        context: context,
        future: () async {
          final file = await fileEvent.downloadAndDecryptAttachment();
          final downloadsDir = Platform.isAndroid
              ? (await AndroidPathProvider.downloadsPath)
              : (await getApplicationDocumentsDirectory()).path;
          final tempFile = File(downloadsDir + '/' + file.name);
          await tempFile.writeAsBytes(file.bytes);
          return tempFile.path;
        });
    if (path.error != null) return;
    AdaptivePageLayout.of(context).showSnackBar(
      SnackBar(
        content: Text(L10n.of(context).downloadComplete),
        action: SnackBarAction(
          label: L10n.of(context).open,
          onPressed: () => OpenFile.open(path.result),
        ),
      ),
    );
  }
}
