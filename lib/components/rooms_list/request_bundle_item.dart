import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/material.dart';
import 'package:adaptive_page_layout/adaptive_page_layout.dart';

import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:future_loading_dialog/future_loading_dialog.dart';
import '../../models/requests/broadcast_content.dart';
import '../../models/requests/request.dart';
import '../../models/requests/request_content.dart';
import '../../styles/colors.dart';
import '../../utils/date_time_extension.dart';
import '../default_modal_bottom_sheet.dart';

import '../mouse_over_builder.dart';
import '../unread_bubble.dart';

/// Generates the widget for each request item.
class RequestBundleItem extends StatelessWidget {
  final RequestBundle request;

  const RequestBundleItem({Key key, @required this.request}) : super(key: key);

  Color get newsColor {
    for (final room in request.rooms) {
      if (room.highlightCount > 0) return FamedlyColors.grapefruit;
      if (room.notificationCount > 0) return FamedlyColors.emerald;
    }
    return Colors.grey;
  }

  void _closeAction(BuildContext context) async {
    if (OkCancelResult.ok ==
        await showOkCancelAlertDialog(
          context: context,
          title: L10n.of(context).archiveThisConversation,
          okLabel: L10n.of(context).yes,
          cancelLabel: L10n.of(context).cancel,
          useRootNavigator: false,
        )) {
      await showFutureLoadingDialog(
        context: context,
        future: () => Request.closeBroadcast(request),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    var displayName = '';
    var description = '';
    DateTime creationTime;
    var accepted = 0;
    var needInfo = 0;
    var rejected = 0;
    if (request.broadcastRoom != null) {
      final broadcast = BroadcastContent.fromJson(
          request.broadcastRoom.getState(Request.broadcastNameSpace).content);
      displayName = broadcast.title;
      description = L10n.of(context).broadcast +
          ': ' +
          broadcast.contacts.length.toString() +
          ' ' +
          L10n.of(context).contacts;
      creationTime = request.broadcastRoom.timeCreated;

      for (final contact in broadcast.contacts) {
        Request r;
        for (final room in request.rooms) {
          final rr = Request(room);
          if (rr.content.contactId == contact.contactId) {
            r = rr;
            break;
          }
        }
        r ??= Request(request.broadcastRoom, contact: contact);
        if (r.remoteAccepted()) {
          accepted++;
        }
        if (r.remoteNeedInfo()) {
          needInfo++;
        }
        if (r.remoteRejected()) {
          rejected++;
        }
      }
    } else {
      // fallback, no broadcast room present
      displayName = L10n.of(context).broadcast;
      description = L10n.of(context).broadcast +
          ': ' +
          request.rooms.length.toString() +
          ' ' +
          L10n.of(context).contacts;
      if (request.rooms.isNotEmpty) {
        final state = request.rooms[0].getState(Request.requestNameSpace);
        if (state != null) {
          final content = RequestContent.fromJson(state.content);
          if (content.requestTitle != '') {
            displayName = content.requestTitle;
          }
        }
      }

      creationTime = request.rooms[0].timeCreated;
      for (final room in request.rooms) {
        if (room.timeCreated < creationTime) {
          creationTime = room.timeCreated;
        }
      }

      for (final room in request.rooms) {
        final r = Request(room);
        if (r.remoteAccepted()) {
          accepted++;
        }
        if (r.remoteNeedInfo()) {
          needInfo++;
        }
        if (r.remoteRejected()) {
          rejected++;
        }
      }
    }
    final showCloseAction = (request.broadcastRoom != null &&
            request.broadcastRoom.membership != Membership.leave) ||
        request.broadcastRoom == null;

    return MouseOverBuilder(
      builder: (context, hover) => ListTile(
        onLongPress: !showCloseAction
            ? null
            : () => showModalBottomSheet(
                  context: context,
                  backgroundColor: Colors.transparent,
                  builder: (context) => DefaultModalBottomSheet(
                    child: ListView(shrinkWrap: true, children: [
                      ListTile(
                        title: Text(L10n.of(context).archiveAction),
                        onTap: () => _closeAction(context),
                      ),
                    ]),
                  ),
                ),
        title: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              displayName,
              style: TextStyle(
                fontSize: 14,
                color: FamedlyColors.metallicBlue,
                fontWeight: FontWeight.w600,
              ),
            ),
            Spacer(),
            Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(accepted.toString(),
                    style: TextStyle(
                      color: FamedlyColors.aquaMarine,
                    )),
                Text('/'),
                Text(needInfo.toString(),
                    style: TextStyle(
                      color: FamedlyColors.yellow,
                    )),
                Text('/'),
                Text(
                  rejected.toString(),
                  style: TextStyle(
                    color: FamedlyColors.grapefruit,
                  ),
                ),
              ],
            ),
            Opacity(
              // We need to hide it this way because otherwise the button would stop existing
              // when the user stop hovers it to select a PopupMenuButtonItem.
              opacity: hover ? 1 : 0,
              child: Container(
                height: UnreadBubble.size,
                margin:
                    hover ? const EdgeInsets.only(left: 8.0) : EdgeInsets.zero,
                width: hover ? 20 : 0,
                child: PopupMenuButton(
                  padding: EdgeInsets.zero,
                  itemBuilder: (context) => [
                    PopupMenuItem(
                      value: 'archive',
                      child: Text(L10n.of(context).archiveAction),
                    ),
                  ],
                  onSelected: (action) => _closeAction(context),
                ),
              ),
            ),
          ],
        ),
        subtitle: Row(
          children: <Widget>[
            Text(
              description,
              style: TextStyle(
                fontSize: 13,
                color: FamedlyColors.slate,
              ),
            ),
            Spacer(),
            Text(
              creationTime.localizedTimeShort(context),
              key: Key('time_created'),
              style: TextStyle(
                color: FamedlyColors.blueyGrey,
                fontSize: 13,
                fontWeight: FontWeight.w400,
                fontStyle: FontStyle.normal,
                letterSpacing: 0,
              ),
            ),
          ],
        ),
        onTap: () => AdaptivePageLayout.of(context)
            .pushNamed('/broadcast/' + request.id),
      ),
    );
  }
}
