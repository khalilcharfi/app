/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'dart:math';

import 'package:adaptive_page_layout/adaptive_page_layout.dart';
import 'package:famedly/components/profile_scaffold.dart';
import 'package:famedly/components/user_directory_login_builder.dart';
import 'package:famedly/utils/famedly_file_picker.dart';
import 'package:famedly/utils/url_launcher.dart';
import 'package:famedly_directory_client_sdk/famedly_directory_client_sdk.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:file_picker_cross/file_picker_cross.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:future_loading_dialog/future_loading_dialog.dart';
import 'package:matrix_link_text/link_text.dart';

import '../components/sentry_switch_list_tile.dart';
import '../models/famedly.dart';
import '../models/roles/client_extension.dart';
import '../utils/matrix_file_thumbnail_extension.dart';

class Intro extends StatefulWidget {
  final String username;
  final String welcomeMessage;
  const Intro({
    Key key,
    this.username,
    this.welcomeMessage,
  }) : super(key: key);

  @override
  _IntroState createState() => _IntroState();
}

class _IntroState extends State<Intro> {
  Future<Organisation> organisationFuture;
  Future<Profile> profileFuture;
  bool _addToUserDirectory = true;

  void _nextAction(BuildContext context) async {
    final userDirectoryClient =
        await Famedly.of(context).getUserDirectoryClient();
    if (userDirectoryClient.accessToken != null && _addToUserDirectory) {
      await showFutureLoadingDialog(
          context: context,
          future: () async {
            final userData = await userDirectoryClient.addUserDataToDirectory();
            if (userData.active == false) {
              await userDirectoryClient.activeUserSearch();
            }
          });
    }
    return AdaptivePageLayout.of(context).pushNamedAndRemoveAllOthers(
        Famedly.of(context).magicLink != null
            ? '/settings/security'
            : '/backup');
  }

  @override
  Widget build(BuildContext context) {
    final famedly = Famedly.of(context);
    organisationFuture ??= famedly.client
        .getOwnOrganisation(famedly.getOrganizationDirectoryClient());
    profileFuture ??= famedly.client.ownProfile;
    return FutureBuilder<Organisation>(
      future: organisationFuture,
      builder: (BuildContext context,
          AsyncSnapshot<Organisation> organisationSnapshot) {
        return FutureBuilder<Profile>(
          future: profileFuture,
          builder:
              (BuildContext context, AsyncSnapshot<Profile> profileSnapshot) {
            final displayname = profileSnapshot.hasData
                ? profileSnapshot.data.displayname ??
                    famedly.client.userID.localpart
                : (famedly.client.userID.localpart != ''
                    ? famedly.client.userID.localpart
                    : '');
            final avatarUrl = profileSnapshot.hasData
                ? profileSnapshot.data.avatarUrl
                : Uri.parse('');
            return ProfileScaffold(
              key: Key('IntroPage'),
              title: Text(
                organisationSnapshot.hasData
                    ? organisationSnapshot.data.name
                    : famedly.client.homeserver
                        .toString()
                        .replaceAll('https://', ''),
                key: Key('title'),
                style: const TextStyle(
                  fontSize: 20,
                ),
              ),
              automaticallyImplyLeading: false,
              profileName: displayname,
              profileContent: avatarUrl,
              onAvatarTap: () async {
                MatrixFile file;
                final tempFile = await FamedlyFilePicker.pickFiles(
                  type: FileTypeCross.image,
                  context: context,
                );
                if (tempFile == null) return;
                try {
                  file = await MatrixImageFile(
                    bytes: tempFile.toUint8List(),
                    name: tempFile.path,
                  ).createThumbnail();
                } catch (e, s) {
                  Logs().d('Could not create thumbnail for avatar', e, s);
                  file = MatrixImageFile(
                    bytes: tempFile.toUint8List(),
                    name: tempFile.path,
                  );
                }

                if (file != null) {
                  final success = await showFutureLoadingDialog(
                      context: context,
                      future: () => famedly.client.setAvatar(file));
                  if (success.error == null) {
                    setState(() => profileFuture = null);
                  }
                }
              },
              body: Padding(
                padding: EdgeInsets.symmetric(
                    horizontal:
                        max(16, (MediaQuery.of(context).size.width - 400) / 2)),
                child: Column(
                  children: <Widget>[
                    SizedBox(height: 32),
                    LinkText(
                      text: widget.welcomeMessage ??
                          L10n.of(context).introPage(
                            profileSnapshot.hasData
                                ? profileSnapshot.data.displayname
                                : '⌛ ${L10n.of(context).loading} ⌛',
                            widget.username ?? famedly.client.userID.localpart,
                          ),
                      onLinkTap: (url) => UrlLauncher(context, url).launchUrl(),
                      textStyle: TextStyle(
                        fontSize: 18,
                      ),
                    ),
                    SizedBox(height: 23),
                    Container(
                      width: double.infinity,
                      child: ElevatedButton(
                        key: Key('NextButton'),
                        onPressed: () => _nextAction(context),
                        child: Text(L10n.of(context).continueText),
                      ),
                    ),
                    SizedBox(height: 23),
                    SentrySwitchListTile(padding: EdgeInsets.zero),
                    UserDirectoryLoginBuilder(
                      builder: (context) => SwitchListTile(
                        contentPadding: EdgeInsets.zero,
                        value: _addToUserDirectory,
                        onChanged: (b) =>
                            setState(() => _addToUserDirectory = b),
                        title: Text(
                            L10n.of(context).addToUserDirectoryDescription),
                      ),
                    ),
                  ],
                ),
              ),
            );
          },
        );
      },
    );
  }
}
