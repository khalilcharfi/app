/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'package:famedly/styles/famedly_icons_v2_icons.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import 'package:flutter_gen/gen_l10n/l10n.dart';
import '../styles/colors.dart';

class SearchBar extends StatefulWidget {
  final TextEditingController filterTextController;
  final void Function(String text) onTextChanged;
  final bool autofocus;
  final String labelText;
  final Function onTap;
  final Function onCancel;

  SearchBar({
    this.filterTextController,
    this.onTextChanged,
    this.autofocus = false,
    this.labelText,
    this.onTap,
    this.onCancel,
    Key key,
  }) : super(key: key);

  @override
  SearchBarState createState() => SearchBarState();
}

class SearchBarState extends State<SearchBar> {
  String text;
  final FocusNode _focusNode = FocusNode();
  bool _selected = false;

  void _setSelected() => setState(() => _selected = _focusNode.hasFocus);

  @override
  void initState() {
    _focusNode.addListener(_setSelected);
    super.initState();
  }

  @override
  void dispose() {
    _focusNode.removeListener(_setSelected);
    super.dispose();
  }

  @override
  TextField build(BuildContext context) {
    return TextField(
      focusNode: _focusNode,
      controller: widget.filterTextController,
      keyboardType: TextInputType.text,
      autofocus: widget.autofocus,
      maxLines: 1,
      readOnly: widget.filterTextController == null,
      onTap: widget.onTap,
      style: TextStyle(
        color: FamedlyColors.blueyGrey,
        fontSize: 15,
        fontWeight: FontWeight.w400,
        fontStyle: FontStyle.normal,
        letterSpacing: 0,
      ),
      onChanged: (String newText) {
        setState(() {
          text = newText;
        });
        if (widget.onTextChanged != null) widget.onTextChanged(newText);
      },
      decoration: InputDecoration(
        fillColor: Theme.of(context).secondaryHeaderColor,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(20),
          borderSide: BorderSide(
            width: 0,
            style: BorderStyle.none,
          ),
        ),
        filled: true,
        contentPadding: EdgeInsets.all(11),
        prefixIcon: Icon(
          FamedlyIconsV2.search,
          color: FamedlyColors.blueyGrey,
        ),
        suffixIcon: ((text?.isNotEmpty ?? false) || _selected) &&
                widget.filterTextController != null
            ? IconButton(
                icon: Icon(
                  FamedlyIconsV2.close,
                  color: FamedlyColors.blueyGrey,
                ),
                onPressed: () {
                  // workaround
                  // see https://github.com/flutter/flutter/issues/35848#issuecomment-527854562
                  WidgetsBinding.instance.addPostFrameCallback((_) {
                    widget.filterTextController.clear(); // clear text
                    FocusScope.of(context)
                        .requestFocus(FocusNode()); // hide keyboard
                    setState(() {
                      text = '';
                    });
                  });
                  widget.onCancel?.call();
                },
              )
            : null,
        hintText: widget.labelText ?? L10n.of(context).filter,
        hintStyle: TextStyle(
          color: FamedlyColors.blueyGrey,
          fontSize: 15,
          fontWeight: FontWeight.w400,
          fontStyle: FontStyle.normal,
          letterSpacing: 0,
        ),
      ),
    );
  }
}
