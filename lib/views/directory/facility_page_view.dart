/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import 'package:famedly/components/famedly_icon_button.dart';
import 'package:famedly/components/profile_scaffold.dart';
import 'package:famedly/components/settings_list_group.dart';
import 'package:famedly/models/roles/role_entity.dart';
import 'package:famedly/styles/colors.dart';
import 'package:famedly/views/directory/facility_page.dart';
import 'package:famedly_directory_client_sdk/famedly_directory_client_sdk.dart';
import 'package:famedlysdk/famedlysdk.dart' hide Tag;
import 'package:flutter/material.dart';
import 'package:famedly/models/roles/client_extension.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';

class FacilityPageView extends StatelessWidget {
  final FacilityPageController controller;

  const FacilityPageView(this.controller, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final organisationLocation = controller.organisation.location;
    return ProfileScaffold(
      title: Text(
        controller.organisation.name,
        style: TextStyle(
          fontSize: 15,
        ),
      ),
      profileName: controller.organisation.name,
      body: FutureBuilder<List<RoleEntity>>(
        future: controller.famedly.client.getAllRoles('role'),
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            Logs().w('Could not get roles', snapshot.error);
          }
          if (snapshot.connectionState != ConnectionState.done) {
            return Center(child: CircularProgressIndicator());
          }
          controller.getContacts(snapshot.data);
          return Padding(
            padding: FamedlyStyles.getTwoColumnListViewPadding(context),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                controller.contactEmails.isNotEmpty &&
                        controller.widget.onInvite ==
                            null // custom onInvite handlers can't do email
                    ? FamedlyIconButton(
                        iconData: Icons.alternate_email,
                        onTap: () =>
                            controller.onTapEmail(controller.contactEmails),
                      )
                    : Container(),
                SizedBox(height: 10),
                if (controller.contactPointTiles.isNotEmpty)
                  SettingsListGroup(
                    title: L10n.of(context).contactPoints,
                    children: controller.contactPointTiles,
                  ),
                SettingsListGroup(
                  title: L10n.of(context).information,
                  children: <Widget>[
                    if (Directory
                            .subtypeCache[controller.organisation.orgType] !=
                        null)
                      ListTile(
                          title: Text(L10n.of(context).category,
                              style: FacilityPageController.titleTextStyle),
                          subtitle: Text(Directory
                              .subtypeCache[controller.organisation.orgType]
                              .description)),
                    controller.organisation.location != null
                        ? ListTile(
                            title: Text(L10n.of(context).address,
                                style: FacilityPageController.titleTextStyle),
                            subtitle: Text(
                              '${organisationLocation.street} ${organisationLocation.number}, ${organisationLocation.zipCode} ${organisationLocation.city}',
                            ),
                          )
                        : Container()
                  ],
                ),
                //if (controller.organisation.tags.isNotEmpty) WIP?
                SettingsListGroup(
                  title: L10n.of(context).tags,
                  children: [
                    TagListBuilder(
                      tagIDList: ['Bla', 'Blubb'],
                    )
                  ],
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}

class TagListBuilder extends StatelessWidget {
  final List<String> tagIDList;
  const TagListBuilder({this.tagIDList, Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var tagText = '';
    for (var i = 0; i < tagIDList.length; i++) {
      final tag = Directory.tagCache[tagIDList[i]] ??
          Tag(description: L10n.of(context).unknown, id: tagIDList[i]);
      tagText += tag.description + ', ';
    }
    tagText = tagText.substring(0, tagText.length - 2);
    return Container(
      height: 52,
      alignment: Alignment.center,
      child: ListView(
        scrollDirection: Axis.horizontal,
        padding: const EdgeInsets.all(16.0),
        children: [Text(tagText)],
      ),
    );
  }
}
