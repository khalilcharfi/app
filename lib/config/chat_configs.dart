/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
abstract class ChatConfigs {
  static const int maxAutoLoadImageSize = 500000;
  static const String sentryDsn =
      'https://f547434868e64292b70380ab05a5af26@sentry.io/1448986';
  static const String fallbackUserDirectoryBaseUrl =
      'https://user-directory.famedly.de';
  static const String fallbackOrganizationDirectoryBaseUrl =
      'https://directory.famedly.de';
  static const List<String> serverWhitelist = [
    'https://famedly.de',
    'https://matrix.famedly.de',
    'https://test-sp-gen-1.famedly.de',
    'https://test-ap-gen-1.famedly.de',
    'https://test-kh-gen-1.famedly.de',
    'https://janian.de',
    'https://friends.famedly.de',
    'https://matrix.shine.horse',
    'https://matrix.bubu1.eu',
    'https://s.bubu.chat',
    'https://matrix.neko.dev',
    'https://sorunome.de',
    'https://matrix.imbitbu.de'
  ];
}
