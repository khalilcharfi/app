/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'dart:math';

import 'package:famedly/styles/colors.dart';
import 'package:flutter/material.dart';

class DefaultModalBottomSheet extends StatelessWidget {
  final Widget child;

  const DefaultModalBottomSheet({Key key, this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: Navigator.of(context, rootNavigator: false).pop,
      child: Container(
        alignment: FamedlyStyles.columnMode
            ? Alignment.bottomLeft
            : Alignment.bottomCenter,
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: Theme.of(context).scaffoldBackgroundColor.withOpacity(0.9),
          ),
          width: min(
            MediaQuery.of(context).size.width,
            FamedlyStyles.columnWidth,
          ),
          child: SafeArea(
            child: child,
          ),
        ),
      ),
    );
  }
}
