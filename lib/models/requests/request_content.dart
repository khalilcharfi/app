class RequestContent {
  final String creator;
  final String requestingOrganisation;
  final String requestedOrganisation;
  final String requestTitle;
  final String requestId;
  final String organisationId;
  final String organisationName;
  final String contactId;
  final String contactDescription;
  final String requestType;
  final bool isBroadcast;
  final String msgtype;
  final String body;
  final String format;
  final String formattedBody;

  RequestContent(
      {this.creator,
      this.requestingOrganisation,
      this.requestedOrganisation,
      this.requestTitle,
      this.requestId,
      this.organisationId,
      this.organisationName,
      this.contactId,
      this.contactDescription,
      this.requestType,
      this.isBroadcast,
      this.msgtype,
      this.body,
      this.format,
      this.formattedBody});

  RequestContent.fromJson(Map<String, dynamic> json)
      : creator = json['creator'],
        requestingOrganisation = json['requesting_organisation'],
        requestedOrganisation = json['requested_organisation'],
        requestTitle = json['request_title'],
        requestId = json['request_id'],
        organisationId = json['organisation_id'],
        organisationName = json['organisation_name'],
        contactId = json['contact_id'],
        contactDescription = json['contact_description'],
        requestType = json['request_type'],
        isBroadcast = json['is_broadcast'],
        msgtype = json['msgtype'],
        body = json['body'],
        format = json['format'],
        formattedBody = json['formatted_body'];

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    final data = map;
    data['creator'] = creator;
    data['requesting_organisation'] = requestingOrganisation;
    data['requested_organisation'] = requestedOrganisation;
    data['request_title'] = requestTitle;
    data['request_id'] = requestId;
    data['organisation_id'] = organisationId;
    data['organisation_name'] = organisationName;
    data['contact_id'] = contactId;
    data['contact_description'] = contactDescription;
    data['request_type'] = requestType;
    data['is_broadcast'] = isBroadcast;
    data['msgtype'] = msgtype;
    data['body'] = body;
    data['format'] = format;
    data['formatted_body'] = formattedBody;
    return data;
  }
}
