import 'package:famedlysdk/famedlysdk.dart';

import '../famedly.dart';

abstract class FamedlyPlugin {
  final Famedly famedly;
  Client get client => famedly.client;

  const FamedlyPlugin(this.famedly);
}
