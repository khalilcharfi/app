# Issue Management

For every bug, every not implemented feature, every todo, there should be an issue created at: https://gitlab.com/famedly/app/-/issues

We create issues for everything to keep track of it. That means that much issues don't mean that the app has a lot of bugs. Much issues labeled with bugs mean that the app has a lot of bugs. Also keep in mind that just knowing that there are bugs could also mean that the app has a good bug reporting where even extremly small bugs are reported. It doesn't mean that the app is unstable. The number of issues doesn't tell anything about the quality. Issues can be ideas of new features, stuff that needs to be discussed or simple todos about changing the color of a button.

However the number of issues labeled with `type::bug` or `type::regression` AND the label `Priority::Max` tells us a lot about the stability! You can use these links to check it out:

* [Regressions with Max priority](https://gitlab.com/famedly/app/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=type%3A%3Abug&label_name[]=Priority%3A%3AMax)
* [Bugs with Max priority](https://gitlab.com/famedly/app/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Priority%3A%3AMax&label_name[]=type%3A%3Aregression)

## Labels

All issues should have a category. Possible categories could be:
* Bug
* Regression
* Support
* New feature
* UX/UI design
* Refactoring
* Discussion
* Docs

All issues should have a labeled priority of max, medium or low.

If an issue affects **not all** platforms it should have labeled all platforms that are affected.

If an issue can't be solved yet and is blocked, it should be marked with a `Need::` or `Dependency::` label. For example:
* `Need::More Informations`
* `Dependency::Flutter`

If something needs an upstream fix or depends on another git project, please link the issue or merge request from the other project in the issue.

For issues which are belonging to a specific technical category, there can be created a new label for it. For example: `E2EE`, `Push Notifications`.

There are additional labels which may be useful, like `From::(origin)` or `Good first issue`.

#### Priority

There are three priority levels: Max, Medium, Low. To determine how to choose which label use the following guide:

##### Priority Max:

* Regressions - Features that worked in the past and now are broken. This leads to a very bad user experience and angry customers
* Bugs that affect the user experience very badly, for example black text on black background which makes it unreadable
* Missing features that are needed for a complete user experience, for example if a previous feature hasn't been implemented completely
* Customers need this feature as soon as possible
* Issues that could make it much easier to fix other issues or improves the exception handling massively

##### Priority Medium:
* Other bugs and regressions that are annoying but not that critical
* Usual refactoring
* Improvements of the CI or the release cycle
* New features without a timeline
* Design and UX improvements without a timeline

##### Priority Low:
* Typos
* Very minor bugs or bugs which affects only a tiny minority of the users
* Warnings in the logs that doesn't affect the functionality or the user experience
* Missing localizations
* Features that would be nice to have but not a must have

Of course this is very subjective. Even warnings in the logs can have a huge impact if they are ignored too long. It belongs to the developer to find a good compromise.

#### Critical Label

If something is labeled as `Critical` it needs to be solved as soon as possible! After the issue is solved it may be necessary to perform a bugfix release.

## Assigning

All issues that are not blocked by a `Need::` or `Dependency::` label and that are not discussions should be assigned to a developer who is then responsible to solve  this issue.

This is to make sure that not two developers are working at the same issue at the same time. Contact the developer if you want to contribute something to a assigned task.

The maintainers are responsible to assign issues. However. All developers are free to pick unassigned issues and assign those to themself.

All unassigned issues should become assigned and labeled every morning.

## Issue board

We are using an issue board at GitLab to track issues at: https://gitlab.com/famedly/app/-/boards/977024

All developers have their own list in this board with assigned issues. The list is ordered, so the developer has a timeline which issue he/she might target next. This should result in an approximately timeline. New assigned issues are sorted at the end of the list by default. The sort order can be changed of course but all developers should try to provide a good overview about which issues will be targeted when!

The following rules should help you to find a good sort order:

* Critical issues must always be on top of the list
* Regressions and bugs are more important than new features
* Priority Max issues are more important than Medium or Low
* Old issues shouldn't get lost in the list and rise up in the order instead of falling down

## Milestones

Most of the issues won't have a deadline. For all releases a milestone will be created. Every milestone will contain a list of issues which must be solved before the milestone ends. This gives all issues in this list a deadline. At the frontend meeting the developers should discuss which deadlines are realistic.