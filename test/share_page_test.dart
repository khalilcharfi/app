import 'package:famedly/components/matrix_chat_list.dart';
import 'package:famedly/views/share_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'utils.dart';

void main() {
  group('SharePage', () {
    NavigatorObserver mockObserver;

    setUp(() {
      mockObserver = MockNavigatorObserver();
    });
    testWidgets('should have a MatrixChatList', (WidgetTester tester) async {
      await tester.runAsync(() async {
        await tester.pumpWidget(
          await Utils.getWidgetWrapper(
            SharePage(),
            mockObserver,
          ),
        );

        await tester.pumpAndSettle();

        expect(find.byType(MatrixChatList), findsOneWidget);
      });
    });
  });
}
