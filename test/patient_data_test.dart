import 'package:famedly/models/patients/patient_data.dart';
import 'package:test/test.dart';

void main() {
  /// All Tests related to the DateTime extension
  group('PatientData', () {
    test('from and to json', () async {
      final patientDataJson = {
        'name': 'Hilde Hardt', // String
        'birthday':
            '1965-01-01', // Date format like https://www.hl7.org/fhir/datatypes.html#date
        'sex': 'female', // male | female | other | unknown
        'case_id': '42' // String
      };
      final patientData = PatientData.fromJson(patientDataJson);
      expect(PatientData.fromJson(patientDataJson).toJson(), patientDataJson);
      expect(patientData.birthday, DateTime.parse('1965-01-01'));
      expect(patientData.name, 'Hilde Hardt');
      expect(patientData.sex, Sex.female);
      expect(patientData.caseId, '42');
    });
  });
}
