/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'dart:async';

import 'package:famedlysdk/famedlysdk.dart' hide RoomFilter;
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:adaptive_page_layout/adaptive_page_layout.dart';

import '../models/requests/request.dart';
import '../models/famedly.dart';
import 'rooms_list/request_bundle_item.dart';
import 'rooms_list/request_item.dart';

class RequestsList extends StatefulWidget {
  final RoomFilter filter;
  final RequestBundleFilter requestBundleFilter;
  final onlyLeft;
  final TextEditingController filterTextGetter;
  final String activeChatId;
  final String type;

  /// Widget shown when there are no
  /// chats to list.
  final Widget emptyListPlaceholder;

  RequestsList(
    this.filterTextGetter, {
    this.onlyLeft = false,
    this.filter,
    this.requestBundleFilter,
    this.activeChatId,
    this.emptyListPlaceholder,
    this.type,
    Key key,
  }) : super(key: key);

  @override
  RequestsListState createState() => RequestsListState();
}

class RequestsListState extends State<RequestsList> {
  List<Room> roomList;
  List<RequestBundle> requests;

  Famedly famedly;
  String filterText;

  void handleFilter() {
    if (widget.filterTextGetter == null) return;
    setState(() {
      filterText = widget.filterTextGetter.text;
    });
  }

  void rebuildView() {
    roomList = null;
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    if (widget.filterTextGetter != null) {
      widget.filterTextGetter.addListener(handleFilter);
    }
  }

  bool textSearchFilter(RequestBundle bundle) {
    if (filterText == null || filterText == '') return true;
    if (bundle.broadcastRoom != null) {
      final req = Request(bundle.broadcastRoom);
      return req.title.toLowerCase().contains(filterText.toLowerCase());
    } else if (bundle.rooms.isNotEmpty) {
      final req = Request(bundle.rooms[0]);
      return req.title.toLowerCase().contains(filterText.toLowerCase());
    } else {
      return false;
    }
  }

  Widget _buildItem(
      List<RequestBundle> doRequests, BuildContext context, int index) {
    if (index >= doRequests.length) return Container();
    final request = doRequests[index];
    if (request.broadcastRoom != null) {
      final req = Request(request.broadcastRoom);
      if (req.isAuthor() || request.rooms.length > 1) {
        return RequestBundleItem(
          key: Key('request_$index'),
          request: request,
        );
      } else if (request.rooms.length == 1) {
        return RequestItem(
          key: Key('request_$index'),
          room: request.rooms.first,
          type: widget.type,
          activeChat: widget.activeChatId == request.rooms.first.id,
        );
      } else {
        return RequestItem(
          key: Key('request_$index'),
          room: request.broadcastRoom,
          contacts: Request.getBroadcastContacts(
              request.broadcastRoom, famedly.client.userID),
          type: widget.type,
        );
      }
    } else if (request.rooms.length != 1) {
      return RequestBundleItem(
        key: Key('request_$index'),
        request: request,
      );
    } else {
      return RequestItem(
        key: Key('request_$index'),
        room: request.rooms[0],
        type: widget.type,
        activeChat: widget.activeChatId == request.rooms.first.id,
      );
    }
  }

  Future<List<Room>> getList() async {
    List<Room> list;
    if (roomList != null) list = roomList;
    if (widget.onlyLeft) {
      list = await famedly.client.archive;
    } else {
      list = famedly.client.rooms;
    }
    return list;
  }

  @override
  void dispose() {
    roomList = null;
    if (widget.filterTextGetter != null) {
      widget.filterTextGetter.removeListener(handleFilter);
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    famedly = Famedly.of(context);

    return StreamBuilder<Object>(
        stream: famedly.client.onSync.stream,
        builder: (context, snapshot) {
          return FutureBuilder(
              future: getList(),
              builder:
                  (BuildContext context, AsyncSnapshot<List<Room>> snapshot) {
                if (!snapshot.hasData) {
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                }

                roomList = snapshot.data;
                requests = Request.bundleFromRoomList(
                  roomList: roomList,
                  roomFilter: widget.filter,
                  requestBundleFilter: (RequestBundle bundle) =>
                      !((widget.requestBundleFilter != null &&
                              !widget.requestBundleFilter(bundle)) ||
                          !textSearchFilter(bundle)),
                );

                if (requests == null || requests.isEmpty) {
                  return widget.emptyListPlaceholder ?? Container();
                }

                // In column mode we should directly display the other columns.
                if (AdaptivePageLayout.of(context).columnMode(context) &&
                    AdaptivePageLayout.of(context).currentViewData.routeName ==
                        '/${widget.type}') {
                  if (requests.isNotEmpty && requests.first.rooms.length == 1) {
                    WidgetsBinding.instance.addPostFrameCallback(
                      (_) => AdaptivePageLayout.of(context).popAndPushNamed(
                          '/room/${widget.type}/${requests.first.rooms.single.id}'),
                    );
                  }
                }

                return ListView.separated(
                  separatorBuilder: (context, int i) => Divider(
                    height: 8,
                    thickness: 1,
                    indent: 15,
                  ),
                  shrinkWrap: true,
                  itemCount: requests.length,
                  itemBuilder: (context, index) => _buildItem(
                    requests,
                    context,
                    index,
                  ),
                );
              });
        });
  }
}
