/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'package:famedly/styles/famedly_icons_v2_icons.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/material.dart';
import 'package:adaptive_page_layout/adaptive_page_layout.dart';

import 'package:flutter_gen/gen_l10n/l10n.dart';
import '../models/room_type_extension.dart';
import '../styles/colors.dart';
import '../models/famedly.dart';
import 'nav_scaffold.dart';
import 'unread_bubble.dart';

class MainHeaderButtons extends StatefulWidget {
  final double width;
  final MainPage activePage;

  const MainHeaderButtons({
    @required this.width,
    @required this.activePage,
  });

  @override
  __MainHeaderButtonsState createState() => __MainHeaderButtonsState();
}

class __MainHeaderButtonsState extends State<MainHeaderButtons> {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<Object>(
        stream: Famedly.of(context).client.onRoomUpdate.stream,
        builder: (context, snapshot) {
          return Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              _MainHeaderButton(
                width: widget.width,
                isActivePage: widget.activePage == MainPage.chats,
                route: '/chats',
                title: L10n.of(context).chats,
                iconData: FamedlyIconsV2.chats,
                unreadBubble: UnreadBubble(
                  Famedly.of(context)
                      .client
                      .rooms
                      .where((Room room) =>
                          room.type == roomTypePatient ||
                          room.type == roomTypeMessaging)
                      .toList(),
                  showNumbers: false,
                ),
              ),
              _MainHeaderButton(
                width: widget.width,
                route: '/requests',
                isActivePage: widget.activePage == MainPage.requests,
                title: L10n.of(context).requests,
                iconData: FamedlyIconsV2.requests,
                unreadBubble: UnreadBubble(
                  Famedly.of(context)
                      .client
                      .rooms
                      .where((Room room) => room.type == roomTypeRequest)
                      .toList(),
                  showNumbers: false,
                  color: Colors.orange,
                ),
              ),
              _MainHeaderButton(
                width: widget.width,
                route: '/directory',
                isActivePage: widget.activePage == MainPage.directory,
                title: L10n.of(context).directory,
                iconData: FamedlyIconsV2.directory,
                unreadBubble: Container(),
              ),
              _MainHeaderButton(
                width: widget.width,
                route: '/tasks',
                title: L10n.of(context).tasks,
                isActivePage: widget.activePage == MainPage.tasks,
                iconData: FamedlyIconsV2.tasks,
                unreadBubble: Container(),
              ),
            ],
          );
        });
  }
}

class _MainHeaderButton extends StatelessWidget {
  final double width;
  final IconData iconData;
  final String title;
  final String route;
  final Widget unreadBubble;
  final bool isActivePage;

  const _MainHeaderButton({
    @required this.width,
    @required this.iconData,
    @required this.title,
    @required this.route,
    @required this.unreadBubble,
    this.isActivePage = false,
  });

  @override
  Widget build(BuildContext context) {
    final chatsButtonTextColor =
        isActivePage ? FamedlyColors.azul : FamedlyColors.cloudyBlue;
    return Container(
      width: width,
      child: TextButton(
        style: ButtonStyle(
          padding: MaterialStateProperty.resolveWith((_) => EdgeInsets.zero),
          shape: MaterialStateProperty.resolveWith(
              (_) => RoundedRectangleBorder()),
          backgroundColor: MaterialStateProperty.resolveWith((states) {
            if (states.contains(MaterialState.hovered)) {
              return FamedlyColors.hoverDark;
            }
            if (states.isEmpty) {
              return isActivePage
                  ? FamedlyColors.darkerGrey
                  : FamedlyColors.charcoalGrey;
            }
            return null;
          }),
        ),
        onPressed: () {
          AdaptivePageLayout.of(context).pushNamedAndRemoveUntilIsFirst(route);
        },
        child: Center(
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Icon(
                iconData,
                color: chatsButtonTextColor,
              ),
              SizedBox(
                width: 6,
              ),
              Text(
                title,
                style: TextStyle(
                  fontSize: 16,
                  color: chatsButtonTextColor,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 10.0),
                child: unreadBubble,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
