/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'package:famedly/styles/famedly_icons_v2_icons.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import 'package:flutter_gen/gen_l10n/l10n.dart';
import '../models/room_type_extension.dart';
import '../styles/colors.dart';
import 'package:adaptive_page_layout/adaptive_page_layout.dart';
import '../models/famedly.dart';
import 'search_bar.dart';
import 'unread_bubble.dart';

class NavScaffold extends StatelessWidget {
  @override
  final Key key;
  final Widget leading;
  final List<Widget> actions;
  final Widget body;
  final Widget floatingActionButton;
  final TextEditingController filterTextController;
  final onTextChanged;
  final Function(int i) onTab;
  final bool inlineView;

  /// If provided, it will override [leading], [actions],
  /// [editButton] and [filterTestController].
  final AppBar appBar;

  /// If provided a bottom navigation with requests, chats and tasks
  /// buttons will be visible, highlighting the active page.
  final MainPage activePage;

  /// If true the normal back button gets hidden and replaced by a custom one.
  /// This is needed as the default one gets behind the grey bar on the web/column
  /// design. To fix it the back button gets added to the same line as the search bar.
  final bool needsWebBack;

  NavScaffold({
    this.key,
    this.leading,
    this.actions,
    this.body,
    this.floatingActionButton,
    this.activePage,
    this.filterTextController,
    this.onTextChanged,
    this.appBar,
    this.needsWebBack = false,
    this.onTab,
    this.inlineView = false,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: key,
      appBar: appBar ??
          PreferredSize(
            preferredSize: const Size.fromHeight(105.0),
            child: AppBar(
              titleSpacing: 8,
              shape: AdaptivePageLayout.of(context).columnMode(context)
                  ? Border(
                      bottom:
                          BorderSide(width: 1.0, color: FamedlyColors.black10),
                    )
                  : null,
              elevation:
                  AdaptivePageLayout.of(context).columnMode(context) ? 0 : null,
              title: Center(
                child: Container(
                  height: 40,
                  child: SearchBar(
                    filterTextController: filterTextController,
                    onTextChanged: onTextChanged,
                  ),
                ),
              ),
              leading: inlineView ? BackButton() : null,
              automaticallyImplyLeading: inlineView,
              bottom: PreferredSize(
                preferredSize: Size.fromHeight(50),
                child: _NavScaffoldTabBar(activePage, onTab),
              ),
            ),
          ),
      body: body ?? Container(),
      floatingActionButton: floatingActionButton,
      bottomNavigationBar:
          !inlineView && activePage != null && !FamedlyStyles.columnMode
              ? _NavScaffoldBottomNavigationBar(activePage)
              : null,
    );
  }
}

enum MainPage { chats, requests, directory, tasks, settings }

class MainPageTab {
  final String title;
  final Function filter;

  const MainPageTab(this.title, {this.filter});

  String localizedTitle(BuildContext context) {
    switch (title) {
      case 'messages':
        return L10n.of(context).messages;
      case 'patients':
        return L10n.of(context).patients;
      case 'integrations':
        return L10n.of(context).integrations;
      case 'people':
        return L10n.of(context).people;
      case 'myroles':
        return L10n.of(context).myRoles;
      case 'roles':
        return L10n.of(context).roles;
      case 'organisations':
        return L10n.of(context).organisations;
      case 'requests':
        return L10n.of(context).requests;
      case 'requests-overview':
        return L10n.of(context).overview;
      default:
        return title;
    }
  }
}

class MainPageDetails {
  final String title;
  final String route;
  final IconData icon;
  final List<MainPageTab> tabs;

  const MainPageDetails(
    String title,
    this.icon, {
    String route,
    this.tabs = const <MainPageTab>[],
  })  : title = title,
        route = route ?? '/$title';

  String localizedTitle(BuildContext context) {
    switch (title) {
      case 'chats':
        return L10n.of(context).chats;
      case 'requests':
        return L10n.of(context).requests;
      case 'tasks':
        return L10n.of(context).tasks;
      case 'directory':
        return L10n.of(context).directory;
      case 'more':
        return L10n.of(context).more;
      default:
        return title;
    }
  }
}

Map<MainPage, MainPageDetails> mainPages = {
  MainPage.chats: MainPageDetails(
    'chats',
    FamedlyIconsV2.chats,
    tabs: [
      MainPageTab('messages',
          filter: (Room room) =>
              room.type == roomTypeMessaging &&
              room.directChatMatrixID != Famedly.covidBotId),
      MainPageTab('patients',
          filter: (Room room) => room.type == roomTypePatient),
      MainPageTab('integrations',
          filter: (Room room) =>
              room.type == roomTypeMessaging &&
              room.directChatMatrixID == Famedly.covidBotId),
    ],
  ),
  MainPage.requests: MainPageDetails(
    'requests',
    FamedlyIconsV2.requests,
    tabs: [
      MainPageTab('requests-overview',
          filter: (Room room) => room.type == roomTypeRequest),
      MainPageTab('myroles', filter: (Room room) => false),
    ],
  ),
  MainPage.directory: MainPageDetails(
    'directory',
    FamedlyIconsV2.directory,
    tabs: [
      MainPageTab('people'),
      MainPageTab('roles'),
      MainPageTab('organisations'),
      MainPageTab('integrations'),
    ],
  ),
  MainPage.tasks: MainPageDetails('tasks', FamedlyIconsV2.tasks),
  MainPage.settings: MainPageDetails(
    'more',
    FamedlyIconsV2.settings,
    route: '/settings',
  ),
};

class _NavScaffoldBottomNavigationBar extends StatefulWidget {
  final MainPage activePage;

  const _NavScaffoldBottomNavigationBar(this.activePage);

  @override
  __NavScaffoldBottomNavigationBarState createState() =>
      __NavScaffoldBottomNavigationBarState();
}

class __NavScaffoldBottomNavigationBarState
    extends State<_NavScaffoldBottomNavigationBar> {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<Object>(
        stream: Famedly.of(context).client.onRoomUpdate.stream,
        builder: (context, snapshot) {
          return BottomNavigationBar(
            showUnselectedLabels: true,
            type: BottomNavigationBarType.fixed,
            selectedLabelStyle: TextStyle(color: FamedlyColors.azul),
            selectedIconTheme: IconThemeData(color: FamedlyColors.azul),
            items: [
              for (final mainPage in mainPages.values)
                BottomNavigationBarItem(
                  icon: (['chats', 'requests'].contains(mainPage.title))

                      /// handles unread icon on bottomNavigationBar
                      ? Stack(
                          children: [
                            Center(
                                child: Icon(
                              mainPage.icon,
                              key: Key(mainPage.title),
                            )),
                            Align(
                              alignment: Alignment.center,
                              child: Padding(
                                padding: const EdgeInsets.only(
                                    left: 16.0, bottom: 8.0),
                                child: UnreadBubble(
                                  Famedly.of(context)
                                      .client
                                      .rooms
                                      .where((Room room) => mainPage.title ==
                                              'requests'
                                          ? room.type == roomTypeRequest
                                          : room.type == roomTypeMessaging ||
                                              room.type == roomTypePatient)
                                      .toList(),
                                  showNumbers: false,
                                  color: mainPage.title == 'requests'
                                      ? Colors.orange
                                      : FamedlyColors.grapefruit,
                                ),
                              ),
                            ),
                          ],
                        )
                      : Icon(
                          mainPage.icon,
                          key: Key(mainPage.title),
                        ),
                  label: mainPage.localizedTitle(context),
                ),
            ],
            currentIndex: mainPages.keys.toList().indexOf(widget.activePage),
            selectedItemColor: FamedlyColors.azul,
            onTap: (i) {
              if (widget.activePage.index != i) {
                /// Don't repush page if it's already selected
                AdaptivePageLayout.of(context).popUntilIsFirst();
                if (mainPages.values.toList()[i].route != '/chats') {
                  AdaptivePageLayout.of(context).pushNamed(
                    mainPages.values.toList()[i].route,
                  );
                }
              }
            },
          );
        });
  }
}

class _NavScaffoldTabBar extends StatefulWidget {
  final Function onTab;
  final MainPage activePage;

  const _NavScaffoldTabBar(this.activePage, this.onTab);

  @override
  __NavScaffoldTabBarState createState() => __NavScaffoldTabBarState();
}

class __NavScaffoldTabBarState extends State<_NavScaffoldTabBar> {
  String currentTab;
  int widgetIndex;

  void sendMatomoAction(index) {
    widgetIndex = widget.activePage.index;
    if (widgetIndex == 0) {
      currentTab = ['messages', 'patients', 'integrations'][index];
    } else if (widgetIndex == 1) {
      currentTab = ['overview', 'my roles'][index];
    } else if (widgetIndex == 3) {
      currentTab = ['people', 'roles', 'organisations', 'integrations'][index];
    }

    Famedly.of(context).analyticsPlugin.trackEventWithName(
        ['chats', 'requests', 'tasks', 'directory'][widgetIndex],
        'tab',
        currentTab);
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<Object>(
        stream: Famedly.of(context).client.onRoomUpdate.stream,
        builder: (context, snapshot) {
          return TabBar(
            indicatorColor: FamedlyColors.azul,
            indicatorWeight: 3,
            onTap: (context) {
              widget.onTab;
              sendMatomoAction(context);
            },
            labelPadding: widget.activePage == MainPage.directory
                ? null
                : EdgeInsets.zero,
            isScrollable: widget.activePage == MainPage.directory,
            tabs: [
              for (final tab in mainPages[widget.activePage].tabs)
                Tab(
                  key: Key(
                      'NavScaffoldTab-${widget.activePage.toString().split('.').last}-${tab.title}'),
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Text(
                        tab.localizedTitle(context),
                        maxLines: 1,
                        softWrap: true,
                        overflow: TextOverflow.ellipsis,
                        textAlign: TextAlign.center,
                      ),
                      if ([MainPage.chats, MainPage.requests]
                          .contains(widget.activePage))
                        UnreadBubble(
                          Famedly.of(context)
                              .client
                              .rooms
                              .where(tab.filter)
                              .toList(),
                          showNumbers: false,
                          color: widget.activePage == MainPage.requests
                              ? Colors.orange
                              : FamedlyColors.grapefruit,
                        ),
                    ],
                  ),
                ),
            ],
          );
        });
  }
}
