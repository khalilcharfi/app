/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'package:adaptive_page_layout/adaptive_page_layout.dart';
import 'package:famedly/styles/famedly_icons_v2_icons.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:photo_view/photo_view.dart';

import '../models/famedly.dart';
import '../styles/colors.dart';
import '../utils/downloader.dart';

class ImageView extends StatefulWidget {
  final String eventId;
  final String roomId;

  const ImageView({
    Key key,
    @required this.eventId,
    @required this.roomId,
  }) : super(key: key);

  @override
  _ImageViewState createState() => _ImageViewState();
}

class _ImageViewState extends State<ImageView> {
  Future<MatrixFile> _file;
  Future<Event> _event;

  @override
  void initState() {
    if (widget.eventId != null && widget.roomId != null) {
      final client = Famedly.of(context).client;
      _event = client.getRoomById(widget.roomId)?.getEventById(widget.eventId);
      _file = _event?.then((value) => value.downloadAndDecryptAttachment());
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: FamedlyColors.slate,
      systemNavigationBarColor: FamedlyColors.slate,
      statusBarBrightness: Brightness.light,
    ));
    return Scaffold(
      appBar: AppBar(
        brightness: Brightness.dark,
        backgroundColor: Colors.black.withOpacity(0.75),
        automaticallyImplyLeading: false,
        elevation: 0,
        leading: IconButton(
          color: Colors.white,
          icon: Icon(FamedlyIconsV2.close),
          onPressed: () {
            SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
              systemNavigationBarColor: Theme.of(context).backgroundColor,
              systemNavigationBarIconBrightness: Brightness.dark,
              statusBarColor: Theme.of(context).backgroundColor,
            ));
            AdaptivePageLayout.of(context).pop();
          },
        ),
        actions: <Widget>[
          if (widget.eventId != null && widget.roomId != null)
            FutureBuilder<Event>(
                future: _event,
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    final event = snapshot.data;
                    return IconButton(
                      color: Colors.white,
                      icon: Icon(FamedlyIconsV2.down_1),
                      onPressed: () => Downloader(context).start(
                        event,
                        fileName: event.body,
                        mimeType: event.content['info'] is Map<String, dynamic>
                            ? event.content['info']['mimetype'] ?? 'image/png'
                            : 'image/png',
                      ),
                    );
                  } else {
                    return Container();
                  }
                }),
        ],
      ),
      backgroundColor: FamedlyColors.slate,
      body: FutureBuilder<MatrixFile>(
        future: _file,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            final file = snapshot.data;
            return PhotoView(
              minScale: PhotoViewComputedScale.contained * 1.0,
              initialScale: PhotoViewComputedScale.contained * 1.0,
              imageProvider: MemoryImage(file.bytes),
            );
          } else if (snapshot.hasError) {
            return Container(
              color: Theme.of(context).backgroundColor,
              alignment: Alignment.center,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Icon(Icons.broken_image, size: 40),
                  Text(
                    L10n.of(context).oopsSomethingWentWrong,
                  ),
                  Text(snapshot.error.toString()),
                ],
              ),
            );
          } else {
            return Container(
              color: Colors.black,
              alignment: Alignment.center,
              child: CircularProgressIndicator(),
            );
          }
        },
      ),
    );
  }
}
