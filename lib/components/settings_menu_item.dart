/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'package:famedly/styles/famedly_icons_v2_icons.dart';
import 'package:flutter/material.dart';

import '../styles/colors.dart';

class SettingsMenuItem extends StatelessWidget {
  final Icon icon;
  final String title;
  final Widget data;
  final Widget trailing;
  final GestureTapCallback onTap;

  const SettingsMenuItem(
    this.icon,
    this.title,
    this.data,
    this.onTap, {
    this.trailing,
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      contentPadding: EdgeInsets.symmetric(vertical: 4.0, horizontal: 16.0),
      leading: icon,
      title: Text(title),
      trailing: trailing ??
          Row(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(right: 20),
                child: data,
              ),
              Icon(
                FamedlyIconsV2.right_1,
                color: FamedlyColors.blueyGrey,
              ),
            ],
          ),
      onTap: onTap,
    );
  }
}
