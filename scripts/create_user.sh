#!/bin/bash

set -e

# get access token
token=$(curl -S -H "Content-Type: application/json" -d "{\"username\":\"$INTEGRATION_SERVER_ADMIN_USERNAME\",\"password\":\"$INTEGRATION_SERVER_ADMIN_PASSWORD\"}" $INTEGRATION_SERVER_ADMIN_API/api/login | jq -r '.token')
# create the user
curl -S -f ${INTEGRATION_SERVER_ADMIN_API}/api/user -H 'Content-Type: application/json' -H "Authorization: Bearer $token" -d "{\"name\":\"$INTEGRATION_TEST_DYNAMIC_FULL_NAME\",\"password\":\"$INTEGRATION_TESTUSER_PASSWORD\"}"
# Get an access token for the newly created user so we can call _matrix/client/r0/account/whoami
usertoken=$(curl -S  https://${INTEGRATION_SERVER_URL}/_matrix/client/r0/login -H "Content-Type: application/json" -d "{\"type\": \"m.login.password\", \"identifier\": {\"type\": \"m.id.user\",\"user\": \"${INTEGRATION_TEST_DYNAMIC_USERNAME}\"},\"password\":\"$INTEGRATION_TESTUSER_PASSWORD\"}" | jq -r '.access_token')
# get our mxid
mxid=$(curl -S https://${INTEGRATION_SERVER_URL}/_matrix/client/r0/account/whoami -H "Authorization: Bearer $usertoken" | jq -r .user_id)
# add mxid to the test role
curl -S -f https://$INTEGRATION_SERVER_URL/_matrix/client/unstable/com.famedly/role/r0/role/${INTEGRATION_TEST_ROLEID}/user -H 'Content-Type: application/json' -H "Authorization: Bearer $token" --data-raw "{\"mxid\":\"$mxid\"}"
# add mxid to test group
curl -S -f https://$INTEGRATION_SERVER_URL/_matrix/client/unstable/com.famedly/role/r0/role/${INTEGRATION_TEST_GROUPID}/user -H 'Content-Type: application/json' -H "Authorization: Bearer $token" --data-raw "{\"mxid\":\"$mxid\"}"
