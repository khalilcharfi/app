/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'dart:convert';

import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:famedly/components/avatar.dart';
import 'package:famedly/components/user_directory_login_builder.dart';
import 'package:famedly/models/famedly.dart';
import 'package:famedly/styles/colors.dart';
import 'package:famedly/styles/famedly_icons_icons.dart';
import 'package:famedly_user_directory_client_sdk/famedly_user_directory_client_sdk.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:future_loading_dialog/future_loading_dialog.dart';

class UserDirectorySettingsPage extends StatefulWidget {
  @override
  _UserDirectorySettingsPageState createState() =>
      _UserDirectorySettingsPageState();
}

class _UserDirectorySettingsPageState extends State<UserDirectorySettingsPage> {
  Future<UserData> _userDataRequest;

  Future<UserData> _requestUserData(BuildContext context) async {
    final userDirectoryClient =
        await Famedly.of(context).getUserDirectoryClient();
    try {
      return await userDirectoryClient.requestOwnUserData();
    } catch (e) {
      String errorStr;
      try {
        errorStr = jsonDecode(
            e.toString().replaceFirst('Exception: ', '').trim())['msg'];
      } catch (_) {}
      errorStr ??= e.toString();
      if (errorStr != 'User not found') {
        throw errorStr;
      }
      return null;
    }
  }

  Future<void> _becomeMember(BuildContext context) async {
    final consent = await showOkCancelAlertDialog(
      context: context,
      title: L10n.of(context).addToUserDirectoryDescription,
      okLabel: L10n.of(context).yes,
      cancelLabel: L10n.of(context).cancel,
      useRootNavigator: false,
    );
    if (consent != OkCancelResult.ok) return;
    final userDirectoryClient =
        await Famedly.of(context).getUserDirectoryClient();
    final userData = await showFutureLoadingDialog(
      context: context,
      future: () => userDirectoryClient.addUserDataToDirectory(),
    );
    if (userData.error != null) return;
    if (userData.result.active == false) {
      await showFutureLoadingDialog(
        context: context,
        future: () => userDirectoryClient.activeUserSearch(),
      );
    }
    setState(() => _userDataRequest = null);
    return;
  }

  Future<void> _setActive(BuildContext context, bool active) async {
    final userDirectoryClient =
        await Famedly.of(context).getUserDirectoryClient();
    await showFutureLoadingDialog(
      context: context,
      future: () => active
          ? userDirectoryClient.activeUserSearch()
          : userDirectoryClient.deactiveUserSearch(),
    );
    setState(() => _userDataRequest = null);
    return;
  }

  Future<void> _setSecret(BuildContext context) async {
    final userDirectoryClient =
        await Famedly.of(context).getUserDirectoryClient();
    final secret = await showTextInputDialog(
      context: context,
      title: L10n.of(context).secureOwnSearchEntryWithPassword,
      textFields: [DialogTextField(hintText: 'Banana')],
      okLabel: L10n.of(context).next,
      cancelLabel: L10n.of(context).cancel,
      useRootNavigator: false,
    );
    if (secret == null) return;
    await showFutureLoadingDialog(
      context: context,
      future: () => userDirectoryClient.changeUserSecret(secret.single),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(L10n.of(context).userSearch),
        leading: BackButton(),
      ),
      body: UserDirectoryLoginBuilder(
        builder: (context) {
          _userDataRequest ??= _requestUserData(context);
          return FutureBuilder<UserData>(
            future: _userDataRequest,
            builder: (_, AsyncSnapshot<UserData> snapshot) {
              if (snapshot.hasError) {
                return Center(
                  child: Text(L10n.of(context).oopsSomethingWentWrong),
                );
              }
              if (snapshot.connectionState != ConnectionState.done) {
                return Center(child: CircularProgressIndicator());
              }
              final userData = snapshot.data;
              return ListView(
                padding: FamedlyStyles.getTwoColumnListViewPadding(context),
                children: [
                  CheckboxListTile(
                    controlAffinity: ListTileControlAffinity.leading,
                    value: userData?.active ?? false,
                    onChanged: (active) => userData == null
                        ? _becomeMember(context)
                        : _setActive(context, active),
                    title: Text(L10n.of(context).iWantToBeFoundByUserDirectory),
                  ),
                  Divider(height: 1),
                  if (userData != null)
                    ListTile(
                        title: Text('${L10n.of(context).ownSearchEntry}:')),
                  if (userData != null)
                    ListTile(
                      leading: Avatar(
                        Uri.parse(userData.avatarUrl ?? ''),
                        name: userData.displayname,
                        size: 40,
                      ),
                      title: Text(userData.displayname ??
                          L10n.of(context).usernameIncorrect),
                      subtitle: Text(userData.dnsDomain),
                    ),
                  if (userData != null) Divider(height: 1),
                  if (userData != null)
                    ListTile(
                      leading: CircleAvatar(
                        child: Icon(FamedlyIcons.security),
                      ),
                      title: Text(
                          L10n.of(context).secureOwnSearchEntryWithPassword),
                      onTap: () => _setSecret(context),
                    ),
                ],
              );
            },
          );
        },
      ),
    );
  }
}
