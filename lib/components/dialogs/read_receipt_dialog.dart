import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';

import '../../utils/date_time_extension.dart';
import '../../utils/event_receipts_extension.dart';
import '../avatar.dart';

class ReadReceiptDialog extends StatelessWidget {
  final Event event;

  const ReadReceiptDialog(this.event, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final sortedEvent = event.readSoFar;
    sortedEvent.sort((a, b) =>
        a.time.millisecondsSinceEpoch.compareTo(b.time.millisecondsSinceEpoch));

    final _scrollController = ScrollController();

    return AlertDialog(
      title: Text(
        '${L10n.of(context).readSoFar}:',
        key: Key('readSoFarHeader'),
      ),
      content: Container(
        width: 200,
        height: 200.0,
        child: Scrollbar(
          isAlwaysShown: kIsWeb,
          controller: _scrollController,
          child: ListView.builder(
            controller: _scrollController,
            shrinkWrap: true,
            itemCount: sortedEvent.length,
            itemBuilder: (BuildContext context, int i) => ListTile(
              leading: Avatar(
                sortedEvent[i].user.avatarUrl,
                name: sortedEvent[i]
                    .user
                    .calcDisplayname(mxidLocalPartFallback: false),
                matrixClient: event.room.client,
              ),
              title: Text(
                sortedEvent[i]
                    .user
                    .calcDisplayname(mxidLocalPartFallback: false),
              ),
              subtitle: Text(
                sortedEvent[i].time.localizedTime(context),
              ),
            ),
          ),
        ),
      ),
      actions: <Widget>[
        TextButton(
          key: Key('backButton'),
          onPressed: () => Navigator.of(context, rootNavigator: false).pop(),
          child: Text(L10n.of(context).back),
        ),
      ],
    );
  }
}
