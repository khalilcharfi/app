import 'package:famedly_directory_client_sdk/famedly_directory_client_sdk.dart';
import 'package:famedlysdk/famedlysdk.dart';

import 'role_availability.dart';
import 'role_entity.dart';
import 'user_availability.dart';

final String ROLE_BASE = '/client/unstable/com.famedly/role/r0';

extension RolesClientExtension on Client {
  Future<List<RoleEntity>> getAllRoles([String type]) async {
    final res = await request(
      RequestType.GET,
      ROLE_BASE +
          '/roles' +
          (type != null ? '/' + Uri.encodeComponent(type) : ''),
    );
    final ret = <RoleEntity>[];
    for (final r in res['roles']) {
      ret.add(RoleEntity.fromJson(this, r));
    }
    return ret;
  }

  Future<RoleEntity> getRole(int id) async {
    final res = await request(
      RequestType.GET,
      ROLE_BASE + '/role/' + id.toString(),
    );
    return RoleEntity.fromJson(this, res);
  }

  Future<RoleAvailability> getRoleAvailability(int id) async {
    final res = await request(
      RequestType.GET,
      ROLE_BASE + '/role/' + id.toString() + '/availability',
    );
    return RoleAvailability.fromJson(res);
  }

  Future<UserAvailability> getUserAvailability([String mxid]) async {
    mxid ??= userID;
    final res = await request(
      RequestType.GET,
      ROLE_BASE + '/user/' + Uri.encodeComponent(mxid) + '/availability',
    );
    return UserAvailability.fromJson(res);
  }

  Future<List<RoleEntity>> getOwnRoles([String type, String mxid]) async {
    if (mxid == null) {
      if (type != null && type[0] == '@') {
        mxid = type;
        type = null;
      } else {
        mxid = userID;
      }
    }
    final allRoles = await getAllRoles(type);
    final ownRoles = allRoles.where((r) => r.users.contains(mxid)).toList();
    final availability = await getUserAvailability(mxid);
    for (final r in ownRoles) {
      r.userAvailable = availability.roles.contains(r.id);
    }
    return ownRoles;
  }

  Future<String> getOwnOrganisationId() async {
    final res = await request(
      RequestType.GET,
      ROLE_BASE + '/organisation_id',
    );
    return res['organisation_id'];
  }

  Future<Organisation> getOwnOrganisation(
      Future<Directory> directoryFuture) async {
    final directory = await directoryFuture;
    final orgId = await getOwnOrganisationId();
    if (orgId == null) {
      return null;
    }
    return await directory.getOrganisation(orgId);
  }
}
