/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'package:adaptive_page_layout/adaptive_page_layout.dart';
import 'package:famedly/components/default_floating_action_button.dart';
import 'package:famedly/components/nav_scaffold.dart';
import 'package:famedly/components/request_group_list_tile.dart';
import 'package:famedly/components/request_list.dart';
import 'package:famedly/components/search_bar.dart';
import 'package:famedly/models/requests/filters.dart';
import 'package:famedly/models/roles/role_entity.dart';
import 'package:famedly/styles/colors.dart';
import 'package:famedly/styles/famedly_icons_v2_icons.dart';
import 'package:famedly/views/requests/requests_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';

class RequestsPageView extends StatelessWidget {
  final RequestsPageController controller;

  const RequestsPageView(this.controller, {Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    // In column mode we should directly display the other columns.
    if (AdaptivePageLayout.of(context).columnMode(context) &&
        AdaptivePageLayout.of(context).currentViewData.routeName ==
            '/requests') {
      WidgetsBinding.instance.addPostFrameCallback(
        (_) => AdaptivePageLayout.of(context)
            .popAndPushNamed('/requests/conversations'),
      );
    }

    return StreamBuilder<Object>(
        stream: controller.famedly.client.onSync.stream,
        builder: (context, snapshot) {
          return DefaultTabController(
            initialIndex: controller.initialTabIndex,
            length: 2,
            child: NavScaffold(
              key: Key('RequestsPage'),
              appBar: AppBar(
                automaticallyImplyLeading: false,
                titleSpacing: 8,
                shape: AdaptivePageLayout.of(context).columnMode(context)
                    ? Border(
                        bottom: BorderSide(
                            width: 1.0, color: FamedlyColors.black10),
                      )
                    : null,
                elevation: AdaptivePageLayout.of(context).columnMode(context)
                    ? 0
                    : null,
                title: Center(
                  child: Container(
                    height: 40,
                    child: SearchBar(
                      filterTextController: controller.filterTextController,
                      onTextChanged: (String text) =>
                          controller.searchBarTextChanged(text),
                      onCancel: () => controller.searchBarOnCancel(),
                    ),
                  ),
                ),
              ),
              floatingActionButton: controller.initialTabIndex != 0
                  ? null
                  : DefaultFloatingActionButton(),
              actions: <Widget>[
                Padding(
                  padding: EdgeInsets.only(
                    right: 15, top: 15, //bottom: 15
                  ),
                  child: IconButton(
                    key: Key('NotificationsButton'),
                    icon: Icon(FamedlyIconsV2.on),
                    iconSize: 24,
                    color: FamedlyColors.slate,
                    onPressed: () {
                      // FIXME Not implemented
                    },
                  ),
                ),
              ],
              activePage: MainPage.requests,
              body: controller.searchActive == false
                  ? FutureBuilder<List<RoleEntity>>(
                      future: controller.roleFuture,
                      builder: (BuildContext context,
                          AsyncSnapshot<List<RoleEntity>> snapshot) {
                        // Ignore errors here
                        if (snapshot.hasError) {
                          snapshot.error;
                        }
                        // okay, we have loaded the roles, let's add them
                        final unreadRoles = snapshot.hasData
                            ? controller.countRoleRooms(
                                controller.famedly.client, snapshot.data)
                            : <int, int>{};
                        final roomCountsRoles = snapshot.hasData
                            ? controller.countRoleRooms(
                                controller.famedly.client, snapshot.data,
                                countUnread: false)
                            : <int, int>{};
                        ;
                        return ListView(
                          children: [
                            SizedBox(height: 8),
                            RequestGroupListTile(
                              onTap: () => AdaptivePageLayout.of(context)
                                  .pushNamed('/requests/conversations'),
                              unread: 0,
                              count: 0,
                              highlightUnread: false,
                              title: L10n.of(context).myConversations,
                              iconData: FamedlyIconsV2.requests,
                              selected: controller.widget.currentType ==
                                  'requests/conversations',
                            ),
                            Divider(
                              indent: 52,
                              height: 1,
                              thickness: 1,
                            ),
                            RequestGroupListTile(
                              onTap: () {
                                AdaptivePageLayout.of(context)
                                    .pushNamed('/requests/assigned');
                              },
                              unread: controller.unread.myAssignedRequests,
                              count: controller.roomCounts.myAssignedRequests,
                              highlightUnread: controller
                                      .countRooms(controller.famedly.client)
                                      .myAssignedRequests >
                                  0,
                              title: L10n.of(context).myRequests,
                              iconData: FamedlyIconsV2.profile,
                              indent: true,
                              selected: controller.widget.currentType ==
                                  'requests/assigned',
                            ),
                            RequestGroupListTile(
                              onTap: () {
                                AdaptivePageLayout.of(context)
                                    .pushNamed('/requests/shared');
                              },
                              unread: controller.unread.mySharedRequets,
                              count: controller.roomCounts.mySharedRequets,
                              highlightUnread: controller
                                      .countRooms(controller.famedly.client)
                                      .mySharedRequets >
                                  0,
                              title: L10n.of(context).sharedWithMe,
                              iconData: FamedlyIconsV2.share,
                              indent: true,
                              selected: controller.widget.currentType ==
                                  'requests/shared',
                            ),
                            RequestGroupListTile(
                              onTap: () {
                                AdaptivePageLayout.of(context)
                                    .pushNamed('/requests/broadcasts');
                              },
                              unread: controller.unread.myBroadcasts,
                              count: controller.roomCounts.myBroadcasts,
                              highlightUnread: controller
                                      .countRooms(controller.famedly.client)
                                      .myBroadcasts >
                                  0,
                              title: L10n.of(context).pendingBroadcasts,
                              iconData: Icons.speaker_phone,
                              indent: true,
                              selected: controller.widget.currentType ==
                                  'requests/broadcasts',
                            ),
                            SizedBox(height: 16),
                            RequestGroupListTile(
                              unread: 0,
                              count: 0,
                              highlightUnread: false,
                              title: L10n.of(context).myRolesm,
                              iconData: FamedlyIconsV2.profile,
                              onTap: () {
                                AdaptivePageLayout.of(context)
                                    .pushNamed('/requests/incoming');
                              },
                              selected: controller.widget.currentType ==
                                  'requests/incoming',
                            ),
                            Divider(
                              indent: 52,
                              height: 1,
                              thickness: 1,
                            ),
                            if (snapshot.hasData && snapshot.data.isNotEmpty)
                              for (final role in snapshot.data)
                                RequestGroupListTile(
                                  onTap: () {
                                    AdaptivePageLayout.of(context).pushNamed(
                                        '/requests/role/${role.id.toString()}',
                                        arguments: role);
                                  },
                                  unread: unreadRoles[role.id],
                                  count: roomCountsRoles[role.id],
                                  highlightUnread: unreadRoles[role.id] > 0,
                                  title: role.name,
                                  selected: controller.widget.currentType ==
                                      'requests/role/${role.id.toString()}',
                                  leftPadding: 9,
                                  iconAvatar: Container(
                                    width: AdaptivePageLayout.of(context)
                                            .columnMode(context)
                                        ? 22
                                        : 32,
                                    height: AdaptivePageLayout.of(context)
                                            .columnMode(context)
                                        ? 22
                                        : 32,
                                    alignment: Alignment.center,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(16),
                                      border: Border.all(
                                        color: Theme.of(context).dividerColor,
                                        width: 1,
                                      ),
                                    ),
                                    child: Text(
                                      role.name.substring(0, 2),
                                      style: TextStyle(fontSize: 12),
                                    ),
                                  ),
                                  indent: true,
                                ),
                            if (!snapshot.hasData || snapshot.data.isEmpty)
                              Center(
                                child: Padding(
                                  padding: const EdgeInsets.all(12.0),
                                  child: Text(L10n.of(context)
                                      .youAreCurrentlyNotAssignedAnyRoles),
                                ),
                              ),
                            SizedBox(height: 60),
                          ],
                        );
                      })
                  : FutureBuilder<bool>(
                      future: controller.waitForFirstSync(),
                      builder:
                          (BuildContext context, AsyncSnapshot<bool> snapshot) {
                        if (snapshot.hasData) {
                          return RequestsList(
                            controller.filterTextController,
                            emptyListPlaceholder: Center(
                              child: Text(L10n.of(context).noInquiries),
                            ),
                            onlyLeft: false,
                            filter: RequestFilters.isRequestRoomFilter,
                            type: 'requests/conversations',
                          );
                        } else {
                          return Container(
                            color: Theme.of(context).backgroundColor,
                            child: Center(
                              child: CircularProgressIndicator(),
                            ),
                          );
                        }
                      },
                    ),
            ),
          );
        });
  }
}
