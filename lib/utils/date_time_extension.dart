/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'package:flutter/material.dart';

import 'package:flutter_gen/gen_l10n/l10n.dart';

/// Provides extra functionality for formatting the time.
extension DateTimeExtension on DateTime {
  @Deprecated('Use [millisecondsSinceEpoch] instead.')
  num toTimeStamp() => millisecondsSinceEpoch;

  @deprecated
  String toTimeString() => localizedTimeOfDay(null);
  @deprecated
  String toEventTimeString() => localizedTime(null);

  bool operator <(DateTime other) {
    return millisecondsSinceEpoch < other.millisecondsSinceEpoch;
  }

  bool operator >(DateTime other) {
    return millisecondsSinceEpoch > other.millisecondsSinceEpoch;
  }

  bool operator >=(DateTime other) {
    return millisecondsSinceEpoch >= other.millisecondsSinceEpoch;
  }

  bool operator <=(DateTime other) {
    return millisecondsSinceEpoch <= other.millisecondsSinceEpoch;
  }

  int get age {
    final now = DateTime.now();
    var age = now.year - year;
    if (now.month < month || (now.month == month && now.day < day)) {
      age--;
    }
    return age;
  }

  /// Two message events can belong to the same environment. That means that they
  /// don't need to display the time they were sent because they are close
  /// enaugh.
  static final minutesBetweenEnvironments = 5;

  /// Checks if two DateTimes are close enough to belong to the same
  /// environment.
  bool sameEnvironment(DateTime prevTime) {
    return millisecondsSinceEpoch - prevTime.millisecondsSinceEpoch <
        1000 * 60 * minutesBetweenEnvironments;
  }

  /// Returns a simple time String.
  /// TODO: Add localization
  String localizedTimeOfDay(BuildContext context) => L10n.of(context).timeOfDay(
      ((hour % 12) == 0 ? 12 : (hour % 12)).toString().padLeft(2, '0'),
      (hour).toString().padLeft(2, '0'),
      hour < 12 ? 'am' : 'pm',
      (minute).toString().padLeft(2, '0'));

  /// Returns [localizedTimeOfDay()] if the ChatTime is today, the name of the week
  /// day if the ChatTime is this week and a date string else.
  String localizedTimeShort(BuildContext context) {
    final now = DateTime.now();

    final sameYear = now.year == year;

    final sameDay = sameYear && now.month == month && now.day == day;

    final sameWeek = sameYear &&
        !sameDay &&
        now.millisecondsSinceEpoch - millisecondsSinceEpoch <
            1000 * 60 * 60 * 24 * 7 &&
        now > this;

    if (sameDay) {
      return localizedTimeOfDay(context);
    } else if (sameWeek) {
      switch (weekday) {
        case 1:
          return L10n.of(context).monday;
        case 2:
          return L10n.of(context).tuesday;
        case 3:
          return L10n.of(context).wednesday;
        case 4:
          return L10n.of(context).thursday;
        case 5:
          return L10n.of(context).friday;
        case 6:
          return L10n.of(context).saturday;
        case 7:
          return L10n.of(context).sunday;
      }
    } else if (sameYear) {
      return L10n.of(context).dateWithoutYear(
          month.toString().padLeft(2, '0'), day.toString().padLeft(2, '0'));
    }
    return L10n.of(context).dateWithYear(year.toString(),
        month.toString().padLeft(2, '0'), day.toString().padLeft(2, '0'));
  }

  String localizedDay(BuildContext context) => L10n.of(context).dateWithYear(
      year.toString(),
      month.toString().padLeft(2, '0'),
      day.toString().padLeft(2, '0'));

  /// If the DateTime is today, this returns [localizedTimeOfDay()], if not it also
  /// shows the date.
  /// TODO: Add localization
  String localizedTime(BuildContext context) {
    final now = DateTime.now();

    final sameYear = now.year == year;

    final sameDay = sameYear && now.month == month && now.day == day;

    if (sameDay) return localizedTimeOfDay(context);
    return L10n.of(context)
        .fullDateTime(localizedTimeShort(context), localizedTimeOfDay(context));
  }
}
