/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import '../models/famedly.dart';

class MagicLink {
  String _username;
  String _password;
  String _domain;

  String get username => _username;
  String get password => _password;
  String get domain => _domain;

  MagicLink(String magicLink) {
    final credentials =
        Uri.decodeComponent(magicLink.replaceAll(Famedly.magicLinkPrefix, ''));
    _domain = credentials.split('@').last.split('://').last;
    _username = credentials.split(':').first;
    _password = credentials.split(':')[1].split('@').first;
  }
}
