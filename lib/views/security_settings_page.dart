/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'package:famedly/components/settings_list_group.dart';
import 'package:famedly/styles/colors.dart';
import 'package:famedly/styles/famedly_icons_v2_icons.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/material.dart';
import 'package:adaptive_page_layout/adaptive_page_layout.dart';

import '../models/famedly.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';

class SecuritySettingsPage extends StatefulWidget {
  @override
  _SecuritySettingsPageState createState() => _SecuritySettingsPageState();
}

class _SecuritySettingsPageState extends State<SecuritySettingsPage> {
  String _oldPasswordError;
  String _newPasswordError;
  String _newPassword2Error;
  bool _isLoading = false;
  void _changePasswordAction(BuildContext context) async {
    setState(() =>
        _oldPasswordError = _newPasswordError = _newPassword2Error = null);

    if (oldPasswordFromMagicLink == null && currentPassword.text.isEmpty) {
      setState(
          () => _oldPasswordError = L10n.of(context).pleaseFillOutTextField);
      return;
    }
    if (newPassword.text.isEmpty) {
      setState(
          () => _newPasswordError = L10n.of(context).pleaseFillOutTextField);
      return;
    }
    if (newPassword2.text.isEmpty) {
      setState(
          () => _newPassword2Error = L10n.of(context).pleaseFillOutTextField);
      return;
    }
    if (newPassword.text != newPassword2.text) {
      setState(() => _newPassword2Error = L10n.of(context).passwordsDoNotMatch);
      return;
    }
    setState(() => _isLoading = true);
    try {
      await Famedly.of(context).client.changePassword(
            newPassword.text,
            oldPassword: oldPasswordFromMagicLink ?? currentPassword.text,
          );
      setState(() => _isLoading = false);
      _goBack();
    } catch (e) {
      if (e is MatrixException && e.error == MatrixError.M_FORBIDDEN) {
        setState(() => _newPassword2Error = L10n.of(context).invalidPassword);
      } else {
        setState(() {
          _newPassword2Error = L10n.of(context).oopsSomethingWentWrong;
          _isLoading = false;
        });
        rethrow;
      }
    }
  }

  final TextEditingController currentPassword = TextEditingController();

  final TextEditingController newPassword = TextEditingController();

  final TextEditingController newPassword2 = TextEditingController();

  String oldPasswordFromMagicLink;

  void _goBack() {
    if (_isLoading) return;
    if (AdaptivePageLayout.of(context).canPop()) {
      AdaptivePageLayout.of(context).pop();
    } else {
      AdaptivePageLayout.of(context).pushNamedAndRemoveAllOthers(
          oldPasswordFromMagicLink != null ? '/backup' : '/');
    }
  }

  @override
  Widget build(BuildContext context) {
    if (Famedly.of(context).magicLink != null) {
      oldPasswordFromMagicLink = Famedly.of(context).magicLink.password;
      Famedly.of(context).magicLink = null;
    }
    return WillPopScope(
      onWillPop: () async {
        _goBack();
        return false;
      },
      child: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(FamedlyIconsV2.close),
            onPressed: _goBack,
          ),
          title: Text(L10n.of(context).changePassword),
        ),
        backgroundColor: Theme.of(context).secondaryHeaderColor,
        body: ListView(
          padding: FamedlyStyles.getTwoColumnListViewPadding(context),
          children: [
            SettingsListGroup(
              padding: EdgeInsets.all(16),
              children: <Widget>[
                if (oldPasswordFromMagicLink == null)
                  TextField(
                    controller: currentPassword,
                    autofocus: true,
                    obscureText: true,
                    autocorrect: true,
                    readOnly: _isLoading,
                    decoration: InputDecoration(
                      labelText: L10n.of(context).oldPassword,
                      hintText: '******',
                      errorText: _oldPasswordError,
                      border: OutlineInputBorder(),
                    ),
                  ),
                SizedBox(height: 16),
                TextField(
                  controller: newPassword,
                  autocorrect: true,
                  obscureText: true,
                  readOnly: _isLoading,
                  decoration: InputDecoration(
                    labelText: L10n.of(context).newPassword,
                    hintText: '******',
                    errorText: _newPasswordError,
                    border: OutlineInputBorder(),
                  ),
                ),
                SizedBox(height: 16),
                TextField(
                  controller: newPassword2,
                  obscureText: true,
                  autocorrect: true,
                  readOnly: _isLoading,
                  decoration: InputDecoration(
                    labelText: L10n.of(context).repeatPassword,
                    hintText: '******',
                    errorText: _newPassword2Error,
                    border: OutlineInputBorder(),
                  ),
                ),
                SizedBox(height: 16),
                Container(
                  width: double.infinity,
                  child: ElevatedButton(
                    onPressed: _isLoading
                        ? null
                        : () => _changePasswordAction(context),
                    child: _isLoading
                        ? LinearProgressIndicator()
                        : Text(L10n.of(context).changePassword),
                  ),
                ),
                SizedBox(height: 16),
                if (oldPasswordFromMagicLink == null)
                  Text(
                    L10n.of(context).changePasswordWarning,
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Colors.red),
                  ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
