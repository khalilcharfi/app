import 'package:famedly/utils/date_time_extension.dart';
import 'package:test/test.dart';

void main() {
  /// All Tests related to the DateTime extension
  group('DateTime', () {
    test('Comparing', () async {
      final originServerTs = DateTime.now().millisecondsSinceEpoch -
          (DateTimeExtension.minutesBetweenEnvironments - 1) * 1000 * 60;
      final oldOriginServerTs = DateTime.now().millisecondsSinceEpoch -
          (DateTimeExtension.minutesBetweenEnvironments + 1) * 1000 * 60;

      final chatTime = DateTime.fromMillisecondsSinceEpoch(originServerTs);
      final oldChatTime =
          DateTime.fromMillisecondsSinceEpoch(oldOriginServerTs);
      final nowTime = DateTime.now();

      expect(chatTime.millisecondsSinceEpoch, originServerTs);
      expect(nowTime.millisecondsSinceEpoch > chatTime.millisecondsSinceEpoch,
          true);
      expect(nowTime.sameEnvironment(chatTime), true);
      expect(nowTime.sameEnvironment(oldChatTime), false);

      expect(chatTime > oldChatTime, true);
      expect(chatTime < oldChatTime, false);
      expect(chatTime >= oldChatTime, true);
      expect(chatTime <= oldChatTime, false);
      expect(chatTime == chatTime, true);
      expect(chatTime == oldChatTime, false);
    });
  });
}
