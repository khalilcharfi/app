/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'package:famedly/components/default_modal_bottom_sheet.dart';
import 'package:flutter/material.dart';

import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:future_loading_dialog/future_loading_dialog.dart';
import '../models/tasks/tasklist.dart';
import '../styles/colors.dart';
import 'date_display.dart';

class AddTaskModal extends StatefulWidget {
  final TaskList taskList;

  AddTaskModal(this.taskList);

  @override
  State<StatefulWidget> createState() => AddTaskModalState();
}

class AddTaskModalState extends State<AddTaskModal> {
  final TextEditingController textController = TextEditingController();
  final TextEditingController descController = TextEditingController();
  bool showDescTextField = false;
  DateTime taskTime;

  void submit() async {
    final text2 = textController.text;
    final text = text2;
    if (text.isEmpty) return;
    final success = await showFutureLoadingDialog(
        context: context,
        future: () => widget.taskList.create(
            notificationTitle: L10n.of(context).upcomingTODO(text),
            title: text,
            description: descController.text,
            date: taskTime));
    if (success.error == null) {
      Navigator.of(context, rootNavigator: false).pop();
    }
  }

  @override
  Widget build(BuildContext context) {
    return DefaultModalBottomSheet(
      child: SingleChildScrollView(
        key: Key('AddTaskModal'),
        child: Container(
          height: 130.0 +
              (showDescTextField ? 38 : 0) +
              (taskTime != null ? 41 : 0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              TextField(
                key: Key('NewTaskTextField'),
                onTap: () {
                  setState(() {});
                },
                onSubmitted: (s) => submit(),
                controller: textController,
                autofocus: true,
                decoration: InputDecoration(
                  hintText: L10n.of(context).newTask,
                  hintStyle:
                      TextStyle(color: FamedlyColors.cloudyBlue, fontSize: 17),
                  contentPadding: EdgeInsets.all(25),
                  border: InputBorder.none,
                  disabledBorder: InputBorder.none,
                  enabledBorder: InputBorder.none,
                  errorBorder: InputBorder.none,
                  focusedBorder: InputBorder.none,
                  focusedErrorBorder: InputBorder.none,
                ),
              ),
              showDescTextField
                  ? TextField(
                      key: Key('DescriptionTextField'),
                      controller: descController,
                      style: TextStyle(fontSize: 13),
                      decoration: InputDecoration(
                        hintText: L10n.of(context).addDetails,
                        hintStyle: TextStyle(
                            color: FamedlyColors.cloudyBlue, fontSize: 13),
                        contentPadding: EdgeInsets.only(
                            left: 25, right: 25, top: 0, bottom: 15),
                        border: InputBorder.none,
                        disabledBorder: InputBorder.none,
                        enabledBorder: InputBorder.none,
                        errorBorder: InputBorder.none,
                        focusedBorder: InputBorder.none,
                        focusedErrorBorder: InputBorder.none,
                      ),
                    )
                  : Container(),
              taskTime != null
                  ? Padding(
                      padding: EdgeInsets.only(left: 20, right: 20),
                      child: DateDisplay(taskTime))
                  : Container(),
              Padding(
                padding: EdgeInsets.only(left: 10, right: 10),
                child: Row(
                  children: <Widget>[
                    IconButton(
                      key: Key('ShowDescTextFieldIconButton'),
                      icon: Icon(Icons.menu,
                          color: showDescTextField
                              ? FamedlyColors.slate
                              : FamedlyColors.azul),
                      onPressed: () {
                        setState(() {
                          showDescTextField = !showDescTextField;
                        });
                      },
                      iconSize: 20,
                    ),
                    IconButton(
                      icon:
                          Icon(Icons.calendar_today, color: FamedlyColors.azul),
                      onPressed: () async {
                        final date = await showDatePicker(
                            context: context,
                            firstDate: DateTime.now(),
                            lastDate: DateTime.fromMillisecondsSinceEpoch(
                                DateTime.now().millisecondsSinceEpoch +
                                    (1000 * 60 * 60 * 24 * 365 * 10)),
                            initialDate: DateTime.now());
                        final timeOfDay = await showTimePicker(
                          context: context,
                          initialTime: TimeOfDay.now(),
                        );
                        setState(() {
                          taskTime = DateTime(
                            date.year,
                            date.month,
                            date.day,
                            timeOfDay.hour,
                            timeOfDay.minute,
                          );
                        });
                      },
                      iconSize: 20,
                    ),
                    Spacer(),
                    TextButton(
                      key: Key('CreateTextButton'),
                      onPressed: submit,
                      child: Text(
                        L10n.of(context).create,
                        style: TextStyle(color: FamedlyColors.azul),
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
