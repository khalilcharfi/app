import 'package:famedly/models/roles/client_extension.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter_test/flutter_test.dart';

import 'fake_matrix_api.dart';

void main() {
  group('Roles', () {
    final client = Client('testclient', httpClient: FakeMatrixApi());
    client.checkHomeserver('https://fakeServer.notExisting');
    client.login(
        identifier: AuthenticationUserIdentifier(
          user: '@test:fakeServer.notExisting',
        ),
        password: '1234');

    test('getRoleAvailability', () async {
      final res = await client.getRoleAvailability(1);
      expect(res.available, true);
      expect(res.users.length, 3);
    });

    test('getOwnRoles', () async {
      final res = await client.getOwnRoles();
      expect(res.length, 1);
      expect(res[0].id, 1);
      expect(res[0].name, 'Test Role');
      expect(res[0].avatarUrl.toString(), 'mxc://example.com/avatar');
      expect(res[0].owner, '@owner:example.org');
      expect(res[0].users.length, 4);
      expect(res[0].parents.length, 0);
      expect(res[0].children.length, 0);
      expect(res[0].privateInvite, true);
      expect(res[0].publicInvite, true);
      expect(res[0].mxid, '@_botty_blah:example.org');
    });
  });
}
