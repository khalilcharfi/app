class RoleAvailability {
  final bool available;
  final List<String> users;
  final String welcomeMessage;

  RoleAvailability({
    this.available,
    this.users,
    this.welcomeMessage,
  });

  RoleAvailability.fromJson(Map<String, dynamic> json)
      : available = json['available'],
        users = json['users'].cast<String>(),
        welcomeMessage = json['welcome_message'];
}
