/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'package:famedly/models/requests/request.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/material.dart';

import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'localized_presence_extension.dart';
import '../models/room_type_extension.dart';
import '../models/patients/room_patients_extension.dart';
import 'matrix_locals.dart';

extension RoomStatusExtension on Room {
  Presence get directChatPresence => client.presences[directChatMatrixID];

  String getLocalizedFamedlyDisplayname(MatrixLocals matrixLocals) {
    switch (type) {
      case roomTypeRequest:
        return Request(this).description ??
            (membership == Membership.invite
                ? matrixLocals.l10n.newIncomingRequest
                : getLocalizedDisplayname(matrixLocals));
      case roomTypePatient:
        return (patientData?.name ?? matrixLocals.l10n.newPatient);
      default:
        return getLocalizedDisplayname(matrixLocals);
    }
  }

  String getLocalizedStatus(BuildContext context) {
    if (isDirectChat) {
      if (directChatPresence != null) {
        return directChatPresence.getLocalized(context);
      }
      return L10n.of(context).lastSeenLongTimeAgo;
    }
    if (topic?.isNotEmpty ?? false) {
      return topic;
    }
    return L10n.of(context).numMembers(mJoinedMemberCount);
  }

  String getLocalizedTypingText(BuildContext context) {
    var typingText = '';
    final typingUsers = this.typingUsers;
    typingUsers.removeWhere((User u) => u.id == client.userID);

    if (typingUsers.length == 1) {
      typingText = L10n.of(context).isTyping;
      if (typingUsers.first.id != directChatMatrixID) {
        typingText =
            L10n.of(context).userIsTyping(typingUsers.first.calcDisplayname());
      }
    } else if (typingUsers.length > 1) {
      typingText = L10n.of(context).userAndOthersAreTyping(
          typingUsers.first.calcDisplayname(),
          (typingUsers.length - 1).toString());
    }
    return typingText;
  }
}
