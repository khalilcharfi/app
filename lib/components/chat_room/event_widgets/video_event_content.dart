import 'dart:io';

import 'package:adaptive_page_layout/adaptive_page_layout.dart';
import 'package:chewie/chewie.dart';
import 'package:famedly/config/chat_configs.dart';
import 'package:famedly/styles/colors.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_blurhash/flutter_blurhash.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:path_provider/path_provider.dart';
import 'package:universal_html/html.dart' as html;
import 'package:video_player/video_player.dart';

import 'event_widget.dart';

class VideoEventContent extends StatefulWidget {
  final Event videoEvent;
  final bool isReply;

  const VideoEventContent(this.videoEvent, {Key key, bool isReply = false})
      : isReply = isReply,
        super(key: key);

  @override
  _VideoEventContentState createState() => _VideoEventContentState();
}

class _VideoEventContentState extends State<VideoEventContent> {
  MatrixFile _thumbnail;
  String _error;
  bool _showVideo = false;
  VideoPlayerController _videoController;
  ChewieController _chewieController;

  @override
  void initState() {
    final loadThumbnail = widget.videoEvent.hasThumbnail &&
        (kIsWeb ||
            (widget.videoEvent.content['info'] is Map &&
                widget.videoEvent.content['info']['thumbnail_info'] is Map &&
                widget.videoEvent.content['info']['thumbnail_info']['size'] !=
                    null &&
                widget.videoEvent.content['info']['thumbnail_info']['size'] <
                    ChatConfigs.maxAutoLoadImageSize) ||
            (widget.videoEvent.content['info'] is Map &&
                widget.videoEvent.content['info']['size'] != null &&
                widget.videoEvent.content['info']['size'] <
                    ChatConfigs.maxAutoLoadImageSize));

    if (loadThumbnail) {
      widget.videoEvent
          .downloadAndDecryptAttachment(
            getThumbnail: true,
          )
          .then((value) => setState(() => {_thumbnail = value}))
          .catchError((error) {}); //just ignore error
    }
    super.initState();
  }

  @override
  void dispose() {
    _videoController?.dispose();
    _chewieController?.dispose();
    _videoController = null;
    _chewieController = null;
    super.dispose();
  }

  Future<void> fetchVideoAndInitializePlayer() async {
    if (!_showVideo) {
      try {
        setState(() => {_showVideo = true});
        final matrixFile =
            await widget.videoEvent.downloadAndDecryptAttachment();

        if (kIsWeb) {
          final blob = html.Blob([matrixFile.bytes]);
          final url = html.Url.createObjectUrlFromBlob(blob);
          _videoController = VideoPlayerController.network(url);
        } else {
          final directory = await getTemporaryDirectory();
          await Directory(
                  '${directory.path}/${widget.videoEvent.eventId.substring(1)}')
              .create();
          final tmpFile = await File(
                  '${directory.path}/${widget.videoEvent.eventId.substring(1)}/${matrixFile.name}')
              .writeAsBytes(matrixFile.bytes);
          _videoController = VideoPlayerController.file(tmpFile);
        }
        if (mounted) {
          await _videoController.initialize();
          _chewieController = ChewieController(
            videoPlayerController: _videoController,
            autoPlay: true,
            allowPlaybackSpeedChanging: false,
            allowMuting: false,
            allowFullScreen: !kIsWeb, // is buggy in web
            errorBuilder: (context, error) => Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Icon(Icons.broken_image, size: 40),
                Text(
                  L10n.of(context).oopsSomethingWentWrong,
                ),
                Text(error.toString()),
              ],
            ),
          );
          setState(() {});
        }
      } catch (e) {
        setState(() {
          _error = e.toString();
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    final size = EventWidget.getMaxWidth(
        context, AdaptivePageLayout.of(context).columnMode(context),
        maxMode: false);
    final height = widget.isReply ? size / 4 : size;
    var box = Size(size, height);

    // fit the image inside the maximum allowed size, if it does not have an absurd aspect ratio
    if (widget.videoEvent.content['info'] is Map &&
        widget.videoEvent.content['info']['h'] != null &&
        widget.videoEvent.content['info']['w'] != null) {
      final h = widget.videoEvent.content['info']['h'];
      final w = widget.videoEvent.content['info']['w'];

      if (h > 10 && w > 10) {
        final maxAspect = 16 / 9;
        final realAspect = h / w;
        if (realAspect <= maxAspect && realAspect >= 1 / maxAspect) {
          // fits within max aspect ratio
          box = applyBoxFit(BoxFit.contain, Size(w.toDouble(), h.toDouble()),
                  Size(size, height * maxAspect))
              .destination;
        } else if (realAspect > maxAspect) {
          // wider than max aspect ratio
          box = Size(size, size / maxAspect);
        } else {
          // higher than max aspect ratio
          box = Size(size / maxAspect, size);
        }
      }
    }
    final _colors = <Color>[FamedlyColors.aquaGreen, FamedlyColors.azul];

    if (_error != null) {
      return Container(
        width: box.width,
        height: box.height,
        alignment: Alignment.center,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Icon(Icons.broken_image, size: 40),
            Text(
              L10n.of(context).oopsSomethingWentWrong,
            ),
            Text(_error.toString()),
          ],
        ),
      );
    } else if (_chewieController == null ||
        !_chewieController.videoPlayerController.value.isInitialized) {
      return InkWell(
        onTap: () => fetchVideoAndInitializePlayer(),
        child: Container(
          width: box.width,
          height: box.height,
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: _colors,
              end: Alignment.bottomRight,
            ),
          ),
          child: Stack(
            children: [
              if (_thumbnail != null)
                Image.memory(
                  _thumbnail.bytes,
                  width: box.width,
                  height: box.height,
                  fit: BoxFit.cover,
                )
              else if (widget.videoEvent.content['info'] is Map &&
                  widget.videoEvent.content['info']['xyz.amorgan.blurhash']
                      is String)
                BlurHash(
                    hash: widget.videoEvent.content['info']
                        ['xyz.amorgan.blurhash']),
              Center(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    if (_showVideo)
                      CircularProgressIndicator()
                    else
                      CircleAvatar(
                        backgroundColor:
                            Theme.of(context).dialogBackgroundColor,
                        radius: 32,
                        child: Icon(
                          Icons.play_arrow,
                          color: Colors.black,
                        ),
                      ),
                    if (!_showVideo) SizedBox(height: 4),
                    if (!_showVideo &&
                        widget.videoEvent.content['info'] is Map &&
                        widget.videoEvent.content['info']['size'] is num)
                      Text(
                          '${(widget.videoEvent.content['info']['size'] / 1000).round()} kB'),
                  ],
                ),
              ),
            ],
          ),
        ),
      );
    } else {
      return InkWell(
          child: Container(
              width: box.width,
              height: box.height,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: _colors,
                  end: Alignment.bottomRight,
                ),
              ),
              child: Scaffold(
                body: Center(
                    child: Chewie(
                  controller: _chewieController,
                )),
              )));
    }
  }
}
