import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/material.dart';

import '../avatar.dart';
import '../../utils/localized_presence_extension.dart';
import '../../models/famedly.dart';

/// Generates the widget for each room item.
class UserItem extends StatelessWidget {
  final Profile contact;
  final String subtitle;

  final VoidCallback onTap;

  UserItem({
    key,
    @required this.contact,
    @required this.onTap,
    this.subtitle,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Avatar(contact.avatarUrl, size: 45, name: contact.displayname),
      title: Text(contact.displayname),
      subtitle: Text(subtitle ??
          Famedly.of(context)
              .client
              .presences[contact.userId]
              ?.getLocalized(context) ??
          L10n.of(context).lastSeenLongTimeAgo),
      onTap: onTap,
    );
  }
}
