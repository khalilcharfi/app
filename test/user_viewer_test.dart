import 'package:famedly/views/user_viewer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

import 'utils.dart';

void main() {
  /// All Tests related to the UserItem

  group(
    'UserViewer',
    () {
      NavigatorObserver mockObserver;

      setUp(() {
        mockObserver = MockNavigatorObserver();
      });

      testWidgets(
        'has default State Items',
        (WidgetTester tester) async {
          await tester.runAsync(() async {
            // Create widget
            await tester.pumpWidget(
              await Utils.getWidgetWrapper(
                UserViewer(
                  roomId: '!726s6s6q:example.com',
                  userId: '@alice:example.com',
                ),
                mockObserver,
              ),
            );

            await tester.pumpAndSettle();

            // Title has username
            expect(find.text('Alice Margatroid'), findsOneWidget);

            // Avatar
            expect(find.byKey(Key('Avatar')), findsOneWidget);

            // 3 Action Buttons
            expect(
                find.byKey(Key('ChatProfileScaffoldButton')), findsOneWidget);
            expect(find.byKey(Key('CallProfileScaffoldVideoButton')),
                findsOneWidget);
            expect(find.byKey(Key('NotificationOnProfileScaffoldButton')),
                findsOneWidget);

            // Media Section
            expect(find.byKey(Key('ImagesSettingsMenuItem')), findsOneWidget);
            expect(find.byKey(Key('VideosSettingsMenuItem')), findsOneWidget);
            expect(
                find.byKey(Key('DocumentsSettingsMenuItem')), findsOneWidget);
          });
        },
      );

      testWidgets(
        'has clickable image media buttons',
        (WidgetTester tester) async {
          await tester.runAsync(() async {
            // Create widget
            await tester.pumpWidget(
              await Utils.getWidgetWrapper(
                UserViewer(
                  roomId: '!726s6s6q:example.com',
                  userId: '@alice:example.com',
                ),
                mockObserver,
              ),
            );

            await tester.pumpAndSettle();

            // Media Section
            expect(find.byKey(Key('ImagesSettingsMenuItem')), findsOneWidget);

            await Utils.tapItem(tester, Key('ImagesSettingsMenuItem'));

            verify(mockObserver.didPush(any, any));
          });
        },
      );

      testWidgets(
        'has clickable video media buttons',
        (WidgetTester tester) async {
          await tester.runAsync(() async {
            // Create widget
            await tester.pumpWidget(
              await Utils.getWidgetWrapper(
                UserViewer(
                  roomId: '!726s6s6q:example.com',
                  userId: '@alice:example.com',
                ),
                mockObserver,
              ),
            );

            await tester.pumpAndSettle();

            expect(find.byKey(Key('VideosSettingsMenuItem')), findsOneWidget);
            await Utils.tapItem(tester, Key('VideosSettingsMenuItem'));
            verify(mockObserver.didPush(any, any));
          });
        },
      );

      testWidgets(
        'has clickable documents media buttons',
        (WidgetTester tester) async {
          await tester.runAsync(() async {
            // Create widget
            await tester.pumpWidget(
              await Utils.getWidgetWrapper(
                UserViewer(
                  roomId: '!726s6s6q:example.com',
                  userId: '@alice:example.com',
                ),
                mockObserver,
              ),
            );

            await tester.pumpAndSettle();

            expect(
                find.byKey(Key('DocumentsSettingsMenuItem')), findsOneWidget);
            await Utils.tapItem(tester, Key('DocumentsSettingsMenuItem'));
            verify(mockObserver.didPush(any, any));
          });
        },
      );
    },
  );
}
