/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
abstract class StaticInAppDocuments {
  static const String termsOfUse = '''
Bedingungen für die Nutzung von Famedly

§ 1 Vertragsgegenstand und Geltungsbereich

(1) Die Famedly GmbH (nachfolgend „Anbieter“) hat eine digitale Kommunikationslö¬sung (nachfolgend „Famedly“) für Unternehmen aus der Gesundheitsbranche (nachfolgend „Kunden“) entwickelt. Mitarbeiter der Kunden sowie zugelassene Einzelpersonen, die vom Anbieter freigeschaltet worden sind (nachfolgend „autorisierte Nutzer“), können Famedly für die interne und externe Kommunikation sowie zum Austausch von Doku¬menten nutzen. Kunden, die mit dem Anbieter Vertrag über die Bereitstellung von Famedly abgeschlossen haben (nachfolgend „Kundenvertrag“) können während der Laufzeit des Kundenvertrags im vertraglich vereinbarten Umfang bestimmte Personen für die Nutzung von Famedly zu autorisieren.
(2) Das Teilnahmeangebot des Anbieters richtet sich nur an Personen, die das 18. Lebensjahr vollendet haben.
(3) Die vorliegenden Nutzungsbedingungen regeln die vertraglichen Rechtsbeziehungen zwischen dem Anbieter und dem autorisierten Nutzer (nachfolgend auch „Nutzungsvertrag“).
(4) Sofern die Parteien nicht ausdrücklich etwas anderes vereinbaren, stellt der Anbieter dem autorisierten Nutzer Leistungen nach diesem Nutzungsvertrag unentgeltlich zur Verfügung.

§ 2 Verhältnis Kunden und autorisierten Nutzern

Der Kunde steht für Handlungen und Unterlassungen der von ihm autorisierten Nutzer wie für eigene Handlungen und Unterlassungen ein und verpflichtet sie zur vertragsgemäßen Nutzung von Famedly. Es liegt ausschließlich in der Verantwortung des Kunden, 
•	die autorisierten Nutzer über alle relevanten Vereinbarungen und Einstellungen zu informieren, die die Verarbeitung der Kundendaten beeinträchtigen könnten,
•	die autorisierten Nutzer insbesondere darüber zu informieren, dass durch die Verwendung eines dezentralen Protokolls auch Inhalte auf Server anderer Famedly-Kunden repliziert werden,
•	alle Rechte, Genehmigungen oder Zustimmungen von den autorisierten Nutzern einzuholen, die für die rechtmäßige Benutzung der Kundendaten und den Betrieb von Famedly erforderlich sind,
•	zu gewährleisten, dass die Übertragung und Verarbeitung von Kundendaten im Rahmen des Kundenvertrags rechtmäßig ist und
•	auf alle Streitfälle mit autorisierten Nutzern im Zusammenhang mit den Kundendaten, Famedly oder der Nichteinhaltung der Nutzungsbedingungen zu reagieren und sie beizulegen. 

§ 3 Registrierung zu Famedly und Vertragsschluss

(1) Voraussetzung für die Nutzung von Famedly ist die Registrierung sowie die Zustimmung zu diesen Nutzungsbedingungen. Die bei der Registrierung abgefragten Daten sind vollständig und korrekt anzugeben. Der Vertrag zwischen dem Nutzer und dem Anbieter kommt zustande, sobald die Registrierung abgeschlossen ist und der Nutzer den Nutzungsbedingungen zugestimmt hat.
(2) Der autorisierte Nutzer ist verpflichtet, die im Rahmen des Registrierungsprozesses angegebenen Daten aktuell zu halten. Tritt während der Laufzeit der Nutzungsvereinbarung eine Änderung ein, so hat der autorisierte Nutzer die entsprechenden Angaben in den Einstellungen unverzüglich zu korrigieren. Können die Angaben in den Einstellungen nicht geändert werden, ist der autorisierte Nutzer verpflichtet, die geänderten Daten in Textform mitzuteilen. Dies gilt insbesondere für den Fall, dass ein Nutzer – etwa infolge eines Arbeitgeberwechsels – nicht mehr für die Nutzung autorisiert ist. 

§ 4 Umfang der erlaubten Nutzung und Verfügbarkeit

(1) Der Anbieter räumt dem autorisierten Nutzer ein auf die Laufzeit des Nutzungsvertrags beschränktes, widerrufliches, nicht ausschließliches und nicht übertragbares Nutzungsrecht an Famedly und den darin bereitgestellten Inhalten und Funktionen ein. 
(2) Der Anspruch auf Nutzung der über Famedly verfügbaren Funktionen besteht nur im Rahmen der technischen und betrieblichen Möglichkeiten des Anbieters. Der Anbieter bemüht sich um eine möglichst unterbrechungsfreie Nutzbarkeit des Portals und seiner Funktionen. Jedoch können durch technische Störungen (wie z.B. Unterbrechung der Stromversorgung, Hardware- und Softwarefehler, technische Probleme in den Datenleitungen) zeitweilige Beschränkungen oder Unterbrechungen auftreten.
(3) Um Famedly nutzen zu können, benötigt der autorisierte Nutzer ein Endgerät, das die gültigen System- und Kompatibilitätsanforderungen erfüllt sowie über einen funktionierenden Internetzugang und kompatible Software verfügt.
(4) Sofern der Anbieter während der Laufzeit neue Versionen, Upgrades oder Updates an Famedly vornimmt, gelten die vorstehenden Rechte auch für diese. Der autorisierte Nutzer ist dafür verantwortlich, in seinem Einflussbereich die technischen Voraussetzungen für die Nutzung der über Famedly verfügbaren Funktionen zu schaffen.

§ 5 Funktion „Globales Nutzerverzeichnis“

Sofern der Kunde die Funktion „Globales Nutzerverzeichnis“ für die ihm zugeordneten Nutzer aktiviert hat, kann der autorisiert Nutzer über Famedly auch von solchen Nutzern gefunden werden, die einem anderen Kunden zugeordnet sind (etwa weil sie einem anderen Unternehmen angehören). Hierzu wird anderen Nutzern von Famedly der Nutzername des autorisierten Nutzers angezeigt. Der autorisierte Nutzer kann diese Funktion jederzeit in den Einstellungen deaktivieren.

§ 6 Pflichten der autorisierten Nutzer

Der autorisierte Nutzer hat insbesondere folgende Pflichten:
a)	Die bereitgestellten Funktionen dürfen nicht missbräuchlich oder rechtswidrig genutzt werden, insbesondere
•	dürfen keine gesetzlich verbotenen Inhalte hochgeladen, verwendet oder übersandt werden, wie z. B. unerwünschte und unverlangte Werbung.
•	dürfen keine Informationen mit rechts- oder sittenwidrigen Inhalten übermittelt oder eingestellt werden und es darf nicht auf solche Informationen hingewiesen werden. Dazu zählen vor allem Informationen, die im Sinne der §§ 130, 130a und 131 StGB der Volksverhetzung dienen, zu Straftaten anleiten oder Gewalt verherrlichen oder verharmlosen, sexuell anstößig sind, im Sinne des § 184 StGB pornografisch sind, geeignet sind, Kinder oder Jugendliche sittlich schwer zu gefährden oder in ihrem Wohl zu beeinträchtigen oder das Ansehen des Anbieters schädigen können. 
•	ist dafür Sorge zu tragen, dass durch die Inanspruchnahme einzelner Funktionalitäten und insbesondere durch die Einstellung oder das Teilen von Inhalten keinerlei Beeinträchtigungen für den Anbieter oder Dritte entstehen. 
•	sind die nationalen und internationalen Urheber- und Marken-, Patent-, Namens- und Kennzeichenrechte sowie sonstigen gewerblichen Schutzrechte und Persönlichkeitsrechte Dritter zu beachten.
b)	Persönliche Zugangsdaten dürfen nicht an Dritte weitergegeben werden und sind vor dem Zugriff durch Dritte geschützt aufzubewahren. Zugangsdaten sollten zur Sicherheit in regelmäßigen Abständen geändert werden. Soweit Anlass zu der Vermutung besteht, dass unberechtigte Personen von den Zugangsdaten Kenntnis erlangt haben, hat der autorisierte Nutzer diese unverzüglich zu ändern.
c)	Der Anbieter und seine Erfüllungsgehilfen sind von sämtlichen Ansprüchen Dritter freizustellen, die auf einer rechtswidrigen Verwendung von Famedly und der hiermit verbundenen Leistungen durch den autorisierten Nutzer beruhen oder mit seiner Billigung erfolgen oder die aus datenschutzrechtlichen, urheberrechtlichen oder sonstigen Pflichtverstößen des autorisierten Nutzers resultieren. Erkennt der autorisierte Nutzer oder muss er erkennen, dass ein solcher Verstoß droht, besteht die Pflicht zur unverzüglichen Unterrichtung des Anbieters. Eine Pflicht zur Freistellung besteht nicht, sofern der autorisierte Nutzer die Pflichtverletzung nicht zu vertreten hat.
d)	Der autorisierte Nutzer stellt sicher, dass sich auf seinen Geräten keine Schadsoftware (z.B. Computerviren, Trojaner, etc.) befinden, die zu Schäden oder Beeinträchtigungen der Hard- oder Software des Anbieters oder über Famedly vernetzte autorisierte Nutzer führen können. Entsprechendes gilt bezüglich der vom autorisierten Nutzer verwendeten Fremdsoftware einschließlich besonderer Verschlüsselungssoftware. 

§ 7 Haftungsbeschränkung

(1) Der Anbieter haftet aus diesen Nutzungsbedingungen nur bei Verletzung einer vertragswesentlichen Pflicht, deren Verletzung die Erreichung des Vertragszwecks gefährdet oder deren Erfüllung die ordnungsgemäße Durchführung des Vertrags erst ermöglicht und auf deren Einhaltung der autorisierte Nutzer regelmäßig vertrauen darf (sog. „Kardinalpflicht“), und nur für den vertragstypischen, voraussehbaren Schaden. 
(2) Die vorstehende Haftungsbeschränkung gilt nicht bei Schäden, die auf Vorsatz oder grober Fahrlässigkeit beruhen, Personenschäden (Verletzung von Leben, Körper, Gesundheit) sowie Garantieübernahmen.
(3) Der Anbieter haftet nicht, wenn die einen Anspruch gegen den Anbieter begründenden Umstände auf einem ungewöhnlichen und unvorhersehbaren Ereignis beruhen, auf das der Anbieter keinen Einfluss hat und dessen Folgen trotz Anwendung der gebotenen Sorgfalt nicht hätten vermieden werden können oder von dem Anbieter auf Grund einer gesetzlichen Verpflichtung herbeigeführt wurden.
(4) Der Anbieter haftet ferner nicht für Ausfälle oder Störungen in der außerhalb des Verantwortungsbereichs von dem Anbieter liegenden technischen Infrastruktur (höhere Gewalt). 
(5) Gesetzliche Haftungsbeschränkungen im Zusammenhang mit unentgeltlichen Leistungen des Anbieters (z.B. Haftungsbeschränkung auf Vorsatz und grobe Fahrlässigkeit nach § 599 BGB) bleiben unberührt.
(6) Eine Haftung nach dem Produkthaftungsgesetz bleibt unberührt.

§ 8 Laufzeit 

(1) Wurde der autorisierte Nutzer auf Grundlage eines Kundenvertrags freigeschaltet, endet der Nutzungsvertrag spätestens mit Ablauf des jeweiligen Kundenvertrags. 
(2) Im Übrigen kann der Vertrag von jeder Partei jederzeit gekündigt werden. Jede Kündigung muss in Textform erfolgen. Abweichend hiervon kann die Kündigung durch den Anbieter auch durch Sperrung des jeweiligen Nutzerkontos erfolgen.
(3) Nach Beendigung des Nutzungsvertrags ist der Anbieter berechtigt, sämtliche Daten, Einstellungen und Zugangsdaten des Nutzers zu löschen. 
 
§ 9 Änderungen der Nutzungsbedingungen

Der Anbieter behält sich vor, diese Nutzungsbedingungen jederzeit mit Wirksamkeit auch innerhalb der bestehenden Vertragsverhältnisse zu ändern. Über derartige Änderungen setzt der Anbieter die autorisierten Nutzer mindestens 30 Kalendertage vor dem geplanten Inkrafttreten der Änderungen in Kenntnis. Sofern der autorisierte Nutzer nicht innerhalb von 30 Tagen ab Zugang der Mitteilung widerspricht und die Inanspruchnahme von Famedly auch nach Ablauf der Widerspruchsfrist fortsetzt, so gelten die Änderungen ab Fristablauf als wirksam vereinbart. Im Fall eines Widerspruchs wird der Vertrag zu den bisherigen Bedingungen fortgesetzt. In der Änderungsmitteilung wird der Anbieter den autorisierten Nutzer auf sein Widerspruchsrecht und auf die Folgen hinweisen.

§ 10 Streitbeilegungsverfahren

(1) Der Anbieter nimmt nicht an Streitbeilegungsverfahren vor einer Verbraucherschlichtungsstelle teil. Dem Anbieter ist vielmehr daran gelegen, Streitigkeiten mit Nutzern im direkten Kontakt zu klären. Nutzer können sich hierzu an Dr. Phillipp Kurtz, c/o Famedly GmbH, Altensteinstraße 40, 14195 Berlin, E-Mail: p.kurtz@famedly.com wenden. 
(2) Informationen zur Online-Streitbeilegung nach Artikel 14 Abs. 1 der EU-Verordnung über Online-Streitbeilegung in Verbraucherangelegenheiten (ODR-VO): Die EU-Kommission stellt eine Plattform zur Online-Streitbeilegung (OS-Plattform) verbraucherrechtlicher Streitigkeiten, die aus Online-Kaufverträgen und Online-Dienstleistungsverträgen resultieren, bereit. Diese Plattform ist im Internet unter http://ec.europa.eu/consumers/odr/ erreichbar.

§11 Abschlussbestimmungen

(1) Die Nutzungsbedingungen unterliegen dem Recht der Bundesrepublik.
(2) Gerichtsstand für alle aus den Nutzungsbedingungen entstehenden Streitigkeiten zwischen dem Anbieter und den autorisierten Nutzer ist, soweit eine solche Gerichtsstandsvereinbarung zulässig ist, Berlin. 
(3) Sollte eine Bestimmung dieser Nutzungsbedingungen unwirksam sein oder werden, bleibt die Wirksamkeit der übrigen Bestimmungen hiervon unberührt. 
***
''';

  static const String privacyPolicyText = '''
# Datenschutzerklärung Famedly
*Fassung vom 26.04.2021*
 
## 1. Allgemeine Hinweise
Diese Datenschutzerklärung klärt über die Art, den Umfang und Zweck der Verarbeitung von personenbezogenen Daten innerhalb unserer App und der mit diesen verbundenen Funktionen und Inhalte auf. Zu Definitionen verwendeten Begrifflichkeiten wie z. B. „personenbezogene Daten“ oder deren „Verarbeitung“, verweisen wir auf die Definitionen des Art. 4 Datenschutzgrundverordnung (DSGVO).\
Diese Datenschutzerklärung ist jederzeit unter dem Menüeintrag “Mehr” in der Rubrik “Über Famedly” innerhalb der App aufrufbar.
 
### 1.1 Verantwortlicher für die Datenerfassung
Famedly GmbH\
Prenzlauer Allee 36g\
10405 Berlin\
E-Mail: info@famedly.com
 
### 1.2 Unser Datenschutzbeauftragter ist unter folgenden Kontaktdaten zu erreichen: 
 
PROLIANCE GmbH / [www.datenschutzexperte.de](https://www.datenschutzexperte.de)\
Datenschutzbeauftragter\
Leopoldstr. 21\
80802 München\
datenschutzbeauftragter@datenschutzexperte.de
 
## 2. Datenerfassung in unserer App
Wir, die Famedly GmbH, nehmen den Schutz Ihrer persönlichen Daten sehr ernst. Wir behandeln Ihre personenbezogenen Daten vertraulich und entsprechend der gesetzlichen Datenschutzvorschriften sowie dieser Datenschutzerklärung.
Wenn Sie sich für den von uns angebotenen Dienst registrieren und nutzen, werden verschiedene personenbezogene Daten erhoben. Personenbezogene Daten sind Daten, mit denen Sie persönlich identifiziert werden können. Die vorliegende Datenschutzerklärung erläutert, welche Daten wir erheben und wofür wir sie nutzen. Sie erläutert auch, wie und zu welchem Zweck das geschieht.\
Wir weisen darauf hin, dass die Datenübertragung im Internet (z.B. bei der Kommunikation per E-Mail) Sicherheitslücken aufweisen kann. Ein lückenloser Schutz der Daten vor dem Zugriff durch Dritte ist nicht möglich.
 
## 3. Wie erfassen wir Ihre Daten?
Bestimmte Informationen werden bereits automatisch verarbeitet, sobald Sie die App verwenden. Welche personenbezogenen Daten genau verarbeitet werden, haben wir im Folgenden für Sie aufgeführt:
 
### 3.1 Informationen, die beim Download erhoben werden
Beim Download der App werden bestimmte erforderliche Informationen an den von Ihnen ausgewählten App Store (Apple App Store, Google Play Store) übermittelt, insbesondere können dabei der Nutzername, die E-Mail-Adresse, die Kundennummer Ihres Accounts, der Zeitpunkt des Downloads, Zahlungsinformationen sowie die individuelle Gerätekennziffer verarbeitet werden. Die Verarbeitung dieser Daten erfolgt ausschließlich durch den jeweiligen App Store und liegt außerhalb unseres Einflussbereiches.

### 3.2 Informationen, die automatisch erhoben werden
Im Rahmen Ihrer Nutzung der App erheben wir bestimmte Daten automatisch, die für die Nutzung der App erforderlich sind. Hierzu gehören: [interne Geräte-ID, Version Ihres Betriebssystems, Zeitpunkt des Zugriffs, IP-Adresse, Inhalt des Zugriffs, Nutzerpseudonym (localpart der gehashten mxid)]
Diese Daten werden automatisch an uns übermittelt, aber nicht gespeichert, (1) um den Dienst und die damit verbundenen Funktionen zur Verfügung zu stellen; (2) die Funktionen und Leistungsmerkmale der App zu verbessern und (3) Missbrauch sowie Fehlfunktionen vorzubeugen und zu beseitigen. Diese Datenverarbeitung ist dadurch gerechtfertigt, dass (1) die Verarbeitung für die Erfüllung des Vertrags zwischen Ihnen als Betroffener und uns gemäß Art. 6 Abs. 1 lit. b DSGVO zur Nutzung der App erforderlich ist, oder (2) wir ein berechtigtes Interesse daran haben, die Funktionsfähigkeit und den fehlerfreien Betrieb der App zu gewährleisten und einen markt- und interessengerechten Dienst anbieten zu können, dass hier Ihre Rechte und Interessen am Schutz Ihrer personenbezogenen Daten im Sinne von Art. 6 Abs. 1 lit. f DSGVO überwiegt.

### 3.3 Verantwortlichkeit der Famedly GmbH, Serverhosting
Die Famedly-App wird in der Regel auf von Ihrer Einrichtung gehosteten Servern betrieben. In diesem Fall hat Famedly keinerlei Zugriff auf Ihre dort gespeicherten personenbezogenen Daten. Ausnahmen bilden die nachfolgend beschriebenen Verarbeitungen, insbesondere das „globale Nutzerverzeichnis“. Die datenschutzrechtlichen Verpflichtungen von Famedly sowie der Gesundheitseinrichtungen werden gesondert vertraglich nach den gesetzlichen Vorschriften festgelegt.\
Für den Fall, dass die Gesundheitseinrichtung keine eigenen Server bereitstellt, sowie für das globale Nutzerverzeichnis werden die Server von Famedly bereitgestellt. Hierzu werden die Dienste der Hetzner Online GmbH, Industriestr. 25, 91710 Gunzenhausen, in Anspruch genommen. Dieser Dienstleister unterliegt ebenfalls den strengen Datenschutzanforderungen von Famedly, ferner wurde ein Vertrag zu Auftragsverarbeitung abgeschlossen.
 
### 3.4 Erstellung eines Nutzeraccounts (Registrierung) und Anmeldung
Sofern Ihre Institution keinen eigengehosteten Server für Famedly betreibt, erheben und verarbeiten wir personenbezogene Daten für die Nutzerregistrierung.\
Die Verarbeitung der bei der Registrierung eingegebenen Daten erfolgt auf Grundlage Ihrer uns freiwillig gegebenen Einwilligung (Art. 6 Abs. 1 lit. a DSGVO).\
Sie können eine von Ihnen erteilte Einwilligung jederzeit für die Zukunft widerrufen. Dazu reicht eine formlose Mitteilung per E-Mail an uns (info@famedly.de). Die Rechtmäßigkeit der bereits erfolgten Datenverarbeitung bleibt vom Widerruf unberührt. Mit dem Widerruf ist die Famedly App für Sie nicht mehr funktionsfähig, da das Angebot nur
für die Dauer der vorstehend erteilten Einwilligungserklärungen funktioniert.
Auch nach einem Widerruf verwendet die Famedly GmbH anonymisiert von Ihnen gebildete Daten, d.h. solche Daten, die zu keinem Zeitpunkt einen unmittelbaren oder mittelbaren
Rückschluss auf Ihre Person zulassen, zeitlich unbegrenzt für eigene Zwecke weiter.\
Wenn Sie einen Nutzeraccount erstellen oder sich anmelden, verwenden wir Ihre Zugangsdaten (Vor- und Nachname (Benutzername) und Passwort), um Ihnen den Zugang zu Ihrem Nutzeraccount zu gewähren und diesen zu verwalten („Pflichtangaben“). Pflichtangaben sind als solche gekennzeichnet und für den Abschluss des Nutzungsvertrages erforderlich. Wenn Sie diese Daten nicht angeben, können Sie keinen Nutzeraccount erstellen.
Darüber hinaus können Sie folgende freiwillige Angaben im Rahmen der Registrierung machen:
 
* E-Mail-Adresse
* Profilfoto
 
Des Weiteren werden folgende Daten bei Verwendung der App erhoben und auf dem Server Ihrer Einrichtung, nicht jedoch von der Famedly GmbH, verarbeitet:

* Datum Ihrer Registrierung
* Datum Ihres letzten Logins

Der Umfang der Famedly App erfassten Daten hängt von Ihrer Registrierung und Nutzung unserer Produkte ab. Wir verarbeiten nur jene Nutzerdaten, die Sie gegenüber der Famedly App aktiv und freiwillig angeben. Die Eingabe abgefragter Nutzerdaten ist allerdings Voraussetzung für die umfassende Nutzung unserer Produkte. Wenn Sie optionale Daten nicht eingeben, ist die davon abhängige Funktionalität unserer Produkte entsprechend eingeschränkt.
Diese Datenverarbeitung ist dadurch gerechtfertigt, dass (1) die Verarbeitung für die Erfüllung des Vertrags zwischen Ihnen als Betroffener und uns gemäß Art. 6 Abs. 1 lit. b DSGVO zur Nutzung der App erforderlich ist, oder (2) wir ein berechtigtes Interesse daran haben, die Funktionsfähigkeit und den fehlerfreien Betrieb der App zu gewährleisten, das hier Ihre Rechte und Interessen am Schutz Ihrer personenbezogenen Daten im Sinne von Art. 6 Abs. 1 lit. f DSGVO überwiegt.

## 4. Nutzung der App
Im Rahmen der App können Sie diverse Informationen, Aufgaben und Aktivitäten eingeben, verwalten und bearbeiten. Diese Informationen umfassen insbesondere Daten über Ihre Tätigkeit in der Gesundheitseinrichtung, die den Famedly-Server verwaltet, auf welchem Ihr Account gehostet wird. Im Speziellen handelt es sich dabei um die folgenden Informationen: 
* Ihre Nutzerinformationen: Der Administrator Ihrer Einrichtung legt Ihren Nutzerzugang unter Angabe Ihres vollständigen Namens und optional Ihrer E-Mail-Adresse an. Darüber hinaus können Sie auch andere Informationen zu Ihrem Nutzerzugang hinzufügen (insbesondere ein Profilbild).
* Ihre Nachrichten: Sämtliche Textnachrichten, Bilddateien, Dokumente oder Sprachnachrichten, die Sie in die App eingeben oder von anderen Nutzern erhalten wird zwecks Zustellung zu Ihrem Kommunikationspartner Ende-zu-Ende-verschlüsselt auf den Server Ihrer und ggf. der empfangenden Einrichtung gespeichert. Ein Zugriff auf diese Informationen ist somit weder Famedly noch etwaigen Dritten möglich.

Zusätzlich dazu können Sie optional weitere Daten mit Famedly teilen:
* Nutzungs- und Absturzdaten: Mit Ihrer Einwilligung erheben wir pseudonymisierte Nutzungsinformationen. Die Einwilligung können Sie in den Einstellungen der App erteilen. Weitere Informationen entnehmen Sie Punkt 8 (Analyse-Tools) der Datenschutzerklärung.
* Zentrales Nutzerverzeichnis: Aufgrund des dezentralen Charakters der Famedly Architektur ist eine Kontaktaufnahme mit Nutzern anderer Server ohne direkten, persönlichen Kontakt nur möglich, wenn beide Nutzer im „globalen Nutzerverzeichnis“ eingetragen sind. Diese Funktion ist standardmäßig aktiviert, Sie können sie jedoch jederzeit in der App unter “Mehr” und dort unter der Schaltfläche “Benutzersuche” deaktivieren.

Die App erfordert darüber hinaus folgende Berechtigungen:
* Internetzugriff: Dieser wird benötigt, um Nachrichten und Anfragen zu übermitteln sowie ggf. Ihre Eingaben auf unseren Servern zu speichern.
* Kamerazugriff: Dieser wird benötigt, damit Sie ein Profilfoto von sich aufnehmen und speichern, Fotos zum Versand aufnehmen sowie Video-Telefonie durchführen können.
* Mikrofonzugriff: Dieser wird benötigt, damit Sie Sprachnachrichten aufnehmen und speichern, sowie Video-Telefonie durchführen können.
* Push-Notifications: Dieser wird benötigt, um Ihnen zielgerichtete Push-Nachrichten für eingehende Nachrichten/Anfragen senden zu können.
* Gerätespeicher: Dieser wird benötigt, wenn Daten mit anderen NutzerInnen ausgetauscht werden sollen.
* Bluetooth-Zugriff: Dieser wird benötigt, um Bluetooth-Headsets oder Kopfhörer verwenden zu können.
* Biometrische Zugangsdaten (z.B. FaceID) für App-Lock

Die Berechtigungen zum Zugriff auf die oben genannten Funktionen werden spätestens bei der ersten Nutzung auf dem Gerät explizit angefragt und können bestätigt oder abgelehnt werden.\
Eine erteilte Berechtigung kann in den Einstellungen des Gerätes im Normalfall jederzeit rückgängig gemacht werden (dies ist allerdings von dem Gerät und dem Betriebssystem abhängig, worauf wir keinen Einfluss haben.\
Berechtigungen, die nicht erteilt worden sind, schränken die Nutzung von den Apps ein. Alle von der Berechtigung nicht betroffenen Funktionen sind dann aber weiter nutzbar.\
Die Verarbeitung und Verwendung von Nutzungsdaten erfolgen zur Bereitstellung des Dienstes. Diese Datenverarbeitung ist dadurch gerechtfertigt, dass die Verarbeitung für die Erfüllung des Vertrags zwischen Ihnen als Betroffener und uns gemäß Art. 6 Abs. 1 lit. b DSGVO zur Nutzung der App erforderlich ist. Sollten Sie uns Ihre Einwilligung für die einzelnen Berechtigungen erteilt haben, beruht die Verarbeitung auf dieser nach Art. 6 Abs. 1 lit. a DSGVO.
 
## 5. Versand von Nachrichten/Push-Benachrichtigungen
Famedly nutzt den Firebase Cloud Messaging-Dienst für Push-Benachrichtigungen auf Android und iOS. Dies geschieht in den folgenden Schritten:
Der Matrix-Server sendet die Push-Benachrichtigung an das Famedly Push Gateway.\
Das Famedly Push Gateway leitet die Nachricht in einem anderen Format an den Firebase Cloud Messaging-Dienst weiter. Dieser wartet bis das Gerät des Benutzers wieder online ist.\
Das Gerät empfängt die Push-Nachricht von dem Firebase Cloud Messaging-Dienst und zeigt sie als Benachrichtigung an.\
Der Quellcode des Push-Gateways kann [hier](https://gitlab.com/famedly/services/hedwig) eingesehen werden.\
Als Format für die Push-Benachrichtigung wird event_id_only verwendet. Eine typische Push-Benachrichtigung enthält also nur:
* Ereignis-ID
* Raum-ID
* Anzahl ungelesene Nachrichten
* Informationen über das Gerät, das die Nachricht erhalten soll
 
Eine tatsächliche Übertragung der Nachrichten und ihrer Inhalte auf das Mobilgerät findet nicht statt.
 
## 6. Kontaktformular und Kontaktaufnahme per E-Mail
Wenn Sie uns per Kontaktformular oder E-Mail Anfragen zukommen lassen, werden Ihre Angaben aus dem Anfrageformular bzw. Ihrer E-Mail inklusive der von Ihnen dort angegebenen Kontaktdaten zwecks Bearbeitung der Anfrage und für den Fall von Anschlussfragen bei uns gespeichert. Diese Daten geben wir in keinem Fall ohne Ihre Einwilligung weiter. Rechtsgrundlage für die Verarbeitung der Daten ist unser berechtigtes Interesse an der Beantwortung Ihres Anliegens gemäß Art. 6 Abs. 1 lit. f DSGVO sowie ggf. Art. 6 Abs. 1 lit. b DSGVO, sofern Ihre Anfrage auf den Abschluss eines Vertrages abzielt. Ihre Daten werden nach abschließender Bearbeitung der Anfrage gelöscht, sofern keine gesetzlichen Aufbewahrungspflichten entgegenstehen.
 
 
## 7. Zweckänderungen
Verarbeitungen Ihrer personenbezogenen Daten zu anderen als den beschriebenen Zwecken erfolgen nur, soweit eine Rechtsvorschrift dies erlaubt oder Sie in den geänderten Zweck der Datenverarbeitung eingewilligt haben. Im Falle einer Weiterverarbeitung zu anderen Zwecken als denen, für den die Daten ursprünglich erhoben worden sind, informieren wir Sie vor der Weiterverarbeitung über diese anderen Zwecke und stellen Ihnen sämtliche weitere hierfür maßgeblichen Informationen zur Verfügung.
 
## 8. Zeitraum der Datenspeicherung
Wir löschen oder anonymisieren Ihre personenbezogenen Daten, sobald sie für die Zwecke, für die wir sie nach den vorstehenden Ziffern erhoben oder verwendet haben, nicht mehr erforderlich sind. In der Regel speichern wir Ihre personenbezogenen Daten für die Dauer des Nutzungs- bzw. des Vertragsverhältnisses über die App zzgl. eines Zeitraumes von [7] Tagen, während welchem wir nach der Löschung Sicherungskopien aufbewahren, soweit diese Daten nicht für die strafrechtliche Verfolgung oder zur Sicherung, Geltendmachung oder Durchsetzung von Rechtsansprüchen länger benötigt werden.
Spezifische Angaben in dieser Datenschutzerklärung oder rechtliche Vorgaben zur Aufbewahrung und Löschung personenbezogener Daten, insbesondere solcher, die wir aus steuerrechtlichen Gründen aufbewahren müssen, bleiben unberührt.
 
## 9. Rechtsgrundlage der Verarbeitung
Wir erheben, verarbeiten und nutzen personenbezogene Daten nur, soweit sie für die Begründung, inhaltliche Ausgestaltung oder Änderung des Rechtsverhältnisses erforderlich sind (Bestandsdaten). Dies erfolgt auf Grundlage von Art. 6 Abs. 1 lit. b DSGVO, der die Verarbeitung von Daten zur Erfüllung eines Vertrags oder vorvertraglicher Maßnahmen gestattet. Personenbezogene Daten über die Inanspruchnahme unserer Internetseiten (Nutzungsdaten) erheben, verarbeiten und nutzen wir nur, soweit dies erforderlich ist, um dem Nutzer die Inanspruchnahme des Dienstes zu ermöglichen oder abzurechnen.
Die erhobenen Kundendaten werden nach Abschluss des Auftrags oder Beendigung der Geschäftsbeziehung gelöscht. Gesetzliche Aufbewahrungsfristen bleiben unberührt.
 
## 10. Analyse-Tools
Zur Analyse unserer App und der angebotenen Funktionen erheben wir bestimmte Analysedaten zur App-Nutzung und Fehlerberichten.
Beides geschieht jedoch nur mit Ihrer Einwilligung. Hierzu verwenden wir die folgenden Anbieter:
 
### Matomo
Mit Ihrer Einwilligung sammeln und speichern wir in dieser App unter Einsatz der Webanalysedienst-Software Matomo ([www.matomo.org](https://www.matomo.org)), einem Dienst des Anbieters InnoCraft Ltd., 150 Willis St, 6011 Wellington, Neuseeland, („Matomo“) zur statistischen Analyse des Nutzerverhaltens zu Optimierungs- und Marketingzwecken Daten. Aus diesen Daten können zum selben Zweck pseudonymisierte Nutzungsprofile erstellt und ausgewertet werden. Hierzu können Cookies eingesetzt werden. Die mit der Matomo-Technologie erhobenen Daten werden nur auf unseren Servern verarbeitet, eine Übermittlung an Matomo oder Dritte erfolgt nicht.
Die Daten werden gelöscht, sobald sie für die Erreichung des Zweckes ihrer Erhebung nicht mehr erforderlich sind. Eine Löschung der Daten auf Nutzer- und Ereignisebene, die mit Cookies, Nutzerkennungen (z. B. User-ID) und Werbe-IDs (z. B. DoubleClick-Cookies, Android-Werbe-ID, IDFA [Apple-Kennung für Werbetreibende]) verknüpft sind erfolgt spätestens nach 24 Monaten.
Die durch das Cookie erzeugten Informationen im pseudonymen Nutzerprofil werden nicht dazu benutzt, den Nutzer dieser App persönlich zu identifizieren und nicht mit personenbezogenen Daten über den Träger des Pseudonyms zusammengeführt. Wenn Sie mit der Speicherung und Auswertung dieser Daten aus Ihrem Besuch nicht einverstanden sind, dann können Sie die Einwilligung in die Speicherung und Nutzung nachfolgend in den Einstellungen jederzeit widerrufen. 
 
### Sentry
Mit Ihrer Einwilligung verwenden wir den Dienst Sentry (Sentry, 1501 Mariposa St #408, San Francisco, CA 94107, USA), um die technische Stabilität unseres Dienstes durch Überwachung der Systemstabilität und Ermittlung von Codefehlern zu verbessern. Dies stützt sich daher auf Art. 6 Abs. 1 lit. a DSGVO. Sentry dient allein diesen Zielen und wertet keine Daten zu Werbezwecken aus. Die Daten der Nutzer, wie z.B. Angaben zum Gerät oder Fehlerzeitpunkt werden anonym erhoben und nicht personenbezogen genutzt sowie anschließend gelöscht. Ihre Einwilligung können Sie jederzeit in den Einstellungen widerrufen.
Weitere Informationen hierzu finden Sie in der Datenschutzerklärung von Sentry: [https://sentry.io/privacy/](https://sentry.io/privacy/).
 
## 11. Ihre Rechte als Betroffener
Famedly ist in der Regel nicht verantwortlich i.S.d. DSGVO für alle Datenverarbeitungen, da Institutionen die App auf eigenen Servern hosten. Bitte wenden Sie sich in diesem Fall an Ihren lokalen IT-Administrator oder Datenschutzbeauftragten.
 
### Auskunftsrecht
Sie haben das Recht, von uns jederzeit auf Antrag eine Auskunft über die von uns verarbeiteten, Sie betreffenden personenbezogenen Daten im Umfang des Art. 15 DSGVO zu erhalten. Hierzu können Sie einen Antrag postalisch oder per E-Mail an die unten angegebene Adresse stellen.
 
 
### Recht zur Berichtigung unrichtiger Daten
Sie haben das Recht, von uns die unverzügliche Berichtigung der Sie betreffenden personenbezogenen Daten zu verlangen, sofern diese unrichtig sein sollten.
 
 
### Recht auf Löschung
Sie haben das Recht, unter den in Art. 17 DSGVO beschriebenen Voraussetzungen von uns die Löschung Ihrer personenbezogenen Daten zu verlangen. Diese Voraussetzungen sehen insbesondere ein Löschungsrecht vor, wenn die personenbezogenen Daten für die Zwecke, für die sie erhoben oder auf sonstige Weise verarbeitet wurden, nicht mehr notwendig sind, sowie in Fällen der unrechtmäßigen Verarbeitung, des Vorliegens eines Widerspruchs oder des Bestehens einer Löschungspflicht nach Unionsrecht oder dem Recht des Mitgliedstaates, dem wir unterliegen. Zum Zeitraum der Datenspeicherung siehe im Übrigen Ziffer 5 dieser Datenschutzerklärung.
 
 
### Recht auf Einschränkung der Verarbeitung
Sie haben das Recht, von uns die Einschränkung der Verarbeitung Ihrer personenbezogenen Daten nach Maßgabe des Art. 18 DSGVO zu verlangen. Dieses Recht besteht insbesondere, wenn die Richtigkeit der personenbezogenen Daten zwischen dem Nutzer und uns umstritten ist, für die Dauer, welche die Überprüfung der Richtigkeit erfordert, sowie im Fall, dass der Nutzer bei einem bestehenden Recht auf Löschung anstelle der Löschung eine eingeschränkte Verarbeitung verlangt; ferner für den Fall, dass die Daten für die von uns verfolgten Zwecke nicht länger erforderlich sind, der Nutzer sie jedoch zur Geltendmachung, Ausübung oder Verteidigung von Rechtsansprüchen benötigt sowie, wenn die erfolgreiche Ausübung eines Widerspruchs zwischen uns und dem Nutzer noch umstritten ist.
 
### Recht auf Datenübertragbarkeit
Sie haben das Recht, von uns die personenbezogenen Daten, die Sie uns bereitgestellt haben, in einem strukturierten, gängigen, maschinenlesbaren Format nach Maßgabe des Art. 20 DSGVO zu erhalten.
 
## 12. Widerspruchsrecht
Sie haben das Recht, aus Gründen, die sich aus Ihrer besonderen Situation ergeben, jederzeit gegen die Verarbeitung Ihrer personenbezogenen Daten, die u.a. aufgrund von Art. 6 Abs. 1 lit. e oder f DSGVO erfolgt, Widerspruch nach Art. 21 DSGVO einzulegen. Wir werden die Verarbeitung Ihrer personenbezogenen Daten einstellen, es sei denn, wir können zwingende schutzwürdige Gründe für die Verarbeitung nachweisen, die Ihren Interessen, Rechte und Freiheiten überwiegen, oder wenn die Verarbeitung der Geltendmachung, Ausübung oder Verteidigung von Rechtsansprüchen dient.
 
## 13. Beschwerderecht

Im Falle datenschutzrechtlicher Verstöße steht dem Betroffenen ein Beschwerderecht bei der zuständigen Aufsichtsbehörde zu. Zuständige Aufsichtsbehörde in datenschutzrechtlichen Fragen ist der Landesdatenschutzbeauftragte des Bundeslandes, in dem unser Unternehmen seinen Sitz hat. Eine Liste der Datenschutzbeauftragten sowie deren Kontaktdaten können folgendem Link entnommen werden: [https://www.bfdi.bund.de/DE/Infothek/Anschriften_Links/anschriften_links-node.html.](https://www.bfdi.bund.de/DE/Infothek/Anschriften_Links/anschriften_links-node.html)
 
## 14. Automatische Entscheidungsfindung/Profiling
Die Famedly GmbH führt keine automatische Entscheidungsfindung oder sogenanntes Profiling durch.
 
## 15. Änderungen und Aktualisierungen dieser Datenschutzrichtlinie
Wir können unsere Datenschutzbestimmungen bei Bedarf aktualisieren. Die letzte Änderung ist anhand des Datums unter der Überschrift auf Seite 1 ersichtlich.
''';
}
