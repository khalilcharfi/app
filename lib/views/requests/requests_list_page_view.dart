/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'package:adaptive_page_layout/adaptive_page_layout.dart';
import 'package:famedly/components/nav_scaffold.dart';
import 'package:famedly/components/request_list.dart';
import 'package:famedly/models/requests/filters.dart';
import 'package:famedly/views/requests/requests_list_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';

class RequestsListPageView extends StatelessWidget {
  final RequestsListPageController controller;

  const RequestsListPageView(this.controller, {Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return NavScaffold(
      key: Key('RequestsPage'),
      activePage: MainPage.requests,
      appBar: AppBar(
        leading: AdaptivePageLayout.of(context).columnMode(context)
            ? null
            : BackButton(),
        title: Text(controller.getTitle()),
      ),
      filterTextController: controller.filterTextController,
      body: FutureBuilder<bool>(
        future: controller.waitForFirstSync(),
        builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
          if (snapshot.hasData) {
            return RequestsList(
              controller.filterTextController,
              activeChatId: controller.widget.activeChatId,
              emptyListPlaceholder: Center(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    L10n.of(context).noMoreRequests,
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 20),
                  ),
                ),
              ),
              filter: RequestFilters.isRequestRoomFilter,
              requestBundleFilter: controller.getBundleFilter(),
              type: 'requests/${controller.widget.type}',
            );
          } else {
            return Container(
              color: Theme.of(context).backgroundColor,
              child: Center(
                child: CircularProgressIndicator(),
              ),
            );
          }
        },
      ),
    );
  }
}
