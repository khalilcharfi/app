import 'dart:async';
import 'dart:io';
import 'dart:ui';

import 'package:famedly/models/famedly.dart';
import 'package:famedly/utils/downloader.dart';
import 'package:famedly/utils/error_reporter.dart';
import 'package:famedly/utils/famedlysdk_store.dart';
import 'package:famedly/utils/matrix_locals.dart';
import 'package:famedly/utils/platform_extension.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:flutter_gen/gen_l10n/l10n_de.dart';
import 'package:flutter_gen/gen_l10n/l10n_en.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:http/http.dart' as http;
import 'package:path_provider/path_provider.dart';
import 'package:universal_html/html.dart' as darthtml;

import '../../utils/room_status_extension.dart';
import '../famedly.dart';
import 'background_push_plugin.dart';

// inspired by _lookupL10n in .dart_tool/flutter_gen/gen_l10n/l10n.dart
L10n _lookupL10n(Locale locale) {
  switch (locale.languageCode) {
    case 'de':
      return L10nDe();
    default:
      return L10nEn();
  }
}

class LocalNotificationPlugin {
  static const String channelId = 'com.famedly.app.message';
  static const String channelName = 'Normal Message';
  static const String channelDescription = 'Any regular Message';

  Client client;
  final i18n = MatrixLocals(_lookupL10n(window.locale));
  Future<void> _initNotifications;
  bool reinited = false;
  Famedly famedly;

  LocalNotificationPlugin._(this.client) {
    Logs().v('[Notify] setup');
    if (kIsWeb) {
      onFocusSub = darthtml.window.onFocus.listen((_) => webHasFocus = true);
      onBlurSub = darthtml.window.onBlur.listen((_) => webHasFocus = false);
    }
    if (kIsWeb || kIsMobile) {
      onSync ??= client.onSync.stream.listen((r) => update());
      onEvent ??= client.onEvent.stream.listen((e) => event(e));
      onPreNotify ??= BackgroundPushPlugin.clientOnly(client)
          .onPreNotify
          .stream
          .listen(preNotify);

      _initNotifications = kIsWeb
          ? registerDesktopNotifications()
          : _flutterLocalNotificationsPlugin.initialize(
              InitializationSettings(
                android: AndroidInitializationSettings('notifications_icon'),
                iOS: IOSInitializationSettings(
                  // constructor is called early (before runApp),
                  // so don't request permissions here.
                  requestAlertPermission: false,
                  requestBadgePermission: false,
                  requestSoundPermission: false,
                ),
              ),
            );

      _initNotifications.then((_) => update());
    }
  }

  static LocalNotificationPlugin _instance;
  factory LocalNotificationPlugin.clientOnly(Client client) {
    _instance ??= LocalNotificationPlugin._(client);
    return _instance;
  }

  factory LocalNotificationPlugin(Famedly famedly) {
    _instance ??= LocalNotificationPlugin._(famedly.client);
    _instance.famedly = famedly;
    return _instance;
  }

  void update() {
    if (kIsWeb) updateTabTitle();
    client.rooms.forEach((r) async {
      try {
        await _notification(r);
      } catch (e, s) {
        // ignore: unawaited_futures
        ErrorReporter.reportError(e, s);
      }
    });
  }

  void reinit() {
    if (reinited == true) {
      return;
    }
    Logs().v('[Notify] reinit');
    _initNotifications = _flutterLocalNotificationsPlugin?.initialize(
        InitializationSettings(
          android: AndroidInitializationSettings('notifications_icon'),
          iOS: IOSInitializationSettings(),
        ), onSelectNotification: (String payload) async {
      Logs().v('[Notify] Selected: $payload');
      await famedly?.roomsPlugin?.enterRoom(payload);
      return null;
    });
    reinited = true;
  }

  void event(EventUpdate e) {
    notYetLoaded[e.roomID]?.removeWhere((x) => x == e.content['event_id']);
    dirtyRoomIds.add(e.roomID);
  }

  Map<String, Set<String>> notYetLoaded = {};

  /// Rooms that has been changed since app start
  final dirtyRoomIds = <String>{};

  void preNotify(PreNotify pn) {
    final r = client.getRoomById(pn.roomId);
    if (r == null) return;
    notYetLoaded[pn.roomId] ??= {};
    notYetLoaded[pn.roomId].add(pn.eventId);
    dirtyRoomIds.add(pn.roomId);
    _notification(r);
  }

  final _flutterLocalNotificationsPlugin =
      kIsWeb ? null : FlutterLocalNotificationsPlugin();

  StreamSubscription<SyncUpdate> onSync;
  StreamSubscription<EventUpdate> onEvent;
  StreamSubscription<PreNotify> onPreNotify;

  final roomEvent = <String, String>{};

  Future<dynamic> _notification(Room room) async {
    await _initNotifications;

    final dirty = dirtyRoomIds.remove(room.id);

    final notYet = notYetLoaded[room.id]?.length ?? 0;
    if (notYet == 0 &&
        room.notificationCount == 0 &&
        room.membership != Membership.invite) {
      if (!roomEvent.containsKey(room.id) || roomEvent[room.id] != null) {
        Logs().v('[Notify] clearing ' + room.id);
        roomEvent[room.id] = null;
        final id = await mapRoomIdToInt(room.id);
        await _flutterLocalNotificationsPlugin?.cancel(id);
      }
      return;
    }

    final event = notYet == 0 ? room.lastEvent : null;
    final eventId = event?.eventId ??
        (room.membership == Membership.invite ? 'invite' : '');
    if (roomEvent[room.id] == eventId) {
      return;
    }

    if (webHasFocus && room.id == famedly?.activeRoomID) {
      return;
    }

    if (room.lastEvent.content['msgtype'] ==
        Downloader.fileDownloadMessageType) {
      // File download notifiers should not trigger a notification
      return;
    }

    // Calculate the body
    final body = room.membership == Membership.invite
        ? i18n.l10n.youHaveBeenInvitedToChat
        : event?.getLocalizedBody(
              i18n,
              withSenderNamePrefix: !room.isDirectChat ||
                  room.lastEvent.senderId == client.userID,
              hideReply: true,
            ) ??
            i18n.l10n.unreadMessages;

    Logs().v('[Notify] showing ' + room.id);
    roomEvent[room.id] = eventId;

    // Show notification
    if (_flutterLocalNotificationsPlugin != null) {
      // The person object for the android message style notification
      final person = Person(
        name: room.getLocalizedFamedlyDisplayname(i18n),
        icon: room.avatar == null || room.avatar.toString().isEmpty
            ? null
            : BitmapFilePathAndroidIcon(
                await downloadAndSaveAvatar(
                  room.avatar,
                  client,
                  width: 126,
                  height: 126,
                ),
              ),
      );
      final androidPlatformChannelSpecifics =
          AndroidNotificationDetails(channelId, channelName, channelDescription,
              styleInformation: MessagingStyleInformation(
                person,
                messages: [
                  Message(
                    body,
                    event?.originServerTs ?? DateTime.now(),
                    person,
                  )
                ],
              ),
              importance: Importance.max,
              priority: Priority.high,
              when: event?.originServerTs?.millisecondsSinceEpoch,
              onlyAlertOnce: !dirty);
      final iOSPlatformChannelSpecifics = IOSNotificationDetails();
      final platformChannelSpecifics = NotificationDetails(
        android: androidPlatformChannelSpecifics,
        iOS: iOSPlatformChannelSpecifics,
      );
      final id = await mapRoomIdToInt(room.id);
      await _flutterLocalNotificationsPlugin.show(
        id,
        room.getLocalizedFamedlyDisplayname(i18n),
        body,
        platformChannelSpecifics,
        payload: room.id,
      );
    } else if (kIsWeb) {
      sendDesktopNotification(
        room.getLocalizedFamedlyDisplayname(i18n),
        body,
        roomId: room.id,
        icon: (event?.sender?.avatarUrl?.getThumbnail(client,
                    width: 64, height: 64, method: ThumbnailMethod.crop) ??
                room?.avatar?.getThumbnail(client,
                    width: 64, height: 64, method: ThumbnailMethod.crop))
            ?.toString(),
      );
    } else {
      Logs().w('No platform support for notifications');
    }
  }

  static Future<String> downloadAndSaveAvatar(Uri content, Client client,
      {int width, int height}) async {
    final thumbnail = width == null && height == null ? false : true;
    final tempDirectory = (await getTemporaryDirectory()).path;
    final prefix = thumbnail ? 'thumbnail' : '';
    final file =
        File('$tempDirectory/${prefix}_${content.toString().split("/").last}');

    if (!file.existsSync()) {
      final url = thumbnail
          ? content.getThumbnail(client, width: width, height: height)
          : content.getDownloadLink(client);
      if (url.host.isEmpty) return '';
      final request = await http
          .get(url)
          .timeout(Duration(seconds: 5))
          .catchError((e) => null);
      if (request == null) return '';
      if (request.bodyBytes == null) return '';
      await file.writeAsBytes(request.bodyBytes);
    }

    return file.path;
  }

  /// Workaround for the problem that local notification IDs must be int but we
  /// sort by [roomId] which is a String. To make sure that we don't have duplicated
  /// IDs we map the [roomId] to a number and store this number.
  static Future<int> mapRoomIdToInt(String roomId) async {
    final storage = Store();
    int currentInt;
    try {
      currentInt = int.parse(await storage.getItem(currentIdKey(roomId)));
    } catch (_) {
      currentInt = null;
    }
    if (currentInt == null) {
      final allItems = await storage.getAllItems();
      currentInt = allItems.keys
              .where((key) => key.startsWith(currentIdKeyPrefix))
              .length +
          1;
      await storage.setItem(currentIdKey(roomId), currentInt.toString());
    }
    return currentInt;
  }

  StreamSubscription<darthtml.Event> onFocusSub;
  StreamSubscription<darthtml.Event> onBlurSub;

  bool webHasFocus = true;

  void sendDesktopNotification(
    String title,
    String body, {
    String icon,
    String roomId,
  }) async {
    try {
      darthtml.AudioElement()
        ..src = 'assets/assets/sounds/pop.wav'
        ..autoplay = true
        ..load();
      final notification = darthtml.Notification(
        title,
        body: body,
        icon: icon,
      );
      notification.onClick.listen((e) => famedly.roomsPlugin.enterRoom(roomId));
    } catch (e, s) {
      await ErrorReporter.reportError(e, s);
    }
  }

  Future<void> registerDesktopNotifications() async {
    await client.onSync.stream.first;
    await darthtml.Notification.requestPermission();
  }

  void updateTabTitle() {
    final unreadRooms = client.rooms
        .where(
            (r) => r.membership == Membership.invite || r.notificationCount > 0)
        .length;
    if (unreadRooms > 0) {
      darthtml.document.title = '($unreadRooms) Famedly';
    } else {
      darthtml.document.title = 'Famedly';
    }
  }

  static const String latestIdKey = 'com.famedly.app.notification_id.latest';
  static const String currentIdKeyPrefix = 'com.famedly.app.notification_id';
  static String currentIdKey(String roomId) => '$currentIdKeyPrefix.$roomId';
}
