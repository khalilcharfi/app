/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'package:adaptive_page_layout/adaptive_page_layout.dart';
import 'package:famedly/utils/famedlysdk_client.dart';
import 'package:famedly/utils/famedlysdk_store.dart';
import 'package:famedly/utils/platform_extension.dart';
import 'package:famedly/views/enter_pass_code_view.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:future_loading_dialog/future_loading_dialog.dart';
import 'package:flutter_app_lock/flutter_app_lock.dart';

import '../models/famedly.dart';

class HomePicker extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final client = Famedly.of(context).client;
    if (getCurrentLoginState() != null) {
      WidgetsBinding.instance.addPostFrameCallback((_) =>
          AdaptivePageLayout.of(context).pushNamedAndRemoveAllOthers(
              getCurrentLoginState() == LoginState.logged ? '/' : '/home'));
    } else {
      client.onLoginStateChanged.stream.first.then((loginState) {
        // Display the app lock
        WidgetsBinding.instance.addPostFrameCallback((_) {
          if (kIsMobile) {
            Logs().v('Display app lock on first start');
            Store().getItem(EnterPassCodeView.appLockNamespace).then((lock) {
              if (lock?.isNotEmpty ?? false) {
                if (loginState == LoginState.logged) {
                  AppLock.of(context).enable();
                  AppLock.of(context).showLockScreen();
                } else {
                  Logs().d(
                      'App lock is enabled but user is not logged in -> disable it');
                  Store().setItem(EnterPassCodeView.appLockNamespace, null);
                }
              }
            });
          }
          AdaptivePageLayout.of(context).pushNamedAndRemoveAllOthers(
              loginState == LoginState.logged ? '/' : '/home');
        });
      });
    }

    LoadingDialog.defaultTitle = L10n.of(context).loading;
    return Scaffold(
        body: Center(
      child: CircularProgressIndicator(),
    ));
  }
}
