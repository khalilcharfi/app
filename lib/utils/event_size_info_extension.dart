/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'package:famedlysdk/famedlysdk.dart';

extension EventSizeInfoExtension on Event {
  String get formattedSizeString {
    String fileSizeFixed;
    if (content.containsKey('info') &&
        content['info'].containsKey('size') &&
        content['info']['size'] is int) {
      final int filesize = content['info']['size'];
      if (filesize != 0) {
        fileSizeFixed = ((filesize / 10000).round() / 100).toString();
      } else {
        fileSizeFixed = 'unknown Size';
      }
    } else {
      fileSizeFixed = 'unknown Size';
    }
    return '$fileSizeFixed MB';
  }
}
