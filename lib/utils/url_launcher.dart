/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'package:famedly/models/famedly.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class UrlLauncher {
  final String url;
  final BuildContext context;
  const UrlLauncher(this.context, this.url);

  static const String urlsToAddMxidLocalpart = 'https://s2survey.net/kompan/';

  void launchUrl() {
    if (url.toLowerCase().startsWith('https://matrix.to/#/') ||
        {'#', '@', '!', '+', '\$'}.contains(url[0]) ||
        url.toLowerCase().startsWith('matrix:')) {
      return openMatrixToUrl();
    }
    var launchUrl = url;
    if (url.startsWith(urlsToAddMxidLocalpart)) {
      launchUrl =
          '$url&s=${Uri.encodeQueryComponent(Famedly.of(context).client.userID.localpart)}';
    }
    launch(launchUrl);
  }

  void openMatrixToUrl() async {
    // TODO: handle tapping on matrix identifiers
  }
}
