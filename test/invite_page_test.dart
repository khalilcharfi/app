import 'package:famedly/views/invite_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'utils.dart';

void main() {
  /// All Tests related to the EmptyPage
  /// This test only exists for the coverage value
  group('InvitePage', () {
    NavigatorObserver mockObserver;

    setUp(() {
      mockObserver = MockNavigatorObserver();
    });

    testWidgets('should have a FacilityPickerPage',
        (WidgetTester tester) async {
      await tester.runAsync(() async {
        await tester.pumpWidget(
          await Utils.getWidgetWrapper(
            InvitePage(),
            mockObserver,
          ),
        );

        await tester.pumpAndSettle();

        expect(find.byKey(Key('InvitePage')), findsOneWidget);
      });
    });
  });
}
