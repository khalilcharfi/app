/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:adaptive_page_layout/adaptive_page_layout.dart';
import 'package:famedly/models/requests/request.dart';
import 'package:famedly/models/room_type_extension.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:future_loading_dialog/future_loading_dialog.dart';

extension RoomArchiveDialogExtension on Room {
  Future<void> showArchiveDialog(BuildContext context,
      {bool popAfter = true}) async {
    if (OkCancelResult.ok ==
        await showOkCancelAlertDialog(
          context: context,
          title: L10n.of(context).archiveThisConversation,
          okLabel: L10n.of(context).yes,
          cancelLabel: L10n.of(context).cancel,
        )) {
      {
        if (type == roomTypeRequest && membership == Membership.join) {
          // ask for the request to be resolved
          final confirmed = await showOkCancelAlertDialog(
            context: context,
            title: L10n.of(context).confirmRequestResolvedDialog,
            okLabel: L10n.of(context).yes,
            cancelLabel: L10n.of(context).no,
            useRootNavigator: false,
          );
          if (confirmed == OkCancelResult.ok) {
            final req = Request(this);
            await showFutureLoadingDialog(
              context: context,
              future: () => req.resolve(),
            );
            final success = await showFutureLoadingDialog(
              context: context,
              future: () => leave(),
            );
            if (success.error == null && popAfter) {
              AdaptivePageLayout.of(context).pop();
            }
          } else {
            await showFutureLoadingDialog(
                context: context, future: () => leave());
          }
        } else {
          final success = await showFutureLoadingDialog(
            context: context,
            future: () => leave(),
          );
          if (success.error == null && popAfter) {
            AdaptivePageLayout.of(context).pop();
          }
        }
      }
    }
  }
}
