/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'package:famedly/components/draggable_scrollbar.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:adaptive_page_layout/adaptive_page_layout.dart';
import 'package:future_loading_dialog/future_loading_dialog.dart';

import '../components/avatar.dart';
import '../models/famedly.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import '../styles/famedly_icons_icons.dart';

class NewGroupPage extends StatefulWidget {
  @override
  _NewGroupPageState createState() => _NewGroupPageState();
}

class _NewGroupPageState extends State<NewGroupPage> {
  Set<String> inviteSet = <String>{};
  bool _groupnameSet = false;

  final TextEditingController controller = TextEditingController();

  ListTile headingItem(String letter) {
    return ListTile(
      title: Text(
        letter.substring(0, 1),
        style: TextStyle(fontWeight: FontWeight.bold),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final famedly = Famedly.of(context);

    return Scaffold(
      appBar: AppBar(
        leading: BackButton(),
        title: Text(L10n.of(context).newGroup),
        actions: <Widget>[
          IconButton(
            key: Key('StartGroupChat'),
            icon: Icon(FamedlyIcons.tick),
            onPressed: () async {
              if (!_groupnameSet && inviteSet.isEmpty) {
                return;
              }
              final roomId = await showFutureLoadingDialog(
                context: context,
                future: () => famedly.client.createRoom(
                    invite: inviteSet.toList(), name: controller.text),
              );
              if (roomId.result != null) {
                await AdaptivePageLayout.of(context)
                    .pushNamedAndRemoveUntilIsFirst('/room/${roomId.result}');
              }
            },
          ),
        ],
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(16),
            child: TextField(
              key: Key('newGroupTextField'),
              controller: controller,
              autofocus: true,
              autocorrect: false,
              onChanged: (_) => (_groupnameSet != controller.text.isNotEmpty)
                  ? _groupnameSet = controller.text.isNotEmpty
                  : null,
              decoration: InputDecoration(
                hintText: L10n.of(context).groupName,
                labelText: L10n.of(context).groupName,
                border: OutlineInputBorder(),
              ),
            ),
          ),
          Divider(height: 1),
          Expanded(
              child: FutureBuilder(
                  key: Key('NewGroupPage'),
                  future: famedly.contactDiscoveryPlugin.loadContacts(),
                  builder: (BuildContext context,
                      AsyncSnapshot<List<User>> snapshot) {
                    if (snapshot.data == null) {
                      return Container(
                        color: Theme.of(context).backgroundColor,
                        child: Center(
                          child: CircularProgressIndicator(),
                        ),
                      );
                    }
                    final contactList = snapshot.data;
                    contactList.sort((a, b) => a
                        .calcDisplayname(mxidLocalPartFallback: false)
                        .toUpperCase()
                        .compareTo(b
                            .calcDisplayname(mxidLocalPartFallback: false)
                            .toUpperCase()));

                    final _scrollController = ScrollController();

                    return FamedlyDraggableScrollbar(
                      isAlwaysShown: kIsWeb,
                      controller: _scrollController,
                      child: ListView.separated(
                        controller: _scrollController,
                        separatorBuilder: (context, i) {
                          final item = contactList[i];
                          if (i == 0) {
                            return headingItem(item
                                .calcDisplayname(mxidLocalPartFallback: false)
                                .toUpperCase());
                          }
                          final prevItem = contactList[i - 1];
                          if (prevItem
                                  .calcDisplayname(mxidLocalPartFallback: false)
                                  .substring(0, 1)
                                  .toUpperCase() !=
                              item
                                  .calcDisplayname(mxidLocalPartFallback: false)
                                  .substring(0, 1)
                                  .toUpperCase()) {
                            return headingItem(item
                                .calcDisplayname(mxidLocalPartFallback: false)
                                .toUpperCase());
                          }
                          return Container();
                        },
                        key: Key('UsersListView'),
                        // Let the ListView know how many items it needs to build
                        itemCount: contactList.length + 1,
                        // Provide a builder function. This is where the magic happens! We'll
                        // convert each item into a Widget based on the type of item it is.
                        itemBuilder: (context, index) {
                          if (index == 0) {
                            return Container();
                          }
                          final item = contactList[index - 1];

                          return ContactListItem(
                              contact: item,
                              onChecked: (bool selected) {
                                if (selected) {
                                  inviteSet.add(item.id);
                                } else {
                                  inviteSet.remove(item.id);
                                }
                              });
                        },
                      ),
                    );
                  })),
        ],
      ),
    );
  }
}

class ContactListItem extends StatefulWidget {
  final onChecked;

  final User contact;

  ContactListItem({Key key, this.onChecked, this.contact}) : super(key: key);

  @override
  _ContactListItemState createState() {
    return _ContactListItemState();
  }
}

class _ContactListItemState extends State<ContactListItem> {
  bool checked = false;

  @override
  Widget build(BuildContext context) {
    return CheckboxListTile(
      value: checked,
      onChanged: (bool selected) {
        widget.onChecked(selected);
        setState(() {
          checked = selected;
        });
      },
      secondary: Avatar(widget.contact.avatarUrl,
          name: widget.contact.calcDisplayname(mxidLocalPartFallback: false)),
      title: Text(widget.contact.calcDisplayname(mxidLocalPartFallback: false)),
    );
  }
}
