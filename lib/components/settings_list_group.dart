/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'package:famedly/styles/colors.dart';
import 'package:flutter/material.dart';

class SettingsListGroup extends StatelessWidget {
  final List<Widget> children;
  final String title;
  final EdgeInsets padding;
  final bool expanded;

  const SettingsListGroup({
    Key key,
    this.children,
    this.title,
    this.padding,
    this.expanded = false,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final groupWidget = ClipRRect(
      borderRadius: BorderRadius.circular(10),
      child: Material(
        color: Theme.of(context).appBarTheme.color,
        borderRadius: BorderRadius.circular(10),
        child: Padding(
          padding: padding ?? EdgeInsets.zero,
          child: Column(
              mainAxisSize: expanded ? MainAxisSize.max : MainAxisSize.min,
              children: children),
        ),
      ),
    );
    return Padding(
      padding: EdgeInsets.all(5),
      child: title == null
          ? groupWidget
          : Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Container(
                  alignment: Alignment.centerLeft,
                  padding: EdgeInsets.only(top: 15, left: 11, bottom: 10),
                  child: Text(
                    title,
                    style: TextStyle(
                      color: FamedlyColors.blueyGrey,
                      fontSize: 13,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ),
                groupWidget,
              ],
            ),
    );
  }
}
