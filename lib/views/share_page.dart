/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'package:famedly/styles/famedly_icons_v2_icons.dart';
import 'package:flutter/material.dart';
import 'package:adaptive_page_layout/adaptive_page_layout.dart';

import '../models/famedly.dart';
import '../components/matrix_chat_list.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';

class SharePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        Famedly.of(context).forwardContent =
            Famedly.of(context).forwardFile = null;
        return Future.value(true);
      },
      child: Scaffold(
        key: Key('share_page'),
        appBar: AppBar(
          title: Text(L10n.of(context).forward),
          leading: IconButton(
            key: Key('CloseForwardModeButton'),
            icon: Icon(FamedlyIconsV2.close),
            onPressed: () {
              Famedly.of(context).forwardContent =
                  Famedly.of(context).forwardFile = null;
              AdaptivePageLayout.of(context).pop();
            },
          ),
        ),
        body: MatrixChatList(null),
      ),
    );
  }
}
