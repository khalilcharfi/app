#!/usr/bin/env python3

import json
from re import sub

messages = json.load(open("intl_en.arb", 'r'))

newmessages = {}
for key, value in messages.items():
    if key.startswith("@@"):
        newmessages.update({key: value})
        continue
    newkey = sub(r'(\.|\?|!|,|:|/|-|\(|\)|\'|\"|\\|\*)', " ", key)
    if " " in newkey:
        newkey = newkey.title().replace(" ", "")
    if newkey[0] == '@':
        newkey = newkey[0] + newkey[1].lower() + newkey[2:]
    else:
        newkey = newkey[0].lower() + newkey[1:]
    print(newkey)
    newmessages.update({newkey: value})

f = open("new.arb", 'w')
json.dump(newmessages, f, sort_keys=False, indent=2)
