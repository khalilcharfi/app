/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'package:famedly/components/device_keys_grid.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/material.dart';

import '../models/famedly.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';

class RoomEncryptionSettingsPage extends StatefulWidget {
  final String id;

  const RoomEncryptionSettingsPage(this.id, {Key key}) : super(key: key);

  @override
  _RoomEncryptionSettingsPageState createState() =>
      _RoomEncryptionSettingsPageState();
}

class _RoomEncryptionSettingsPageState
    extends State<RoomEncryptionSettingsPage> {
  Room room;

  @override
  Widget build(BuildContext context) {
    room ??= Famedly.of(context).client.getRoomById(widget.id);

    return StreamBuilder<Object>(
        stream: room.onUpdate.stream,
        builder: (context, snapshot) {
          return Scaffold(
            appBar: AppBar(
              leading: BackButton(),
              title: Text(L10n.of(context).participatingUserDevices),
            ),
            body: SafeArea(
              child: FutureBuilder<List<DeviceKeys>>(
                future: room.getUserDeviceKeys(),
                builder: (BuildContext context, snapshot) {
                  if (snapshot.hasError) {
                    return Center(
                      child: Text(L10n.of(context).oopsSomethingWentWrong +
                          ': ' +
                          snapshot.error.toString()),
                    );
                  }
                  if (!snapshot.hasData) {
                    return Center(child: CircularProgressIndicator());
                  }
                  final deviceKeys = snapshot.data;
                  final users = <User>[];
                  for (final key in deviceKeys) {
                    if (users.any((u) => u.id == key.userId)) continue;
                    users.add(room.getUserByMXIDSync(key.userId));
                  }
                  return ListView.builder(
                    itemCount: users.length,
                    itemBuilder: (BuildContext context, int i) =>
                        DeviceKeysGrid(
                      key: Key('DeviceKeysGrid-${users[i].id}'),
                      user: users[i],
                      deviceKeys: deviceKeys
                          .where((key) => key.userId == users[i].id)
                          .toList(),
                    ),
                  );
                },
              ),
            ),
          );
        });
  }
}
