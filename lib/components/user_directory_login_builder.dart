/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:famedly/utils/error_reporter.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';

import '../models/famedly.dart';

class UserDirectoryLoginBuilder extends StatelessWidget {
  final Widget Function(BuildContext context) builder;
  final Widget Function(BuildContext context) errorBuilder;

  const UserDirectoryLoginBuilder({
    Key key,
    @required this.builder,
    this.errorBuilder,
  }) : super(key: key);

  Future<bool> _requestToken(BuildContext context) async {
    final userDirectoryClient =
        await Famedly.of(context).getUserDirectoryClient();
    if (userDirectoryClient.accessToken != null) return true;
    final matrixClient = Famedly.of(context).client;
    try {
      final openIdCredentials =
          await matrixClient.requestOpenIdToken(matrixClient.userID);
      await userDirectoryClient.login(
        openIdCredentials.accessToken,
        matrixClient.userID,
        matrixClient.homeserver.toString(),
      );
    } on SocketException catch (_) {
      throw L10n.of(context).notConnected;
    } on MatrixConnectionException catch (_) {
      throw L10n.of(context).notConnected;
    } on TimeoutException catch (_) {
      throw L10n.of(context).notConnected;
    } catch (e, s) {
      String errorStr;
      try {
        errorStr = jsonDecode(
            e.toString().replaceFirst('Exception: ', '').trim())['msg'];
      } catch (_) {}
      if (errorStr == null) {
        errorStr =
            kDebugMode ? e.toString() : L10n.of(context).oopsSomethingWentWrong;
        // ignore: unawaited_futures
        ErrorReporter.captureException(exception: e, stackTrace: s);
      }
      throw errorStr;
    }
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: _requestToken(context),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.hasError) {
            return errorBuilder?.call(context) ??
                Center(
                  child: Padding(
                    padding: const EdgeInsets.all(16),
                    child: Text(
                      snapshot.error.toString(),
                      textAlign: TextAlign.center,
                    ),
                  ),
                );
          }
          if (!snapshot.hasData) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
          return builder(context);
        });
  }
}
