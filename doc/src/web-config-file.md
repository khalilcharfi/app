# Web Config File

Optionally configure by serving a config.json at the same path as Famedly.
An example can be found at config.sample.json. None of these
values have to exist, the ones stated here are the default ones. If you e.g. only want
to change the default homeserver, then only modify the `homeserver` key.