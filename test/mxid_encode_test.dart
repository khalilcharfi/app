import 'package:famedly/utils/mxid_encode.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  final testStrs = <List<String>>[
    ['foxies', 'foxies'],
    ['Fox', '_fox'],
    ['fox@hole', 'fox=40hole'],
    ['Füchschen', '_f=c3=bcchschen'],
    ['fox_hole', 'fox__hole'],
    ['fox\x01hole', 'fox=01hole'],
  ];
  group('str2mxid/mxid2str', () {
    test('str2mxid', () {
      for (final test in testStrs) {
        expect(str2mxid(test[0]), test[1]);
      }
    });
    test('mxid2str', () {
      for (final test in testStrs) {
        expect(mxid2str(test[1]), test[0]);
      }
    });
  });
}
