import 'package:famedlysdk/famedlysdk.dart';

import '../room_type_extension.dart';
import 'patient_data.dart';

const String patientRoomNamespace = 'com.famedly.app.patient_room';

extension ClientPatientsExtension on Client {
  Future<String> createPatientRoom(PatientData patientData, {Uri avatar}) =>
      createRoom(
        preset: CreateRoomPreset.private_chat,
        name: patientRoomNamespace,
        initialState: [
          {
            'type': 'm.room.type',
            'content': {'type': roomTypePatient},
          },
          {
            'type': patientRoomNamespace,
            'content': patientData.toJson(),
          },
          {
            'type': 'm.room.history_visibility',
            'content': {'history_visibility': 'shared'}
          },
          if (avatar != null)
            {
              'type': 'm.room.avatar',
              'content': {'url': avatar.toString()}
            },
          {
            'type': 'm.room.guest_access',
            'content': {'guest_access': 'forbidden'}
          },
          {
            'type': 'm.room.encryption',
            'content': {
              'algorithm': Client.supportedGroupEncryptionAlgorithms.first,
            }
          },
        ],
        powerLevelContentOverride: {
          'events': {
            patientRoomNamespace: 100,
          },
        },
      );
}
