import 'package:famedlysdk/famedlysdk.dart';

import 'broadcast_content.dart';
import 'request.dart';
import 'request_content.dart';
import 'request_state_content.dart';

extension RequestRoomExtension on Room {
  bool get isBroadcastRequest {
    final req = Request(this);
    if (req.content == null) return false;
    // if both parties accepted the broadcast then it shouldn't appear as one anymore
    if (req.broadcast != null) return true;
    return req.content.isBroadcast == true &&
        (!req.remoteAccepted() || !req.youAccepted());
  }

  RequestStateContent get requestStateContent =>
      getState(Request.requestStateNameSpace) != null
          ? RequestStateContent.fromJson(
              getState(Request.requestStateNameSpace).content)
          : null;

  bool get isRoomHandoff =>
      getState(Request.requestStateNameSpace) != null &&
      requestStateContent.roomHandoff;

  RequestContent get requestContent =>
      getState(Request.requestNameSpace) != null
          ? RequestContent.fromJson(getState(Request.requestNameSpace).content)
          : null;

  BroadcastContent get broadcastContant =>
      getState(Request.broadcastNameSpace) != null
          ? BroadcastContent.fromJson(
              getState(Request.broadcastNameSpace).content)
          : null;
}
