# Forms and Questionnaires

Forms in the format of FHIR Questionnaires are used for broadcasts in the app. On starting a broadcast you can pick a form, select the contact points supporting this form and then fill out the form and send the response of it as a broadcast request.

Forms are located under `assets/questionnaires/` as JSON files in the format of the
FHIR Questionnaire specification: https://www.hl7.org/fhir/questionnaire.html.

All added forms are visible in the broadcast picker. When a form is selected, only contact points supporting this type of form can be selected for the broadcast request. The contact points need a tag which contains the name of the questionnaire and the prefix `questionnaire::`. So if you have a questionnaire named `patient_transfer` only contact points with a tag named `questionnaire::patient_transfer` can be selected.

## How to add a new form

1. Create a questionnaire JSON file and make sure that it contains a human friendly`title` and a computer friendly `name` attribute. Name the file `name.json` and place it under `assets/questionnaires/name.json` in the app repository. For example if the `title` is `Patient Transfer` and the  `name` is `patient_transfer` you name the file `patient_transfer.json`. Please use lowercase and snake_case for the name!

2. Add the `name` to the list at `lib/config/forms.dart`.

3. In the Famedly Organisations Directory create a new tag with the name `questionnaire::name`. So if your questionnaire has the name `patient_transfer` create a new tag with the name `questionnaire::patient_transfer`.

4. Add the tag to the contact points, the user should be able to send the filled form.