/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'package:famedly/styles/colors.dart';
import 'package:famedly/styles/famedly_icons_v2_icons.dart';
import 'package:famedly/utils/famedly_file_picker.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:file_picker_cross/file_picker_cross.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:adaptive_page_layout/adaptive_page_layout.dart';
import 'package:future_loading_dialog/future_loading_dialog.dart';

import '../components/avatar.dart';
import '../models/famedly.dart';
import '../models/patients/client_patients_extension.dart';
import '../models/patients/patient_data.dart';
import '../models/patients/room_patients_extension.dart';
import '../styles/famedly_icons_icons.dart';
import '../utils/matrix_file_thumbnail_extension.dart';
import '../utils/date_time_extension.dart';

class PatientSettingsPage extends StatefulWidget {
  final String roomId;

  const PatientSettingsPage({this.roomId, Key key}) : super(key: key);

  @override
  _PatientSettingsPageState createState() => _PatientSettingsPageState();
}

class _PatientSettingsPageState extends State<PatientSettingsPage> {
  bool get createNewPatient => widget.roomId == null;

  void _createPatientAction(BuildContext context) async {
    final patientData = PatientData(
      name: patientName,
      caseId: caseId,
      sex: sex,
      birthday: birthday,
    );
    final roomId = await showFutureLoadingDialog(
      context: context,
      future: () => Famedly.of(context).client.createPatientRoom(
            patientData,
            avatar: avatar,
          ),
    );
    if (roomId.error == null) {
      await AdaptivePageLayout.of(context).pushNamedAndRemoveUntilIsFirst(
        '/room/${roomId.result}/invite',
      );
    }
  }

  void _updatePatientAction(BuildContext context) async {
    final success = await showFutureLoadingDialog(
      context: context,
      future: () => room.setPatientData(
        PatientData(
          name: patientName,
          birthday: birthday,
          sex: sex,
          caseId: caseId,
        ),
      ),
    );
    if (success.error == null) {
      AdaptivePageLayout.of(context).pop();
    }
    return;
  }

  void _setAvatarAction(BuildContext context) async {
    final tempFile = await FamedlyFilePicker.pickFiles(
      type: FileTypeCross.image,
      context: context,
    );
    if (tempFile == null) return;
    MatrixFile file;
    try {
      file = await MatrixImageFile(
        bytes: tempFile.toUint8List(),
        name: tempFile.path,
      ).createThumbnail();
    } catch (e, s) {
      Logs().d('Could not create thumbnail for avatar', e, s);
      file = MatrixImageFile(
        bytes: tempFile.toUint8List(),
        name: tempFile.path,
      );
    }
    if (createNewPatient) {
      final mxcUrl = await showFutureLoadingDialog(
        context: context,
        future: () =>
            Famedly.of(context).client.uploadContent(file.bytes, file.name),
      );
      if (mxcUrl.error == null) {
        setState(() => avatar = Uri.parse(mxcUrl.result));
      }
    } else {
      final eventId = await showFutureLoadingDialog(
        context: context,
        future: () => room.setAvatar(file),
      );
      if (eventId.error == null) {
        await room.client.onEvent.stream
            .firstWhere((e) => e.content['event_id'] == eventId.result)
            .then((e) {
          setState(() => avatar = room.avatar);
        });
      }
    }
  }

  Sex sex = Sex.unknown;
  DateTime birthday;
  String given;
  String family;
  String caseId;
  Uri avatar;
  Room room;

  String get patientName =>
      (given?.isEmpty ?? true) && (family?.isEmpty ?? true)
          ? null
          : '${given ?? ""} ${family ?? ""}';

  @override
  Widget build(BuildContext context) {
    final client = Famedly.of(context).client;
    String initialCaseId, initialGiven, initialFamily;
    if (!createNewPatient && room == null) {
      room = client.getRoomById(widget.roomId);
      setState(() {
        room = client.getRoomById(widget.roomId);
        sex = room.patientData.sex;
        birthday = room.patientData.birthday;
        given = initialGiven = room.patientData.name.split(' ').first;
        family = initialFamily = room.patientData.name.split(' ').last;
        caseId = initialCaseId = room.patientData.caseId;
        avatar = room.avatar;
      });
    }
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          key: Key('cancelPatientSettingsButton'),
          icon: Icon(FamedlyIconsV2.close),
          onPressed: AdaptivePageLayout.of(context).pop,
        ),
        title: Text(createNewPatient
            ? L10n.of(context).newPatient
            : room.patientData.name),
        actions: <Widget>[
          if (createNewPatient)
            IconButton(
              key: Key('save'),
              icon: Icon(
                FamedlyIcons.tick,
                color: FamedlyColors.azul,
              ),
              onPressed: patientName?.isEmpty ?? true
                  ? null
                  : () => _createPatientAction(context),
            ),
          if (!createNewPatient)
            IconButton(
              icon: Icon(
                Icons.save,
                color: FamedlyColors.azul,
              ),
              key: Key('save'),
              onPressed: patientName?.isEmpty ?? true
                  ? null
                  : () => _updatePatientAction(context),
            ),
        ],
      ),
      body: ListView(
        key: Key('patientSettingsListView'),
        padding: FamedlyStyles.getTwoColumnListViewPadding(context),
        children: <Widget>[
          SizedBox(height: 16),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            child: TextField(
              key: Key('caseId'),
              onChanged: (s) => setState(() => caseId = s),
              controller: initialCaseId == null
                  ? null
                  : TextEditingController(text: initialCaseId),
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: L10n.of(context).caseId,
                hintText: L10n.of(context).theCaseNumberOfThePatient,
              ),
            ),
          ),
          SizedBox(height: 16),
          ListTile(
            leading: Avatar(avatar ?? Uri.parse(''),
                name: patientName ?? L10n.of(context).patient),
            title: Text(L10n.of(context).addPicture),
            trailing: Icon(Icons.camera_alt),
            onTap: () => _setAvatarAction(context),
          ),
          SizedBox(height: 16),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            child: TextField(
              key: Key('firstName'),
              controller: initialGiven == null
                  ? null
                  : TextEditingController(text: initialGiven),
              onChanged: (s) => setState(() => given = s),
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: L10n.of(context).firstName,
                hintText: L10n.of(context).pleaseType,
              ),
            ),
          ),
          SizedBox(height: 16),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            child: TextField(
              key: Key('lastName'),
              controller: initialFamily == null
                  ? null
                  : TextEditingController(text: initialFamily),
              onChanged: (s) => setState(() => family = s),
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: L10n.of(context).lastName,
                hintText: L10n.of(context).pleaseType,
              ),
            ),
          ),
          SizedBox(height: 8),
          ListTile(
            title: Text(
              L10n.of(context).birthdayDate,
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
          ListTile(
            key: Key('setBirthDate'),
            title: Text(L10n.of(context).setBirthdayDate),
            subtitle: Text(
                birthday?.localizedDay(context) ?? L10n.of(context).unknown),
            trailing: Icon(Icons.date_range),
            onTap: () async {
              final date = await showDatePicker(
                context: context,
                initialDate: birthday ?? DateTime.parse('1990-01-01'),
                firstDate: DateTime.parse('1890-01-01'),
                lastDate: DateTime.now(),
              );
              if (date == null) return;
              setState(() => birthday = date);
            },
          ),
          ListTile(
            title: Text(
              L10n.of(context).gender,
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
          RadioListTile(
            key: Key('female'),
            value: Sex.female,
            groupValue: sex,
            onChanged: (Sex v) => setState(() => sex = v),
            title: Text(L10n.of(context).female),
          ),
          RadioListTile(
            key: Key('male'),
            value: Sex.male,
            groupValue: sex,
            onChanged: (Sex v) => setState(() => sex = v),
            title: Text(L10n.of(context).male),
          ),
          RadioListTile(
            key: Key('other'),
            value: Sex.other,
            groupValue: sex,
            onChanged: (Sex v) => setState(() => sex = v),
            title: Text(L10n.of(context).diverse),
          ),
          RadioListTile(
            key: Key('unknown'),
            value: Sex.unknown,
            groupValue: sex,
            onChanged: (Sex v) => setState(() => sex = v),
            title: Text(L10n.of(context).unknown),
          ),
          SizedBox(height: 16),
        ],
      ),
    );
  }
}
