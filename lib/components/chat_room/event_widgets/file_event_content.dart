import 'package:famedlysdk/famedlysdk.dart' as sdk;
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import '../../../utils/event_forwarded_extension.dart';

import '../../../styles/colors.dart';
import '../../../utils/event_size_info_extension.dart';
import '../../../models/famedly.dart';
import 'event_widget.dart';

class FileEventContent extends StatelessWidget {
  final sdk.Event fileEvent;

  final Uri file;

  final prevEvent;
  final nextEvent;
  final Function onDownload;

  FileEventContent(
    this.fileEvent, {
    Key key,
    this.prevEvent,
    this.nextEvent,
    this.onDownload,
  })  : file = Uri.parse(fileEvent.content['url'] ?? ''),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    final famedly = Famedly.of(context);
    // TODO combine with next text message of same user if present
    final defaultRadius = 18.0;
    final noneRadius = 1.0;

    var topLeft = defaultRadius;
    var topRight = defaultRadius;
    var bottomLeft = defaultRadius;
    var bottomRight = defaultRadius;

    final prevEventSameSender = EventWidget.hasSameSender(prevEvent, fileEvent);
    final nextEventSameSender = EventWidget.hasSameSender(nextEvent, fileEvent);

    final isMe = fileEvent.isMine;

    if (prevEventSameSender && isMe) topRight = noneRadius;
    if (nextEventSameSender && isMe) bottomRight = noneRadius;
    if (prevEventSameSender && !isMe) topLeft = noneRadius;
    if (nextEventSameSender && !isMe) bottomLeft = noneRadius;

    final radius = BorderRadius.only(
      topLeft: Radius.circular(topLeft),
      topRight: Radius.circular(topRight),
      bottomLeft: Radius.circular(bottomLeft),
      bottomRight: Radius.circular(bottomRight),
    );

    final String filename = fileEvent.content.containsKey('filename') &&
            fileEvent.content['filename'].toString().isNotEmpty
        ? fileEvent.content['filename']
        : fileEvent.body;

    var extension = '';
    final lastDot = filename.isNotEmpty
        ? filename.lastIndexOf('.', filename.length - 1)
        : -1;
    if (lastDot != -1) {
      extension = filename.substring(lastDot + 1);
    }

    return InkWell(
      borderRadius: radius,
      onTap: onDownload,
      child: Material(
        elevation: 2,
        borderRadius: radius,
        color: EventWidget.messageBubbleColor(context, fileEvent, famedly),
        child: Container(
          decoration: BoxDecoration(
            border: EventWidget.messageBubbleBorder(context, fileEvent, isMe),
            borderRadius: radius,
          ),
          child: Column(
            children: <Widget>[
              Padding(
                padding:
                    EdgeInsets.only(left: 8.0, top: 8.0, right: 8.0, bottom: 0),
                child: Row(
                  children: <Widget>[
                    Icon(Icons.insert_drive_file),
                    SizedBox(
                      width: 11,
                    ),
                    Expanded(
                      child: Text(
                        // As Riot doesn't follow spec we need this check
                        filename,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          color: FamedlyColors.metallicBlue,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Divider(
                color: FamedlyColors.slate,
              ),
              Padding(
                padding:
                    EdgeInsets.only(left: 8.0, top: 0, right: 8.0, bottom: 8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      extension,
                      style: TextStyle(
                        color: FamedlyColors.blueyGrey,
                      ),
                    ),
                    Text(
                      fileEvent.formattedSizeString,
                      style: TextStyle(
                        color: FamedlyColors.blueyGrey,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
