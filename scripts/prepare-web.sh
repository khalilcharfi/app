#!/bin/sh -ve
rm -r assets/js || true
mkdir assets/js
cd web/
curl -L 'https://gitlab.com/famedly/libraries/olm/-/jobs/artifacts/master/download?job=build_js' > olm.zip
unzip olm.zip
rm olm.zip
mv javascript/* .
rmdir javascript
curl -L 'https://gitlab.com/famedly/libraries/native_imaging/-/jobs/artifacts/master/download?job=build_js' > native_imaging.zip
unzip native_imaging.zip
mv js/* .
rmdir js
rm native_imaging.zip
rm sql-wasm.js sql-wasm.wasm || true
curl -L 'https://github.com/sql-js/sql.js/releases/latest/download/sqljs-wasm.zip' > sqljs-wasm.zip
unzip sqljs-wasm.zip
rm sqljs-wasm.zip
cd ..
