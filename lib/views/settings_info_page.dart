/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import 'dart:async';

import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:adaptive_page_layout/adaptive_page_layout.dart';
import 'package:famedly/config/current_commit_hash.dart';
import 'package:famedly/styles/colors.dart';
import 'package:famedly/styles/famedly_icons_v2_icons.dart';
import 'package:famedly/utils/platform_extension.dart';
import 'package:famedly/utils/voip/callkeep_manager.dart';
import 'package:famedly_directory_client_sdk/famedly_directory_client_sdk.dart';
import 'package:famedly_user_directory_client_sdk/famedly_user_directory_client_sdk.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:future_loading_dialog/future_loading_dialog.dart';
import 'package:package_info/package_info.dart';

import '../models/famedly.dart';
import '../models/plugins/background_push_plugin.dart';
import '../utils/beautify_string_extension.dart';

class SettingsInfoPage extends StatefulWidget {
  @override
  _SettingsInfoPageState createState() => _SettingsInfoPageState();
}

class _SettingsInfoPageState extends State<SettingsInfoPage> {
  Future<Profile> profileFuture;
  bool pushTest;

  @override
  Widget build(BuildContext context) {
    final client = Famedly.of(context).client;
    profileFuture ??= client.ownProfile;
    return FutureBuilder<Profile>(
      future: profileFuture,
      builder: (BuildContext context, AsyncSnapshot<Profile> profileSnapshot) {
        final displayname = profileSnapshot.hasData
            ? profileSnapshot.data.displayname
            : '⌛ ${L10n.of(context).loading} ⌛';
        return Scaffold(
          appBar: AppBar(
            leading: BackButton(),
            title: FutureBuilder<String>(
                future: !kIsWeb
                    ? PackageInfo.fromPlatform()
                        .then((platform) => platform.version)
                    : null,
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    return Text(
                        L10n.of(context).aboutFamedly + ' ' + snapshot.data);
                  }
                  return Text(L10n.of(context).aboutFamedly);
                }),
          ),
          body: ListView(
            key: Key('SettingsInfoListView'),
            padding: FamedlyStyles.getTwoColumnListViewPadding(context),
            children: <Widget>[
              ListTile(
                  trailing: Icon(FamedlyIconsV2.info),
                  title: Text(
                    '${L10n.of(context).displayName}:',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  subtitle: Text(displayname),
                  onTap: () {
                    showOkAlertDialog(
                      context: context,
                      title: L10n.of(context)
                          .theDisplaynameCanOnlyBeChangedByAnAdmin,
                      okLabel: L10n.of(context).back,
                      useRootNavigator: false,
                    );
                  }),
              if (kIsMobile)
                ListTile(
                  title: Text(
                    L10n.of(context).checkoutPhoneAccountSettings,
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  onTap: () => CallKeepManager()
                      .checkoutPhoneAccountSetting(context)
                      .catchError((_) => true),
                  trailing: Icon(FamedlyIconsV2.phone),
                ),
              ListTile(
                title: Text(
                  L10n.of(context).clearCacheAndReload,
                  key: Key('clearCacheAndReload'),
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: FamedlyColors.grapefruit),
                ),
                onTap: () async {
                  if (OkCancelResult.ok ==
                      await showOkCancelAlertDialog(
                        context: context,
                        title: L10n.of(context).clearCacheConfirmation,
                        okLabel: L10n.of(context).yes,
                        cancelLabel: L10n.of(context).cancel,
                        useRootNavigator: false,
                      )) {
                    await showFutureLoadingDialog(
                        context: context,
                        future: () => Famedly.of(context).client.clearCache());
                    AdaptivePageLayout.of(context).popUntilIsFirst();
                  }
                },
                trailing: Icon(Icons.refresh),
              ),
              Divider(),
              ListTile(
                title: Text(
                  '${L10n.of(context).internalMxid}:',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
                subtitle: SelectableText(
                  client.userID,
                  style: TextStyle(fontStyle: FontStyle.italic),
                ),
                onTap: () => AdaptivePageLayout.of(context).pushNamed('/logs'),
              ),
              if (client.encryptionEnabled)
                Column(
                  children: <Widget>[
                    ListTile(
                      title: Text(
                        '${L10n.of(context).publicKey}:',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      subtitle:
                          SelectableText(client.fingerprintKey.beautified),
                    ),
                  ],
                ),
              FutureBuilder(
                  future: Famedly.of(context).getOrganizationDirectoryClient(),
                  builder: (BuildContext context,
                      AsyncSnapshot<Directory> organizationDirectorySnapshot) {
                    final baseUrl = organizationDirectorySnapshot.hasData
                        ? organizationDirectorySnapshot.data.baseUrl
                        : '⌛ ${L10n.of(context).loading} ⌛';
                    return ListTile(
                      title: Text(
                        'Organization Directory:',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      subtitle: SelectableText(
                        baseUrl,
                        style: TextStyle(fontStyle: FontStyle.italic),
                      ),
                    );
                  }),
              FutureBuilder(
                future: Famedly.of(context).getUserDirectoryClient(),
                builder: (BuildContext context,
                    AsyncSnapshot<UserDirectoryClient> userDirectorySnapshot) {
                  final baseUrl = userDirectorySnapshot.hasData
                      ? userDirectorySnapshot.data.baseUrl
                      : '⌛ ${L10n.of(context).loading} ⌛';
                  return ListTile(
                    title: Text(
                      'User Directory:',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    subtitle: SelectableText(
                      baseUrl,
                      style: TextStyle(fontStyle: FontStyle.italic),
                    ),
                  );
                },
              ),
              if (kIsMobile)
                ListTile(
                  key: Key('PushTest'),
                  title: Text(
                    pushTest == null
                        ? 'Test Push Notification Gateway'
                        : pushTest == false
                            ? 'Push Test failed!'
                            : 'Push notification test was successful.',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  subtitle: SelectableText(
                    BackgroundPushPlugin.gatewayUrl.toString(),
                    style: TextStyle(fontStyle: FontStyle.italic),
                  ),
                  onTap: () async {
                    final result = await Famedly.of(context)
                        .pushNotificationPlugin
                        .sendTestMessageGUI(verbose: true);
                    setState(() => pushTest = result);
                  },
                ),
              if (kDebugMode &&
                  BackgroundPushPlugin(Famedly.of(context)).lastReceivedPush !=
                      null)
                ListTile(
                  title: Text('Last received push'),
                  subtitle: Text(BackgroundPushPlugin(Famedly.of(context))
                      .lastReceivedPush
                      .toIso8601String()),
                ),
              if (kDebugMode)
                ListTile(
                  key: Key('inviteTest'),
                  title: Text('Invite test'),
                  onTap: () async {
                    final username = await showTextInputDialog(
                      context: context,
                      title: 'Username',
                      textFields: [DialogTextField()],
                      useRootNavigator: false,
                    );
                    final password = await showTextInputDialog(
                      context: context,
                      title: 'Password',
                      textFields: [DialogTextField()],
                      useRootNavigator: false,
                    );
                    await showFutureLoadingDialog(
                        context: context,
                        future: () async {
                          final client = Client('Invite client');
                          await client.checkHomeserver(
                              Famedly.of(context).client.homeserver);
                          await client.login(
                            identifier: AuthenticationUserIdentifier(
                              user: username.single,
                            ),
                            password: password.single,
                          );
                          await client.createRoom(
                              invite: [Famedly.of(context).client.userID]);
                          await client.logout();
                        });
                    AdaptivePageLayout.of(context).pop();
                  },
                ),
              if (CurrentCommitHash.hash != 'Unknown')
                ListTile(
                  title: Text(
                    'Commit:',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  subtitle: Text(CurrentCommitHash.hash),
                ),
            ],
          ),
        );
      },
    );
  }
}
