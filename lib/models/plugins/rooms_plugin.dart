import 'package:famedly/utils/platform_extension.dart';
import 'package:famedly/views/image_editor.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/material.dart';
import 'package:future_loading_dialog/future_loading_dialog.dart';
import 'package:pedantic/pedantic.dart';

import '../famedly.dart';
import '../../utils/room_send_file_extension.dart';
import '../requests/request.dart';
import 'famedly_plugin.dart';

class RoomsPlugin extends FamedlyPlugin {
  RoomsPlugin(matrix) : super(matrix);

  OnLeaveRoomCB onLeaveRoom;

  Future<bool> _joinRoom(
      BuildContext context, Room room, bool isRequestRoom) async {
    final update = famedly.client.onRoomUpdate.stream
        .where((RoomUpdate update) =>
            update.id == room.id && update.membership == Membership.join)
        .first;
    var waitForBroadcast = false;
    final directoryUpdate =
        famedly.client.onEvent.stream.where((EventUpdate update) {
      if (update.content['type'] == Request.requestNameSpace &&
          update.content['content'] != null &&
          update.content['content']['requested_organisation'] == 'Broadcast' &&
          update.content['content']['contact_description'] == 'Broadcast' &&
          update.content['content']['organisation_id'] == '' &&
          update.content['content']['contact_id'] == '') {
        waitForBroadcast = true;
      }
      return update.roomID == room.id &&
          update.content['type'] == Request.requestNameSpace;
    }).first;
    final broadcastUpdate = famedly.client.onEvent.stream
        .where((EventUpdate update) =>
            update.roomID == room.id &&
            update.content['type'] == Request.broadcastNameSpace)
        .first;
    await room.join();
    await update;
    if (isRequestRoom) {
      await directoryUpdate;
      if (waitForBroadcast) {
        await broadcastUpdate;
      }
    }
    return true;
  }

  /// Join this room and wait for a sync that this room has been created.
  Future<bool> joinRoomAndWait(BuildContext context, Room item) async {
    if (item.membership == Membership.invite) {
      final isRequestRoom = item.displayname == Request.requestNameSpace;
      final success = await showFutureLoadingDialog(
          context: context,
          future: () => _joinRoom(context, item, isRequestRoom));
      if (success.error != null) return false;
    }

    if (famedly.forwardContent != null) {
      unawaited(item.sendEvent(famedly.forwardContent));
      famedly.forwardContent = null;
    }
    if (famedly.forwardFile != null) {
      if (kIsMobile && famedly.forwardFile is MatrixImageFile) {
        famedly.forwardFile = await Navigator.push(
          context,
          MaterialPageRoute(
              builder: (_) => ImageEditor(file: famedly.forwardFile)),
        );
        if (famedly.forwardFile == null) return true;
      }
      await showFutureLoadingDialog(
        context: context,
        future: () => item.sendFileEventWithThumbnail(famedly.forwardFile),
      );
      famedly.forwardFile = null;
    }
    return true;
  }

  /// Enters a room and joins the room when it is an invite room.
  Future<void> enterRoom(String roomID) async {
    if (roomID != famedly.activeRoomID && famedly.context != null) {
      final room = client.getRoomById(roomID);
      if (room == null) return;
      if (room.membership == Membership.invite) {
        await Famedly.of(famedly.context)
            .roomsPlugin
            .joinRoomAndWait(famedly.context, room);
      }
      await famedly.adaptivePageLayout.currentState
          ?.pushNamedAndRemoveUntilIsFirst('/room/$roomID');
    }
  }
}

typedef OnLeaveRoomCB = void Function(String roomID);
