/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'package:adaptive_page_layout/adaptive_page_layout.dart';
import 'package:famedly/models/famedly.dart';
import 'package:famedly/models/requests/request.dart';
import 'package:famedly/utils/famedly_file_picker.dart';
import 'package:famedly/utils/form_manager.dart';
import 'package:famedly/views/requests/request_composting_page_view.dart';
import 'package:famedly_directory_client_sdk/famedly_directory_client_sdk.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:fhir/r4.dart' as fhir;
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:future_loading_dialog/future_loading_dialog.dart';
import 'package:uuid/uuid.dart';
import 'package:famedly/models/requests/request_client_extension.dart';

class RequestCompositingPage extends StatefulWidget {
  final String formName;
  final List<Contact> contacts;
  final ScrollController scrollController = ScrollController();
  RequestCompositingPage(this.formName, this.contacts, {Key key})
      : super(key: key);
  @override
  RequestCompositingPageController createState() =>
      RequestCompositingPageController();
}

class RequestCompositingPageController extends State<RequestCompositingPage> {
  MatrixFile attachment;
  final FormManager formManager = FormManager();
  final Map<String, dynamic> inputs = {};
  final GlobalKey<FormState> form = GlobalKey<FormState>();

  @override
  void setState(fn) {
    if (!mounted) return;
    super.setState(fn);
  }

  String titleText;
  bool isExternal;
  void getTitle() {
    titleText = L10n.of(context).loading;
    isExternal = false;
    if (widget.contacts.length == 1) {
      titleText = (widget.contacts[0].organisation != null
              ? widget.contacts[0].organisation.name + ' - '
              : '') +
          widget.contacts[0].description;
      isExternal = widget.contacts[0].email != null;
    } else {
      titleText = L10n.of(context).broadcast +
          ': ' +
          widget.contacts.length.toString() +
          ' ' +
          L10n.of(context).contacts;
      for (final contact in widget.contacts) {
        if (contact.email != null) {
          isExternal = true;
          break;
        }
      }
    }
  }

  void addAttachmentOnTap() {
    attachment == null
        ? addAttachmentAction(context)
        : setState(() => attachment = null);
  }

  void addAttachmentAction(BuildContext context) async {
    final tempFile = await FamedlyFilePicker.pickFiles(context: context);
    if (tempFile == null) return;
    final file = MatrixFile(
      bytes: tempFile.toUint8List(),
      name: tempFile.path,
    );
    setState(() => attachment = file);
  }

  void onPressedRequests(fhir.Questionnaire questionnaire) async {
    final uuid = Uuid();
    Future request;
    final title = questionnaire.title ?? questionnaire.name;
    var requestBody = '$title\n\n';

    for (final entry in inputs.entries) {
      var value = entry.value.toString();
      if (entry.value is bool) {
        value = entry.value ? L10n.of(context).yes : L10n.of(context).no;
      }
      requestBody += '${entry.key}: $value\n\n';
    }

    if (widget.contacts.length == 1) {
      final requestId = uuid.v4();
      request = widget.contacts[0].startRequest(
          requestTitle: title,
          requestBody: requestBody,
          requestId: requestId,
          client: Famedly.of(context).client,
          requestType: 'com.famedly.request.${questionnaire.name}');
    } else {
      request = Request.startBroadcast(
        requestTitle: title,
        requestBody: requestBody,
        requestType: 'com.famedly.request.${questionnaire.name}',
        contactList: widget.contacts,
        client: Famedly.of(context).client,
      );
    }
    final roomId = await showFutureLoadingDialog(
      context: context,
      future: () => request,
    );
    if (roomId.error == null) {
      if (attachment != null) {
        await showFutureLoadingDialog(
          context: context,
          future: () =>
              Room(id: roomId.result, client: Famedly.of(context).client)
                  .sendFileEvent(attachment),
        );
      }
      if (widget.contacts.length == 1) {
        if (roomId.result != null) {
          await AdaptivePageLayout.of(context)?.pushNamedAndRemoveUntilIsFirst(
              '/room/requests/conversations/${roomId.result}');
        }
      } else {
        await AdaptivePageLayout.of(context)
            ?.pushNamedAndRemoveUntilIsFirst('/requests');
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return RequestCompositingPageView(this);
  }
}
