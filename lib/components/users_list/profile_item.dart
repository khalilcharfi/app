import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/material.dart';
import 'package:adaptive_page_layout/adaptive_page_layout.dart';

import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:future_loading_dialog/future_loading_dialog.dart';
import '../avatar.dart';
import '../../models/famedly.dart';

class ProfileItem extends StatelessWidget {
  final String mxid;
  final Profile profile;
  final VoidCallback onTap;
  final bool owner;

  ProfileItem({
    Key key,
    @required this.mxid,
    this.profile,
    this.onTap,
    this.owner = false,
  }) : super(key: key);

  Future<void> defaultOnTap(Famedly famedly, BuildContext context) async {
    if (mxid == famedly.client.userID) {
      // we can't message ourself, so might as well short-circuit out of here
      return;
    }
    final list = <PopupMenuItem<String>>[
      PopupMenuItem<String>(
        value: 'message',
        child: Text(L10n.of(context).writeMessage),
      ),
    ];
    final RenderBox bar = context.findRenderObject();
    final RenderBox overlay = Overlay.of(context).context.findRenderObject();
    final position = RelativeRect.fromRect(
      Rect.fromPoints(
        bar.localToGlobal(bar.size.bottomLeft(Offset.zero), ancestor: overlay),
        bar.localToGlobal(bar.size.bottomRight(Offset.zero), ancestor: overlay),
      ),
      Offset.zero & overlay.size,
    );
    final result = await showMenu<String>(
      context: context,
      position: position,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15),
      ),
      items: list,
    );
    if (result == 'message') {
      // alright, let's message that user
      final roomId = await showFutureLoadingDialog(
        context: context,
        future: () =>
            User(mxid, room: Room(client: famedly.client)).startDirectChat(),
      );
      if (roomId.error == null) {
        await AdaptivePageLayout.of(context)
            .pushNamed('/room/${roomId.result}');
        return;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    final famedly = Famedly.of(context);
    Future<Profile> loadedFuture;
    if (profile != null) {
      loadedFuture = Future<Profile>.value(profile);
    } else {
      loadedFuture = famedly.client.getProfileFromUserId(mxid);
    }
    return FutureBuilder<Profile>(
      future: loadedFuture,
      builder: (BuildContext context, AsyncSnapshot<Profile> snapshot) {
        if (!snapshot.hasData && !snapshot.hasError) {
          return Container(
            color: Theme.of(context).backgroundColor,
            child: Center(
              child: CircularProgressIndicator(),
            ),
          );
        }
        Avatar avatar;
        String displayname;
        if (snapshot.hasError) {
          avatar = Avatar(null, size: 45, name: mxid);
          displayname = mxid;
        } else {
          final profile = snapshot.data;
          displayname = profile.displayname ?? mxid;
          avatar = Avatar(profile.avatarUrl, size: 45, name: displayname);
        }
        return ListTile(
          leading: avatar,
          title: Text(displayname),
          onTap: () {
            if (onTap != null) {
              onTap();
              return;
            }
            defaultOnTap(famedly, context);
          },
          trailing: owner ? Text(L10n.of(context).owner) : null,
        );
      },
    );
  }
}
