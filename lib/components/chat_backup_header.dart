import 'package:famedly/models/famedly.dart';
import 'package:adaptive_page_layout/adaptive_page_layout.dart';
import 'package:famedly/styles/famedly_icons_icons.dart';
import 'package:famedly/styles/famedly_icons_v2_icons.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';

class ChatBackupHeader extends StatefulWidget {
  @override
  _ChatBackupHeaderState createState() => _ChatBackupHeaderState();
}

class _ChatBackupHeaderState extends State<ChatBackupHeader> {
  Future<bool> _isCachedFuture;
  static bool _hideBanner = false;

  @override
  Widget build(BuildContext context) {
    if (_hideBanner || !Famedly.of(context).client.encryptionEnabled) {
      return Container();
    }
    _isCachedFuture ??=
        Famedly.of(context).client.encryption.keyManager.isCached();
    return FutureBuilder<bool>(
      future: _isCachedFuture,
      builder: (_, snapshot) {
        final needsBootstrap =
            Famedly.of(context).client.encryption.crossSigning.enabled ==
                    false ||
                snapshot.data == false;
        final isUnknownSession = Famedly.of(context).client.isUnknownSession;
        final displayHeader = needsBootstrap || isUnknownSession;
        return AnimatedContainer(
          height: displayHeader ? null : 0,
          duration: Duration(milliseconds: 500),
          color: Colors.orange,
          child: ClipRRect(
            child: ListTile(
              onTap: needsBootstrap
                  ? () => AdaptivePageLayout.of(context).pushNamed('/backup')
                  : () => AdaptivePageLayout.of(context)
                      .pushNamed('/settings/devices'),
              leading: CircleAvatar(
                backgroundColor: Colors.black.withAlpha(100),
                foregroundColor: Colors.white,
                child: Icon(
                    needsBootstrap ? FamedlyIcons.server : FamedlyIconsV2.lock),
              ),
              trailing: IconButton(
                icon: Icon(FamedlyIconsV2.close),
                onPressed: () => setState(() => _hideBanner = true),
              ),
              title: Text(
                needsBootstrap
                    ? L10n.of(context).pleaseEnableChatBackup
                    : L10n.of(context).pleaseVerifySession,
                style: TextStyle(color: Colors.white),
              ),
            ),
          ),
        );
      },
    );
  }
}
