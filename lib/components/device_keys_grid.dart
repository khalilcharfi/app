/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:famedly/styles/famedly_icons_v2_icons.dart';
import 'package:famedlysdk/encryption/utils/key_verification.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:future_loading_dialog/future_loading_dialog.dart';

import 'avatar.dart';
import 'dialogs/key_verification_dialog.dart';
import '../models/famedly.dart';
import '../utils/device_keys_extension.dart';

class DeviceKeysGrid extends StatefulWidget {
  final User user;
  final List<DeviceKeys> deviceKeys;

  const DeviceKeysGrid({
    Key key,
    @required this.user,
    @required this.deviceKeys,
  }) : super(key: key);
  @override
  _DeviceKeysGridState createState() => _DeviceKeysGridState();
}

enum DeviceAction { verify, block, unblock }

class _DeviceKeysGridState extends State<DeviceKeysGrid> {
  bool _collapsed = true;
  bool get _verified =>
      !widget.deviceKeys.any((k) => !k.verified && !k.blocked);

  @override
  void initState() {
    _collapsed = _verified;
    super.initState();
  }

  void _verifyUserAction(BuildContext context) async {
    final req = await showFutureLoadingDialog(
      context: context,
      future: () => Famedly.of(context)
          .client
          .userDeviceKeys[widget.user.id]
          .startVerification(),
    );
    if (req.error != null) return;
    req.result.onUpdate = () {
      if (req.result.state == KeyVerificationState.done) {
        setState(() => null);
      }
    };
    await KeyVerificationDialog(
      request: req.result,
      context: context,
    ).show(context);
  }

  void _verifyDeviceAction(BuildContext context, DeviceKeys key) async {
    final req = key.startVerification();
    req.onUpdate = () {
      if ({KeyVerificationState.error, KeyVerificationState.done}
          .contains(req.state)) {
        setState(() => null);
      }
    };
    await KeyVerificationDialog(
      request: req,
      context: context,
    ).show(context);
  }

  void _onTapAction(BuildContext context, DeviceKeys key) async {
    final result = await showModalActionSheet<DeviceAction>(
        context: context,
        title: L10n.of(context).pleaseChoose,
        actions: [
          SheetAction(
            label: L10n.of(context).verify,
            key: DeviceAction.verify,
            isDefaultAction: true,
          ),
          if (!key.blocked)
            SheetAction(
              label: L10n.of(context).block,
              key: DeviceAction.block,
              isDestructiveAction: true,
            ),
          if (key.blocked)
            SheetAction(
              label: L10n.of(context).unblock,
              key: DeviceAction.unblock,
            ),
        ]);
    if (result == null) return;
    switch (result) {
      case DeviceAction.verify:
        _verifyDeviceAction(context, key);
        break;
      case DeviceAction.block:
        if (key.directVerified) {
          await key.setVerified(false);
        }
        await key.setBlocked(true);
        setState(() => null);
        break;
      case DeviceAction.unblock:
        await key.setBlocked(false);
        setState(() => null);
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    final listTile = ListTile(
      contentPadding: EdgeInsets.zero,
      leading: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          IconButton(
            icon:
                Icon(_collapsed ? FamedlyIconsV2.right_1 : FamedlyIconsV2.down),
            onPressed: () => setState(() => _collapsed = !_collapsed),
          ),
          Avatar(
            widget.user.avatarUrl,
            name: widget.user.calcDisplayname(mxidLocalPartFallback: false),
          ),
        ],
      ),
      title: Text(
        widget.user.calcDisplayname(mxidLocalPartFallback: false),
        style: TextStyle(fontWeight: FontWeight.bold),
      ),
      trailing: Padding(
        padding: const EdgeInsets.only(right: 8.0),
        child: CircleAvatar(
          backgroundColor: Theme.of(context).secondaryHeaderColor,
          child: Icon(
            FamedlyIconsV2.lock,
            color: _verified ? Colors.green : Colors.orange,
          ),
        ),
      ),
      onTap: () => _verifyUserAction(context),
    );
    if (_collapsed) return listTile;
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        listTile,
        Wrap(
          spacing: 16,
          runSpacing: 16,
          children: [
            for (var key in widget.deviceKeys)
              Container(
                width: 150,
                height: 150,
                child: Material(
                  color: Theme.of(context).secondaryHeaderColor,
                  borderRadius: BorderRadius.circular(8),
                  elevation: 2,
                  child: InkWell(
                    onTap: () => _onTapAction(context, key),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(key.iconData, size: 40),
                        SizedBox(height: 4),
                        Text(key.deviceDisplayName ?? L10n.of(context).unknown),
                        Opacity(
                          opacity: 0.5,
                          child: Text(key.deviceId),
                        ),
                        Text(
                          key.getLocalizedVerificationStatus(context),
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: key.verificationColor,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
          ],
        ),
      ],
    );
  }
}
