/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'dart:async';

import 'package:famedly/styles/famedly_icons_v2_icons.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:famedly/models/plugins/room_key_request_plugin.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_lock/flutter_app_lock.dart';
import 'package:local_auth/local_auth.dart';

import '../styles/colors.dart';
import '../utils/famedlysdk_store.dart';

class EnterPassCodeView extends StatefulWidget {
  static const String appLockNamespace = 'com.famedly.app.app_lock_pass_code';
  static const String appLockBiometricsNamespace =
      'com.famedly.app.app_lock_use_biometrics';

  @override
  _EnterPassCodeViewState createState() => _EnterPassCodeViewState();
}

class _EnterPassCodeViewState extends State<EnterPassCodeView> {
  String passCode = '';
  Timer timer;
  int waitNextSeconds = 1;
  final storage = Store();

  final LocalAuthentication localAuth = LocalAuthentication();

  String get obscurePassCode {
    var obscure = '';
    for (var i = 0; i < passCode.length; i++) {
      obscure += '*';
    }
    return obscure;
  }

  void _readBiometrics() async {
    if (await storage.getItem(EnterPassCodeView.appLockBiometricsNamespace) !=
        'true') {
      return;
    }
    final authenticated = await localAuth.authenticate(
      biometricOnly: true,
      localizedReason: 'Bitte authentifizieren:',
    );
    if (authenticated) {
      AppLock.of(context).didUnlock();
    }
  }

  @override
  void dispose() {
    localAuth.stopAuthentication();
    RoomKeyRequestplugin.locked = false;
    super.dispose();
  }

  void _addDigit(int digit) {
    if (timer != null) return;
    setState(() => passCode += digit.toString());
    if (passCode.length >= 6) {
      _check();
    }
  }

  void _removeDigit() {
    if (timer != null || passCode.isEmpty) return;
    setState(() => passCode = passCode.substring(0, passCode.length - 1));
  }

  void _removeAllDigits() {
    if (timer != null) return;
    setState(() => passCode = '');
  }

  void _check() async {
    final correctCode =
        await storage.getItem(EnterPassCodeView.appLockNamespace);
    if (correctCode == null || passCode == correctCode) {
      if (correctCode == null) {
        AppLock.of(context).disable();
      }
      AppLock.of(context).didUnlock();
    } else {
      passCode = '';
      setState(() {
        waitNextSeconds += waitNextSeconds;
        timer = Timer(Duration(seconds: waitNextSeconds), () {
          if (mounted) setState(() => timer = null);
        });
      });
    }
  }

  @override
  void initState() {
    super.initState();
    RoomKeyRequestplugin.locked = true;
    _hasPassCode = storage
        .getItem(EnterPassCodeView.appLockNamespace)
        .then((dynamic passCode) {
      if (passCode is String) {
        return passCode;
      }
      return null;
    });
    _hasPassCode.then((passCode) {
      if (!(passCode is String)) {
        AppLock.of(context).disable();
        AppLock.of(context).didUnlock();
      } else {
        _readBiometrics();
      }
    });
  }

  Future<dynamic> _hasPassCode;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Famedly',
      localizationsDelegates: L10n.localizationsDelegates,
      supportedLocales: L10n.supportedLocales,
      home: Scaffold(
        body: SafeArea(
          child: FutureBuilder(
              future: _hasPassCode,
              builder: (context, snapshot) {
                if (snapshot.data == null) {
                  return Center(
                      child: Icon(FamedlyIconsV2.lock, key: Key('locked')));
                }
                return Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text(
                      timer != null
                          ? L10n.of(context).unfortunatelyThatWasWrong
                          : L10n.of(context).pleaseEnterPasscode,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 18,
                        color: timer != null
                            ? Colors.red
                            : Theme.of(context).textTheme.bodyText1.color,
                      ),
                    ),
                    Container(
                      height: 24,
                      alignment: Alignment.center,
                      child: Text(
                        obscurePassCode,
                        style: TextStyle(
                          fontSize: 30,
                        ),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        _DigitButton(
                          digit: 1,
                          onPressed: () => _addDigit(1),
                          key: Key('digit_1'),
                        ),
                        _DigitButton(
                          digit: 2,
                          onPressed: () => _addDigit(2),
                          key: Key('digit_2'),
                        ),
                        _DigitButton(
                          digit: 3,
                          onPressed: () => _addDigit(3),
                          key: Key('digit_3'),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        _DigitButton(
                          digit: 4,
                          onPressed: () => _addDigit(4),
                          key: Key('digit_4'),
                        ),
                        _DigitButton(
                          digit: 5,
                          onPressed: () => _addDigit(5),
                          key: Key('digit_5'),
                        ),
                        _DigitButton(
                          digit: 6,
                          onPressed: () => _addDigit(6),
                          key: Key('digit_6'),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        _DigitButton(
                          digit: 7,
                          onPressed: () => _addDigit(7),
                          key: Key('digit_7'),
                        ),
                        _DigitButton(
                          digit: 8,
                          onPressed: () => _addDigit(8),
                          key: Key('digit_8'),
                        ),
                        _DigitButton(
                          digit: 9,
                          onPressed: () => _addDigit(9),
                          key: Key('digit_9'),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        _DigitButton(
                          icon: Icons.clear,
                          onPressed: () => _removeAllDigits(),
                        ),
                        _DigitButton(
                          digit: 0,
                          onPressed: () => _addDigit(0),
                          key: Key('digit_0'),
                        ),
                        _DigitButton(
                          icon: Icons.backspace,
                          onPressed: () => _removeDigit(),
                        ),
                      ],
                    ),
                  ],
                );
              }),
        ),
      ),
    );
  }
}

class _DigitButton extends StatelessWidget {
  final Color color;
  final Color iconColor;
  final int digit;
  final IconData icon;
  final Function onPressed;
  final bool hide;

  const _DigitButton({
    Key key,
    this.digit,
    this.icon,
    this.onPressed,
    this.color,
    this.iconColor,
    this.hide = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final color = this.color ?? Theme.of(context).secondaryHeaderColor;
    final iconColor =
        this.iconColor ?? Theme.of(context).textTheme.bodyText1.color;
    return AnimatedOpacity(
      opacity: hide ? 0.1 : 1,
      duration: Duration(milliseconds: 200),
      child: Container(
        width: 70,
        height: 70,
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
            primary: color,
            onPrimary: Theme.of(context).textTheme.bodyText1.color,
            elevation: 0,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(70)),
              side: BorderSide(width: 1, color: FamedlyColors.slate),
            ),
          ),
          onPressed: hide ? () => null : onPressed,
          child: icon != null
              ? Icon(icon, color: iconColor)
              : Text(
                  digit.toString(),
                  style: TextStyle(
                    fontSize: 20,
                  ),
                ),
        ),
      ),
    );
  }
}
