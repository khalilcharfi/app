/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'dart:io';
import 'dart:math';

import 'package:adaptive_page_layout/adaptive_page_layout.dart';
import 'package:famedly/utils/error_reporter.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';

import '../components/web_enter_listener.dart';
import '../models/famedly.dart';
import '../styles/colors.dart';

class LoginPage extends StatefulWidget {
  @override
  LoginPageState createState() => LoginPageState();
}

class LoginPageState extends State<LoginPage> {
  // This key will uniquely identify the Form widget and allow
  // us to validate the form
  final _formKey = GlobalKey<FormState>();
  final _usernameController = TextEditingController();
  final _passwordController = TextEditingController();
  final _passwordFocusNode = FocusNode();
  String _usernameErrorText;
  String _passwordErrorText;
  bool _isLoading = false;

  static const String famedlyWelcomeMessageNamespace =
      'com.famedly.login.welcome_message';

  Future<String> _loginWithWelcomeMessage(
    Famedly famedly,
    String user,
    String password,
    String initialDeviceDisplayName,
  ) async {
    try {
      await famedly.client.login(
        identifier: AuthenticationUserIdentifier(
          user: _usernameController.text,
        ),
        password: _passwordController.text,
        initialDeviceDisplayName: famedly.clientName +
            ' ' +
            (kIsWeb ? 'Web' : Platform.operatingSystem),
      );
    } on MatrixException catch (exception) {
      if (exception.requireAdditionalAuthentication &&
          exception.authenticationFlows.first.stages
              .contains(famedlyWelcomeMessageNamespace) &&
          exception.authenticationParams[famedlyWelcomeMessageNamespace]
              is Map<String, dynamic>) {
        await famedly.client.login(
          identifier: AuthenticationUserIdentifier(
            user: _usernameController.text,
          ),
          password: _passwordController.text,
          initialDeviceDisplayName: famedly.clientName +
              ' ' +
              (kIsWeb ? 'Web' : Platform.operatingSystem),
          auth: AuthenticationData(
            type: famedlyWelcomeMessageNamespace,
            session: exception.session,
          ),
        );
        return exception.authenticationParams[famedlyWelcomeMessageNamespace]
            ['welcome_message'];
      }
      rethrow;
    }
    return null;
  }

  void submit(BuildContext context) async {
    if (_usernameController.text.isEmpty) {
      setState(
          () => _usernameErrorText = L10n.of(context).pleaseFillOutTextField);
      return;
    }
    _usernameController.text = _usernameController.text.trim();
    _usernameController.text = _usernameController.text.toLowerCase();
    _usernameController.text = _usernameController.text.replaceAll(' ', '_');
    setState(() => _usernameErrorText = null);
    if (_passwordController.text.isEmpty) {
      setState(
          () => _passwordErrorText = L10n.of(context).pleaseFillOutTextField);
      return;
    }
    setState(() => _passwordErrorText = null);

    final famedly = Famedly.of(context);
    setState(() => _isLoading = true);
    String welcomeMessage;
    try {
      welcomeMessage = await _loginWithWelcomeMessage(
        famedly,
        _usernameController.text,
        _passwordController.text,
        famedly.clientName + ' ' + (kIsWeb ? 'Web' : Platform.operatingSystem),
      );
      famedly.loginPlugin.cachedPassword = _passwordController.text;
    } on MatrixConnectionException catch (_) {
      setState(() => _passwordErrorText = L10n.of(context).notConnected);
    } on MatrixException catch (exception) {
      if (exception.error == MatrixError.M_FORBIDDEN) {
        setState(() =>
            _passwordErrorText = L10n.of(context).invalidUsernameOrPassword);
      } else if (exception.error == MatrixError.M_LIMIT_EXCEEDED) {
        setState(() => _passwordErrorText = L10n.of(context).tooManyRequests);
      } else if (exception.error == MatrixError.M_UNRECOGNIZED) {
        setState(() => _passwordErrorText =
            L10n.of(context).organisationServerIsNotResponding);
      } else {
        setState(() => _passwordErrorText = exception.errorMessage);
      }
    } catch (e, s) {
      // ignore: unawaited_futures
      ErrorReporter.reportError(e, s);
      setState(
          () => _passwordErrorText = L10n.of(context).oopsSomethingWentWrong);
    } finally {
      setState(() => _isLoading = false);
      if (!famedly.client.isLogged()) {
        // just to be sure, when we log in we want a new directory session
        (await famedly.getOrganizationDirectoryClient()).invalidateToken();
        return;
      }
    }
    await AdaptivePageLayout.of(context).pushNamed(
      '/intro/${_usernameController.text}',
      arguments: welcomeMessage,
    );
  }

  bool _passwordVisible = false;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      if (Famedly.of(context).magicLink != null) {
        _usernameController.text = Famedly.of(context).magicLink.username;
        _passwordController.text = Famedly.of(context).magicLink.password;
        submit(context);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => !_isLoading,
      child: Scaffold(
        key: Key('LoginPage'),
        appBar: AppBar(
          leading: _isLoading ? Container() : BackButton(),
          elevation: 0,
          title: Text(
            Famedly.of(context)
                .client
                .homeserver
                .toString()
                .replaceAll('https://', ''),
            key: Key('title'),
            style: const TextStyle(
              fontSize: 20,
            ),
          ),
        ),
        body: ListView(
          padding: EdgeInsets.symmetric(
              horizontal:
                  max(0, (MediaQuery.of(context).size.width - 400) / 2)),
          shrinkWrap: true,
          children: <Widget>[
            SizedBox(height: 32),
            Center(
              child: Container(
                width: 256,
                child: Text(
                  L10n.of(context).loginWithYourUsernameAndPassword,
                  textAlign: TextAlign.center,
                ),
              ),
            ),
            SizedBox(height: 32),
            Form(
              key: _formKey,
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16.0),
                    child: TextFormField(
                      key: Key('username'),
                      controller: _usernameController,
                      autocorrect: false,
                      readOnly: _isLoading,
                      autofillHints:
                          _isLoading ? null : [AutofillHints.username],
                      validator: (value) {
                        if (value.isEmpty) {
                          return L10n.of(context).pleaseEnterYourUsername;
                        }
                        if (!(value.length <= 255)) {
                          return L10n.of(context).usernameIncorrect;
                        }
                        return '';
                      },
                      decoration: InputDecoration(
                        errorText: _usernameErrorText,
                        border: OutlineInputBorder(),
                        labelText: L10n.of(context).username,
                      ),
                    ),
                  ),
                  SizedBox(height: 23),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16.0),
                    child: WebEnterListener(
                      focusNode: _passwordFocusNode,
                      onEnter: () => submit(context),
                      child: TextFormField(
                        focusNode: _passwordFocusNode,
                        key: Key('password'),
                        autocorrect: false,
                        readOnly: _isLoading,
                        controller: _passwordController,
                        obscureText: !_passwordVisible,
                        autofillHints:
                            _isLoading ? null : [AutofillHints.password],
                        validator: (value) {
                          if (value.isEmpty) {
                            return L10n.of(context).pleaseEnterYourPassword;
                          }
                          return null;
                        },
                        onEditingComplete: () => null,
                        onFieldSubmitted:
                            kIsWeb ? null : (s) => submit(context),
                        decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          errorText: _passwordErrorText,
                          errorMaxLines: 2,
                          labelText: L10n.of(context).password,
                          suffixIcon: IconButton(
                            icon: Icon(
                              _passwordVisible
                                  ? Icons.visibility
                                  : Icons.visibility_off,
                              color: FamedlyColors.azul,
                            ),
                            onPressed: () {
                              setState(() {
                                _passwordVisible = !_passwordVisible;
                              });
                            },
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 23),
                  Container(
                    width: double.infinity,
                    padding: const EdgeInsets.symmetric(horizontal: 16.0),
                    child: ElevatedButton(
                      key: Key('login'),
                      onPressed: _isLoading ? null : () => submit(context),
                      child: _isLoading
                          ? LinearProgressIndicator()
                          : Text(L10n.of(context).login),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
