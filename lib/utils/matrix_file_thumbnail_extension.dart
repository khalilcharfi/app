/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'dart:typed_data';
import 'dart:ui';

import 'package:famedlysdk/famedlysdk.dart';
import 'package:native_imaging/native_imaging.dart' as native;

extension MatrixFileThumbnailExtension on MatrixImageFile {
  Future<MatrixFile> createThumbnail() async {
    MatrixFile thumbnail;
    await native.init();
    var nativeImg = native.Image();
    try {
      await nativeImg.loadEncoded(bytes);
      width = nativeImg.width;
      height = nativeImg.height;
    } on UnsupportedError {
      final dartCodec = await instantiateImageCodec(bytes);
      final dartFrame = await dartCodec.getNextFrame();
      width = dartFrame.image.width;
      height = dartFrame.image.height;
      final rgbaData = await dartFrame.image.toByteData();
      final rgba = Uint8List.view(
          rgbaData.buffer, rgbaData.offsetInBytes, rgbaData.lengthInBytes);
      dartFrame.image.dispose();
      dartCodec.dispose();
      nativeImg.loadRGBA(width, height, rgba);
    }

    const max = 800;
    if (width > max || height > max) {
      var w = max, h = max;
      if (width > height) {
        h = max * height ~/ width;
      } else {
        w = max * width ~/ height;
      }

      final scaledImg = nativeImg.resample(w, h, native.Transform.lanczos);
      nativeImg.free();
      nativeImg = scaledImg;
    }
    final jpegBytes = await nativeImg.toJpeg(75);
    blurhash = nativeImg.toBlurhash(3, 3);

    thumbnail = MatrixImageFile(
      bytes: jpegBytes,
      name: 'thumbnail.jpg',
      mimeType: 'image/jpeg',
      width: nativeImg.width,
      height: nativeImg.height,
    );

    nativeImg.free();

    if (thumbnail.size > size ~/ 2) {
      thumbnail = null;
    }

    return thumbnail;
  }
}
