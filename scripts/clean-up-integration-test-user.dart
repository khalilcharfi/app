import 'dart:convert';
import 'dart:io';

import 'package:famedlysdk/famedlysdk.dart';

void main() async {
  final client = Client('CleanUpClient');
  Logs().d('Open test-credentials.json...');
  final testCredentialsFile = File('test-credentials.json');
  final Map<String, dynamic> testCredentials =
      jsonDecode(testCredentialsFile.readAsStringSync());
  Logs().d('Login...');
  await client
      .checkHomeserver(Uri.parse('https://' + testCredentials['homeserver']));
  await client.login(
    identifier: AuthenticationUserIdentifier(user: testCredentials['username']),
    password: testCredentials['password'],
  );
  Logs().d('Wait for first sync...');
  while (client.prevBatch == null) {
    await client.onSync.stream.first;
  }
  Logs().d('Leave all rooms...');
  final where = (Room r) =>
      (r.membership == Membership.join || r.membership == Membership.invite) &&
      r.getState('m.room.type')?.content?.tryGet<String>('type') !=
          'com.famedly.room_type.contact_discovery';
  final count = client.rooms.where(where).length;
  while (client.rooms.where(where).isNotEmpty) {
    final room = client.rooms.where(where).first;
    Logs()
        .d('Leave room ${room.displayname} (${client.rooms.length}/$count)...');
    try {
      await room.leave();
      await room.forget();
      await Future.delayed(Duration(seconds: 2));
    } catch (e) {
      if (e is MatrixException && e.error == MatrixError.M_LIMIT_EXCEEDED) {
        Logs().w('Limit exceeded. Wait 20 seconds!');
        await Future.delayed(Duration(seconds: 20));
      } else {
        rethrow;
      }
    }
  }
  Logs().d('Request archive...');
  final archive = await client.archive;
  final archiveCount = archive.length;
  while (archive.isNotEmpty) {
    final room = archive.first;
    Logs().d(
        'Forget room ${room.displayname} (${archive.length}/$archiveCount)...');
    await room.forget().catchError((_) => null);
    archive.removeAt(0);
  }
  Logs().d('Clear Tasks...');
  await client.setAccountData(client.userID, 'com.famedly.talk.tasks', {});
  Logs().d('Logout all...');
  await client.logoutAll();
}
