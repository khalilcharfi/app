/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:famedly/views/directory/directory_page.dart';
import 'package:famedly_directory_client_sdk/famedly_directory_client_sdk.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:future_loading_dialog/future_loading_dialog.dart';
import '../models/famedly.dart';
import '../utils/user_directory_client_extension.dart';

enum InviteType { person, role, contact, integration }

typedef OnInviteCallback = void Function(InviteType, String, [String]);

class InvitePage extends StatelessWidget {
  final String roomId;

  InvitePage({this.roomId});

  Future<void> _onInvite(
      BuildContext context, InviteType type, String uuidOrMxid,
      [String passport]) async {
    final userDirectoryClient =
        await Famedly.of(context).getUserDirectoryClient();
    if (!uuidOrMxid.isValidMatrixId) {
      final userData = await showFutureLoadingDialog(
        context: context,
        future: () =>
            userDirectoryClient.requestUserDataFromUuid(context, uuidOrMxid),
      );
      if (userData.error != null) return;
      uuidOrMxid = userData.result.mxid;
    }
    final room = Famedly.of(context).client.getRoomById(roomId);

    // check if the mxid we are trying to invite already exists in this room
    try {
      final participants = await showFutureLoadingDialog(
        context: context,
        future: () => room.requestParticipants(),
      );
      if (participants.result
          .any((p) => p.id == uuidOrMxid && p.membership == Membership.join)) {
        // the user is already in the room, no need to invite
        await showOkAlertDialog(
          context: context,
          title: L10n.of(context).alreadyInRoom,
          okLabel: L10n.of(context).next,
          useRootNavigator: false,
        );
        return;
      }
    } catch (_) {
      // we'll just assume that the user is *not* in the room
    }

    final bottyInvite = uuidOrMxid.startsWith('@_botty_');
    if (bottyInvite && room.ownPowerLevel < 99) {
      // we can online invite botty if we have PL >= 99
      await showOkAlertDialog(
        context: context,
        title: L10n.of(context).noPermissionForThisAction,
        okLabel: L10n.of(context).next,
        useRootNavigator: false,
      );
      return;
    }
    Profile profile;
    try {
      profile = await room.client.getProfileFromUserId(uuidOrMxid);
    } catch (_) {
      // discard, profile not found
    }
    String displayname;
    if (profile is Profile) {
      displayname = profile.displayname;
    } else {
      displayname = uuidOrMxid.split(':')[0].substring(1);
    }
    var text = 'Invite $type $displayname?';
    switch (type) {
      case InviteType.person:
        text = L10n.of(context).inviteUserToChat(displayname);
        break;
      case InviteType.integration:
        text = L10n.of(context).inviteIntegrationToChat(displayname);
        break;
      case InviteType.role:
        text = L10n.of(context).inviteRoleToChat(displayname);
        break;
      case InviteType.contact:
        text = L10n.of(context).inviteContactToChat(displayname);
        break;
    }

    if (OkCancelResult.ok ==
        await showOkCancelAlertDialog(
          context: context,
          title: text,
          okLabel: L10n.of(context).yes,
          cancelLabel: L10n.of(context).cancel,
          useRootNavigator: false,
        )) {
      await showFutureLoadingDialog(
          context: context,
          future: () async {
            if (bottyInvite) {
              // botty needs to have elevated privileges *prior* being invited
              await room.setPower(
                  uuidOrMxid, room.ownPowerLevel == 99 ? 99 : 100);
            }
            if (passport != null) {
              await room.client.request(
                RequestType.PUT,
                '/client/r0/rooms/${room.id}/state/${Directory.mPassport}/${Uri.encodeComponent(uuidOrMxid)}',
                data: {
                  'token': passport,
                },
              );
            }
            await room.invite(uuidOrMxid);
          });
    }
  }

  @override
  Widget build(BuildContext context) {
    return DirectoryPage(
      key: Key('InvitePage'),
      initialTabIndex: 0,
      inlineView: true,
      onInvite: (InviteType type, String mxid, [String passport]) {
        _onInvite(context, type, mxid, passport);
      },
    );
  }
}
