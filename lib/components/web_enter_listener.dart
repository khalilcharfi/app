/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:universal_html/html.dart' as html;

class WebEnterListener extends StatefulWidget {
  final Widget child;
  final FocusNode focusNode;
  final Function onEnter;

  const WebEnterListener(
      {this.child, @required this.focusNode, @required this.onEnter, Key key})
      : super(key: key);

  @override
  _WebEnterListenerState createState() => _WebEnterListenerState();
}

class _WebEnterListenerState extends State<WebEnterListener> {
  void _onKeyboardEvent(var event) {
    if (widget.focusNode.hasFocus && event.key == 'Enter') {
      widget.onEnter();
    }
  }

  @override
  void initState() {
    super.initState();
    if (kIsWeb) {
      html.document.addEventListener('keydown', _onKeyboardEvent);
    }
  }

  @override
  void dispose() {
    if (kIsWeb) {
      html.document.removeEventListener('keydown', _onKeyboardEvent);
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return widget.child ?? Container();
  }
}
