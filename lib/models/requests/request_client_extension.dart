import 'package:famedly_directory_client_sdk/famedly_directory_client_sdk.dart';
import 'package:famedlysdk/famedlysdk.dart' hide Visibility;
import 'package:famedlysdk/famedlysdk.dart' as sdk show Visibility;

import '../../utils/mxid_encode.dart';
import '../room_type_extension.dart';
import 'request.dart';
import 'request_content.dart';

const String _emailPrefix = '_email_';

extension RequestClientExtension on Contact {
  RequestContent getRequestState({
    String requestTitle,
    String requestBody,
    String requestId,
    String requestType,
    Client client,
  }) {
    var requestedOrganisation = uri;
    if (matrixId != null) {
      requestedOrganisation = matrixId.domain;
    } else if (email != null) {
      requestedOrganisation = email.split('@')[1];
    }
    return RequestContent(
      creator: client.userID,
      requestedOrganisation: requestedOrganisation,
      requestingOrganisation: client.userID.domain,
      requestTitle: requestTitle,
      requestId: requestId,
      msgtype: 'm.text',
      body: requestBody,
      organisationId: organisation?.id,
      organisationName: organisation?.name,
      contactId: id,
      contactDescription: description,
      requestType: requestType,
      isBroadcast: false,
    );
  }

  List<Map<String, dynamic>> getInitialState({
    String requestTitle,
    String requestBody,
    String requestId,
    String requestType,
    Client client,
  }) {
    return [
      {
        'type': Request.requestNameSpace,
        'content': getRequestState(
          requestTitle: requestTitle,
          requestBody: requestBody,
          requestId: requestId,
          requestType: requestType,
          client: client,
        ).toJson(),
      },
      {
        'type': 'm.room.history_visibility',
        'content': {'history_visibility': 'shared'}
      },
      {
        'type': 'm.room.guest_access',
        'content': {'guest_access': 'forbidden'}
      },
      {
        'type': Directory.mPassport,
        'content': {'token': passport},
      },
      {
        'type': 'm.room.type',
        'content': {'type': roomTypeRequest},
      }
    ];
  }

  /// Starts an official request to this organisation at this contact point. Returns
  /// the Matrix room ID if successfull.
  Future<String> startRequest(
      {String requestTitle,
      String requestBody,
      String requestId,
      String requestType,
      Client client}) async {
    // Only matrix.to links are supported for now.
    var roomId = '';
    if (matrixId != null) {
      roomId = await client.createRoom(
        invite: [matrixId],
        visibility: sdk.Visibility.private,
        preset: CreateRoomPreset.private_chat,
        name: Request.requestNameSpace,
        initialState: getInitialState(
          requestTitle: requestTitle,
          requestBody: requestBody,
          requestId: requestId,
          requestType: requestType,
          client: client,
        ),
        powerLevelContentOverride: {
          'events': {
            Request.requestStateNameSpace: 0,
          },
          'users': {
            client.userID: 100,
            matrixId: 100,
          },
        },
      );
    } else if (email != null) {
      final host = client.userID.domain;
      final mxid = '@' + _emailPrefix + str2mxid(email) + ':' + host;
      roomId = await client.createRoom(
        invite: [mxid],
        visibility: sdk.Visibility.private,
        preset: CreateRoomPreset.private_chat,
        name: requestTitle,
        initialState: getInitialState(
          requestTitle: requestTitle,
          requestBody: requestBody,
          requestId: requestId,
          client: client,
        ),
      );
    }

    return roomId;
  }
}
