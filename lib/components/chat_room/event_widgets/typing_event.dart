import 'dart:async';

import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/material.dart';

import 'package:flutter_gen/gen_l10n/l10n.dart';
import '../../../styles/colors.dart';
import '../../avatar.dart';

class TypingEvent extends StatefulWidget {
  final Room room;
  const TypingEvent(this.room);

  @override
  _TypingEventState createState() => _TypingEventState();
}

class _TypingEventState extends State<TypingEvent> {
  Timer animationTimer;
  bool shadowVisible = true;

  @override
  void dispose() {
    animationTimer?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final firstWhere = widget.room.typingUsers.firstWhere(
        (u) => u.id.toLowerCase() != widget.room.client.userID.toLowerCase(),
        orElse: () => null);
    final typingUser = firstWhere;
    if (typingUser != null) {
      animationTimer ??= Timer.periodic(
        Duration(seconds: 1),
        (t) => setState(() => shadowVisible = !shadowVisible),
      );
    }
    return AnimatedContainer(
      duration: Duration(milliseconds: 500),
      margin: EdgeInsets.symmetric(
        horizontal: 15,
      ),
      height: typingUser == null ? 0 : 52,
      child: typingUser == null
          ? Container()
          : Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                AnimatedContainer(
                  width: 32,
                  height: 32,
                  duration: Duration(seconds: 1),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(32),
                    boxShadow: [
                      BoxShadow(
                        color: shadowVisible
                            ? FamedlyColors.blueyGrey
                            : Theme.of(context).backgroundColor,
                        blurRadius: shadowVisible ? 1.0 : 0,
                        spreadRadius: shadowVisible ? 5.0 : 0,
                      ),
                    ],
                  ),
                  child: Avatar(
                    typingUser.avatarUrl,
                    size: 32,
                    name: typingUser.calcDisplayname(
                        mxidLocalPartFallback: false),
                  ),
                ),
                SizedBox(width: 10),
                Expanded(
                  child: Container(
                    height: 32,
                    alignment: Alignment.centerLeft,
                    child: Text(
                      L10n.of(context).isTyping,
                      style: TextStyle(
                        color: FamedlyColors.slateGrey,
                        fontSize: 14,
                      ),
                    ),
                  ),
                ),
              ],
            ),
    );
  }
}
