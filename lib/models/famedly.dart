/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'dart:core';
import 'dart:io';

import 'package:adaptive_page_layout/adaptive_page_layout.dart';
import 'package:famedly/config/chat_configs.dart';
import 'package:famedly/models/plugins/analytics_plugin.dart';
import 'package:famedly/models/plugins/screenshot_warning_plugin.dart';
import 'package:famedly/models/plugins/uia_request_plugin.dart';
import 'package:famedly/utils/famedlysdk_store.dart';
import 'package:famedly/utils/platform_extension.dart';
import 'package:famedly_directory_client_sdk/famedly_directory_client_sdk.dart';
import 'package:famedly_user_directory_client_sdk/famedly_user_directory_client_sdk.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../utils/famedlysdk_client.dart';
import '../utils/magic_link.dart';
import 'plugins/app_outdated_plugin.dart';
import 'plugins/background_push_plugin.dart';
import 'plugins/background_sync_plugin.dart';
import 'plugins/contact_discovery_plugin.dart';
import 'plugins/local_notification_plugin.dart';
import 'plugins/login_plugin.dart';
import 'plugins/matrix_error_plugin.dart';
import 'plugins/room_key_request_plugin.dart';
import 'plugins/rooms_plugin.dart';
import 'plugins/voip_plugin.dart';

/// Use this widget as root for your widget tree and get access to the client
/// connection by calling [Famedly.of(context)].
class Famedly {
  static const inviteLinkPrefix = 'com.famedly.contact://';
  static const magicLinkPrefix = 'com.famedly://';
  static const String covidBotId = '@covbot:shortestpath.dev';
  static const String wallpaperNamespace = 'com.famedly.app.wallpaper';
  static const int wallpaperCount = 6;

  final GlobalKey<AdaptivePageLayoutState> adaptivePageLayout;

  final String clientName;

  final BuildContext context;

  Famedly({
    this.clientName,
    this.context,
    this.adaptivePageLayout,
    Client matrixClient,
  }) {
    client ??= matrixClient ?? getClient(clientName, true);

    if (kIsMobile) {
      pushNotificationPlugin = BackgroundPushPlugin(this);
    }
    if (!kIsWeb && Platform.isIOS) {
      screenshotWarningPlugin = ScreenshotWarningPlugin(this);
    }
    localNotificationPlugin = LocalNotificationPlugin(this);
    contactDiscoveryPlugin = ContactDiscoveryPlugin(this);
    loginPlugin = LoginPlugin(this, clientName);
    matrixErrorPlugin = MatrixErrorPlugin(this);
    uiaRequestPlugin = UiaRequestPlugin(this);
    roomsPlugin = RoomsPlugin(this);
    voipPlugin = VoipPlugin(this);
    appOutdatedPlugin = AppOutdatedPlugin(this);
    roomKeyRequestplugin = RoomKeyRequestplugin(this);
    backgroundSyncPlugin = BackgroundSyncPlugin(this);
    analyticsPlugin = AnalyticsPlugin(this);
  }

  /// Returns the (nearest) Client instance of your application or null if none is found.
  static Famedly of(BuildContext context) =>
      Provider.of<Famedly>(context, listen: false);

  Client client;
  Directory _directory;
  UserDirectoryClient _userDirectoryClient;

  /// If the user intends to forward a message, the content will be stored here. If
  /// this is null, there is nothing to forward.
  Map<String, dynamic> forwardContent;
  MatrixFile forwardFile;

  /// The id of a room which should not send notifications.
  String activeRoomID;

  MagicLink magicLink;

  bool get isInDebugMode {
    // Assume you're in production mode
    var inDebugMode = false;

    // Assert expressions are only evaluated during development. They are ignored
    // in production. Therefore, this code only sets `inDebugMode` to true
    // in a development environment.
    assert(inDebugMode = true);

    return inDebugMode;
  }

  Future<Directory> getOrganizationDirectoryClient() async {
    if (_directory == null) {
      final baseUrl = await Store()
          .getItem('famedlyOrganizationDirectoryBaseUrl')
          .timeout(Duration(seconds: 1))
          .catchError((e) => null);
      initOrganizationDirectory(
          baseUrl ?? ChatConfigs.fallbackOrganizationDirectoryBaseUrl);
    }
    return _directory;
  }

  Future<UserDirectoryClient> getUserDirectoryClient() async {
    final baseUrl = await Store()
        .getItem('famedlyUserDirectoryBaseUrl')
        .timeout(Duration(seconds: 1))
        .catchError((e) => null);
    _userDirectoryClient ??= UserDirectoryClient(
        baseUrl: baseUrl ?? ChatConfigs.fallbackUserDirectoryBaseUrl);
    return _userDirectoryClient;
  }

  void setUserDirectoryClient(UserDirectoryClient newUserDirectoryClient) {
    _userDirectoryClient = newUserDirectoryClient;
  }

  void initOrganizationDirectory(String baseUrl) {
    _directory = Directory(
      getOpenIdCredentials: () async {
        final credentials = await client.requestOpenIdToken(client.userID);
        return credentials.toJson();
      },
      webMode: kIsWeb,
      baseUrl: baseUrl,
    );
  }

  BackgroundPushPlugin pushNotificationPlugin;
  ScreenshotWarningPlugin screenshotWarningPlugin;
  LocalNotificationPlugin localNotificationPlugin;
  ContactDiscoveryPlugin contactDiscoveryPlugin;
  LoginPlugin loginPlugin;
  MatrixErrorPlugin matrixErrorPlugin;
  UiaRequestPlugin uiaRequestPlugin;
  RoomsPlugin roomsPlugin;
  VoipPlugin voipPlugin;
  AppOutdatedPlugin appOutdatedPlugin;
  RoomKeyRequestplugin roomKeyRequestplugin;
  BackgroundSyncPlugin backgroundSyncPlugin;
  AnalyticsPlugin analyticsPlugin;

  void dispose() {
    localNotificationPlugin.onFocusSub?.cancel();
    localNotificationPlugin.onBlurSub?.cancel();
    pushNotificationPlugin?.onLogin?.cancel();
    localNotificationPlugin?.famedly = null;
    loginPlugin.onLogin?.cancel();
    matrixErrorPlugin.olmErrorSub?.cancel();
    uiaRequestPlugin.onUiaRequest?.cancel();
    matrixErrorPlugin.databaseErrorSub?.cancel();
    analyticsPlugin.onLogin?.cancel();
    screenshotWarningPlugin.screenshotCallback.dispose();
  }
}
