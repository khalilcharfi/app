/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'dart:convert';
import 'dart:core';

import 'package:http/http.dart';
import 'package:http/testing.dart';

class FakeDirectory extends MockClient {
  FakeDirectory()
      : super((request) async {
          // Collect data from Request
          final path2 = request.url.path;
          final action = path2;
          final method = request.method;
          final dynamic data =
              method == 'GET' ? request.url.queryParameters : request.body;
          var res;

          //print('Calling: $method $action');

          for (final org in organisations) {
            final orgPath = '/api/v1/organisations/' + org['id'];
            if (api['GET'][orgPath] == null) {
              api['GET'][orgPath] = (var req) => org;
            }
            if (api['GET'][orgPath + '/contacts'] == null) {
              api['GET'][orgPath + '/contacts'] = (var req) => org['contacts'];
            }
            for (final c in org['contacts']) {
              final cPath =
                  '/api/v1/organisations/' + org['id'] + '/contacts/' + c['id'];
              if (api['GET'][cPath] == null) {
                api['GET'][cPath] = (var req) => c;
              }
            }
          }

          for (final type in types) {
            final typePath = '/api/v1/types/' + type['id'];
            if (api['GET'][typePath] == null) {
              api['GET'][typePath] = (var req) => type;
            }
          }

          for (final tag in tags) {
            final tagPath = '/api/v1/tags/' + tag['id'];
            if (api['GET'][tagPath] == null) {
              api['GET'][tagPath] = (var req) => tag;
            }
          }

          // Sync requests with timeout
          if (data is Map<String, dynamic> && data['timeout'] is String) {
            await Future.delayed(Duration(seconds: 5));
          }

          // Call API
          if (api.containsKey(method) && api[method].containsKey(action)) {
            res = api[method][action](data);
          } else {
            res = {
              'errcode': 'M_UNRECOGNIZED',
              'error': 'Unrecognized request'
            };
          }

          return Response(json.encode(res), 200,
              headers: {'Content-Type': 'application/json'});
        });

  static List<Map<String, dynamic>> organisations = [
    {
      'id': '3e4d98f9-1544-4c0d-bce3-24509e92ba0b',
      'name': 'Petrus Krankenhaus Wuppertal',
      'location': {
        'latitude': 52.81,
        'longitude': 14.32,
        'street': 'some street',
        'number': 'some number',
        'zip_code': 'some zip code',
        'state': 'some state',
        'city': 'Berlin',
        'country': 'some country'
      },
      'org_type': '4747daf1-acbc-4102-8f6e-52710c14d57e',
      'tags': [
        '02f1260e-d140-4a16-8a3a-db4716ee36d9',
        '7d35e4b4-b0a6-483a-a2da-7dfea9ee274d'
      ],
      'children': [
        '3e4d98f9-1544-4c0d-bce3-24509e92ba0b',
        '3d8dffd4-5e03-4337-a016-16cffc290e0b'
      ],
      'visibility': {
        'include': ['*'],
        'exclude': []
      },
      'contacts': [
        {
          'visibility': {
            'include': [
              'd9d923ad-1efe-4aa6-b66b-967f5bbf300b',
              'd865fcde-5223-4eea-a72c-1c76955b4e89'
            ],
            'exclude': ['*']
          },
          'id': '83f42853-9530-40e3-a961-34ef00f6e09a',
          'description': 'Allgemeinmedizin (Fox)',
          'uri': 'https://matrix.to/#/@fox:192.168.1.13',
          'passport': '1234pass',
          'tags': [
            '3e4d98f9-1544-4c0d-bce3-24509e92ba0b',
            '3d8dffd4-5e03-4337-a016-16cffc290e0b'
          ]
        },
        {
          'visibility': {
            'include': ['*'],
            'exclude': []
          },
          'id': '05106e21-995e-4b22-81a7-aec0158ca61a',
          'description': 'Abstellkammer (Bunny)',
          'uri': 'https://matrix.to/#/@bunny:192.168.1.13',
          'passport': '1234pass',
          'tags': [
            '3e4d98f9-1544-4c0d-bce3-24509e92ba0b',
            '3d8dffd4-5e03-4337-a016-16cffc290e0b'
          ]
        }
      ]
    },
    {
      'id': 'bb1397d8-e265-467b-9f51-1af9c2cb8277',
      'name': 'Externes Krankenhaus',
      'org_type': 'e2ff0ec1-7032-4423-ba3b-93b3523873a7',
      'tags': [],
      'children': [],
      'visibility': {
        'include': ['*'],
        'exclude': [],
      },
      'contacts': [
        {
          'visibility': {
            'include': ['*'],
            'exclude': [],
          },
          'id': '7b74e5b3-8e1a-4f9e-a955-bec40465805d',
          'description': 'Allgemeine Anfragen',
          'uri': 'mailto:mail@sorunome.de',
          'passport': '1234pass',
          'tags': [],
        },
      ],
    },
    {
      'id': '4af78817-2c4b-4bc9-a8c1-66f38ba8af39',
      'name': 'Externes Multi-Krankenhaus',
      'org_type': 'e2ff0ec1-7032-4423-ba3b-93b3523873a7',
      'tags': [],
      'children': [],
      'visibility': {
        'include': ['*'],
        'exclude': [],
      },
      'contacts': [
        {
          'visibility': {
            'include': ['*'],
            'exclude': [],
          },
          'id': '9b630946-c60b-4e2d-8141-0ddb8fb5f760',
          'description': 'Allgemeine Anfragen',
          'uri': 'mailto:allgemein@example.com',
          'passport': '1234pass',
          'tags': [],
        },
        {
          'visibility': {
            'include': ['*'],
            'exclude': [],
          },
          'id': 'adf26590-ae21-4aeb-9c06-16ad6653de9d',
          'description': 'Spezifische Anfragen',
          'uri': 'mailto:spezifisch@example.com',
          'passport': '1234pass',
          'tags': [],
        },
      ],
    },
  ];

  static List<Map<String, dynamic>> types = [
    {
      'id': '4747daf1-acbc-4102-8f6e-52710c14d57e',
      'description': 'Hospital',
      'parent': '',
    },
    {
      'id': 'e2ff0ec1-7032-4423-ba3b-93b3523873a7',
      'description': 'Extern',
      'parent': '',
    },
  ];

  static List<Map<String, dynamic>> tags = [
    {
      'id': '02f1260e-d140-4a16-8a3a-db4716ee36d9',
      'description': 'Medikamentenbringdienst'
    },
    {
      'id': '3e4d98f9-1544-4c0d-bce3-24509e92ba0b',
      'description': 'Patientenübernahme'
    },
    {'id': '7d35e4b4-b0a6-483a-a2da-7dfea9ee274d', 'description': 'Faxgeräte'},
  ];

  static final Map<String, Map<String, dynamic>> api = {
    'GET': {
      '/api/v1/search': (var req) => {
            'organisations': organisations,
            'types': types,
            'tags': tags,
          },
      '/api/v1/organisations': (var req) => organisations,
      '/api/v1/types': (var req) => types,
      '/api/v1/scopes/3e4d98f9-1544-4c0d-bce3-24509e92ba0b': (var req) =>
          {'id': '3e4d98f9-1544-4c0d-bce3-24509e92ba0b', 'subjects': '.*'},
      '/api/v1/tags': (var req) => tags,
    },
    'POST': {},
    'PUT': {},
    'DELETE': {},
  };
}
