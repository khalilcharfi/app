/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import '../styles/colors.dart';

class FamedlyIconButton extends StatelessWidget {
  @override
  final Key key;
  final IconData iconData;
  final onTap;
  final double size;
  final double iconSize;
  final Color color;
  final Color backgroundColor;
  final double elevation;

  FamedlyIconButton({
    this.key,
    this.iconData,
    this.onTap,
    this.size = 35,
    this.iconSize = 24,
    this.color = FamedlyColors.blueyGrey,
    this.backgroundColor,
    this.elevation = 1,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: size,
      height: size,
      margin: EdgeInsets.all(1.5),
      child: Material(
        elevation: elevation,
        color: backgroundColor ?? FamedlyColors.lightGrey,
        type: MaterialType.circle,
        clipBehavior: Clip.antiAliasWithSaveLayer,
        child: InkWell(
          onTap: onTap,
          child: Center(
            child: Icon(
              iconData,
              size: iconSize,
              color: color,
            ),
          ),
        ),
      ),
    );
  }
}
