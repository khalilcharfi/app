Push notifications are sent from the Matrix server to inform a user about new messages even when the app is in background or terminated. The push notifications are only containing the event ID and the room ID of the new message.

The notification is first sent to the Sygnal Push Gateway which forwards the notification to Firebase Cloud Messaging. If the receiving device is offline, Firebase stores the notification and sends it when the device comes online again.

![Famedly_Push_Notifications](img/famedly-push-notifications.png)

If there are problems with notifications like missing notifications or delays, it's hard to find out which component has a problem. In the app you can send a test notification in Settings -> Info. The app will send a fake notification directly to Sygnal which forwards it then to Firebase and back to your device. If this works without any big delay, the problem might be at the Matrix server side. If this has a delay, but still shows "Push Gateway is working", then you might have a slow internet connection. If there is a delay and a fallback message is shown with "? unread notifications", the push helper in the app might be broken.
