import 'package:famedlysdk/famedlysdk.dart';

import '../famedly.dart';
import '../room_type_extension.dart';
import 'famedly_plugin.dart';

class ContactDiscoveryPlugin extends FamedlyPlugin {
  ContactDiscoveryPlugin(matrix) : super(matrix);

  Future<List<User>> loadContacts({String roomId, String query}) async {
    // Load from contact discovery rooms
    final list = <String, User>{};
    for (final room in famedly.client.rooms) {
      if (room.type == roomTypeContactDiscovery) {
        final users = await room.requestParticipants();
        for (final u in users) {
          if (u.membership == Membership.join) {
            list[u.id] = u;
          }
        }
      }
      if (room.isDirectChat &&
          room.type == roomTypeMessaging &&
          room.directChatMatrixID != Famedly.covidBotId) {
        list[room.directChatMatrixID] =
            room.getUserByMXIDSync(room.directChatMatrixID);
      }
    }

    list.removeWhere(
        (k, u) => u.id == famedly.client.userID || u.id.startsWith('@_botty_'));
    if (roomId != null) {
      final room = famedly.client.getRoomById(roomId);
      final userBlackList = (await room.requestParticipants())
          .where((u) => u.membership != Membership.leave)
          .toList();
      list.removeWhere(
          (k, u) => userBlackList.any((l) => l.stateKey == u.stateKey));
    }

    return List.from(list.values);
  }
}
