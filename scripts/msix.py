import base64
import logging
import os
import re
import subprocess
from benedict import benedict


def write_cert():
    path = os.path.abspath("./code-signing-famedly.pfx")
    with open(os.getenv('windows_sign_cert_file'), 'r') as file:
        certdata_base64 = file.read()
        certdata = base64.b64decode(certdata_base64)
        with open(path, 'wb') as signing_file:
            signing_file.write(certdata)
            return path


def set_cert_path_and_pass_in_pubspec_yaml(path):
    pubspec_path = os.path.abspath("./pubspec.yaml")
    if not os.path.exists(path):
        logging.error("Cert file is missing")
        sys.exit(1)

    if not os.path.isfile(path):
        logging.error("Cert file is not a file")
        sys.exit(1)

    password = os.getenv('WINDOWS_SIGN_CERT_PASSWORD')
    if password is None:
        logging.error("Certificate password is missing")
        sys.exit(1)
        
    version = get_famedly_version_as_windows_version()
    lines = []
    with open(pubspec_path) as infile:
        for line in infile:
            line = line.replace('##CERT_PATH##', "'" + path + "'").replace('##CERT_PASSWORD##', "'" + password + "'").replace('##VERSION##', "'" + version + "'")
            lines.append(line)
    with open(pubspec_path, 'w') as outfile:
        for line in lines:
            outfile.write(line)

def set_assets_path_in_pubspec_yaml(path):
    pubspec_path = os.path.abspath("./pubspec.yaml")
    lines = []
    with open(pubspec_path) as infile:
        for line in infile:
            line = line.replace('##ASSETS##', "'" + path + "'")
            lines.append(line)
    with open(pubspec_path, 'w') as outfile:
        for line in lines:
            outfile.write(line)

def get_famedly_version_as_windows_version():
    pubspec_path = os.path.abspath("./pubspec.yaml")
    d = benedict.from_yaml(pubspec_path)
    original_version = d["version"]
    version = re.sub(r"\+(.*)", ".0", original_version)
    return version

def set_appinstaller_version():
    version = get_famedly_version_as_windows_version()
    appinstaller_file = os.path.abspath("./famedly.appinstaller")
    with open(appinstaller_file, 'r+') as f:
        content = f.read()
        f.seek(0)
        f.truncate()
        f.write(content.replace('%%VERSION%%', version))

def sign_exe_dll(path):
    password = os.getenv('WINDOWS_SIGN_CERT_PASSWORD')
    if password is None:
        logging.error("Certificate password is missing")
        sys.exit(1)

    if not os.path.exists(path):
        logging.error("Cert file is missing")
        sys.exit(1)

    if not os.path.isfile(path):
        logging.error("Cert file is not a file")
        sys.exit(1)

    try:
        subprocess.run(["C:/Program Files (x86)/Windows Kits/10/App Certification Kit/signtool.exe", "sign", "/tr", "http://timestamp.digicert.com", "/debug", "/v", "/fd", "SHA256", "/a", "/f", path, "/p", password, os.path.abspath("./build/windows/runner/Release/famedly.exe")], shell=False)
    except subprocess.CalledProcessError as e:
        logging.error("EXE Sign failed due to: " + e)
        sys.exit(1)
    try:
        subprocess.run(["C:/Program Files (x86)/Windows Kits/10/App Certification Kit/signtool.exe", "sign", "/tr", "http://timestamp.digicert.com", "/debug", "/v", "/fd", "SHA256", "/a", "/f", path, "/p", password, os.path.abspath("./build/windows/runner/Release/libolm.dll")], shell=False)
    except subprocess.CalledProcessError as e:
        logging.error("libolm Sign failed due to: " + e)
        sys.exit(1)
    try:
        subprocess.run(["C:/Program Files (x86)/Windows Kits/10/App Certification Kit/signtool.exe", "sign", "/tr", "http://timestamp.digicert.com", "/debug", "/v", "/fd", "SHA256", "/a", "/f", path, "/p", password, os.path.abspath("./build/windows/runner/Release/libcrypto.dll")], shell=False)
    except subprocess.CalledProcessError as e:
        logging.error("libcrypto Sign failed due to: " + e)
        sys.exit(1)

def run_msix_generator():
    try:
        subprocess.run(["flutter", "pub", "run", "msix:create"], shell=True)
    except subprocess.CalledProcessError as e:
        logging.error("Generating and building msix failed due to: " + e)


if __name__ == '__main__':
    logging.info("Write cert file")
    cert_path = write_cert()
    set_cert_path_and_pass_in_pubspec_yaml(cert_path)
    set_assets_path_in_pubspec_yaml(os.path.abspath("./assets/windows-icons"))

    logging.info("Sign Exe and DLL")
    sign_exe_dll(cert_path)

    logging.info("Run msix generation")
    run_msix_generator()

    logging.info("Set appinstaller version")
    set_appinstaller_version()
