import 'package:famedly/views/room_settings.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'utils.dart';

void main() {
  /// All Tests related to the RoomSettings
  group(
    'RoomSettings',
    () {
      NavigatorObserver mockObserver;

      setUp(() {
        mockObserver = MockNavigatorObserver();
      });

      /// GroupChatSettingsPage Test
      testWidgets(
        'works with GroupChatSettingsPage',
        (WidgetTester tester) async {
          await tester.runAsync(() async {
            // Create widget
            await tester.pumpWidget(
              await Utils.getWidgetWrapper(
                RoomSettings(
                  id: '!726s6s6q:example.com',
                ),
                mockObserver,
              ),
            );

            await tester.pump();
            Utils.printWidgets(tester);

            // Check for MembersHeader
            //expect(find.text('Members'), findsOneWidget);

            // Check for MediaHeader
            //expect(find.text('Media'), findsOneWidget);

            // Check for Images Button
            //expect(find.text('Images'), findsOneWidget);

            /*// Check for Videos Button
          expect(find.text('Videos'), findsOneWidget);

          // Check for Files Button
          expect(find.text('Files'), findsOneWidget);*/

            // Check for Leave group Button
            //expect(find.text('Leave group'), findsOneWidget);
          });
        },
      );
    },
  );
}
