/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'package:famedly/components/circular_check_box.dart';
import 'package:famedly/styles/colors.dart';
import 'package:famedly/views/tasks/task_details_page.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';

class TaskDetailsPageView extends StatelessWidget {
  final TaskDetailsPageController controller;

  const TaskDetailsPageView(this.controller, {Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final id = int.parse(controller.widget.id);
    if (id >= controller.taskList.tasks.length) {
      return Scaffold(
        appBar: AppBar(
          leading: BackButton(),
          elevation: 0,
        ),
        body: Center(
          child: Text(L10n.of(context).noTasks),
        ),
      );
    }
    return Scaffold(
        key: Key('taskPage'),
        appBar: AppBar(
          leading: BackButton(),
          elevation: 0,
          title: Text(controller.task.title),
          actions: <Widget>[
            IconButton(
              key: Key('DeleteIconButton'),
              icon: Icon(Icons.delete, color: FamedlyColors.grapefruit),
              onPressed: () => controller.deleteTask(),
            )
          ],
        ),
        body: ListView(
          padding: FamedlyStyles.getTwoColumnListViewPadding(context),
          children: <Widget>[
            Padding(
              padding: EdgeInsets.all(30),
              child: Text(
                controller.task.title,
                style: TextStyle(fontSize: 19, fontWeight: FontWeight.bold),
              ),
            ),
            TextField(
              key: Key('DescriptionTextField'),
              controller: controller.descController,
              decoration: InputDecoration(
                  hintText: controller.task.description.isEmpty
                      ? L10n.of(context).addDescription
                      : controller.task.description,
                  hintStyle: TextStyle(fontSize: 15),
                  border: InputBorder.none,
                  disabledBorder: InputBorder.none,
                  enabledBorder: InputBorder.none,
                  errorBorder: InputBorder.none,
                  focusedBorder: InputBorder.none,
                  focusedErrorBorder: InputBorder.none,
                  prefixIcon: Padding(
                    padding: EdgeInsets.only(left: 30, right: 18, bottom: 5),
                    child: Icon(Icons.menu),
                  )),
              onEditingComplete: () => controller.editingComplete(),
            ),
            ListTile(
              leading: Padding(
                  padding: EdgeInsets.only(top: 15, left: 15, bottom: 15),
                  child: Icon(Icons.calendar_today)),
              title: controller.task.date == null
                  ? Text(L10n.of(context).addDate,
                      style: TextStyle(fontSize: 15))
                  : Align(
                      alignment: Alignment.centerLeft,
                      child: Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(5)),
                            border:
                                Border.all(color: FamedlyColors.cloudyBlue)),
                        child: Padding(
                          padding: EdgeInsets.all(9),
                          child: Row(
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                    DateFormat(L10n.of(context).localTimeFormat)
                                        .format(controller.task.date),
                                    style: TextStyle(
                                        color: FamedlyColors.azul,
                                        fontSize: 15)),
                              ),
                              InkWell(
                                onTap: () => controller.deleteDate(),
                                child: Icon(Icons.clear),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
              onTap: () => controller.changeDate(),
            ),
            Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                      padding: EdgeInsets.only(top: 15, left: 30, bottom: 15),
                      child: Icon(Icons.arrow_forward)),
                  Expanded(
                    child: Container(
                        child: ListView.builder(
                      shrinkWrap: true,
                      // +1 to show the add subtask textfield at bottom.
                      itemCount: controller.task.subtask.length + 1,
                      itemBuilder: (context, index) {
                        if (index < controller.task.subtask.length) {
                          return ListTile(
                            leading: CircularCheckBox(
                              value: controller.task.subtask[index].done,
                              activeColor: FamedlyColors.azul,
                              onChanged: (bool newChecked) => controller
                                  .comepleteSubtask(index, newChecked),
                            ),
                            title: Text(
                              controller.task.subtask[index].title,
                              style: TextStyle(
                                  decoration:
                                      controller.task.subtask[index].done
                                          ? TextDecoration.lineThrough
                                          : TextDecoration.none),
                            ),
                          );
                        }
                        return TextField(
                          key: Key('SubTaskTextField'),
                          controller: controller.subTaskController,
                          decoration: InputDecoration(
                            contentPadding: EdgeInsets.all(17.5),
                            hintText: L10n.of(context).addSubtask,
                            hintStyle: TextStyle(fontSize: 15),
                            border: InputBorder.none,
                            disabledBorder: InputBorder.none,
                            enabledBorder: InputBorder.none,
                            errorBorder: InputBorder.none,
                            focusedBorder: InputBorder.none,
                            focusedErrorBorder: InputBorder.none,
                          ),
                          onSubmitted: (text) => controller.addSubtask(text),
                        );
                      },
                    )),
                  ),
                ]),
          ],
        ));
  }
}
