import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:adaptive_page_layout/adaptive_page_layout.dart';
import 'package:famedly/utils/matrix_locals.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:future_loading_dialog/future_loading_dialog.dart';

import '../../models/famedly.dart';
import '../../models/requests/broadcast_content.dart';
import '../../models/requests/request.dart';
import '../../models/requests/room_extension.dart';
import '../../styles/colors.dart';
import '../../utils/date_time_extension.dart';
import '../../utils/room_archive_dialog_extension.dart';
import '../../utils/room_status_extension.dart';
import '../avatar.dart';
import '../default_modal_bottom_sheet.dart';
import '../dialogs/new_icoming_request_dialog.dart';
import '../mouse_over_builder.dart';
import '../unread_bubble.dart';

/// Generates the widget for each room item.
class RequestItem extends StatelessWidget {
  final Room room;
  final List<BroadcastContactsContent> contacts;
  final bool colorBar;
  final String type;
  final bool activeChat;

  const RequestItem({
    Key key,
    @required this.room,
    this.contacts,
    this.colorBar = false,
    @required this.type,
    this.activeChat = false,
  }) : super(key: key);

  Color get newsColor {
    if (room.highlightCount > 0) return FamedlyColors.grapefruit;
    if (room.notificationCount > 0) return FamedlyColors.emerald;
    return Colors.grey;
  }

  List<BroadcastContactsContent> getUpdatedContactsList() {
    List<BroadcastContactsContent> requestContacts;
    if (Request(room).broadcast != null &&
        (contacts == null || contacts.isEmpty)) {
      requestContacts = Request.getBroadcastContacts(room, room.client.userID);
    }
    requestContacts ??= contacts;
    return requestContacts;
  }

  @override
  Widget build(BuildContext context) {
    Request req;
    if (contacts != null && contacts.length == 1) {
      req = Request(room, contact: contacts[0]);
    } else {
      req = Request(room);
    }
    final multiContact = contacts != null && contacts.length != 1;
    var organisation = req.organisation;
    if (multiContact) {
      organisation = L10n.of(context).broadcast;
    }

    var statusColor = FamedlyColors.cloudyBlue;
    if (req.remoteAccepted()) {
      statusColor = FamedlyColors.aquaMarine;
    }
    if (req.remoteNeedInfo()) {
      statusColor = FamedlyColors.yellow;
    }
    if (req.remoteRejected()) {
      statusColor = FamedlyColors.grapefruit;
    }

    final title = req.description;
    var subtitle = req.isAuthor()
        ? organisation
        : (room.requestContent?.requestingOrganisation
                ?.replaceAll('.famedly.de', '') ??
            L10n.of(context).unknownRequest);
    final requestFuture = !req.isAuthor() && room.requestContent != null
        ? Famedly.of(context).getOrganizationDirectoryClient().then(
            (directory) => directory
                .getOrganisation(room.requestContent.requestingOrganisation)
                .then((o) => o.name))
        : null;
    if (subtitle == null || subtitle.isEmpty) {
      subtitle = L10n.of(context).createdOn +
          room.timeCreated.localizedTimeShort(context);
    }

    final isTyping =
        room.typingUsers.where((u) => u.id != room.client.userID).isNotEmpty;
    final body = room.membership == Membership.invite
        ? L10n.of(context).youHaveBeenInvitedToChat
        : isTyping
            ? room.getLocalizedTypingText(context)
            : room.lastEvent != null
                ? room.lastEvent.getLocalizedBody(
                    MatrixLocals(L10n.of(context)),
                    withSenderNamePrefix: !room.isDirectChat ||
                        room.lastEvent.senderId == room.client.userID,
                    hideReply: true,
                  )
                : '';

    return MouseOverBuilder(
      builder: (context, hover) => Row(
        children: <Widget>[
          if (colorBar)
            Container(
              color: statusColor,
              width: 5,
              height: 80,
            ),
          Expanded(
            child: ListTile(
              selected: activeChat,
              selectedTileColor: Theme.of(context).secondaryHeaderColor,
              onLongPress: () => showModalBottomSheet(
                context: context,
                backgroundColor: Colors.transparent,
                builder: (context) => DefaultModalBottomSheet(
                  child: ListView(shrinkWrap: true, children: [
                    ListTile(
                      title: Text(
                        L10n.of(context).archiveAction,
                        style: TextStyle(color: FamedlyColors.grapefruit),
                      ),
                      onTap: () => room
                          .showArchiveDialog(context, popAfter: false)
                          .then((_) =>
                              Navigator.of(context, rootNavigator: false)
                                  .pop()),
                    ),
                  ]),
                ),
              ),
              leading: room.membership == Membership.leave
                  ? Avatar(
                      Uri.parse(''),
                      size: 40,
                      name: title,
                    )
                  : null,
              title: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  if ((room.requestContent?.isBroadcast ?? false) == true)
                    Icon(
                      Icons.speaker_phone,
                      size: 16,
                    ),
                  Text(
                    title ?? L10n.of(context).unknownRequest,
                    maxLines: 1,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 14,
                    ),
                  ),
                  Spacer(),
                  UnreadBubble([room]),
                  Opacity(
                    // We need to hide it this way because otherwise the button would stop existing
                    // when the user stop hovers it to select a PopupMenuButtonItem.
                    opacity: hover ? 1 : 0,
                    child: Container(
                      height: UnreadBubble.size,
                      margin: hover
                          ? const EdgeInsets.only(left: 8.0)
                          : EdgeInsets.zero,
                      width: hover ? 20 : 0,
                      child: PopupMenuButton(
                        padding: EdgeInsets.zero,
                        itemBuilder: (context) => [
                          PopupMenuItem(
                            value: 'archive',
                            child: Text(L10n.of(context).archiveAction),
                          ),
                        ],
                        onSelected: (action) => room
                            .showArchiveDialog(context, popAfter: false)
                            .then((_) =>
                                Navigator.of(context, rootNavigator: false)
                                    .pop()),
                      ),
                    ),
                  ),
                ],
              ),
              isThreeLine: true,
              subtitle: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: <Widget>[
                      FutureBuilder<String>(
                        future: requestFuture ??
                            Future.delayed(
                                Duration(milliseconds: 0), () => subtitle),
                        builder: (context, snapshot) => Text(
                          snapshot.data ?? subtitle,
                          maxLines: 1,
                          style: TextStyle(
                            fontStyle: FontStyle.italic,
                            fontSize: 14,
                            color: room.membership == Membership.invite
                                ? FamedlyColors.aquaMarine
                                : null,
                          ),
                        ),
                      ),
                      Spacer(),
                      Text(
                        room.timeCreated.localizedTimeShort(context),
                        key: Key('time_created'),
                        style: TextStyle(
                          fontStyle: FontStyle.italic,
                          fontSize: 14,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 6),
                  Text(
                    body.replaceAll('\n', ' '),
                    maxLines: 3,
                    textAlign: TextAlign.left,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      fontSize: 14,
                      color: room.membership == Membership.invite || isTyping
                          ? FamedlyColors.aquaMarine
                          : null,
                    ),
                  ),
                ],
              ),
              onTap: () async {
                if (activeChat) return;
                final routeBase = '/room/$type/';

                if (await Famedly.of(context)
                    .roomsPlugin
                    .joinRoomAndWait(context, room)) {
                  req = Request(room);
                  final requestContacts = getUpdatedContactsList();
                  if (requestContacts != null) {
                    if (req.isAuthor()) {
                      // inform the user that there is no associated room
                      await showOkAlertDialog(
                        context: context,
                        title: L10n.of(context).warnNoRoomInBroadcastDialog,
                        okLabel: L10n.of(context).next,
                        useRootNavigator: false,
                      );
                    } else {
                      if ((requestContacts ?? contacts) != null) {
                        req = Request(room,
                            contact: (requestContacts ?? contacts)[0]);
                      }
                      if (req.remoteRejected()) {
                        // we need to do the initial popup
                        await showDialog(
                            context: context,
                            useRootNavigator: false,
                            builder: (BuildContext ctx) {
                              return NewIncomingRequestDialog(
                                room: room,
                                contacts: requestContacts ?? contacts,
                                type: type,
                                context: context,
                              );
                            });
                      } else {
                        // alright, the remote already rejected the request....let's give a popup and archive it
                        if (OkCancelResult.ok ==
                            await showOkCancelAlertDialog(
                              context: context,
                              title: L10n.of(context).broadcastRejectedDialog,
                              okLabel: L10n.of(context).yes,
                              cancelLabel: L10n.of(context).cancel,
                              useRootNavigator: false,
                            )) {
                          await showFutureLoadingDialog(
                              context: context,
                              future: () async {
                                final allContacts =
                                    getUpdatedContactsList() ?? contacts;
                                for (final contact in allContacts) {
                                  final reqq = Request(room, contact: contact);
                                  await reqq.reject();
                                }
                                await room.leave();
                              });
                          await AdaptivePageLayout.of(context)
                              .pushNamedAndRemoveUntilIsFirst('/requests');
                        }
                      }
                    }
                  } else {
                    if (room.isBroadcastRequest && !req.isAuthor()) {
                      if (!req.youAccepted() &&
                          !req.youRejected() &&
                          !req.youNeedInfo()) {
                        if (!req.remoteRejected()) {
                          await showDialog(
                              context: context,
                              useRootNavigator: false,
                              builder: (BuildContext ctx) {
                                return NewIncomingRequestDialog(
                                  room: room,
                                  type: type,
                                  context: context,
                                );
                              });
                        } else {
                          if (OkCancelResult.ok ==
                              await showOkCancelAlertDialog(
                                context: context,
                                title: L10n.of(context).broadcastRejectedDialog,
                                okLabel: L10n.of(context).yes,
                                cancelLabel: L10n.of(context).cancel,
                                useRootNavigator: false,
                              )) {
                            await showFutureLoadingDialog(
                                context: context, future: () => req.reject());
                            await showFutureLoadingDialog(
                                context: context, future: () => room.leave());
                            await AdaptivePageLayout.of(context)
                                .pushNamedAndRemoveUntilIsFirst('/requests');
                          }
                        }
                      } else {
                        await AdaptivePageLayout.of(context)
                            .pushNamed(routeBase + room.id);
                      }
                    } else {
                      await AdaptivePageLayout.of(context)
                          .pushNamed(routeBase + room.id);
                    }
                  }
                }
              },
            ),
          ),
        ],
      ),
    );
  }
}
