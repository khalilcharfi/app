import 'package:famedly/models/room_type_extension.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/material.dart';
import 'package:adaptive_page_layout/adaptive_page_layout.dart';
import '../../../utils/event_forwarded_extension.dart';

import '../../avatar.dart';

class EventAvatar extends StatelessWidget {
  final String displayname;
  final Event event;
  final Uri avatarUrl;
  final bool display;
  final double size;
  final Room room;

  const EventAvatar({
    Key key,
    this.displayname,
    this.event,
    this.avatarUrl,
    this.display,
    this.size = 32.0,
    this.room,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final displayname = this.displayname ??
        event.sender.calcDisplayname(mxidLocalPartFallback: false);
    final avatarUrl = this.avatarUrl ?? event.sender.avatarUrl;
    if (!display) {
      if (event.isMine) {
        return Container();
      } else {
        return Container(width: size);
      }
    }
    return Padding(
      padding: EdgeInsets.only(top: 4),
      child: InkWell(
        borderRadius: BorderRadius.circular(size),
        onTap: () {
          if (room.type == roomTypeRequest) return;
          AdaptivePageLayout.of(context)
              .pushNamed('/room/${room.id}/member/${event.sender.id}');
        },
        child: Avatar(
          avatarUrl,
          size: size,
          name: displayname,
        ),
      ),
    );
  }
}
