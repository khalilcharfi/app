import 'package:famedly/models/room_type_extension.dart';
import 'package:famedly/models/requests/request.dart';
import 'package:famedly/models/requests/broadcast_content.dart';
import 'package:famedly/models/requests/request_content.dart';
import 'package:famedly/models/requests/request_state_content.dart';
import 'package:famedly_directory_client_sdk/famedly_directory_client_sdk.dart';
import 'package:famedlysdk/famedlysdk.dart' hide Visibility;
import 'package:flutter_test/flutter_test.dart';

import 'dart:convert';

import 'fake_matrix_api.dart';

void main() {
  group('Requests', () {
    final client = Client('testclient', httpClient: FakeMatrixApi());
    client.checkHomeserver('https://fakeServer.notExisting');
    client.login(
        identifier: AuthenticationUserIdentifier(
          user: '@test:fakeServer.notExisting',
        ),
        password: '1234');

    final nonRequestRoom = Room(
      id: '!nonrequest:localhost',
      client: client,
    );

    final requestInviteRoom = Room(
      id: '!invite:localhost',
      membership: Membership.invite,
      client: client,
    );
    requestInviteRoom.setState(Event(
      content: {'name': Request.requestNameSpace},
      type: 'm.room.name',
      stateKey: '',
    ));
    requestInviteRoom.setState(Event(
      content: {'membership': 'invite'},
      type: 'm.room.member',
      stateKey: '@test:fakeServer.notExisting',
      senderId: '@inviter:localhost',
      room: requestInviteRoom,
    ));

    final requestRoom = Room(
      id: '!request:localhost',
      client: client,
    );
    requestRoom.setState(Event(
      content: RequestContent(
        creator: '@creator:localhost',
        requestingOrganisation: 'matrix.org',
        requestedOrganisation: 'sorunome.de',
        requestTitle: 'Awesome Request',
        requestId: '03793e1b-f1fb-48e4-ae1d-d00ed043f546',
        organisationId: 'c1261f71-8cf6-4878-b8c9-ee8e525ae56f',
        organisationName: 'Some Hospital',
        contactId: 'f54f216e-745a-4c57-8f4a-f9cee375c2ff',
        contactDescription: 'Emergency takein',
        body: 'Hey, anybody there?',
      ).toJson(),
      type: Request.requestNameSpace,
      stateKey: '',
    ));
    requestRoom.setState(Event(
      content: {
        'users': [
          '@floof:localhost',
        ],
      },
      type: Request.usersNameSpace,
      stateKey: '@fox:localhost',
    ));
    requestRoom.setState(Event(
      content: {
        'users': [
          '@paw:localhost',
        ],
      },
      type: Request.usersNameSpace,
      stateKey: '@floof:localhost',
    ));
    requestRoom.setState(Event(
      content: {
        'users': [
          '@recurse_other:localhost',
        ],
      },
      type: Request.usersNameSpace,
      stateKey: '@recurse:localhost',
    ));
    requestRoom.setState(Event(
      content: {
        'users': [
          '@recurse:localhost',
        ],
      },
      type: Request.usersNameSpace,
      stateKey: '@recurse_other:localhost',
    ));

    final requestRoomOwner = Room(
      id: '!request:localhost',
      client: client,
    );
    requestRoomOwner.setState(Event(
      content: RequestContent(
        creator: '@test:fakeServer.notExisting',
        requestingOrganisation: 'matrix.org',
        requestedOrganisation: 'sorunome.de',
        requestTitle: 'Awesome Request',
        requestId: '910d1916-9359-46b5-8900-0c1191e5d385',
        organisationId: '3e4d98f9-1544-4c0d-bce3-24509e92ba0b',
        organisationName: 'Some Hospital',
        contactId: '83f42853-9530-40e3-a961-34ef00f6e09a',
        contactDescription: 'Emergency takein',
        body: 'Hey, anybody there?',
      ).toJson(),
      type: Request.requestNameSpace,
      stateKey: '',
    ));

    final oldRequestRoom = Room(
      id: '!oldrequest:localhost',
      client: client,
    );
    oldRequestRoom.setState(Event(
      content: RequestContent(
        requestingOrganisation: 'matrix.org',
        requestedOrganisation: 'sorunome.de',
        requestId: '910d1916-9359-46b5-8900-0c1191e5d385',
        organisationId: '3e4d98f9-1544-4c0d-bce3-24509e92ba0b',
        organisationName: 'Some Hospital',
        contactId: '83f42853-9530-40e3-a961-34ef00f6e09a',
        contactDescription: 'Emergency takein',
        body: 'Hey, anybody there?',
      ).toJson(),
      type: Request.requestNameSpace,
      stateKey: '',
      senderId: '@oldinviter:localhost',
      room: oldRequestRoom,
    ));

    final broadcastRoom = Room(
      id: '!broadcast:localhost',
      client: client,
    );
    broadcastRoom.setState(Event(
      content: RequestContent(
        creator: '@creator:localhost',
        requestedOrganisation: 'Broadcast',
        requestingOrganisation: 'fakeServer.notExisting',
        requestTitle: 'Awesome Broadcast',
        requestId: 'ee9a2d02-1481-4364-9e93-d40ea6808069',
        body: 'Hey there to this awesome broadcast',
        organisationId: '',
        organisationName: 'Awesome Broadcast',
        contactId: '',
        contactDescription: 'Broadcast',
      ).toJson(),
      type: Request.requestNameSpace,
      stateKey: '',
      room: broadcastRoom,
    ));
    final broadcastContacts = [
      BroadcastContactsContent(
        requestedOrganisation: 'example.com',
        organisationId: 'a63973d7-57a6-4882-81e9-3d1761328273',
        organisationName: 'Example Organisation',
        contactId: '5030715b-0f21-4e9d-aa80-3b065b969b34',
        contactDescription: 'Example Contact',
        contactUri: 'https://matrix.to/#/@contact:example.com',
      ),
      BroadcastContactsContent(
        requestedOrganisation: 'foxies.com',
        organisationId: '6773cf9d-b896-4f8d-8ac8-3832830bbc8c',
        organisationName: 'Foxies Organisation',
        contactId: '35d236c1-33ee-4a4b-b75b-19008619aeb3',
        contactDescription: 'Foxies Contact',
        contactUri: 'https://matrix.to/#/@contact:foxies.com',
      ),
      BroadcastContactsContent(
        requestedOrganisation: 'raccoon.com',
        organisationId: 'b4c318d7-a6e7-4708-90b0-7dbb28b2d6b1',
        organisationName: 'Raccoon Organisation',
        contactId: '554d1d66-4665-42c6-9dca-d9b487e326f3',
        contactDescription: 'Raccoon Contact',
        contactUri: 'https://matrix.to/#/@contact:raccoon.com',
      ),
      BroadcastContactsContent(
        requestedOrganisation: 'bunny.com',
        organisationId: 'ecd1efd5-a55b-4077-a0ba-137646581472',
        organisationName: 'Bunny Organisation',
        contactId: '31671c33-e5c9-4930-80ad-d02ac74a461b',
        contactDescription: 'Bunny Contact',
        contactUri: 'https://matrix.to/#/@contact:bunny.com',
      ),
    ];
    broadcastRoom.setState(Event(
      content: BroadcastContent(
        title: 'Awesome Broadcast',
        contacts: broadcastContacts,
      ).toJson(),
      type: Request.broadcastNameSpace,
      stateKey: '',
    ));
    broadcastRoom.setState(Event(
      content: RequestStateContent(
        requesting: RequestStateElementContent(
          accept: false,
          reject: false,
          needInfo: false,
        ),
        requested: RequestStateElementContent(
          accept: true,
          reject: false,
          needInfo: false,
        ),
        roomHandoff: true,
      ).toJson(),
      type: Request.requestStateNameSpace,
      stateKey: '35d236c1-33ee-4a4b-b75b-19008619aeb3',
    ));
    broadcastRoom.setState(Event(
      content: RequestStateContent(
        requesting: RequestStateElementContent(
          accept: false,
          reject: false,
          needInfo: false,
        ),
        requested: RequestStateElementContent(
          accept: false,
          reject: true,
          needInfo: false,
        ),
        roomHandoff: false,
      ).toJson(),
      type: Request.requestStateNameSpace,
      stateKey: '554d1d66-4665-42c6-9dca-d9b487e326f3',
    ));

    final requestRoomFoxies = Room(
      id: '!foxies:localhost',
      client: client,
    );
    requestRoomFoxies.setState(Event(
      content: RequestContent(
        creator: '@creator:localhost',
        requestedOrganisation: 'foxies.com',
        requestingOrganisation: 'fakeServer.notExisting',
        requestTitle: 'Awesome Broadcast',
        requestId: 'ee9a2d02-1481-4364-9e93-d40ea6808069',
        body: 'Hey there to this awesome broadcast',
        organisationId: '6773cf9d-b896-4f8d-8ac8-3832830bbc8c',
        organisationName: 'Foxies Organisation',
        contactId: '35d236c1-33ee-4a4b-b75b-19008619aeb3',
        contactDescription: 'Foxies Contact',
      ).toJson(),
      type: Request.requestNameSpace,
      stateKey: '',
      room: requestRoomFoxies,
    ));
    requestRoomFoxies.setState(Event(
      content: RequestStateContent(
        requesting: RequestStateElementContent(
          accept: false,
          reject: false,
          needInfo: false,
        ),
        requested: RequestStateElementContent(
          accept: true,
          reject: false,
          needInfo: false,
        ),
        roomHandoff: true,
      ).toJson(),
      type: Request.requestStateNameSpace,
      stateKey: '',
    ));

    client.rooms = [
      nonRequestRoom,
      requestInviteRoom,
      requestRoom,
      requestRoomOwner,
      broadcastRoom,
      requestRoomFoxies
    ];

    test('isRequestRoom', () {
      expect(nonRequestRoom.type, 'm.room_type.messaging');
      expect(requestInviteRoom.type, 'com.famedly.room_type.request');
      expect(requestRoom.type, 'com.famedly.room_type.request');
    });

    test('constructor', () {
      Request req;
      req = Request(requestInviteRoom);
      expect(req.isRequest, false);
      expect(req.requesting, false);
      req = Request(requestRoom);
      expect(req.isRequest, true);
      expect(req.content.requestId, '03793e1b-f1fb-48e4-ae1d-d00ed043f546');
      expect(req.state.requested.accept, false);
      req = Request(requestRoomFoxies);
      expect(req.content.requestId, 'ee9a2d02-1481-4364-9e93-d40ea6808069');
      expect(req.state.requested.accept, true);
      req = Request(broadcastRoom, contact: broadcastContacts[0]);
      expect(req.contact, broadcastContacts[0]);
      req = Request(broadcastRoom, contact: broadcastContacts[2]);
      expect(req.state.requested.reject, true);
      req = Request(broadcastRoom,
          contactId: '35d236c1-33ee-4a4b-b75b-19008619aeb3');
      expect(req.contact.contactId, '35d236c1-33ee-4a4b-b75b-19008619aeb3');
    });

    test('get authorId', () {
      Request req;
      req = Request(requestInviteRoom);
      expect(req.authorId, '@inviter:localhost');
      req = Request(requestRoom);
      expect(req.authorId, '@creator:localhost');
      req = Request(oldRequestRoom);
      expect(req.authorId, '@oldinviter:localhost');
    });

    test('isSubUser', () {
      final req = Request(requestRoom);
      bool ret;
      // it should detect itself
      ret = req.isSubUser('@user1:localhost', '@user1:localhost');
      expect(ret, true);
      // it should detect via users name space
      ret = req.isSubUser('@fox:localhost', '@floof:localhost');
      expect(ret, true);
      ret = req.isSubUser('@fox:localhost', '@paw:localhost');
      expect(ret, true);
      ret = req.isSubUser('@fox:localhost', '@unknwon:localhost');
      expect(ret, false);
      // it should detect recursion
      ret = req.isSubUser('@recurse:localhost', '@blah:localhost');
      expect(ret, false);
    });

    test('isAuthor', () {
      final req = Request(requestRoom);
      expect(req.isAuthor(), false);
      expect(req.isAuthor('@creator:localhost'), true);
    });

    test('get title', () {
      Request req;
      req = Request(requestInviteRoom);
      expect(req.title, 'Unknown user');
      req = Request(requestRoom);
      expect(req.title, 'Awesome Request');
      req = Request(oldRequestRoom);
      expect(req.title, 'Unknown user');
    });

    test('get organisation', () {
      Request req;
      req = Request(requestInviteRoom);
      expect(req.organisation, null);
      req = Request(requestRoom);
      expect(req.organisation, 'Some Hospital');
      req = Request(broadcastRoom, contact: broadcastContacts[1]);
      expect(req.organisation, 'Foxies Organisation');
    });

    test('get description', () {
      Request req;
      req = Request(requestInviteRoom);
      expect(req.description, null);
      req = Request(requestRoom);
      expect(req.description, 'Emergency takein');
      req = Request(broadcastRoom, contact: broadcastContacts[1]);
      expect(req.description, 'Foxies Contact');
    });

    test('accept', () async {
      Request req;
      req = Request(requestRoom);
      await req.accept();
      expect(req.state.requested.accept, true);
      expect(req.state.requested.reject, false);
      expect(req.state.requested.needInfo, false);
      expect(req.state.requesting.accept, false);
      expect(req.state.requesting.reject, false);
      expect(req.state.requesting.needInfo, false);
      req = Request(requestRoomOwner);
      await req.accept();
      expect(req.state.requested.accept, false);
      expect(req.state.requested.reject, false);
      expect(req.state.requested.needInfo, false);
      expect(req.state.requesting.accept, true);
      expect(req.state.requesting.reject, false);
      expect(req.state.requesting.needInfo, false);
    });

    test('reject', () async {
      Request req;
      req = Request(requestRoom);
      await req.reject();
      expect(req.state.requested.accept, false);
      expect(req.state.requested.reject, true);
      expect(req.state.requested.needInfo, false);
      expect(req.state.requesting.accept, false);
      expect(req.state.requesting.reject, false);
      expect(req.state.requesting.needInfo, false);
      req = Request(requestRoomOwner);
      await req.reject();
      expect(req.state.requested.accept, false);
      expect(req.state.requested.reject, false);
      expect(req.state.requested.needInfo, false);
      expect(req.state.requesting.accept, false);
      expect(req.state.requesting.reject, true);
      expect(req.state.requesting.needInfo, false);
    });

    test('needInfo', () async {
      Request req;
      req = Request(requestRoom);
      await req.needInfo();
      expect(req.state.requested.accept, false);
      expect(req.state.requested.reject, false);
      expect(req.state.requested.needInfo, true);
      expect(req.state.requesting.accept, false);
      expect(req.state.requesting.reject, false);
      expect(req.state.requesting.needInfo, false);
      req = Request(requestRoomOwner);
      await req.needInfo();
      expect(req.state.requested.accept, false);
      expect(req.state.requested.reject, false);
      expect(req.state.requested.needInfo, false);
      expect(req.state.requesting.accept, false);
      expect(req.state.requesting.reject, false);
      expect(req.state.requesting.needInfo, true);
    });

    test('sendState', () async {
      var sentData = {};
      var gotStateRequest = false;
      FakeMatrixApi.api['PUT']['/client/r0/rooms/' +
          requestRoom.id +
          '/state/com.famedly.app.request_state'] = (var r) {
        sentData = json.decode(r);
        gotStateRequest = true;
        return {};
      };
      Request req;
      req = Request(requestRoom);
      req.state.requested.accept = true;
      await req.sendState();
      expect(gotStateRequest, true);
      expect(sentData['requested']['accept'], true);

      sentData = {};
      var gotStateRequestWithKey = false;
      FakeMatrixApi.api['PUT']['/client/r0/rooms/' +
          broadcastRoom.id +
          '/state/com.famedly.app.request_state/' +
          broadcastContacts[3].contactId] = (var r) {
        sentData = json.decode(r);
        gotStateRequestWithKey = true;
        return {};
      };
      req = Request(broadcastRoom, contact: broadcastContacts[3]);
      req.state.requested.needInfo = true;
      await req.sendState(false);
      expect(gotStateRequestWithKey, true);
      expect(sentData['requested']['need_info'], true);

      var createdRoom = false;
      var createRoomData = {};
      final oldCreateRoom = FakeMatrixApi.api['POST']['/client/r0/createRoom'];
      FakeMatrixApi.api['POST']['/client/r0/createRoom'] = (var r) {
        createdRoom = true;
        createRoomData = json.decode(r);
        return {'room_id': '!newroom:localhost'};
      };
      req = Request(broadcastRoom, contact: broadcastContacts[3]);
      req.state.requested.needInfo = true;
      await req.sendState();
      FakeMatrixApi.api['POST']['/client/r0/createRoom'] = oldCreateRoom;
      expect(createdRoom, true);
      expect(createRoomData['invite'].length, 2);
      expect(createRoomData['invite'].contains('@creator:localhost'), true);
      expect(createRoomData['invite'].contains('@contact:bunny.com'), true);
      expect(
          createRoomData['power_level_content_override']['users']
              ['@test:fakeServer.notExisting'],
          100);
      expect(
          createRoomData['power_level_content_override']['users']
              ['@creator:localhost'],
          100);
      expect(
          createRoomData['power_level_content_override']['users']
              ['@contact:bunny.com'],
          100);
    });

    test('remoteAccepted', () {
      Request req;
      req = Request(requestRoom);
      req.state.requesting.accept = true;
      expect(req.remoteAccepted(), true);
      req = Request(requestRoomOwner);
      req.state.requested.accept = true;
      expect(req.remoteAccepted(), true);
    });

    test('remoteRejected', () {
      Request req;
      req = Request(requestRoom);
      req.state.requesting.reject = true;
      expect(req.remoteRejected(), true);
      req = Request(requestRoomOwner);
      req.state.requested.reject = true;
      expect(req.remoteRejected(), true);
    });

    test('remoteNeedInfo', () {
      Request req;
      req = Request(requestRoom);
      req.state.requesting.needInfo = true;
      expect(req.remoteNeedInfo(), true);
      req = Request(requestRoomOwner);
      req.state.requested.needInfo = true;
      expect(req.remoteNeedInfo(), true);
    });

    test('youAccepted', () {
      Request req;
      req = Request(requestRoom);
      req.state.requested.accept = true;
      expect(req.youAccepted(), true);
      req = Request(requestRoomOwner);
      req.state.requesting.accept = true;
      expect(req.youAccepted(), true);
    });

    test('youRejected', () {
      Request req;
      req = Request(requestRoom);
      req.state.requested.reject = true;
      expect(req.youRejected(), true);
      req = Request(requestRoomOwner);
      req.state.requesting.reject = true;
      expect(req.youRejected(), true);
    });

    test('youNeedInfo', () {
      Request req;
      req = Request(requestRoom);
      req.state.requested.needInfo = true;
      expect(req.youNeedInfo(), true);
      req = Request(requestRoomOwner);
      req.state.requesting.needInfo = true;
      expect(req.youNeedInfo(), true);
    });

    test('getBroadcastContacts', () {
      List<BroadcastContactsContent> ret;
      ret = Request.getBroadcastContacts(broadcastRoom, '@contact:bunny.com');
      expect(ret.length, 1);
      expect(ret[0].contactId, '31671c33-e5c9-4930-80ad-d02ac74a461b');
      ret = Request.getBroadcastContacts(broadcastRoom, '@contact:foxies.com');
      expect(ret.length, 0);
      ret = Request.getBroadcastContacts(broadcastRoom, '@contact:raccoon.com');
      expect(ret.length, 0);
    });

    test('getRequestBundle', () {
      final ret = Request.getRequestBundle(
          client, 'ee9a2d02-1481-4364-9e93-d40ea6808069');
      expect(ret.broadcastRoom, broadcastRoom);
      expect(ret.rooms.length, 1);
      expect(ret.rooms[0], requestRoomFoxies);
    });

    test('startBroadcast', () async {
      var createdBroadcast = false;
      var createBroadcastData = {};
      FakeMatrixApi.api['POST']
          ['/client/unstable/com.famedly/lisa/r0/createBroadcast'] = (var r) {
        createdBroadcast = true;
        createBroadcastData = json.decode(r);
        return {'request_id': 'blubb'};
      };
      final contactList = <Contact>[
        Contact(
          visibility: Visibility(include: ['*'], exclude: []),
          description: 'New Fox',
          uri: 'https://matrix.to/#/@new:foxies.com',
          passport: 'blah1',
          tags: [],
          id: 'a002a056-dba1-4e34-936f-0ff0ab14bd59',
        ),
        Contact(
          visibility: Visibility(include: ['*'], exclude: []),
          description: 'New Bunny',
          uri: 'https://matrix.to/#/@new:bunny.com',
          passport: 'blah2',
          tags: [],
          id: 'e21ca1a3-6c16-43f3-8b4b-c7fd34f434de',
        ),
      ];
      await Request.startBroadcast(
        requestTitle: 'New Broadcast',
        requestBody: 'Where is my coffee machine?',
        requestType: 'com.famedly.request.coffee',
        contactList: contactList,
        client: client,
      );
      expect(createdBroadcast, true);
      final createFox = createBroadcastData['contacts'].singleWhere(
          (d) => d['contact_id'] == 'a002a056-dba1-4e34-936f-0ff0ab14bd59');
      final createBunny = createBroadcastData['contacts'].singleWhere(
          (d) => d['contact_id'] == 'e21ca1a3-6c16-43f3-8b4b-c7fd34f434de');

      expect(createBroadcastData['content']['body'],
          'Where is my coffee machine?');
      expect(createBroadcastData['request_type'], 'com.famedly.request.coffee');
      expect(createFox['contact_uri'], 'https://matrix.to/#/@new:foxies.com');
      expect(createFox['passport'], 'blah1');
      expect(createBunny['contact_uri'], 'https://matrix.to/#/@new:bunny.com');
      expect(createBunny['passport'], 'blah2');
    });
    test('closeBroadcast', () async {
      final client = Client('testclient', httpClient: FakeMatrixApi());
      await client.checkHomeserver('https://fakeServer.notExisting');
      await client.login(
          identifier: AuthenticationUserIdentifier(
            user: '@test:fakeServer.notExisting',
          ),
          password: '1234');

      var closedBroadcast = false;
      var closeBroadcastData = {};
      FakeMatrixApi.api['POST']
          ['/client/unstable/com.famedly/lisa/r0/closeBroadcast'] = (var r) {
        closedBroadcast = true;
        closeBroadcastData = json.decode(r);
        return {};
      };
      final room1 = Room(
        id: '!room1:localhost',
        client: client,
      );
      final room2 = Room(
        id: '!room2:localhost',
        client: client,
      );
      final bundle = RequestBundle(
        id: 'beep',
        rooms: [room1, room2],
      );
      await Request.closeBroadcast(bundle, room1);
      expect(closedBroadcast, true);
      expect(closeBroadcastData['accept'], '!room1:localhost');
      expect(closeBroadcastData['rooms'][0], '!room1:localhost');
      expect(closeBroadcastData['rooms'][1], '!room2:localhost');
    });
  });
}
