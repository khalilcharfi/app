/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'dart:convert'; // for utf8
import 'dart:typed_data';

import 'package:convert/convert.dart'; // for hex

String str2mxid(String a) {
  final utf8 = Utf8Codec();
  final buf = utf8.encode(a);
  var encoded = '';
  for (final b in buf) {
    if (b == 0x5F) {
      // underscore
      encoded += '__';
    } else if ((b >= 0x61 && b <= 0x7A) || (b >= 0x30 && b <= 0x39)) {
      // [a-z0-9]
      encoded += String.fromCharCode(b);
    } else if (b >= 0x41 && b <= 0x5A) {
      encoded += '_' + String.fromCharCode(b + 0x20);
    } else {
      encoded += '=' + hex.encode([b]);
    }
  }
  return encoded;
}

String mxid2str(String b) {
  var j = 0;
  final decoded = Uint8List(b.length);
  final utf8 = Utf8Codec();
  for (var i = 0; i < b.length; i++) {
    final char = b[i];
    if (char == '_') {
      i++;
      if (b[i] == '_') {
        decoded[j] = 0x5F;
      } else {
        decoded[j] = b[i].codeUnitAt(0) - 0x20;
      }
    } else if (char == '=') {
      i++;
      decoded[j] = hex.decode(b[i] + b[i + 1])[0];
      i++;
    } else {
      decoded[j] = b[i].codeUnitAt(0);
    }
    j++;
  }
  return utf8.decode(decoded.sublist(0, j));
}
