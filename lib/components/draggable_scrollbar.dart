/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'package:draggable_scrollbar/draggable_scrollbar.dart';
import 'package:famedly/styles/colors.dart';
import 'package:flutter/material.dart';

class FamedlyDraggableScrollbar extends StatelessWidget {
  @required
  final Widget child;
  @required
  final ScrollController controller;
  final bool isAlwaysShown;
  static const double width = 6.0;
  static const double height = 48.0;

  const FamedlyDraggableScrollbar({
    Key key,
    this.child,
    @required this.controller,
    @required this.isAlwaysShown,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DraggableScrollbar(
      heightScrollThumb: height,
      backgroundColor: Theme.of(context).brightness == Brightness.light
          ? Color(0xffbcbcbc) // Default light theme scrollbar color
          // Default dark theme scrollbar color is 0xffcccccc
          // but `slate` looks a lot better there
          : FamedlyColors.slate,
      scrollThumbBuilder: (
        Color backgroundColor,
        Animation<double> thumbAnimation,
        Animation<double> labelAnimation,
        double height, {
        Text labelText,
        BoxConstraints labelConstraints,
      }) {
        return isAlwaysShown
            ? Container(
                height: height,
                width: width,
                color: backgroundColor,
              )
            : FadeTransition(
                opacity: thumbAnimation,
                child: Container(
                  height: height,
                  width: width,
                  color: backgroundColor,
                ),
              );
      },
      controller: controller,
      child: child,
    );
  }
}
