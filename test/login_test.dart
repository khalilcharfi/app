import 'package:flutter_gen/gen_l10n/l10n_en.dart';
import 'package:famedly/views/login_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'utils.dart';

void main() {
  final localization = L10nEn();

  /// All Tests related to the Login
  group('LoginPage', () {
    NavigatorObserver mockObserver;

    setUp(() {
      mockObserver = MockNavigatorObserver();
    });

    /// Check if all Elements get created
    testWidgets('should get created', (WidgetTester tester) async {
      await tester.runAsync(() async {
        await tester.pumpWidget(
          await Utils.getWidgetWrapper(
            LoginPage(),
            mockObserver,
          ),
        );

        await tester.pump(Duration.zero);
        expect(find.text(localization.username),
            findsOneWidget); // Benutzernamen Input
        expect(
            find.text(localization.password), findsOneWidget); // Passwort Input
        expect(find.text(localization.login), findsOneWidget); // Button Input
      });
    });

    /// Check if all Elements are working
    testWidgets('should have working inputs', (WidgetTester tester) async {
      await tester.runAsync(() async {
        await tester.pumpWidget(
          await Utils.getWidgetWrapper(
            LoginPage(),
            mockObserver,
          ),
        );

        await tester.pumpAndSettle();

        await tester.enterText(
            find.byKey(Key('username')), '@test:famedly.com');
        //await tester.enterText(find.byKey(Key('password')), '123456');

        await tester.pumpAndSettle();

        expect(find.text('@test:famedly.com'),
            findsOneWidget); // Benutzernamen Input
        await Utils.tapItem(tester, Key('login'));
        await tester.pumpAndSettle();
        expect(find.text(localization.pleaseFillOutTextField), findsOneWidget);

        /*expect(find.text('123456'), findsOneWidget); // Passwort Input*/
      });
    });
  });
}
