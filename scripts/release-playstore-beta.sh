#!/bin/sh -ve
cd android && bundle install && cd ..
cd android && bundle update fastlane && cd ..
cd android && echo $PLAYSTORE_UPLOAD_KEY | base64 --decode --ignore-garbage > upload.jks && cd ..
cd android/fastlane && echo $PLAYSTORE_DEPLOY_KEY | base64 --decode --ignore-garbage > api.json && cd ../..
cd android/app && echo $FIREBASE_KEY | base64 --decode --ignore-garbage > google-services.json && cd ../..
cd android && echo "storePassword=${PLAYSTORE_UPLOAD_KEY_PASS}" >> key.properties && cd ..
cd android && echo "keyPassword=${PLAYSTORE_UPLOAD_KEY_PASS}" >> key.properties && cd ..
cd android && echo "keyAlias=upload" >> key.properties && cd ..
cd android && echo "storeFile=../upload.jks" >> key.properties && cd ..
cd android && bundle exec fastlane set_build_code_internal && cd ..
cd android && bundle exec fastlane build_internal_test && cd ..
cd android/fastlane && rm -Rf api.json && cd ../..
cd android/fastlane && echo $PLAYSTORE_DEPLOY_KEY | base64 --decode --ignore-garbage > api.json && cd ../..
cd android && bundle exec fastlane deploy_internal_test && cd ..