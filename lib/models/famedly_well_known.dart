import 'package:matrix_api_lite/src/utils/try_get_map_extension.dart';

class FamedlyWellKnownInformation {
  FamedlyDirectoryServer famedlyOrganizationDirectory;
  FamedlyDirectoryServer famedlyUserDirectory;

  FamedlyWellKnownInformation.fromJson(Map<String, dynamic> json) {
    famedlyOrganizationDirectory = FamedlyDirectoryServer(json
        .tryGetMap<String, dynamic>('com.famedly.organisation_directory')
        ?.tryGet<String>('base_url'));

    famedlyUserDirectory = FamedlyDirectoryServer(json
        .tryGetMap<String, dynamic>('com.famedly.user_directory')
        ?.tryGet<String>('base_url'));
  }
}

class FamedlyDirectoryServer {
  String baseUrl;

  FamedlyDirectoryServer(this.baseUrl);
}
