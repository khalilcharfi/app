import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/material.dart';

import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:future_loading_dialog/future_loading_dialog.dart';
import '../../../styles/colors.dart';
import '../../../utils/event_forwarded_extension.dart';

class BadEncryptedEventContent extends StatelessWidget {
  final Event event;

  BadEncryptedEventContent(this.event, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final textColor = event.isMine
        ? Colors.white
        : Theme.of(context).textTheme.bodyText1.color;

    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          '🔒 ${L10n.of(context).couldNotDecrypt}: ${event.body}',
          style: TextStyle(
            color: textColor,
            fontWeight: FontWeight.w400,
            fontStyle: FontStyle.normal,
            fontSize: 15.0,
          ),
        ),
        if (event.content['can_request_session'] == true)
          TextButton(
            style: TextButton.styleFrom(
              padding: EdgeInsets.zero,
            ),
            onPressed: () => showFutureLoadingDialog(
              context: context,
              future: () => event.requestKey(),
            ),
            child: Text(
              L10n.of(context).requestPermission,
              style: TextStyle(
                color: FamedlyColors.azul,
                decoration: TextDecoration.underline,
              ),
            ),
          ),
      ],
    );
  }
}
