import 'dart:async';

import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:callkeep/callkeep.dart';
import 'package:uuid/uuid.dart';

import '../../models/plugins/voip_plugin.dart';

class CallKeeper {
  CallKeeper(this.callKeepManager, this.uuid, this.number, this.session) {
    session.onCallStateChanged.listen(_handleCallState);
  }
  CallKeepManager callKeepManager;
  String number;
  String uuid;
  bool held = false;
  bool muted = false;
  bool connected = false;
  CallSession session;
  void _handleCallState(CallState state) {
    Logs().v('CallKeepManager::handleCallState: ${state.toString()}');
    switch (state) {
      case CallState.kConnecting:
        break;
      case CallState.kConnected:
        if (!connected) {
          callKeepManager.answer(uuid);
        } else {
          callKeepManager.setMutedCall(uuid, false);
          callKeepManager.setOnHold(uuid, false);
        }
        break;
      case CallState.kHangup:
        callKeepManager.hangup(uuid);
        break;
      case CallState.kMuted:
        callKeepManager.setMutedCall(uuid, true);
        break;
      case CallState.kHeld:
        callKeepManager.setOnHold(uuid, true);
        break;
    }
  }
}

class CallKeepManager {
  factory CallKeepManager() {
    return _instance;
  }
  CallKeepManager._internal() {
    _callKeep = FlutterCallkeep();
  }
  static final CallKeepManager _instance = CallKeepManager._internal();

  FlutterCallkeep _callKeep;
  VoipPlugin _voipPlugin;
  Map<String, CallKeeper> calls = <String, CallKeeper>{};
  String newUUID() => Uuid().v4();
  String get appName => 'Famedly';
  Map<String, dynamic> get alertOptions => <String, dynamic>{
        'alertTitle': 'Permissions required',
        'alertDescription': '$appName needs to access your phone accounts!',
        'cancelButton': 'Cancel',
        'okButton': 'ok',
      };

  void setVoipPlugin(VoipPlugin plugin) {
    if (kIsWeb) {
      throw 'Not support callkeep for flutter web';
    }
    _voipPlugin = plugin;
    _voipPlugin.onIncomingCall =
        (CallSession session) => displayIncomingCall(session);
  }

  void removeCall(String callUUID) {
    calls.remove(callUUID);
  }

  void addCall(String callUUID, CallKeeper callKeeper) {
    calls[callUUID] = callKeeper;
  }

  String findCallUUID(String number) {
    var uuid = '';
    calls.forEach((String key, CallKeeper item) {
      if (item.number == number) {
        uuid = key;
        return;
      }
    });
    return uuid;
  }

  void setCallHeld(String callUUID, bool held) {
    calls[callUUID].held = held;
  }

  void setCallMuted(String callUUID, bool muted) {
    calls[callUUID].muted = muted;
  }

  void didDisplayIncomingCall(CallKeepDidDisplayIncomingCall event) {
    final callUUID = event.callUUID;
    final number = event.handle;
    Logs().v('[displayIncomingCall] $callUUID number: $number');
    addCall(callUUID, CallKeeper(this, callUUID, number, null));
  }

  void onPushKitToken(CallKeepPushKitToken event) {
    Logs().v('[onPushKitToken] token => ${event.token}');
  }

  Future<void> initialize() async {
    _callKeep.on(CallKeepPerformAnswerCallAction(), answerCall);
    _callKeep.on(CallKeepDidPerformDTMFAction(), didPerformDTMFAction);
    _callKeep.on(
        CallKeepDidReceiveStartCallAction(), didReceiveStartCallAction);
    _callKeep.on(CallKeepDidToggleHoldAction(), didToggleHoldCallAction);
    _callKeep.on(
        CallKeepDidPerformSetMutedCallAction(), didPerformSetMutedCallAction);
    _callKeep.on(CallKeepPerformEndCallAction(), endCall);
    _callKeep.on(CallKeepPushKitToken(), onPushKitToken);
    _callKeep.on(CallKeepDidDisplayIncomingCall(), didDisplayIncomingCall);
  }

  Future<void> hangup(String callUUID) async {
    await _callKeep.endCall(callUUID);
    removeCall(callUUID);
  }

  Future<void> reject(String callUUID) async {
    await _callKeep.rejectCall(callUUID);
  }

  Future<void> answer(String callUUID) async {
    final keeper = calls[callUUID];
    if (!keeper.connected) {
      await _callKeep.answerIncomingCall(callUUID);
      keeper.connected = true;
    }
  }

  Future<void> setOnHold(String callUUID, bool held) async {
    await _callKeep.setOnHold(callUUID, held);
    setCallHeld(callUUID, held);
  }

  Future<void> setMutedCall(String callUUID, bool muted) async {
    await _callKeep.setMutedCall(callUUID, muted);
    setCallMuted(callUUID, muted);
  }

  Future<void> updateDisplay(String callUUID) async {
    final number = calls[callUUID].number;
    // Workaround because Android doesn't display well displayName, se we have to switch ...
    if (isIOS) {
      await _callKeep.updateDisplay(callUUID,
          displayName: 'New Name', handle: number);
    } else {
      await _callKeep.updateDisplay(callUUID,
          displayName: number, handle: 'New Name');
    }
  }

  Future<void> displayIncomingCall(CallSession session) async {
    final callUUID = newUUID();
    addCall(callUUID, CallKeeper(this, callUUID, session.displayname, session));
    await _callKeep.displayIncomingCall(callUUID, session.displayname,
        handleType: 'number', hasVideo: session.type == CallType.kVideo);
  }

  Future<void> checkoutPhoneAccountSetting(BuildContext context) async {
    await _callKeep.setup(<String, dynamic>{
      'ios': <String, dynamic>{
        'appName': appName,
      },
      'android': alertOptions,
    });
    final hasPhoneAccount = await _callKeep.hasPhoneAccount();
    if (!hasPhoneAccount) {
      await _callKeep.hasDefaultPhoneAccount(context, alertOptions);
    }
  }

  /// CallActions.
  Future<void> answerCall(CallKeepPerformAnswerCallAction event) async {
    final callUUID = event.callUUID;
    final keeper = calls[event.callUUID];
    final number = keeper.number;
    if (!keeper.connected) {
      // Answer Call
      keeper.session.answer();
      keeper.connected = true;
    }
    await _callKeep.startCall(event.callUUID, number, number);
    Timer(const Duration(seconds: 1), () {
      _callKeep.setCurrentCallActive(callUUID);
    });
  }

  Future<void> endCall(CallKeepPerformEndCallAction event) async {
    final keeper = calls[event.callUUID];
    keeper?.session?.hangup();
    removeCall(event.callUUID);
  }

  Future<void> didPerformDTMFAction(CallKeepDidPerformDTMFAction event) async {
    final keeper = calls[event.callUUID];
    keeper.session.sendDTMF(event.digits);
  }

  Future<void> didReceiveStartCallAction(
      CallKeepDidReceiveStartCallAction event) async {
    if (event.handle == null) {
      // @TODO: sometime we receive `didReceiveStartCallAction` with handle` undefined`
      return;
    }
    final callUUID = event.callUUID ?? newUUID();
    if (event.callUUID == null) {
      final session =
          await _voipPlugin.inviteToCall(event.handle, CallType.kVideo);
      addCall(
          callUUID, CallKeeper(this, callUUID, session.displayname, session));
    }
    await _callKeep.startCall(callUUID, event.handle, event.handle);
    Timer(const Duration(seconds: 1), () {
      _callKeep.setCurrentCallActive(callUUID);
    });
  }

  Future<void> didPerformSetMutedCallAction(
      CallKeepDidPerformSetMutedCallAction event) async {
    final keeper = calls[event.callUUID];
    if (event.muted) {
      keeper.session.muteMic(muted: true);
    } else {
      keeper.session.muteMic(muted: false);
    }
    setCallMuted(event.callUUID, event.muted);
  }

  Future<void> didToggleHoldCallAction(
      CallKeepDidToggleHoldAction event) async {
    final keeper = calls[event.callUUID];
    if (event.hold) {
      keeper.session.hold();
    } else {
      keeper.session.unhold();
    }
    setCallHeld(event.callUUID, event.hold);
  }
}
