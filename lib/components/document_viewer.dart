/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'package:famedly/components/html_message.dart';
import 'package:famedly/config/static_in_app_documents.dart';
import 'package:famedly/styles/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:markdown/markdown.dart' as markdown;

class DocumentViewer extends StatelessWidget {
  final String document;

  const DocumentViewer(this.document, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var title = L10n.of(context).documentNotFound;
    var text = L10n.of(context).documentNotFound;
    switch (document) {
      case 'privacyPolicy':
        title = L10n.of(context).privacyPolicy;
        text = StaticInAppDocuments.privacyPolicyText;
        break;
      case 'termsOfUse':
        title = L10n.of(context).termsOfUse;
        text = StaticInAppDocuments.termsOfUse;
        break;
    }
    final documentViewerWidget = Scaffold(
      appBar: AppBar(
        title: Text(title),
        leading: BackButton(),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: HtmlMessage(
              html: markdown.markdownToHtml(text),
              linkStyle: TextStyle(
                  color: FamedlyColors.azul,
                  decoration: TextDecoration.underline)),
        ),
      ),
    );
    return documentViewerWidget;
  }
}
