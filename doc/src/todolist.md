Task lists are stored in the ```account_data```of the Matrix user.

### Namespace:
```com.famedly.app.tasks```

## Example Tasks event

```json
{
  "type": "com.famedly.app.tasks",
  "content": {
    "chunk": [
      {
        "title": "Task 1",
        "done": false,
      },
      {
        "title": "Task 2",
        "subtitle": "Very important",
        "done": true,
        "subtask": [
          {
            "title": "Task 1",
            "done": false,
          },
          {
            "title": "Task 1",
            "done": false,
          },
        ],
      },
    ]
  }
}
```

### TasksEvent
| Name   | Type | Description |
| ------ | ------ | --------- |
| type | String | Must be "com.famedly.app.tasks". |
| content | TasksContent | Content of this tasks event.|

### TasksContent
| Name   | Type | Description |
| ------ | ------ | --------- |
| chunk | List of Task | List of tasks.|

### Task
| Name   | Type | Description |
| ------ | ------ | --------- |
| title | String | The (required) title of this task. |
| description | String | The description of this task if given. |
| date | int | The timestamp duo this task should be finished if given.|
| done | bool | Wheither this task is done or not. At beginning this is false.|
| subtask | List of Subtask | List of subtasks for this task.|

### Subtask
| Name   | Type | Description |
| ------ | ------ | --------- |
| title | String | The (required) title of this subtask. |
| done | bool | Wheither this subtask is done or not. At beginning this is false.|
