# How the MSIX works

## How to recreate the `windows` folder

If the windows folder needs to be recreated the correct way to do this is to rerun the `flutter create .` command inside of the app main directory. You might need to manually revert changes in other parts of the app.

Also you will need to replace the icon in `windows/runner/resources/app_icon.ico` and update some text in `windows/runner/Runner.rc`

## How to rereate the image

Ths was done using Visual Studio and cant be done on linux or using CI automaticly yet. Please read https://docs.microsoft.com/en-us/windows/uwp/design/style/app-icons-and-logos#generating-all-assets-at-once for more details on how this works.

## Where is the download available?

The download link that should be used for updates is: https://famedly-windows.s3.eu-central-1.amazonaws.com/famedly.appinstaller

If you for some reason need the msix directly use: https://famedly-windows.s3.eu-central-1.amazonaws.com/famedly.msix

If a install fails do a reboot. This fixes the issue in 99%.
## What does the python script do?

The python script does the main packaging tasks. It signs the olm dll and the exe as well as updating the msix and appinstaller versions to reflect the pubspec version. It also does the packaging task for the msix and uploads it afterwards to the amazon url.

## Variables used

* `WINDOWS_SIGN_CERT_PASSWORD`
* `windows_sign_cert_file`
* `AWS_ACCESS_KEY_ID`
* `AWS_DEFAULT_REGION`
* `AWS_SECRET_ACCESS_KEY`
