package com.famedly.talk

import io.flutter.embedding.android.FlutterFragmentActivity
import io.flutter.embedding.engine.FlutterEngine

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.WindowManager

// workaround for https://github.com/flutter/flutter/issues/81830
class UndestroyableFlutterEngine(
    context: Context,
    dartVmArgs: Array<String>,
    automaticallyRegisterPlugins: Boolean,
    waitForRestorationData: Boolean
) : FlutterEngine(context, dartVmArgs, automaticallyRegisterPlugins, waitForRestorationData) {
    override fun destroy() {
        Log.w("UndestroyableFlutterEng", "destroy called")
    }
}

class MainActivity : FlutterFragmentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (!BuildConfig.DEBUG) {
            window.setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                    WindowManager.LayoutParams.FLAG_SECURE)
        }
    }

    override fun provideFlutterEngine(context: Context): FlutterEngine {
        return provideEngine(this)
    }

    override fun configureFlutterEngine(flutterEngine: FlutterEngine) {
        // do nothing, because the engine was been configured in provideEngine
    }

    companion object {
        var engine: FlutterEngine? = null
        fun provideEngine(context: Context): FlutterEngine {
            val eng = engine ?: UndestroyableFlutterEngine(context, emptyArray(),
                automaticallyRegisterPlugins = true,
                waitForRestorationData = false
            )
            engine = eng
            return eng
        }
    }
}
