import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/material.dart';
import 'package:adaptive_page_layout/adaptive_page_layout.dart';

import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:future_loading_dialog/future_loading_dialog.dart';
import '../../models/requests/broadcast_content.dart';
import '../../models/requests/request.dart';
import '../../models/room_type_extension.dart';
import '../../styles/colors.dart';

class NewIncomingRequestDialog extends StatelessWidget {
  final Room room;
  final List<BroadcastContactsContent> contacts;
  final String type;
  final BuildContext context;

  const NewIncomingRequestDialog({
    Key key,
    this.room,
    this.contacts,
    this.type,
    @required this.context,
  }) : super(key: key);

  List<BroadcastContactsContent> getUpdatedContactsList() {
    List<BroadcastContactsContent> requestContacts;
    if (Request(room).broadcast != null &&
        (contacts == null || contacts.isEmpty)) {
      requestContacts = Request.getBroadcastContacts(room, room.client.userID);
    }
    requestContacts ??= contacts;
    return requestContacts;
  }

  Future<BroadcastContactsContent> _chooseContact() async {
    final requestContacts = getUpdatedContactsList();
    if (requestContacts == null) {
      return null;
    }
    if (requestContacts.length == 1) {
      return requestContacts.first;
    } else {
      return await showConfirmationDialog<BroadcastContactsContent>(
        context: context,
        title: L10n.of(context).pleaseChoose,
        useRootNavigator: false,
        actions: requestContacts.map(
          (c) => AlertDialogAction(
            key: c,
            label: c.contactDescription,
          ),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    if (room.type != roomTypeRequest) {
      return null; // something went really wrong
    }
    final req = Request(room);
    final routeBase = type != null ? '/room/$type/' : '/room/';
    final popupActions = <Widget>[];
    if (room.membership != Membership.leave) {
      popupActions.add(NewIncomingRequestDialogButton(
          color: FamedlyColors.yellow,
          text: L10n.of(context).requestInformation,
          onTap: () async {
            final contact = await _chooseContact();
            final reqq = Request(room, contact: contact);
            final roomId = await showFutureLoadingDialog(
              context: context,
              future: () => reqq.needInfo(),
            );
            if (roomId.error == null) {
              final requestContacts = getUpdatedContactsList();
              if (roomId.result != room.id &&
                  requestContacts != null &&
                  requestContacts.length <= 1) {
                // we need to leave the old broadcast room
                await showFutureLoadingDialog(
                  context: context,
                  future: () => room.leave(),
                );
              }
              Navigator.of(context, rootNavigator: false).pop();
              await AdaptivePageLayout.of(this.context)
                  .pushNamed(routeBase + roomId.result);
            }
          }));
      popupActions.add(NewIncomingRequestDialogButton(
          color: FamedlyColors.azul,
          text: L10n.of(context).acceptInquiry,
          onTap: () async {
            final contact = await _chooseContact();
            final reqq = Request(room, contact: contact);
            final roomId = await showFutureLoadingDialog(
              context: context,
              future: () => reqq.accept(),
            );
            if (roomId.error == null) {
              final requestContacts = getUpdatedContactsList();
              if (roomId.result != room.id &&
                  requestContacts != null &&
                  requestContacts.length <= 1) {
                // we need to leave the old broadcast room
                await showFutureLoadingDialog(
                  context: context,
                  future: () => room.leave(),
                );
              }
              Navigator.of(context, rootNavigator: false).pop();
              await AdaptivePageLayout.of(this.context)
                  .pushNamed(routeBase + roomId.result);
            }
          }));
      popupActions.add(
        NewIncomingRequestDialogButton(
          color: FamedlyColors.grapefruit,
          text: L10n.of(context).rejectInquiry,
          onTap: () async {
            final contact = await _chooseContact();
            if (OkCancelResult.ok ==
                await showOkCancelAlertDialog(
                  context: context,
                  title: L10n.of(context).rejectAndArchiveInquiryWarning,
                  okLabel: L10n.of(context).yes,
                  cancelLabel: L10n.of(context).cancel,
                  useRootNavigator: false,
                )) {
              final reqq = Request(room, contact: contact);
              final success = await showFutureLoadingDialog(
                context: context,
                future: () => reqq.reject(),
              );
              if (success.error == null) {
                final requestContacts = getUpdatedContactsList();
                if ((requestContacts != null && requestContacts.length <= 1) ||
                    requestContacts == null) {
                  // we need to leave the old broadcast room
                  await showFutureLoadingDialog(
                    context: context,
                    future: () => room.leave(),
                  );
                }
                Navigator.of(context, rootNavigator: false).pop();
                await AdaptivePageLayout.of(this.context)
                    .pushNamedAndRemoveUntilIsFirst('/requests');
              }
            }
          },
        ),
      );
    } else {
      popupActions.add(Text(L10n.of(context).archivedInquiryNotice));
    }
    return AlertDialog(
      title: Text(
        req.organisation,
      ),
      content: Column(
        children: [
          Expanded(
            child: SingleChildScrollView(
              child: Container(
                width: double.maxFinite,
                child: Text(req.content.body),
              ),
            ),
          ),
          Divider(),
          Column(
            mainAxisSize: MainAxisSize.min,
            children: popupActions,
          ),
        ],
      ),
    );
  }
}

class NewIncomingRequestDialogButton extends StatelessWidget {
  final Color color;
  final String text;
  final onTap;

  const NewIncomingRequestDialogButton(
      {Key key, this.color, this.text, this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        decoration: BoxDecoration(
          color: color,
          borderRadius: BorderRadius.all(Radius.circular(10)),
        ),
        height: 50,
        alignment: Alignment(0.0, 0.0),
        margin: EdgeInsets.all(5),
        padding: EdgeInsets.all(16),
        child: Text(text),
      ),
    );
  }
}
