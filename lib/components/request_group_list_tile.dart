/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'package:famedly/styles/famedly_icons_v2_icons.dart';
import 'package:flutter/material.dart';
import 'package:adaptive_page_layout/adaptive_page_layout.dart';

typedef OnTap = void Function();

class RequestGroupListTile extends StatelessWidget {
  const RequestGroupListTile(
      {Key key,
      this.iconData = FamedlyIconsV2.archive,
      this.iconAvatar,
      this.title = '',
      this.unread = 0,
      this.highlightUnread = false,
      this.onTap,
      this.indent = false,
      this.count = 0,
      this.selected = false,
      this.leftPadding = 12.0})
      : super(key: key);

  final Widget iconAvatar;
  final IconData iconData;
  final String title;
  final int unread;
  final int count;
  final bool highlightUnread;
  final OnTap onTap;
  final bool indent;
  final bool selected;
  final double leftPadding;

  @override
  Widget build(BuildContext context) {
    return Material(
      color: selected
          ? Theme.of(context).secondaryHeaderColor
          : Theme.of(context).scaffoldBackgroundColor,
      child: InkWell(
        onTap: onTap,
        child: Padding(
          padding: EdgeInsets.only(
            top: 12,
            bottom: 12,
            right: 12,
            left: indent ? (leftPadding + 12) : leftPadding,
          ),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              iconAvatar ??
                  Icon(
                    iconData,
                    size: AdaptivePageLayout.of(context).columnMode(context)
                        ? 16
                        : 26,
                    color: Theme.of(context).textTheme.headline1.color,
                  ),
              SizedBox(width: leftPadding),
              if (highlightUnread)
                Container(
                  margin: EdgeInsets.only(right: 4),
                  width: 11,
                  height: 11,
                  decoration: BoxDecoration(
                      color: Colors.orange,
                      borderRadius: BorderRadius.circular(20)),
                ),
              Expanded(
                child: Text(
                  title,
                  style: TextStyle(
                      color: Theme.of(context).textTheme.headline1.color,
                      fontWeight: highlightUnread ? FontWeight.bold : null,
                      fontSize: 15),
                ),
              ),
              Text(count != null && count > 0 ? count.toString() : ''),
            ],
          ),
        ),
      ),
    );
  }
}
