import 'package:famedly/views/device_settings_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'utils.dart';

void main() {
  /// All Tests related to the DevicesSettingsPage
  group('DevicesSettingsPage', () {
    NavigatorObserver mockObserver;

    setUp(() {
      mockObserver = MockNavigatorObserver();
    });

    /// Check that we fallback to a loadingcircle if we have no devices
    /// TODO make it possible with the fakeAPI
    /*testWidgets('should show loading circle if no devices exist',
        (WidgetTester tester) async {
      await tester.runAsync(() async {
        await tester.pumpWidget(
          await Utils.getWidgetWrapper(
            DevicesSettingsPage(),
            mockObserver,
          ),
        );

        await tester.pump();

        expect(find.byType(CircularProgressIndicator), findsOneWidget);
      });
    });*/

    testWidgets('should show devices', (WidgetTester tester) async {
      await tester.runAsync(() async {
        await tester.pumpWidget(
          await Utils.getWidgetWrapper(
            DevicesSettingsPage(),
            mockObserver,
          ),
        );

        await tester.pumpAndSettle();

        expect(find.byKey(Key('device_own')), findsOneWidget);
        expect(find.byKey(Key('device_0')), findsOneWidget);
      });
    });
  });
}
