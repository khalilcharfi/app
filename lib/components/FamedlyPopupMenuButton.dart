/*
 *   Famedly
 *   Copyright (C) 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import 'package:flutter/material.dart';
import 'package:keep_keyboard_popup_menu/keep_keyboard_popup_menu.dart';

Widget defaultPopupMenuBackgroundBuilder(BuildContext context, Widget child) {
  return Material(
    borderRadius: BorderRadius.circular(10.0),
    color: Theme.of(context).dialogBackgroundColor,
    elevation: 10,
    child: child,
  );
}

/// Verbatim copy from [KeepKeyboardPopupMenuButton] with added background styling option
///
/// KeepKeyboardPopup version of [PopupMenuButton]. This is basically a remix of
/// [PopupMenuButton] and [WithKeepKeyboardPopupMenu], you can find the
/// documentation of the properties from them.
///
class FamedlyPopupMenuButton extends StatelessWidget {
  final Widget child;
  final Widget icon;
  final bool enabled;
  final MenuBuilder menuBuilder;
  final MenuItemBuilder menuItemBuilder;
  final EdgeInsetsGeometry padding;
  final double iconSize;
  final PopupMenuBackgroundBuilder backgroundBuilder;

  const FamedlyPopupMenuButton({
    Key key,
    this.icon,
    this.enabled = true,
    this.menuBuilder,
    this.menuItemBuilder,
    this.padding = const EdgeInsets.all(8.0),
    this.iconSize,
    this.child,
    this.backgroundBuilder = defaultPopupMenuBackgroundBuilder,
  })  : assert((menuBuilder == null) != (menuItemBuilder == null),
            'You can only pass one of [menuBuilder] and [menuItemBuilder].'),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return WithKeepKeyboardPopupMenu(
      menuBuilder: menuBuilder,
      menuItemBuilder: menuItemBuilder,
      backgroundBuilder: backgroundBuilder,
      childBuilder: (context, openPopup) {
        if (child != null) {
          return InkWell(
            onTap: enabled ? openPopup : null,
            child: child,
          );
        } else {
          return IconButton(
            icon: icon ?? Icon(Icons.adaptive.more),
            padding: padding,
            iconSize: iconSize ?? 24.0,
            onPressed: enabled ? openPopup : null,
          );
        }
      },
    );
  }
}
