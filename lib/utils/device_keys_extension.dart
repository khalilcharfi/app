/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';

extension DeviceKeysExtension on DeviceKeys {
  static const IconData defaultIconData = Icons.phone_android_outlined;
  IconData get iconData {
    switch (deviceDisplayName) {
      case 'Famedly Web':
        return Icons.desktop_mac;
      case 'Famedly ios':
        return Icons.phone_iphone_outlined;
      case 'Famedly android':
      default:
        return defaultIconData;
    }
  }

  String getLocalizedVerificationStatus(BuildContext context) {
    if (verified) return L10n.of(context).verified;
    if (blocked) return L10n.of(context).blocked;
    return L10n.of(context).unverified;
  }

  Color get verificationColor {
    if (verified) return Colors.green;
    if (blocked) return Colors.red;
    return Colors.orange;
  }
}
