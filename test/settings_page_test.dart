import 'package:adaptive_page_layout/adaptive_page_layout.dart';
import 'package:famedly/models/famedly.dart';
import 'package:famedly/styles/colors.dart';
import 'package:famedly/views/settings_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:provider/provider.dart';

import 'utils.dart';

void main() {
  /// All Tests related to the SettingsPage

  group(
    'SettingsPage',
    () {
      NavigatorObserver mockObserver;

      setUp(() {
        mockObserver = MockNavigatorObserver();
      });

      /// SettingsPage Test
      testWidgets(
        'has a Logout Button which works',
        (WidgetTester tester) async {
          await tester.runAsync(() async {
            final client = await Utils.defaultClient;
            await tester.pumpWidget(
              Provider<Famedly>(
                create: (context) => Famedly(
                  context: context,
                  matrixClient: client,
                ),
                child: MaterialApp(
                  title: 'Famedly Talk',
                  theme: Themes.famedlyLightTheme,
                  navigatorObservers: [mockObserver],
                  home: AdaptivePageLayout(
                    onGenerateRoute: (_) =>
                        ViewData(mainView: (_) => SettingsPage()),
                  ),
                  localizationsDelegates: L10n.localizationsDelegates,
                  supportedLocales: L10n.supportedLocales,
                ),
              ),
            );

            await tester.pumpAndSettle();

            expect(find.text('Settings'), findsNWidgets(2));

            await Utils.tapItem(tester, Key('SignOutAction'));

            // Check if all items in dialog got added
            expect(find.text('Are you sure you want to sign out?'),
                findsOneWidget);
            expect(find.text('Cancel'.toUpperCase()), findsOneWidget);
            expect(find.text('Sign out'.toUpperCase()), findsOneWidget);
          });
        },
      );
    },
  );
}
