/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'package:flutter/material.dart';
import 'package:localstorage/localstorage.dart';

import 'package:flutter_gen/gen_l10n/l10n.dart';
import '../models/famedly.dart';

class SentrySwitchListTile extends StatefulWidget {
  final EdgeInsets padding;

  const SentrySwitchListTile({Key key, this.padding}) : super(key: key);
  @override
  _SentrySwitchListTileState createState() => _SentrySwitchListTileState();
}

class _SentrySwitchListTileState extends State<SentrySwitchListTile> {
  bool value = false;

  final LocalStorage storage = LocalStorage('FamedlyLocalStorage');

  void update() {
    if (mounted) {
      setState(() => value = storage.getItem('sentry') ?? false);
    }
  }

  @override
  Widget build(BuildContext context) {
    storage.ready.whenComplete(update);
    return SwitchListTile(
      key: Key('SentrySwitch'),
      title: Text(L10n.of(context).sendAnalyticsToFamedly),
      value: value,
      contentPadding: widget.padding,
      onChanged: (bool b) async {
        await storage.ready;
        await storage.setItem('sentry', b);
        update();
        Famedly.of(context).analyticsPlugin.init();
      },
    );
  }
}
