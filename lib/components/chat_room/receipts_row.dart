import 'dart:math';

import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/material.dart';

import '../../styles/colors.dart';
import '../avatar.dart';
import '../../models/famedly.dart';

class ReceiptsRow extends StatelessWidget {
  final Event event;
  const ReceiptsRow(this.event, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final rowChildren = <Widget>[];
    final famedly = Famedly.of(context);
    final List receipts = event.receipts;
    receipts.removeWhere(
      (r) => r.user.id == famedly.client.userID || r.user.id == event.sender.id,
    );
    for (var i = 0; i < min(receipts.length, 5); i++) {
      final User user = receipts[i].user;
      rowChildren.add(
        Padding(
          padding: EdgeInsets.only(left: 4),
          child: Avatar(
            user.avatarUrl,
            textSize: 8,
            name: user.calcDisplayname(mxidLocalPartFallback: false),
            size: 16,
          ),
        ),
      );
    }

    final more = receipts.length - rowChildren.length;
    if (more > 0) {
      rowChildren.insert(
        0,
        Text(
          '$more +',
          style: TextStyle(fontSize: 13, color: FamedlyColors.blueyGrey),
        ),
      );
    }

    return AnimatedContainer(
      duration: Duration(milliseconds: 300),
      height: rowChildren.isEmpty ? 9 : 26,
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 5),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: rowChildren,
        ),
      ),
    );
  }
}
