import 'package:famedly/models/requests/request.dart';
import 'package:famedly/utils/matrix_locals.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';

import 'package:flutter_gen/gen_l10n/l10n.dart';

class StateEvent extends StatelessWidget {
  final Event event;
  final Event prevEvent;
  final Event nextEvent;
  final void Function(String) unfold;
  final bool unfolded;

  static const String collapsedKey = 'com.famedly.collapsed_state_event';
  static const String collapsedCounterKey =
      'com.famedly.collapsed_state_events_count';

  StateEvent(
    this.event, {
    Key key,
    @required this.prevEvent,
    @required this.nextEvent,
    this.unfold,
    this.unfolded = false,
  }) : super(key: key);

  String getLocalizedBody(BuildContext context) {
    if (event.messageType == Request.requestStateNameSpace) {
      final n = (bool b) => b == true ? 1 : 0;
      final req = Request(event.room);
      final content = event.content;
      final message = {
        1: L10n.of(context).requestYouAccepted,
        2: L10n.of(context).requestYouRejected,
        3: L10n.of(context).requestYouNeedInfo,
        5: L10n.of(context).requestOtherAccepted,
        6: L10n.of(context).requestOtherRejected,
        7: L10n.of(context).requestOtherNeedInfo,
      }[n(content['accept']) +
          2 * n(content['reject']) +
          3 * n(content['need_info']) +
          4 * n(req.isAuthor(event.senderId) != req.isAuthor())];
      return message;
    }
    return event.getLocalizedBody(MatrixLocals(L10n.of(context)));
  }

  /// Whether we should highlight this state event and NEVER collapse it!
  bool get highlightedStateEvent =>
      event.messageType == Request.requestStateNameSpace;

  @override
  Widget build(BuildContext context) {
    final hidden =
        !highlightedStateEvent && event.unsigned[collapsedKey] == true;
    final int counter = event.unsigned[collapsedCounterKey] ?? 0;

    return AnimatedOpacity(
      opacity: hidden ? 0 : 1,
      duration: Duration(milliseconds: 200),
      child: hidden
          ? Container()
          : Center(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Material(
                  color: Theme.of(context).appBarTheme.color,
                  borderRadius: BorderRadius.circular(7),
                  child: InkWell(
                    onTap: (counter != 0 && unfold != null)
                        ? () => unfold(event.eventId)
                        : null,
                    borderRadius: BorderRadius.circular(7),
                    child: Padding(
                      padding: EdgeInsets.all(8),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Text(
                            getLocalizedBody(context),
                            style: TextStyle(
                              fontSize: 14.0,
                              color:
                                  Theme.of(context).textTheme.bodyText2.color,
                              fontWeight: highlightedStateEvent
                                  ? FontWeight.bold
                                  : null,
                            ),
                            textAlign: TextAlign.center,
                          ),
                          if (counter > 0)
                            Text(
                              unfolded
                                  ? L10n.of(context).collapse
                                  : counter == 1
                                      ? L10n.of(context).oneMoreEvent
                                      : L10n.of(context)
                                          .xMoreEvents(counter.toString()),
                              style: TextStyle(
                                  fontSize: 14.0,
                                  color: Theme.of(context)
                                      .textTheme
                                      .bodyText2
                                      .color,
                                  fontWeight: FontWeight.bold),
                            ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
    );
  }
}
