import 'package:flutter/widgets.dart';

import 'task.dart';

class Subtask {
  String title;
  bool done;
  Task task;

  Subtask({@required this.title, this.done = false, @required Task parent})
      : task = parent;

  Subtask.fromJson(Map<String, dynamic> json, Task parent) {
    title = json['title'];
    done = json['done'];
    task = parent;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['title'] = title;
    data['done'] = done;
    return data;
  }

  Future<bool> setTitle(String newTitle) async {
    final oldTitle = title;
    title = newTitle;
    final saved = await task.save();
    if (!saved) title = oldTitle;
    return saved;
  }

  Future<bool> setDone(bool newDone) async {
    final oldDone = done;
    done = newDone;
    final saved = await task.save();
    if (!saved) done = oldDone;
    return saved;
  }
}
