/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'dart:async';

import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';

extension LocalizedExceptionExtension on Exception {
  static String toLocalizedString(BuildContext context, dynamic exception) {
    Logs().v('Localize Exception', exception);
    if (exception is MatrixException) {
      if (exception.error == MatrixError.M_FORBIDDEN) {
        if (exception.errorMessage == 'Invalid password') {
          return L10n.of(context).invalidPassword;
        }
        return L10n.of(context).noPermissionForThisAction;
      }
      if (exception.error == MatrixError.M_LIMIT_EXCEEDED) {
        return L10n.of(context).tooManyRequests;
      }
      return exception.errorMessage;
    }
    if (exception is MatrixConnectionException ||
        exception is TimeoutException) {
      return L10n.of(context).organisationServerIsNotResponding;
    }
    return L10n.of(context).oopsSomethingWentWrong;
  }
}
