/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import 'package:famedly/models/famedly.dart';
import 'package:famedly/styles/colors.dart';
import 'package:famedly/styles/famedly_icons_v2_icons.dart';
import 'package:famedly/views/directory/facility_page_view.dart';
import 'package:famedly/views/invite_page.dart';
import 'package:flutter/material.dart';
import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:adaptive_page_layout/adaptive_page_layout.dart';
import 'package:famedly/config/forms.dart';
import 'package:famedly_directory_client_sdk/famedly_directory_client_sdk.dart';
import 'package:famedlysdk/famedlysdk.dart' hide Tag;
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:famedly/models/requests/room_extension.dart';

class OrganisationData {
  Organisation organisation;
  List<Tag> tagList;
}

class FacilityPage extends StatefulWidget {
  final String organisationID;
  final OnInviteCallback onInvite;

  const FacilityPage(
    this.organisationID, {
    Key key,
    this.onInvite,
  }) : super(key: key);
  @override
  FacilityPageController createState() => FacilityPageController();
}

class FacilityPageController extends State<FacilityPage> {
  Organisation organisation;
  Famedly famedly;
  static const TextStyle titleTextStyle = TextStyle(
    fontWeight: FontWeight.bold,
    fontSize: 13,
  );

  @override
  void initState() {
    famedly = Famedly.of(context);
    organisation = Directory.organisationsCache[widget.organisationID];
    super.initState();
  }

  Future<OrganisationData> getOrganisationData(BuildContext context) async {
    final data = OrganisationData();
    final directory =
        await Famedly.of(context).getOrganizationDirectoryClient();
    data.organisation = await directory.getOrganisation(widget.organisationID);
    data.tagList = await directory.getTags();
    return data;
  }

  void createNewRequestRoom(BuildContext context, Contact contact) =>
      AdaptivePageLayout.of(context).pushNamed(
        '/requestCompositing/${Forms.defaultForm}',
        arguments: [contact],
      );

  void startOutgoingRequestAction(
      BuildContext context, Contact contact, Organisation organisation) async {
    if (widget.onInvite != null) {
      if (contact.matrixId != null) {
        widget.onInvite(InviteType.contact, contact.matrixId, contact.passport);
      }
      return;
    }
    // Check if there is already an outgoing request with this contact point.
    final room = Famedly.of(context).client.rooms.firstWhere(
        (Room room) => room.requestContent?.contactId == contact.id,
        orElse: () => null);
    final roomId = room?.id;
    if (roomId == null) {
      return createNewRequestRoom(context, contact);
    } else {
      final newRoom = await showOkCancelAlertDialog(
        context: context,
        title: L10n.of(context).requestRoomDialog(
          room.requestContent.contactDescription,
          organisation.name,
        ),
        okLabel: L10n.of(context).createNewRoom,
        cancelLabel: L10n.of(context).useExistingRoom,
        useRootNavigator: false,
      );
      if (OkCancelResult.ok == newRoom) {
        return createNewRequestRoom(context, contact);
      }
    }
    await AdaptivePageLayout.of(context).pushNamedAndRemoveUntilIsFirst(
        '/room/requests/conversations/${room.id}');
  }

  void onTapEmail(List<Contact> contactEmails) async {
    if (contactEmails.isEmpty) {
      return;
    }
    if (contactEmails.length == 1) {
      // do single request to contactEmails[0]
      await AdaptivePageLayout.of(context).pushNamed(
          '/requestCompositing/${organisation.id}',
          arguments: [contactEmails[0]]);
    } else {
      // pick whom you want to email
      final contact = await showConfirmationDialog<Contact>(
        title: L10n.of(context).pickEmail,
        context: context,
        actions: contactEmails.map((c) => AlertDialogAction(
              label: c.description,
              key: c,
            )),
        useRootNavigator: false,
      );
      if (contact != null) {
        await AdaptivePageLayout.of(context).pushNamed(
            '/requestCompositing/${organisation.id}',
            arguments: [contact]);
      }
    }
  }

  final contactPointTiles = <Widget>[];
  final contactEmails = <Contact>[];
  void getContacts(roles) {
    for (final contact in organisation.contacts) {
      final role = roles?.firstWhere(
          (element) => element.contact.id == contact.matrixId,
          orElse: () => null);
      if (role != null && role.privateInvite ||
          contact.matrixId == null ||
          contact.matrixId == famedly.client.userID) {
        continue;
      }
      if (contact.email != null) {
        contactEmails.add(contact);
      }
      contactPointTiles.add(
        ListTile(
          onTap: () =>
              startOutgoingRequestAction(context, contact, organisation),
          trailing: Icon(FamedlyIconsV2.chats, color: FamedlyColors.azul),
          title: Text(contact.description,
              style: TextStyle(color: FamedlyColors.azul)),
          subtitle: Text(L10n.of(context).makeAnInquiry),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return FacilityPageView(this);
  }
}
