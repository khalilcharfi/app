This repository contains the **Famedly App**.

*  **The Famedly App** communicates with the matrix network using the [Famedly Matrix SDK](https://gitlab.com/famedly/famedlysdk).
*  The **Famedly App** uses the [Famedly Push Gateway](https://gitlab.com/famedly/services/famedly-push-gateway) to receive Push Notifications on Android and iOS.
*  The **Famedly App** uses the [Famedly Directory SDK](https://gitlab.com/famedly/libraries/famedly-directory-client-sdk) to communicate with the Famedly Directory. (Not yet fully implemented)
*  The **Famedly App** uses the **Famedly Matrix Tasks SDK** to provide a task list in the app.
*  The **Famedly Matrix SDK** is wired with the **Famedly Matrix SDK Store** for persistent storing of data.
*  The **Famedly App** uses the **Famedly Matrix Requests SDK** to provide intersectoral requests between organisations.
