/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'dart:convert';
import 'dart:io';

/// For working integration tests we need a homeserver, an account on this
/// server we can log in with username and password and two other accounts
/// visible on this server. Also the server must have a working Famedly
/// Contact Discovery feature.
class TestUser {
  static const String homeserverDefault = 'your_test_homeserver.invalid';
  static const String usernameDefault = 'tick';
  static const String passwordDefault = 'test';
  static const String username2Default = 'trick';
  static const String username3Default = 'track';
  String homeserver;
  String username;
  String fullname;
  String password;
  String username2;
  String username3;

  TestUser.get() {
    if (File('test-credentials.json').existsSync()) {
      final contents = File('test-credentials.json').readAsStringSync();
      final credentials = jsonDecode(contents);
      homeserver = credentials['homeserver'];
      username = credentials['username'];
      fullname = credentials['fullname'];
      password = credentials['password'];
      username2 = credentials['username2'];
      username3 = credentials['username3'];
    } else {
      homeserver = homeserverDefault;
      username = username2Default;
      fullname = username;
      password = passwordDefault;
      username2 = username2Default;
      username3 = username3Default;
    }
  }
}
