import 'package:flutter/material.dart';
import 'package:screenshot_callback/screenshot_callback.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';

import 'famedly_plugin.dart';

class ScreenshotWarningPlugin extends FamedlyPlugin {
  ScreenshotWarningPlugin(matrix) : super(matrix) {
    screenshotCallback.addListener(() {
      ScaffoldMessenger.of(famedly.context).showSnackBar(SnackBar(
          content: Text(localizations.screenshotWarningMessage),
          duration: Duration(seconds: 30)));
    });
  }

  final screenshotCallback = ScreenshotCallback();
  L10n get localizations => L10n.of(famedly.context);
}
