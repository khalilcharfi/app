/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:famedly/components/dialogs/key_verification_dialog.dart';
import 'package:famedlysdk/encryption/utils/key_verification.dart';
import 'package:famedly/components/settings_list_group.dart';
import 'package:famedly/styles/colors.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:adaptive_page_layout/adaptive_page_layout.dart';

import 'package:flutter/material.dart';
import 'package:future_loading_dialog/future_loading_dialog.dart';

import '../models/famedly.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import '../utils/date_time_extension.dart';
import '../utils/device_keys_extension.dart';

class DevicesSettingsPage extends StatefulWidget {
  @override
  DevicesSettingsPageState createState() => DevicesSettingsPageState();
}

class DevicesSettingsPageState extends State<DevicesSettingsPage> {
  List<Device> devices;

  Future<bool> _loadUserDevices(BuildContext context) async {
    if (devices != null) return true;
    devices = await Famedly.of(context).client.getDevices();
    return true;
  }

  void reload() => setState(() => devices = null);

  Future<void> _removeDevices(
      BuildContext context, List<String> deviceIds) async {
    final famedly = Famedly.of(context);
    try {
      await famedly.client.deleteDevices(deviceIds);
    } on MatrixException catch (exception) {
      if (!exception.requireAdditionalAuthentication) rethrow;
      final passwordInput = await showTextInputDialog(
        context: context,
        title: L10n.of(context).pleaseEnterYourPassword,
        useRootNavigator: false,
        textFields: [
          DialogTextField(
            obscureText: true,
            maxLines: 1,
            hintText: '********',
          ),
        ],
      );
      if (passwordInput != null) {
        await showFutureLoadingDialog(
          context: context,
          future: () => famedly.client.deleteDevices(
            deviceIds,
            auth: AuthenticationPassword(
              password: passwordInput.single,
              user: famedly.client.userID,
              identifier:
                  AuthenticationUserIdentifier(user: famedly.client.userID),
            ),
          ),
        );
      }
    }
  }

  void _removeDevicesAction(BuildContext context, List<Device> devices) async {
    final deviceIds = <String>[];
    for (final userDevice in devices) {
      deviceIds.add(userDevice.deviceId);
    }

    try {
      await _removeDevices(context, deviceIds);
      reload();
    } catch (e) {
      AdaptivePageLayout.of(context).showSnackBar(
        SnackBar(
          content: Text(e.toLocalizedString(context)),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(L10n.of(context).devices),
        leading: BackButton(),
      ),
      body: Padding(
        padding: FamedlyStyles.getTwoColumnListViewPadding(context),
        child: FutureBuilder<bool>(
          future: _loadUserDevices(context),
          builder: (BuildContext context, snapshot) {
            if (snapshot.hasError) {
              return Center(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Icon(Icons.error),
                    Text(snapshot.error.toString()),
                  ],
                ),
              );
            }
            if (!snapshot.hasData || this.devices == null) {
              return Center(child: CircularProgressIndicator());
            }
            final Function isOwnDevice = (Device userDevice) =>
                userDevice.deviceId == Famedly.of(context).client.deviceID;
            final devices = List<Device>.from(this.devices);
            final thisDevice =
                devices.firstWhere(isOwnDevice, orElse: () => null);
            devices.removeWhere(isOwnDevice);
            return SettingsListGroup(
              children: <Widget>[
                if (thisDevice != null)
                  UserDeviceListItem(
                    thisDevice,
                    remove: (d) => _removeDevicesAction(context, [d]),
                    key: Key('device_own'),
                  ),
                Divider(height: 1),
                if (devices.isNotEmpty)
                  ListTile(
                    key: Key('removeAllOtherDevicesButton'),
                    title: Text(
                      L10n.of(context).removeAllOtherDevices,
                      style: TextStyle(color: Colors.red),
                    ),
                    trailing: Icon(Icons.delete_outline),
                    onTap: () => _removeDevicesAction(context, devices),
                  ),
                Divider(height: 1),
                Expanded(
                  child: devices.isEmpty
                      ? Center(
                          child: Icon(
                            Icons.devices_other,
                            size: 60,
                            color: Theme.of(context).secondaryHeaderColor,
                          ),
                        )
                      : ListView.separated(
                          separatorBuilder: (BuildContext context, int i) =>
                              Divider(height: 1),
                          itemCount: devices.length,
                          itemBuilder: (BuildContext context, int i) =>
                              UserDeviceListItem(
                            devices[i],
                            remove: (d) => _removeDevicesAction(context, [d]),
                            key: Key('device_$i'),
                          ),
                        ),
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}

enum DeviceAction { verify, block, unblock, remove }

class UserDeviceListItem extends StatefulWidget {
  final Device userDevice;
  final Function(Device device) remove;

  const UserDeviceListItem(this.userDevice, {this.remove, Key key})
      : super(key: key);

  @override
  _UserDeviceListItemState createState() => _UserDeviceListItemState();
}

class _UserDeviceListItemState extends State<UserDeviceListItem> {
  void _onTapAction(BuildContext context, DeviceKeys key) async {
    final result = await showModalActionSheet<DeviceAction>(
        context: context,
        title: L10n.of(context).pleaseChoose,
        actions: [
          if (key != null) ...{
            SheetAction(
              label: L10n.of(context).verify,
              key: DeviceAction.verify,
              isDefaultAction: true,
            ),
            if (!key.blocked)
              SheetAction(
                label: L10n.of(context).block,
                key: DeviceAction.block,
                isDestructiveAction: true,
              ),
            if (key.blocked)
              SheetAction(
                label: L10n.of(context).unblock,
                key: DeviceAction.unblock,
              ),
          },
          SheetAction(
            label: L10n.of(context).removeDevice,
            key: DeviceAction.remove,
            isDestructiveAction: true,
          ),
        ]);
    if (result == null) return;
    switch (result) {
      case DeviceAction.verify:
        final req = key.startVerification();
        req.onUpdate = () {
          if ({KeyVerificationState.error, KeyVerificationState.done}
              .contains(req.state)) {
            setState(() => null);
          }
        };
        await KeyVerificationDialog(
          request: req,
          context: context,
        ).show(context);
        break;
      case DeviceAction.block:
        if (key.directVerified) {
          await key.setVerified(false);
        }
        await key.setBlocked(true);
        setState(() => null);
        break;
      case DeviceAction.unblock:
        await key.setBlocked(false);
        setState(() => null);
        break;
      case DeviceAction.remove:
        widget.remove(widget.userDevice);
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    final keys = Famedly.of(context)
        .client
        .userDeviceKeys[Famedly.of(context).client.userID]
        ?.deviceKeys[widget.userDevice.deviceId];
    return ListTile(
      onTap: () => _onTapAction(context, keys),
      leading: CircleAvatar(
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        foregroundColor: keys?.verificationColor ?? FamedlyColors.blueyGrey,
        child: Icon(keys?.iconData ?? DeviceKeysExtension.defaultIconData),
      ),
      title: Text(
        (widget.userDevice.displayName?.isNotEmpty ?? false)
            ? widget.userDevice.displayName
            : 'Famedly Client',
        style: TextStyle(fontWeight: FontWeight.bold),
      ),
      trailing: Text(widget.userDevice.lastSeenTs.localizedTimeShort(context)),
      subtitle: Text('${L10n.of(context).id}: ${widget.userDevice.deviceId}'),
    );
  }
}
