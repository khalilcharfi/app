/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'package:famedly/models/requests/request.dart';
import 'package:famedlysdk/famedlysdk.dart';

extension EventForwardedExtension on Event {
  bool get isForward => content['com.famedly.app.forward'] != null;

  Future<Profile> forwardedFrom() {
    final String userId = content['com.famedly.app.forward']['sender'];
    return room.client.getProfileFromUserId(userId);
  }

  bool get isMine {
    return (sender.id == room.client.userID &&
            type != Request.requestNameSpace) ||
        (type == Request.requestNameSpace &&
            (content['creator'] ?? sender.id) == room.client.userID);
  }
}
