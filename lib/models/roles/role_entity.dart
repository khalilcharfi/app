import 'package:adaptive_dialog/adaptive_dialog.dart';

import 'package:famedly_directory_client_sdk/famedly_directory_client_sdk.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/material.dart';
import 'package:future_loading_dialog/future_loading_dialog.dart';
import 'package:uuid/uuid.dart';
import 'package:adaptive_page_layout/adaptive_page_layout.dart';
import '../famedly.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import '../requests/request_client_extension.dart';
import '../requests/room_extension.dart';

final String ROLE_BASE = '/client/unstable/com.famedly/role/r0';

class RoleEntity {
  final Client client;
  final int id;
  final String name;
  final Uri avatarUrl;
  final String owner;
  final List<String> users;
  final List<int> parents;
  final List<int> children;
  final bool privateInvite;
  final bool publicInvite;
  final String mxid;
  bool available = true;
  bool userAvailable = true;

  RoleEntity({
    this.client,
    this.id,
    this.name,
    this.avatarUrl,
    this.owner,
    this.users,
    this.parents,
    this.children,
    this.privateInvite,
    this.publicInvite,
    this.mxid,
  });

  RoleEntity.fromJson(Client c, Map<String, dynamic> json)
      : client = c,
        id = json['id'],
        name = json['name'],
        avatarUrl = Uri.parse(json['avatar_url'] ?? ''),
        owner = json['owner'],
        users = json['users'].cast<String>(),
        parents = json['parents'].cast<int>(),
        children = json['children'].cast<int>(),
        publicInvite = json['public_invite'],
        privateInvite = json['private_invite'],
        mxid = json['mxid'];

  Future<void> userOptOut({String mxid, bool out = true}) async {
    mxid ??= client.userID;
    final ident = 'role;$id;user;$mxid';
    await client.request(
      RequestType.PUT,
      ROLE_BASE + '/timespan/' + Uri.encodeComponent(ident) + '/opt_out',
      data: {
        'opt_out': out,
      },
    );
  }

  Contact get contact {
    return Contact(
      description: name,
      uri: 'https://matrix.to/#/' + mxid,
      id: mxid,
      tags: [],
    );
  }

  Future<LoadingDialogResult<String>> createNewRequestRoom(
      BuildContext context) {
    final uuid = Uuid();
    final requestId = uuid.v4();
    return showFutureLoadingDialog<String>(
        context: context,
        future: () => contact.startRequest(
              requestTitle: name,
              requestBody: L10n.of(context).internalRequestDefaultBody(name),
              requestId: requestId,
              client: Famedly.of(context).client,
            ));
  }

  void startRequest(BuildContext context) async {
    final client = Famedly.of(context).client;
    final room = client.rooms.firstWhere(
        (Room room) => room.requestContent?.contactId == mxid,
        orElse: () => null);
    var roomId = room?.id;
    if (roomId == null) {
      if (OkCancelResult.cancel ==
          await showOkCancelAlertDialog(
            context: context,
            title: L10n.of(context).startRequest,
            okLabel: L10n.of(context).createNewRoom,
            cancelLabel: L10n.of(context).cancel,
            useRootNavigator: false,
          )) {
        return;
      }
      final response = await createNewRequestRoom(context);
      if (response.error != null) return;
      roomId = response.result;
    } else {
      if (OkCancelResult.ok ==
          await showOkCancelAlertDialog(
            context: context,
            title: L10n.of(context).internalRequestRoomDialog(name),
            okLabel: L10n.of(context).createNewRoom,
            cancelLabel: L10n.of(context).useExistingRoom,
            useRootNavigator: false,
          )) {
        roomId = null;
      }
      if (roomId == null) {
        final response = await createNewRequestRoom(context);
        if (response.error != null) return;
        roomId = response.result;
      }
    }
    await AdaptivePageLayout.of(context)
        .pushNamedAndRemoveUntilIsFirst('/room/$roomId');
  }
}
