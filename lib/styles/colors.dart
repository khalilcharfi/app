/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'dart:math';

import 'package:adaptive_page_layout/adaptive_page_layout.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:universal_html/html.dart' as html;

/// Helper Class to make colors easier to copy
/// This class converts a hex string to an integer value
class HexColor extends Color {
  static int _getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll('#', '');
    if (hexColor.length == 6) {
      hexColor = 'FF' + hexColor;
    }
    return int.parse(hexColor, radix: 16);
  }

  HexColor(final String hexColor) : super(_getColorFromHex(hexColor));
}

class Themes {
  static ThemeData get famedlyLightTheme => _getFamedlyTheme();

  static ThemeData get famedlyDarkTheme => _getFamedlyTheme(bright: false);

  static ThemeData _getFamedlyTheme({bool bright = true}) {
    final base = bright ? ThemeData.light() : ThemeData.dark();
    final defaultTextTheme = bright
        ? _buildFamedlyLightTextTheme(base.textTheme)
        : _buildFamedlyDarkTextTheme(base.textTheme);
    return base.copyWith(
      visualDensity: VisualDensity.standard,
      popupMenuTheme: PopupMenuThemeData(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15),
        ),
      ),
      snackBarTheme: SnackBarThemeData(behavior: SnackBarBehavior.floating),
      brightness: bright ? Brightness.light : Brightness.dark,
      backgroundColor: bright ? Colors.white : Colors.black,
      scaffoldBackgroundColor:
          bright ? Colors.white : FamedlyColors.moreDarkerGrey,
      secondaryHeaderColor:
          bright ? FamedlyColors.lightGrey : FamedlyColors.charcoalGrey,
      accentColor: FamedlyColors.azul,
      primaryColor: FamedlyColors.azul,
      selectedRowColor:
          bright ? FamedlyColors.paleGrey : FamedlyColors.charcoalGrey,
      floatingActionButtonTheme: FloatingActionButtonThemeData(
        elevation: 12,
        backgroundColor: FamedlyColors.azul,
        foregroundColor: Colors.white,
      ),
      bottomAppBarTheme: BottomAppBarTheme(
        color: Colors.white,
      ),
      bottomNavigationBarTheme: BottomNavigationBarThemeData(
        selectedLabelStyle: TextStyle(
          color: FamedlyColors.azul,
        ),
      ),
      elevatedButtonTheme: ElevatedButtonThemeData(
        style: ElevatedButton.styleFrom(
          primary: FamedlyColors.azul,
          padding: EdgeInsets.all(16),
          onPrimary: Colors.white,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
        ),
      ),
      tabBarTheme: TabBarTheme(
        labelStyle: TextStyle(
          color: bright ? FamedlyColors.metallicBlue : Colors.white,
        ),
      ),
      pageTransitionsTheme: PageTransitionsTheme(
        builders: {TargetPlatform.android: ZoomPageTransitionsBuilder()},
      ),
      appBarTheme: AppBarTheme(
        iconTheme: IconThemeData(
          color: bright ? FamedlyColors.slateGrey : Colors.white,
        ),
        color: bright ? Colors.white : FamedlyColors.mostDarkerGrey,
        brightness: bright ? Brightness.light : Brightness.dark,
        textTheme: TextTheme(
            headline6: TextStyle(
                color: bright ? FamedlyColors.slate : Colors.white,
                fontSize: 15,
                fontWeight: FontWeight.w500)),
      ),
      bottomAppBarColor: bright ? null : FamedlyColors.slate,
      dialogTheme: DialogTheme(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(25))),
        titleTextStyle: TextStyle(
          color: bright ? FamedlyColors.slate : Colors.white,
          fontSize: 15,
          fontWeight: FontWeight.w600,
        ),
        contentTextStyle: TextStyle(
          color: bright ? FamedlyColors.metallicBlue : Colors.white,
          fontSize: 15,
          fontWeight: FontWeight.w400,
        ),
      ),
      textTheme: defaultTextTheme,
      primaryTextTheme: defaultTextTheme,
      accentTextTheme: defaultTextTheme,
      primaryIconTheme: base.iconTheme.copyWith(color: FamedlyColors.azul),
      sliderTheme: SliderThemeData(
        thumbColor: FamedlyColors.azul,
        thumbShape: RoundSliderThumbShape(
          enabledThumbRadius: 8,
          disabledThumbRadius: 8,
        ),
        activeTrackColor: FamedlyColors.slate,
        inactiveTickMarkColor: FamedlyColors.paleGrey,
      ),
    );
  }

  static TextTheme _buildFamedlyLightTextTheme(TextTheme base) {
    final defaultContent = TextStyle(
      color: FamedlyColors.metallicBlue,
      fontSize: 15,
    );
    return base.copyWith(
      button: TextStyle(fontSize: 15, color: FamedlyColors.azul),
      headline5: TextStyle(color: Colors.black87, fontSize: 15),
      headline6: defaultContent,
      bodyText2: defaultContent,
      headline1: TextStyle(color: FamedlyColors.slate, fontSize: 15),
      headline4: defaultContent,
      headline3: defaultContent,
      headline2: defaultContent,
      subtitle1: defaultContent,
      caption: defaultContent,
      subtitle2: defaultContent,
      overline: defaultContent,
    );
  }

  static TextTheme _buildFamedlyDarkTextTheme(TextTheme base) {
    return base.copyWith(
      button: TextStyle(fontSize: 15, color: FamedlyColors.azul),
      headline1: TextStyle(color: Colors.white, fontSize: 15),
      headline5: TextStyle(color: Colors.white, fontSize: 16.0),
      headline6: TextStyle(color: Colors.white, fontSize: 15),
      bodyText2: TextStyle(color: Colors.white, fontSize: 15),
      bodyText1: TextStyle(color: Colors.white, fontSize: 15),
    );
  }
}

/// Defines colors to be used
/// See: https://app.zeplin.io/project/5d07ad07e700fa160ecee1ba/styleguide
/// Last Updated: 05.10.2019 20:49
class FamedlyColors {
  static const Color aquaGreen = Color(0xff14de92);
  static const Color aquaMarine = Color(0xff1fdcca);
  static const Color black10 = Color(0x19000000);
  static const Color blueyGrey = Color(0xff95acb9); // Previously dark_grey!
  static const Color lightBlueyGrey = Color(0xffeef3fe);
  static const Color cloudyBlue = Color(0xffbfcdd4); // Previously avatar_grey!
  static const Color darkSkyBlue = Color(0xff5eb5d7);
  static const Color emerald = Color(0xff00a74a);
  static const Color grapefruit = Color(0xffff5252);
  static const Color lightGrey = Color(0xfff6f7f7);
  static const Color metallicBlue = Color(0xff4e6e81);
  static const Color offBlue = Color(0xff5373bd);
  static const Color paleGrey = Color(0xffedf0f2);
  static const Color paleGreyLight = Color(0xffd7dce1);
  static const Color paleGrey2 = Color(0xfff6f7f8);
  static const Color slate = Color(0xff4d576d);
  static const Color black10percent = Color(0x19212531);
  static const Color darkPurple = Color(0xff898998);
  static const Color charcoalGrey = Color(0xff3a464d);
  static const Color yellow = Color(0xffffad27);
  static const Color hoverDark = Color(0x33000000);
  static const Color slateGrey = Color(0xff646973);
  static const Color darkGrey = Color(0xff3c414b);
  static const Color darkerGrey = Color(0xff252a31);
  static const Color moreDarkerGrey = Color(0xff1e2326);
  static const Color mostDarkerGrey = Color(0xff0e1316);
  static const Color azul = Color(0xff2267f0);
  static const Color darkLinkColor = Color(0xff2267f0);
  static const Color lightLinkColor = Color(0xffc4d9f8);
}

class FamedlyStyles {
  static const double columnWidth = 360.0;

  static bool get columnMode =>
      (kIsWeb
          ? html.window.innerWidth
          : MediaQueryData.fromWindow(WidgetsBinding.instance.window)
              .size
              .width) >
      2.5 * columnWidth;

  static EdgeInsets getTwoColumnListViewPadding(BuildContext context) =>
      EdgeInsets.symmetric(
        horizontal: AdaptivePageLayout.of(context).columnMode(context) &&
                !AdaptivePageLayout.of(context).threeColumnMode(context)
            ? max(
                0,
                (MediaQuery.of(context).size.width -
                        2.5 * FamedlyStyles.columnWidth) /
                    2)
            : 0,
      );
}
