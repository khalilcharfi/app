import 'package:famedly/components/rooms_list/item.dart';
//import 'package:famedly/views/chat_room.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
//import 'package:mockito/mockito.dart';

import 'utils.dart';

void main() {
  /// All Tests related to the Rooms List Items
  group('Rooms List Item', () {
    NavigatorObserver mockObserver;
    final client = Utils.defaultClient;

    setUp(() {
      mockObserver = MockNavigatorObserver();
    });

    testWidgets('renders all elements', (WidgetTester tester) async {
      await tester.runAsync(() async {
        final room = Room(id: '1234', client: await client);
        await tester.pumpWidget(
          await Utils.getWidgetWrapper(
            Material(
              child: RoomItem(
                item: room,
              ),
            ),
            mockObserver,
          ),
        );

        await tester.pumpAndSettle();

        // Check all exist
        expect(find.byKey(Key('room_name_1234')), findsOneWidget);
        expect(find.byKey(Key('last_message')), findsOneWidget);
        expect(find.byKey(Key('time_created')), findsOneWidget);

        // Check that we do only have one line of last_message
        final last_message =
            tester.widget<Text>(find.byKey(Key('last_message')));

        expect(last_message.maxLines, 1);
      });
    });

/*    testWidgets('click does open ChatRoom', (WidgetTester tester) async {
      await tester.runAsync(() async {
        var room = Room(id: '1234', client: await client);
        await tester.pumpWidget(
          await Utils.getWidgetWrapper(
            RoomItem(
              item: room,
            ),
            mockObserver,
          ),
        );
        verify(mockObserver.didPush(any, any));

        await tester.pumpAndSettle();

        await Utils.tapItemByType(tester, ListTile);

        verify(mockObserver.didPush(any, any));
        expect(find.byType(ChatRoom), findsOneWidget);
      });
    });*/
  });
}
