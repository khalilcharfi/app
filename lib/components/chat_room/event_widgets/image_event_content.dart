import 'package:famedly/styles/famedly_icons_v2_icons.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_blurhash/flutter_blurhash.dart';
import 'package:adaptive_page_layout/adaptive_page_layout.dart';

import '../../../config/chat_configs.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import '../../../styles/colors.dart';
import 'event_widget.dart';

class ImageEventContent extends StatefulWidget {
  final Event imageEvent;
  final bool isReply;

  const ImageEventContent(this.imageEvent, {Key key, bool isReply = false})
      : isReply = isReply,
        super(key: key);

  static final Map<String, MatrixFile> _matrixFileMap = {};

  @override
  _ImageEventContentState createState() => _ImageEventContentState();
}

class _ImageEventContentState extends State<ImageEventContent> {
  MatrixFile get _file =>
      ImageEventContent._matrixFileMap[widget.imageEvent.eventId];

  set _file(MatrixFile file) {
    ImageEventContent._matrixFileMap[widget.imageEvent.eventId] = file;
  }

  Future<MatrixFile> _getFile(BuildContext context) async {
    _file ??= await widget.imageEvent.downloadAndDecryptAttachment(
      getThumbnail: widget.imageEvent.hasThumbnail,
    );
    return _file;
  }

  bool _autoLoad;

  @override
  Widget build(BuildContext context) {
    final size = EventWidget.getMaxWidth(
        context, AdaptivePageLayout.of(context).columnMode(context),
        maxMode: false);
    final height = widget.isReply ? size / 4 : size;
    var box = Size(size, height);

    // fit the image inside the maximum allowed size, if it does not have an absurd aspect ratio
    if (widget.imageEvent.content['info'] is Map &&
        widget.imageEvent.content['info']['h'] != null &&
        widget.imageEvent.content['info']['w'] != null) {
      final h = widget.imageEvent.content['info']['h'];
      final w = widget.imageEvent.content['info']['w'];

      if (h > 10 && w > 10) {
        final maxAspect = 3;
        if (h < maxAspect * w && w < maxAspect * h) {
          // fits within max aspect ratio
          box =
              applyBoxFit(BoxFit.contain, Size(w.toDouble(), h.toDouble()), box)
                  .destination;
        } else if (h < w * maxAspect) {
          // wider than max aspect ratio
          box = Size(size, size / maxAspect);
        } else {
          // higher than max aspect ratio
          box = Size(size / maxAspect, size);
        }
      }
    }

    _autoLoad ??= kIsWeb ||
        (widget.imageEvent.content['info'] is Map &&
            widget.imageEvent.content['info']['thumbnail_info'] is Map &&
            widget.imageEvent.content['info']['thumbnail_info']['size'] !=
                null &&
            widget.imageEvent.content['info']['thumbnail_info']['size'] <
                ChatConfigs.maxAutoLoadImageSize) ||
        (widget.imageEvent.content['info'] is Map &&
            widget.imageEvent.content['info']['size'] != null &&
            widget.imageEvent.content['info']['size'] <
                ChatConfigs.maxAutoLoadImageSize);

    if (!_autoLoad) {
      final _colors = <Color>[FamedlyColors.aquaGreen, FamedlyColors.azul];
      return InkWell(
        onTap: () => setState(() => _autoLoad = true),
        child: Container(
          width: box.width,
          height: box.height,
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: _colors,
              end: Alignment.bottomRight,
            ),
          ),
          child: Stack(
            children: [
              if (widget.imageEvent.content['info'] is Map &&
                  widget.imageEvent.content['info']['xyz.amorgan.blurhash']
                      is String)
                BlurHash(
                    hash: widget.imageEvent.content['info']
                        ['xyz.amorgan.blurhash']),
              Center(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    CircleAvatar(
                      backgroundColor: FamedlyColors.blueyGrey,
                      radius: 30,
                      child: Icon(FamedlyIconsV2.down_1),
                    ),
                    SizedBox(height: 4),
                    if (widget.imageEvent.content['info'] is Map &&
                        widget.imageEvent.content['info']['size'] is num)
                      Text(
                          '${(widget.imageEvent.content['info']['size'] / 1000).round()} kB'),
                  ],
                ),
              ),
            ],
          ),
        ),
      );
    }

    if (_file != null) {
      return Image.memory(
        _file.bytes,
        width: box.width,
        height: box.height,
        fit: BoxFit.cover,
      );
    }
    return FutureBuilder<MatrixFile>(
        future: _getFile(context),
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return Container(
              width: box.width,
              height: box.height,
              alignment: Alignment.center,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Icon(Icons.broken_image, size: 40),
                  Text(
                    L10n.of(context).oopsSomethingWentWrong,
                  ),
                  Text(snapshot.error.toString()),
                ],
              ),
            );
          }
          if (!snapshot.hasData) {
            return Container(
              width: box.width,
              height: box.height,
              alignment: Alignment.center,
              child: Stack(
                children: [
                  if (widget.imageEvent.content['info'] is Map &&
                      widget.imageEvent.content['info']['xyz.amorgan.blurhash']
                          is String)
                    BlurHash(
                        hash: widget.imageEvent.content['info']
                            ['xyz.amorgan.blurhash']),
                  Center(child: CircularProgressIndicator()),
                ],
              ),
            );
          }
          return Image.memory(
            snapshot.data.bytes,
            width: box.width,
            height: box.height,
            fit: BoxFit.cover,
          );
        });
  }
}
