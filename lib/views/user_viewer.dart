/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'dart:async';

import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:adaptive_page_layout/adaptive_page_layout.dart';
import 'package:famedly/components/settings_list_group.dart';
import 'package:famedly/models/plugins/voip_plugin.dart';
import 'package:famedly/styles/famedly_icons_v2_icons.dart';
import 'package:famedly_directory_client_sdk/famedly_directory_client_sdk.dart';
import 'package:famedlysdk/famedlysdk.dart';

import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:future_loading_dialog/future_loading_dialog.dart';

import '../components/profile_scaffold.dart';
import '../components/settings_menu_item.dart';
import '../components/user_settings_popup_menu.dart';
import '../models/famedly.dart';
import '../models/roles/client_extension.dart';
import '../styles/colors.dart';
import '../styles/famedly_icons_icons.dart';
import '../utils/user_directory_client_extension.dart';

/// The User Detail Page
///
/// Accessed when clicking on a User Avatar
///
/// Displays:
/// - Username as Title
/// - Avatar
/// - Actions for DM, Call, Mute
/// - Media -> Pictures, Videos, Documents
class UserViewer extends StatefulWidget {
  /// Leave this null if you dont want to display the user profile in the
  /// context of a room.
  final String roomId;

  /// Please note that this can be the mxid OR the user directory ID!
  final String userId;

  /// Set a profile if you dont want to display the user profile in the context
  /// of a room!
  final Profile profile;

  /// Please set the organisation name if you display a user from the
  /// user directory
  final String organisationName;

  const UserViewer({
    this.roomId,
    @required this.userId,
    this.profile,
    this.organisationName,
    Key key,
  }) : super(key: key);

  @override
  UserViewState createState() => UserViewState();
}

class UserViewState extends State<UserViewer> {
  Future<User> loadUser(Room room) {
    if (room != null && widget.userId.isValidMatrixId) {
      return room.requestUser(widget.userId);
    }
    return Future.value(
      User(
        widget.userId,
        displayName: widget.profile.displayname,
        avatarUrl: widget.profile.avatarUrl?.toString(),
      ),
    );
  }

  void startDirectChat(BuildContext context, String userId) async {
    final userDirectoryClient =
        await Famedly.of(context).getUserDirectoryClient();
    if (userId.isValidMatrixId) {
      final roomId = await showFutureLoadingDialog(
        context: context,
        future: () => Famedly.of(context).client.startDirectChat(userId),
      );
      if (roomId.error == null) {
        await AdaptivePageLayout.of(context)
            .pushNamedAndRemoveUntilIsFirst('/room/${roomId.result}');
      }
    } else {
      final userData = await showFutureLoadingDialog(
        context: context,
        future: () =>
            userDirectoryClient.requestUserDataFromUuid(context, userId),
      );
      if (userData.error != null) return;
      if (userData.result.mxid == Famedly.of(context).client.userID) {
        await AdaptivePageLayout.of(context)
            .pushNamed('/settings/userDirectory');
        return;
      }
      startDirectChat(context, userData.result.mxid);
    }
  }

  @override
  Widget build(BuildContext context) {
    final famedly = Famedly.of(context);

    final directChatRoomId =
        famedly.client.getDirectChatFromUserId(widget.userId) ?? widget.roomId;

    final room = widget.roomId != null
        ? famedly.client.getRoomById(widget.roomId)
        : directChatRoomId == null
            ? null
            : famedly.client.getRoomById(directChatRoomId);

    return StreamBuilder<Object>(
        stream: famedly.client.onEvent.stream.where((e) =>
            e.roomID == widget.roomId &&
            ['m.room.member', 'm.room.power_levels']
                .contains(e.content['type'])),
        builder: (context, snapshot) {
          return FutureBuilder<User>(
            future: loadUser(room),
            builder: (BuildContext context, AsyncSnapshot<User> snapshot) {
              if (snapshot.hasError) {
                Logs().e('Error while loading users', snapshot.error);
              }
              if (!snapshot.hasData) {
                return Container(
                    color: Theme.of(context).backgroundColor,
                    child: Center(
                      child: CircularProgressIndicator(),
                    ));
              }
              final user = snapshot.data;
              final userSettingsPopupMenu =
                  UserSettingsPopupMenu(context, user);
              return ProfileScaffold(
                key: Key('UserViewer'),
                profileContent: user.avatarUrl,
                profileName: user.calcDisplayname(mxidLocalPartFallback: false),
                title: Text(
                  user.calcDisplayname(mxidLocalPartFallback: false),
                ),
                actions: room == null
                    ? null
                    : userSettingsPopupMenu.hasActions
                        ? [
                            userSettingsPopupMenu,
                          ]
                        : null,
                buttons: [
                  Builder(
                      builder: (context) => ProfileScaffoldButton(
                            key: Key('ChatProfileScaffoldButton'),
                            iconData: FamedlyIconsV2.chats,
                            onTap: () =>
                                startDirectChat(context, widget.userId),
                          )),
                  if (room != null)
                    Builder(builder: (context) {
                      return ProfileScaffoldButton(
                        key: Key('CallProfileScaffoldVideoButton'),
                        iconData: FamedlyIconsV2.phone,
                        onTap: () async {
                          final success = await showFutureLoadingDialog(
                              context: context,
                              future: () =>
                                  Famedly.of(context).client.getTurnServer());
                          if (success.result != null) {
                            final voip = Famedly.of(context).voipPlugin;
                            await voip
                                .inviteToCall(widget.roomId, CallType.kVideo)
                                .catchError((e) {
                              AdaptivePageLayout.of(context).showSnackBar(
                                SnackBar(
                                  content: Text(e.toLocalizedString(context)),
                                ),
                              );
                            });
                          } else {
                            await showOkAlertDialog(
                              context: context,
                              title: L10n.of(context)
                                  .thisFeatureHasNotBeenImplementedYet,
                              okLabel: L10n.of(context).next,
                              useRootNavigator: false,
                            );
                          }
                        },
                      );
                    }),
                  if (room != null)
                    Builder(builder: (context) {
                      return ProfileScaffoldButton(
                          key: room.pushRuleState == PushRuleState.notify
                              ? Key('NotificationOnProfileScaffoldButton')
                              : Key('NotificationOffProfileScaffoldButton'),
                          iconData: room.pushRuleState == PushRuleState.notify
                              ? FamedlyIconsV2.on
                              : FamedlyIconsV2.off,
                          onTap: () async {
                            var newState = PushRuleState.notify;
                            if (room.pushRuleState == PushRuleState.notify) {
                              newState = PushRuleState.mentionsOnly;
                            }
                            final Future pushRuleUpdate = room
                                .client.onAccountData.stream
                                .where((u) => u.type == 'm.push_rules')
                                .first;
                            final success = await showFutureLoadingDialog(
                                context: context,
                                future: () => room.setPushRuleState(newState));
                            if (success.error != null) return;
                            await pushRuleUpdate;
                            setState(() {});
                          });
                    }),
                ],
                body: Column(
                  children: [
                    SettingsListGroup(
                      title: L10n.of(context).organisation,
                      children: [
                        ListTile(
                          contentPadding: EdgeInsets.symmetric(
                              vertical: 4.0, horizontal: 16.0),
                          leading: Icon(FamedlyIconsV2.directory),
                          title: widget.organisationName == null &&
                                  widget.userId.domain ==
                                      famedly.client.userID.domain
                              ? FutureBuilder<Organisation>(
                                  future: famedly.client.getOwnOrganisation(
                                      famedly.getOrganizationDirectoryClient()),
                                  builder: (_, snapshot) {
                                    if (snapshot.connectionState !=
                                        ConnectionState.done) {
                                      return LinearProgressIndicator();
                                    }
                                    return Text(snapshot.data?.name ??
                                        famedly.client.userID.domain);
                                  })
                              : Text(widget.organisationName ??
                                  widget.userId.domain),
                        ),
                      ],
                    ),
                    if (room != null)
                      SettingsListGroup(
                        title: L10n.of(context).media,
                        children: <Widget>[
                          SettingsMenuItem(
                            Icon(
                              FamedlyIcons.camera,
                              color: FamedlyColors.blueyGrey,
                            ),
                            L10n.of(context).images,
                            Container(),
                            () => AdaptivePageLayout.of(context).pushNamed(
                                '/room/${widget.roomId}/media/images'),
                            key: Key('ImagesSettingsMenuItem'),
                          ),
                          SettingsMenuItem(
                            Icon(
                              FamedlyIcons.video,
                              color: FamedlyColors.blueyGrey,
                            ),
                            L10n.of(context).videos,
                            Container(),
                            () => AdaptivePageLayout.of(context).pushNamed(
                                '/room/${widget.roomId}/media/videos'),
                            key: Key('VideosSettingsMenuItem'),
                          ),
                          SettingsMenuItem(
                            Icon(
                              FamedlyIcons.document,
                              color: FamedlyColors.blueyGrey,
                            ),
                            L10n.of(context).files,
                            Container(),
                            () => AdaptivePageLayout.of(context).pushNamed(
                                '/room/${widget.roomId}/media/documents'),
                            key: Key('DocumentsSettingsMenuItem'),
                          ),
                          Padding(
                            padding: EdgeInsets.only(
                                right: MediaQuery.of(context).size.width - 60),
                            child: Container(
                              height: 1,
                              color: Theme.of(context).backgroundColor,
                            ),
                          ),
                        ],
                      ),
                  ],
                ),
              );
            },
          );
        });
  }
}
