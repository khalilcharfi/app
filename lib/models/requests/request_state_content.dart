class RequestStateElementContent {
  bool accept;
  bool reject;
  bool needInfo;

  RequestStateElementContent({this.accept, this.reject, this.needInfo});

  RequestStateElementContent.fromJson(Map<String, dynamic> json)
      : accept = json['accept'] ?? false,
        reject = json['reject'] ?? false,
        needInfo = json['need_info'] ?? false;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    final data = map;
    data['accept'] = accept;
    data['reject'] = reject;
    data['need_info'] = needInfo;
    return data;
  }
}

class RequestStateContent {
  RequestStateElementContent requesting;
  RequestStateElementContent requested;
  bool roomHandoff;
  bool resolved;

  RequestStateContent({this.requesting, this.requested, this.roomHandoff});

  RequestStateContent.fromJson(Map<String, dynamic> json)
      : requesting = RequestStateElementContent.fromJson(json['requesting']),
        requested = RequestStateElementContent.fromJson(json['requested']),
        roomHandoff = json['room_handoff'] ?? false,
        resolved = json['resolved'] ?? false;

  RequestStateContent.newBlank()
      : requesting = RequestStateElementContent(
            accept: false, reject: false, needInfo: false),
        requested = RequestStateElementContent(
            accept: false, reject: false, needInfo: false),
        roomHandoff = false,
        resolved = false;

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['requesting'] = requesting.toJson();
    data['requested'] = requested.toJson();
    data['room_handoff'] = roomHandoff;
    data['resolved'] = resolved;
    return data;
  }
}
