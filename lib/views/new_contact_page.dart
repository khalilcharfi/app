/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'dart:math';

import 'package:famedly/styles/famedly_icons_v2_icons.dart';
import 'package:flutter/material.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:share/share.dart';
import '../models/famedly.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'qr_scanner_page.dart';

class NewContactPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final link =
        '${Famedly.inviteLinkPrefix}${Uri.encodeComponent(Famedly.of(context).client.userID)}';

    return Scaffold(
      appBar: AppBar(
        leading: BackButton(),
        elevation: 0,
        title: Text(L10n.of(context).newContact),
      ),
      body: SafeArea(
        child: ListView(
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: Text(
                L10n.of(context).startNewConversation,
                textAlign: TextAlign.justify,
                style: TextStyle(
                  fontSize: 18,
                ),
              ),
            ),
            SizedBox(height: 16),
            Center(
              child: QrImage(
                data: link,
                version: QrVersions.auto,
                size: min(MediaQuery.of(context).size.width - 16, 200),
              ),
            ),
            SizedBox(height: 16),
            Divider(height: 1),
            ListTile(
              title: Text(
                L10n.of(context).scanTheQrCode,
                style: TextStyle(
                  fontSize: 18,
                ),
              ),
              leading: Icon(Icons.camera_alt),
              onTap: () => QrScannerPage.scan(context, onQrCode: (String link) {
                if (link.startsWith(Famedly.inviteLinkPrefix)) {
                  launch(link);
                }
              }),
            ),
            Divider(height: 1),
            ListTile(
              title: Text(
                L10n.of(context).shareInvitationLink,
                style: TextStyle(
                  fontSize: 18,
                ),
              ),
              leading: Icon(FamedlyIconsV2.share),
              onTap: () => Share.share(
                link,
                subject: L10n.of(context).shareInvitationLink,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
