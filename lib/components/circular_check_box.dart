import 'package:flutter/material.dart';

class CircularCheckBox extends StatefulWidget {
  final bool value;
  final Color activeColor;
  final void Function(bool) onChanged;

  const CircularCheckBox({
    Key key,
    @required this.value,
    this.activeColor,
    @required this.onChanged,
  }) : super(key: key);
  @override
  _CircularCheckBoxState createState() => _CircularCheckBoxState();
}

class _CircularCheckBoxState extends State<CircularCheckBox> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      borderRadius: BorderRadius.circular(20),
      onTap: () => widget.onChanged(!widget.value),
      child: Container(
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: widget.value ? widget.activeColor : null,
          border: Border.all(
            width: 1,
            color: Theme.of(context).textTheme.bodyText1.color,
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.all(4.0),
          child: widget.value
              ? Icon(
                  Icons.check,
                  size: 20.0,
                  color: Colors.white,
                )
              : SizedBox(width: 20, height: 20),
        ),
      ),
    );
  }
}
