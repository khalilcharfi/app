import 'package:famedly/components/users_list/item.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'utils.dart';

void main() {
  /// All Tests related to the UserItem

  group(
    'UserItem',
    () {
      NavigatorObserver mockObserver;

      setUp(() {
        mockObserver = MockNavigatorObserver();
      });

      /// RoomList Test
      testWidgets(
        'has a displayName',
        (WidgetTester tester) async {
          await tester.runAsync(() async {
            // Create widget
            await tester.pumpWidget(
              await Utils.getWidgetWrapper(
                Scaffold(
                  body: UserItem(
                    onTap: () {},
                    contact: Profile('Larodar', Uri.parse('mxc://placeholder'))
                      ..userId = '@foo:bar.com',
                  ),
                ),
                mockObserver,
              ),
            );

            // Check if all items got added
            expect(find.text('Larodar'), findsOneWidget);
          });
        },
      );
    },
  );
}
