/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'package:famedlysdk/famedlysdk.dart';
import 'error_reporter.dart';
import 'matrix_file_thumbnail_extension.dart';

extension RoomSendFileExtension on Room {
  Future<String> sendFileEventWithThumbnail(
    MatrixFile file, {
    String txid,
    Event inReplyTo,
    String editEventId,
    bool waitUntilSent,
  }) async {
    MatrixFile thumbnail;
    try {
      if (file is MatrixImageFile) {
        thumbnail = await file.createThumbnail();

        if (thumbnail != null && thumbnail.size > file.size ~/ 2) {
          thumbnail = null;
        }
      }
    } catch (e, s) {
      // send no thumbnail
      // ignore: unawaited_futures
      ErrorReporter.reportError(e, s);
    }

    return sendFileEvent(
      file,
      txid: txid,
      inReplyTo: inReplyTo,
      editEventId: editEventId,
      waitUntilSent: waitUntilSent ?? false,
      thumbnail: thumbnail,
    );
  }
}
