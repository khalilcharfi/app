import 'package:famedly/models/tasks/subtask.dart';
import 'package:famedly/models/tasks/task.dart';
import 'package:famedly/models/tasks/tasklist.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter_test/flutter_test.dart';

import 'fake_matrix_api.dart';

void main() {
  /// All Tests related to the tasks
  group('Tasks', () {
    final client = Client(
      'testclient',
      httpClient: FakeMatrixApi(),
    );
    client.checkHomeserver('https://fakeServer.notExisting');
    final namespace = 'com.famedly.talk.tasks';
    final title = 'testtitle';
    final description = 'testDesc';
    final date = DateTime.now();
    final done = true;

    final jsonTask = {
      'title': title,
      'description': description,
      'date': date.millisecondsSinceEpoch,
      'done': done,
      'subtask': [
        {
          'title': title,
          'done': done,
        },
      ],
    };
    var updateCounter = 0;
    var list = TaskList(client);
    list.onUpdate = () {
      updateCounter++;
    };
    final testTask = Task.fromJson(jsonTask, list);

    test('CreateFromAndToJson', () {
      final testTask = Task.fromJson(jsonTask, list);
      expect(list.tasks.length, 0);
      expect(testTask.title, title);
      expect(testTask.description, description);
      expect(testTask.date.minute, date.minute);
      expect(testTask.done, done);
      expect(testTask.subtask.length, 1);
      expect(testTask.subtask[0].title, title);
      expect(testTask.subtask[0].done, done);
      expect(testTask.toJson(), jsonTask);
    });
    test('getTaskList', () async {
      // Try with no account data
      expect(list.tasks, []);

      // Try empty account data list
      client.accountData[namespace] =
          BasicEvent(type: namespace, content: {'tasks': []});
      expect(list.tasks, []);
      expect(list.tasks.length, 0);

      // Try with task list in account data
      client.accountData[namespace] = BasicEvent(type: namespace, content: {
        'tasks': [jsonTask, jsonTask]
      });
      list = TaskList(client);
      expect(list.tasks.length, 2);
      expect(list.tasks[1].title, title);
      await Future.delayed(Duration(milliseconds: 100));
      final jsonPayload = <String, dynamic>{
        'type': namespace,
        'content': {
          'tasks': [
            {'title': 'test1'},
            {'title': 'test2', 'description': 'desc2'},
            {'title': 'test3', 'done': true},
          ]
        }
      };
      final userUpdate = BasicEvent.fromJson(jsonPayload);
      final syncUpdate = SyncUpdate.fromJson({
        'account_data': {
          'events': [userUpdate.toJson()]
        }
      });
      expect(syncUpdate.accountData.first.toJson(), userUpdate.toJson());
      await client.handleSync(syncUpdate);

      await Future.delayed(Duration(milliseconds: 100));
      expect(updateCounter, 1);
      expect(list.tasks[0].title, 'test1');
      expect(list.tasks[1].title, 'test2');
      expect(list.tasks.length, 3);
    });
    test('create, change and remove', () async {
      var newTask = await list.create(title: title, notificationTitle: title);
      newTask = testTask;
      expect(newTask.title, title);
      await newTask.setTitle('newTitle');
      expect(newTask.title, 'newTitle');
      await newTask.setDescription('newDesc');
      expect(newTask.description, 'newDesc');
      final newDate = DateTime.now();
      await newTask.setDate(newDate);
      expect(newTask.date, newDate);
      expect(newTask.done, true);
      await newTask.setDone(false);
      expect(newTask.done, false);
      await newTask.remove();
    });
    test('Subtasks', () async {
      var newTask = await list.create(title: title, notificationTitle: title);
      newTask = testTask;
      final newSub = Subtask(title: title, parent: newTask);
      expect(Subtask.fromJson(newTask.toJson(), newTask).title, 'newTitle');

      await newTask.addSubtask(title);
      expect(newTask.subtask[0].title, newSub.title);
      expect(newSub.done, false);
      await newSub.setDone(true);
      expect(newSub.done, true);
      await newSub.setTitle('newTitle');
      expect(newSub.title, 'newTitle');
    });
  });
}
