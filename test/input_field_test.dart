/*import 'package:famedly/components/ChatRoom/input_field.dart';
import 'package:famedly/components/matrix.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/material.dart';
import 'package:test/test.dart' show expect;

import 'utils.dart';*/
import 'package:flutter_test/flutter_test.dart' hide expect;
import 'package:flutter_test/flutter_test.dart';

class Listeners {
  String message;
  bool attachmentClicked = false;
  bool audioClicked = false;
  bool cameraClicked = false;

  void onSend(String message) {
    this.message = message;
  }
}

void main() {
  /// All Tests related to the InputField
  group('InputField', () {
    /*NavigatorObserver mockObserver;

    setUp(() {
      mockObserver = MockNavigatorObserver();
    });

    /// Check if all Elements react to input
    testWidgets('should react on inputs', (WidgetTester tester) async {
      await tester.runAsync(() async {
        var _listeners = Listeners();

        await tester.pumpWidget(
          Utils.getWidgetWrapper(
            Builder(builder: (context) {
              return Scaffold(
                body: InputField(
                  Room(id: '1234', client: Famedly.of(context).client),
                  onSendText: _listeners.onSend,
                ),
              );
            }),
            mockObserver,
          ),
        );

        await tester.pump(Duration.zero);

        // Test Typing in Input
        await tester.enterText(find.byKey(Key('input')), 'Test');
        await tester.testTextInput.receiveAction(TextInputAction.done);
        await tester.pump(Duration.zero);

        expect(_listeners.message, 'Test');

        // Cleanup input Test
        await tester.enterText(find.byKey(Key('input')), '_'); // Reset Input
        await tester.testTextInput.receiveAction(TextInputAction.done);
        await tester.pump(Duration.zero);
        expect(_listeners.message, '_');

        /* Test if floatingActionButton Works
        1. Mic Action
        2. Submit
       */
        /* Reactivate when Audio Messages are possible

      await tester.tap(find.byType(FloatingActionButton));
      await tester.pump(Duration.zero);

      expect(_listeners.audioClicked, true);
      */

        await tester.enterText(find.byKey(Key('input')), 'Test2');
        await tester.tap(find.byKey(Key('sendButton')));
        await tester.pump(Duration.zero);

        expect(_listeners.message, 'Test2');

        // Test attachment button
        await tester.tap(find.byKey(Key('attachment')));
        await tester.pump(Duration.zero);

        //expect(_listeners.attachmentClicked, true);
      });
    });*/
  });
}
