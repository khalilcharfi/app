#!/bin/sh -ve
cd android
echo $PLAYSTORE_UPLOAD_KEY | base64 --decode --ignore-garbage > key.jks
echo "storePassword=${PLAYSTORE_UPLOAD_KEY_PASS}" >> key.properties
echo "keyPassword=${PLAYSTORE_UPLOAD_KEY_PASS}" >> key.properties
echo "keyAlias=key" >> key.properties
echo "storeFile=../key.jks" >> key.properties
cd fastlane
echo $PLAYSTORE_DEPLOY_KEY >> keys.json
cd ..
bundle install
bundle update fastlane
bundle exec fastlane set_build_code_internal
cd app
echo $FIREBASE_KEY | base64 --decode --ignore-garbage > google-services.json
cd ../..