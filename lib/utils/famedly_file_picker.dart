import 'package:famedly/utils/platform_extension.dart';
import 'package:file_picker_cross/file_picker_cross.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_lock/flutter_app_lock.dart';

abstract class FamedlyFilePicker {
  static Future<FilePickerCross> pickFiles({
    @required BuildContext context,
    FileTypeCross type = FileTypeCross.any,
  }) async {
    if (kIsMobile) AppLock.of(context).disable();
    FilePickerCross result;
    try {
      result = await FilePickerCross.importFromStorage(type: type);
    } catch (_) {
      return null;
    } finally {
      if (kIsMobile) AppLock.of(context).enable();
    }
    return result;
  }
}
