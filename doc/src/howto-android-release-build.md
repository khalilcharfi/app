Sometimes it's useful to build a non-debug version of the app locally. For this to work, you'll need a signing key.

#### Find or create the android debug keystore
The android debug signing key should live at `~/.android/debug.keystore`. If that file doesn't exist, you can create it with: 
```
$ cd ~/.android/debug.keystore && keytool -genkey -v -keystore debug.keystore -storepass android -alias androiddebugkey -keypass android -keyalg RSA -keysize 2048 -validity 10000
```

#### Make the app use this key:
Create `android/key.properties` with the following content:
```
keyAlias = androiddebugkey
keyPassword = android
storeFile = <absolute path to debug.keystore>
storePassword = android
```

#### Build the release version
Now running `flutter build apk` will generate a (debug) signed release version of the app.
