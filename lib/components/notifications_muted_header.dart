/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'package:famedly/styles/famedly_icons_v2_icons.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:flutter/material.dart';

import '../models/famedly.dart';

class NotificationsMutedHeader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<Object>(
        stream: Famedly.of(context)
            .client
            .onAccountData
            .stream
            .where((a) => a.type == 'm.push_rules'),
        builder: (context, snapshot) => Famedly.of(context)
                .client
                .allPushNotificationsMuted
            ? Container(
                padding: EdgeInsets.all(4),
                color: Theme.of(context).secondaryHeaderColor,
                child:
                    Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                  Icon(FamedlyIconsV2.off, size: 20),
                  SizedBox(width: 4),
                  Text(L10n.of(context).allNotificationsMuted),
                ]),
              )
            : Container());
  }
}
