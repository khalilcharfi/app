/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/material.dart';

import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:future_loading_dialog/future_loading_dialog.dart';

class UserSettingsPopupMenu extends StatelessWidget {
  final BuildContext context;
  final User user;

  const UserSettingsPopupMenu(this.context, this.user);

  @override
  Widget build(BuildContext context) => PopupMenuButton(
        key: Key('userSettingsPopupMenu'),
        itemBuilder: (context) => _itemList,
        onSelected: (result) => _onSelected(result),
      );

  /// Whether there are any actions in this popupmenu
  bool get hasActions => _itemList.isNotEmpty;

  void _onSelected(String result) async {
    switch (result) {
      case 'admin':
        if (OkCancelResult.ok ==
            await showOkCancelAlertDialog(
              context: context,
              title: L10n.of(context).askMakeAdmin(
                  user.calcDisplayname(mxidLocalPartFallback: false)),
              okLabel: L10n.of(context).yes,
              cancelLabel: L10n.of(context).cancel,
              useRootNavigator: false,
            )) {
          await showFutureLoadingDialog(
            context: context,
            future: () => user.setPower(100),
          );
        }
        break;
      case 'kick':
        if (OkCancelResult.ok ==
            await showOkCancelAlertDialog(
              context: context,
              title: L10n.of(context).askKickUser(
                  user.calcDisplayname(mxidLocalPartFallback: false)),
              okLabel: L10n.of(context).yes,
              cancelLabel: L10n.of(context).cancel,
              useRootNavigator: false,
            )) {
          await showFutureLoadingDialog(
            context: context,
            future: () => user.kick(),
          );
        }
        break;
    }
  }

  List<PopupMenuItem<String>> get _itemList {
    final list = <PopupMenuItem<String>>[];
    if (user.canChangePowerLevel) {
      list.add(PopupMenuItem<String>(
        key: Key('UserSettingsPopupMenuAdminButton'),
        value: 'admin',
        child: Text(L10n.of(context).makeAnAdmin),
      ));
    }
    if (user.canKick &&
        user.membership != Membership.leave &&
        user.membership != Membership.ban) {
      list.add(PopupMenuItem<String>(
        key: Key('UserSettingsPopupMenuKickButton'),
        value: 'kick',
        child: Text(L10n.of(context).kick),
      ));
    }
    return list;
  }

  void show() async {
    final list = _itemList;
    if (list.isEmpty) return;

    final RenderBox bar = context.findRenderObject();
    final RenderBox overlay = Overlay.of(context).context.findRenderObject();
    final position = RelativeRect.fromRect(
      Rect.fromPoints(
        bar.localToGlobal(bar.size.bottomLeft(Offset.zero), ancestor: overlay),
        bar.localToGlobal(bar.size.bottomRight(Offset.zero), ancestor: overlay),
      ),
      Offset.zero & overlay.size,
    );

    final result = await showMenu<String>(
      context: context,
      position: position,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15),
      ),
      items: list,
    );
    _onSelected(result);
  }
}
