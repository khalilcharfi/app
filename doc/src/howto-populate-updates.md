In this repository: https://gitlab.com/famedly/services/app-min-update-version we publish the minimal version the user must have installed. The app checks on every start, if it is outdated. If the user has an outdated version, a dialog will inform the user and lead him to the AppStore/PlayStore to update the app.

To increase the minimal version, just edit the [min-update-versions.json](https://gitlab.com/famedly/services/app-min-update-version/-/blob/master/min-update-versions.json) to the version the user must have installed at least.
