/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'package:famedly/models/famedly.dart';
import 'package:famedly/models/requests/request.dart';
import 'package:famedly/views/requests/requests_list_page_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';

/// This wraps the Tab view into a view.
/// It includes the AppBar as well as the Tab View and the Fab Menu

class RequestsListPage extends StatefulWidget {
  final String type;
  final String activeChatId;

  RequestsListPage({
    Key key,
    this.type,
    this.activeChatId,
  }) : super(key: key);

  @override
  RequestsListPageController createState() => RequestsListPageController();
}

class RequestsListPageController extends State<RequestsListPage> {
  @override
  void setState(fn) {
    if (!mounted) return;
    super.setState(fn);
  }

  final filterTextController = TextEditingController();
  Future<bool> waitForFirstSync() async {
    final famedly = Famedly.of(context);
    if (famedly.client.prevBatch != null) return true;
    return famedly.client.onFirstSync.stream.first;
  }

  String getTitle() {
    String title;
    title = {
      'conversations': L10n.of(context).myConversations,
      'assigned': L10n.of(context).myRequests,
      'shared': L10n.of(context).sharedWithMe,
      'broadcasts': L10n.of(context).pendingBroadcasts,
      'incoming': L10n.of(context).incomming,
    }[widget.type];
    title ??= 'Unknown';

    return title;
  }

  Function getBundleFilter() {
    Function bundleFilter;
    bundleFilter = {
      'assigned': (RequestBundle bundle) =>
          !bundle.isOwnBroadcast() && bundle.isAdmin(),
      'shared': (RequestBundle bundle) =>
          !bundle.isOwnBroadcast() && !bundle.isAdmin(),
      'broadcasts': (RequestBundle bundle) => bundle.isOwnBroadcast(),
      // Hard coded until it is clear how to handle this
      'incoming': (RequestBundle bundle) => !bundle.isOwn(),
    }[widget.type];
    bundleFilter ??= (RequestBundle bundle) => true;

    return bundleFilter;
  }

  @override
  Widget build(BuildContext context) {
    return RequestsListPageView(this);
  }
}
