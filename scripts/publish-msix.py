import boto3
import logging
import os
from botocore.exceptions import ClientError


def upload_file(file_name, object_name=None, mime_type=None):
    """Upload a file to an S3 bucket

    :param mime_type: Type of file
    :param file_name: File to upload
    :param object_name: S3 object name. If not specified then file_name is used
    :return: True if file was uploaded, else False
    """

    # If S3 object_name was not specified, use file_name
    if object_name is None:
        object_name = file_name

    # Upload the file
    s3_client = boto3.client('s3')
    try:
        response = s3_client.upload_file(file_name, "famedly-windows", object_name,ExtraArgs={'ContentType': mime_type})
    except ClientError as e:
        logging.error(e)
        return False
    return True


if __name__ == '__main__':
    logging.info("Upload msix and appinstaller")
    upload_file(os.path.abspath("./build/windows/runner/Release/famedly.msix"), "famedly.msix", "application/msix")
    upload_file(os.path.abspath("./famedly.appinstaller"), "famedly.appinstaller", "application/appinstaller")
