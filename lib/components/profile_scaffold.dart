/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'package:famedly/styles/famedly_icons_v2_icons.dart';
import 'package:flutter/material.dart';
import 'package:adaptive_page_layout/adaptive_page_layout.dart';

import '../styles/colors.dart';
import 'avatar.dart';
import 'famedly_icon_button.dart';

typedef AvatarActionFunction = void Function();

class ProfileScaffold extends StatelessWidget {
  @override
  final Key key;
  final Widget title;
  final List<Widget> actions;
  final Widget body;
  final Uri profileContent;
  final String profileName;
  final List<Widget> buttons;
  final AvatarActionFunction onAvatarTap;
  final Color backgroundColor;
  final bool automaticallyImplyLeading;
  ProfileScaffold(
      {this.key,
      this.title,
      this.actions,
      this.body,
      this.profileContent,
      this.profileName,
      this.onAvatarTap,
      this.backgroundColor,
      this.automaticallyImplyLeading = true,
      this.buttons = const []});

  @override
  Widget build(BuildContext context) {
    final backgroundColor =
        this.backgroundColor ?? Theme.of(context).secondaryHeaderColor;
    return Scaffold(
        key: key,
        backgroundColor: backgroundColor,
        appBar: AppBar(
          leading: automaticallyImplyLeading
              ? AdaptivePageLayout.of(context).threeColumnMode(context)
                  ? IconButton(
                      icon: Icon(FamedlyIconsV2.close),
                      onPressed: AdaptivePageLayout.of(context).pop,
                    )
                  : BackButton()
              : null,
          actions: actions,
          title: title,
          elevation: 0,
          automaticallyImplyLeading: automaticallyImplyLeading,
        ),
        body: ListView(
          children: <Widget>[
            Container(
              height: 160,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  stops: [0, 0.5, 0.5, 1],
                  colors: [
                    Theme.of(context).appBarTheme.color,
                    Theme.of(context).appBarTheme.color,
                    backgroundColor,
                    backgroundColor,
                  ],
                ),
              ),
              child: Center(
                child: Container(
                  height: 160,
                  width: 160,
                  child: Stack(
                    children: <Widget>[
                      Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: Theme.of(context).backgroundColor,
                            boxShadow: [
                              BoxShadow(
                                  color: FamedlyColors.slate.withOpacity(0.1),
                                  offset: Offset(0, 0),
                                  blurRadius: 10,
                                  spreadRadius: 1),
                              BoxShadow(
                                  color: FamedlyColors.slate.withOpacity(0.1),
                                  offset: Offset(0, 1),
                                  blurRadius: 1,
                                  spreadRadius: 0),
                              BoxShadow(
                                  color: FamedlyColors.slate.withOpacity(0.1),
                                  offset: Offset(0, 2),
                                  blurRadius: 10,
                                  spreadRadius: 0),
                            ],
                            shape: BoxShape.circle,
                          ),
                          height: 130,
                          width: 130,
                          child: Center(
                            child: InkWell(
                              borderRadius: BorderRadius.circular(124),
                              child: Avatar(
                                profileContent,
                                key: Key('Avatar'),
                                size: 124,
                                name: profileName,
                                textSize: 30,
                              ),
                            ),
                          ),
                        ),
                      ),
                      onAvatarTap == null
                          ? Container()
                          : Positioned(
                              top: 62,
                              right: 0,
                              child: Material(
                                elevation: 2,
                                color: Theme.of(context).backgroundColor,
                                borderRadius: BorderRadius.circular(35),
                                child: InkWell(
                                  onTap: onAvatarTap,
                                  child: Container(
                                      width: 35,
                                      height: 35,
                                      child: Icon(FamedlyIconsV2.add,
                                          color: FamedlyColors.cloudyBlue)),
                                ),
                              ))
                    ],
                  ),
                ),
              ),
            ),
            buttons.isNotEmpty
                ? Center(
                    child: Padding(
                      padding: EdgeInsets.only(
                        top: 15,
                        left: 32,
                        right: 32,
                        bottom: 30,
                      ),
                      child: Wrap(
                        alignment: WrapAlignment.spaceAround,
                        children: buttons
                            .map((b) => Padding(
                                padding: EdgeInsets.symmetric(horizontal: 16),
                                child: b))
                            .toList(),
                      ),
                    ),
                  )
                : Container(),
            body ?? Container()
          ],
        ));
  }
}

class ProfileScaffoldButton extends StatelessWidget {
  final IconData iconData;
  final onTap;

  ProfileScaffoldButton({Key key, this.iconData, this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FamedlyIconButton(
      size: 60,
      iconSize: 24,
      backgroundColor: Theme.of(context).backgroundColor,
      iconData: iconData,
      onTap: onTap,
    );
  }
}
