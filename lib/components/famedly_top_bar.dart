/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'package:famedly/styles/colors.dart';
import 'package:famedly/styles/famedly_icons_v2_icons.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/material.dart';
import 'package:adaptive_page_layout/adaptive_page_layout.dart';
import 'main_header_buttons.dart';
import '../models/famedly.dart';
import 'nav_scaffold.dart';

class FamedlyTopBar extends StatelessWidget {
  static const Color dark = FamedlyColors.darkerGrey;
  static const Color light = FamedlyColors.charcoalGrey;
  static const Color text = FamedlyColors.cloudyBlue;
  static const Color textActive = FamedlyColors.azul;

  static const double height = 76;

  @override
  Widget build(BuildContext context) {
    var activePage = MainPage.chats;
    var settingsButton = light;
    var settingsHoverButton = dark;
    var settingsButtonTextColor = text;
    final viewData = AdaptivePageLayout.of(context).currentViewData;

    if (viewData.routeName.startsWith('/requests') ||
        viewData.routeName.startsWith('/room/requests')) {
      activePage = MainPage.requests;
    } else if (viewData.routeName.startsWith('/tasks')) {
      activePage = MainPage.tasks;
    } else if (viewData.routeName.startsWith('/directory')) {
      activePage = MainPage.directory;
    }

    if (viewData.routeName.startsWith('/settings')) {
      activePage = MainPage.settings;
      settingsButton = dark;
      settingsHoverButton = light;
      settingsButtonTextColor = textActive;
    }
    final famedly = Famedly.of(context);
    return AppBar(
      titleSpacing: 0,
      brightness: Brightness.dark,
      backgroundColor: dark,
      automaticallyImplyLeading: false,
      toolbarHeight: height,
      elevation: 0,
      title: Material(
        color: light,
        child: Container(
          height: height,
          decoration: BoxDecoration(
            border: Border(
              bottom: BorderSide(color: Theme.of(context).dividerColor),
            ),
          ),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              MainHeaderButtons(
                width: FamedlyStyles.columnWidth / 2,
                activePage: activePage,
              ),
              Spacer(),
              // FIXME factor out this element to allow having that status circle
              Padding(
                padding: EdgeInsets.only(right: 8),
                child: Container(
                  width: 115,
                  child: FutureBuilder(
                    future: famedly.client.ownProfile,
                    builder: (BuildContext context,
                        AsyncSnapshot<Profile> snapshot) {
                      if (snapshot.hasData) {
                        final profile = snapshot.data;
                        return Material(
                          type: MaterialType.transparency,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                profile.displayname,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                  color: Colors.white,
                                ),
                              ),
                              Text(
                                famedly.client.homeserver
                                    .toString()
                                    .replaceAll('https://', ''),
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                  color: FamedlyColors.blueyGrey,
                                ),
                              ),
                            ],
                          ),
                        );
                      } else {
                        return Container(
                          color: Colors.transparent,
                        );
                      }
                    },
                  ),
                ),
              ),
              TextButton(
                style: ButtonStyle(
                  shape: MaterialStateProperty.resolveWith(
                      (_) => RoundedRectangleBorder()),
                  backgroundColor: MaterialStateProperty.resolveWith((states) {
                    if (states.contains(MaterialState.hovered)) {
                      return settingsHoverButton;
                    }
                    if (states.isEmpty) return settingsButton;
                    return null;
                  }),
                ),
                onPressed: () =>
                    AdaptivePageLayout.of(context).pushNamed('/settings'),
                child: Icon(
                  FamedlyIconsV2.settings,
                  color: settingsButtonTextColor,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
