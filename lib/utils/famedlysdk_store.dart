/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'dart:async';
import 'dart:convert';
import 'dart:core';

import 'package:famedly/utils/platform_extension.dart';
import 'package:famedly/views/enter_pass_code_view.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:localstorage/localstorage.dart';
import 'package:moor/backends.dart';
import 'package:random_string/random_string.dart';

import 'database/shared.dart';

Future<void> destroyDatabase([Client client]) async {
  while (_generateDatabaseLock) {
    await Future.delayed(Duration(milliseconds: 50));
  }
  _generateDatabaseLock = true;
  try {
    await deleteDb(filename: _dbFilename);
  } finally {
    _generateDatabaseLock = false;
  }
}

Future<Database> getDatabase([Client client, bool important = false]) async {
  while (_generateDatabaseLock) {
    await Future.delayed(Duration(milliseconds: 50));
  }
  _generateDatabaseLock = true;
  try {
    if (_db != null) {
      Logs().v('[Moor] Attemtping to use cached database object...');
      try {
        if (!(_db.executor is DelegatedDatabase) ||
            (_db.executor as DelegatedDatabase).delegate.isOpen) {
          await _db.customSelect('SELECT 1').get();
          Logs().v('[Moor] using cached database object...');
          _important |= important;
          return _db;
        }
      } catch (_) {
        // do nothing, re-build new db
      }
      if (_important && !important) {
        throw 'Database is important but defunct';
      }
      await _db.close(); // close the old instance
    }
    _important |= important;
    Logs().v('[Moor] generating new database object...');
    final store = Store();
    var password = await store.getItem('database-password');
    var newPassword = false;
    if (password == null || password.isEmpty) {
      newPassword = true;
      password = randomString(255);
    }
    _db = await constructDb(
      logStatements: false,
      filename: _dbFilename,
      password: password,
    );
    if (newPassword) {
      await store.setItem('database-password', password);
    }
    return _db;
  } finally {
    _generateDatabaseLock = false;
  }
}

const _dbFilename = 'moor.sqlite';
Database _db;
bool _important = false;
bool _generateDatabaseLock = false;

// see https://github.com/mogol/flutter_secure_storage/issues/161#issuecomment-704578453
class AsyncMutex {
  Completer<void> _completer;

  Future<void> lock() async {
    while (_completer != null) {
      await _completer.future;
    }

    _completer = Completer<void>();
  }

  void unlock() {
    assert(_completer != null);
    final completer = _completer;
    _completer = null;
    completer.complete();
  }
}

class Store {
  final LocalStorage storage;
  final FlutterSecureStorage secureStorage;
  static final _mutex = AsyncMutex();
  static bool isWidgetTest = false;
  static final Map<String, dynamic> _widgetTestCache = {
    EnterPassCodeView.appLockNamespace: '1234',
    EnterPassCodeView.appLockBiometricsNamespace: false,
  };

  Store()
      : storage = LocalStorage('LocalStorage'),
        secureStorage = !kIsMobile ? null : FlutterSecureStorage();

  Future<dynamic> getItem(String key) async {
    if (isWidgetTest) {
      return _widgetTestCache[key];
    }
    if (!kIsMobile) {
      await storage.ready;
      try {
        return await storage.getItem(key);
      } catch (_) {
        return null;
      }
    }
    try {
      await _mutex.lock();
      return await secureStorage.read(key: key);
    } catch (_) {
      return null;
    } finally {
      _mutex.unlock();
    }
  }

  Future<void> setItem(String key, String value) async {
    if (isWidgetTest) {
      _widgetTestCache[key] = value;
      return;
    }
    if (!kIsMobile) {
      await storage.ready;
      return await storage.setItem(key, value);
    }
    if (value == null) {
      return await secureStorage.delete(key: key);
    } else {
      try {
        await _mutex.lock();
        return await secureStorage.write(key: key, value: value);
      } finally {
        _mutex.unlock();
      }
    }
  }

  Future<Map<String, dynamic>> getAllItems() async {
    if (isWidgetTest) {
      return _widgetTestCache;
    }
    if (!kIsMobile) {
      try {
        final rawStorage = await getLocalstorage('LocalStorage');
        return json.decode(rawStorage);
      } catch (_) {
        return {};
      }
    }
    try {
      await _mutex.lock();
      return await secureStorage.readAll();
    } catch (_) {
      return {};
    } finally {
      _mutex.unlock();
    }
  }
}
