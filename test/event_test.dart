import 'package:famedly/components/chat_room/event_widgets/event_widget.dart';
import 'package:famedly/components/chat_room/event_widgets/file_event_content.dart';
import 'package:famedly/components/chat_room/event_widgets/image_event_content.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'utils.dart';

void main() {
  /// All Tests related to the Events

  group(
    'Test all events',
    () {
      final client = Utils.defaultClient;

      NavigatorObserver mockObserver;

      setUp(() {
        mockObserver = MockNavigatorObserver();
      });

      /// File Event Tests
      testWidgets(
        'FileEvent',
        (WidgetTester tester) async {
          await tester.runAsync(() async {
            final room = Room(id: '1234', client: await client);
            final event = Event(
              eventId: '1234',
              senderId: '@alice:example.com',
              originServerTs: DateTime.now(),
              room: room,
              type: 'm.room.message',
              content: {
                'msgtype': 'm.file',
                'body': 'test',
                'filename': 'Testfile',
                'url': 'example.org/1234'
              },
            );

            await tester.pumpWidget(
              await Utils.getWidgetWrapper(
                Scaffold(
                  body: EventWidget(
                    event,
                    room: room,
                    key: Key('TestEvent'),
                    content: FileEventContent(event),
                  ),
                ),
                mockObserver,
              ),
            );

            await tester.pump(Duration.zero);

            // Is in a message bubble?
            expect(find.byKey(Key('MessageBubble')), findsOneWidget);
            expect(find.text('Testfile'), findsOneWidget);
          });
        },
      );

      /// Image Event Tests
      testWidgets(
        'ImageEvent',
        (WidgetTester tester) async {
          await tester.runAsync(() async {
            final room = Room(id: '1234', client: await client);
            final dynamic event = Event(
              eventId: '1234',
              senderId: '@alice:example.com',
              originServerTs: DateTime.now(),
              room: room,
              type: 'm.room.message',
              content: {
                'msgtype': 'm.image',
                'body': 'test',
                'url': 'example.org/1234'
              },
            );

            await tester.pumpWidget(
              await Utils.getWidgetWrapper(
                Scaffold(
                  body: EventWidget(
                    event,
                    room: room,
                    key: Key('TestEvent'),
                    content: ImageEventContent(event),
                  ),
                ),
                mockObserver,
              ),
            );

            await tester.pump(Duration.zero);

            // Is in a message bubble?
            expect(find.byKey(Key('MessageBubble')), findsOneWidget);
          });
        },
      );
    },
  );
}
