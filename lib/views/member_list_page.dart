/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'dart:async';
import 'dart:io';

import 'package:famedly/components/settings_list_group.dart';
import 'package:famedly/styles/famedly_icons_v2_icons.dart';
import 'package:famedly/utils/matrix_locals.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:adaptive_page_layout/adaptive_page_layout.dart';

import '../models/famedly.dart';
import '../components/member_list_item.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import '../utils/room_status_extension.dart';
import '../styles/colors.dart';

enum Answers { yes, no }

class MemberListPage extends StatefulWidget {
  final String id;

  const MemberListPage(
    this.id, {
    Key key,
  }) : super(key: key);

  @override
  MemberListPageState createState() => MemberListPageState();
}

class MemberListPageState extends State<MemberListPage> {
  bool remoteParticipants = false;

  Future<List<User>> loadParticipants(Room room) async {
    final participants = room.getParticipants();
    if (participants
            .where((User user) =>
                ![Membership.join, Membership.invite].contains(user.membership))
            .length ==
        room.mJoinedMemberCount + room.mInvitedMemberCount) return participants;
    return room.requestParticipants();
  }

  Future<Room> loadRoom(Famedly famedly) async {
    final room = famedly.client.getRoomById(widget.id);
    return room;
  }

  @override
  Widget build(BuildContext context) {
    final famedly = Famedly.of(context);
    final room = famedly.client.getRoomById(widget.id);

    if (room == null) {
      return Scaffold(
        appBar: AppBar(
          leading: BackButton(),
          title: Text(L10n.of(context).notConnected),
        ),
        body: Center(
          child: Icon(FamedlyIconsV2.info),
        ),
      );
    }
    return StreamBuilder<Object>(
        stream: famedly.client.onEvent.stream.where((e) =>
            e.roomID == widget.id &&
            ['m.room.member', 'm.room.power_levels']
                .contains(e.content['type'])),
        builder: (context, snapshot) {
          return Scaffold(
            key: Key('MemberListPage'),
            backgroundColor: Theme.of(context).secondaryHeaderColor,
            appBar: AppBar(
              leading: BackButton(),
              title: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: kIsWeb || Platform.isAndroid
                    ? CrossAxisAlignment.start
                    : CrossAxisAlignment.center,
                children: <Widget>[
                  Text(L10n.of(context).members),
                  Text(
                    room.getLocalizedFamedlyDisplayname(
                        MatrixLocals(L10n.of(context))),
                    style: TextStyle(
                      color: FamedlyColors.blueyGrey,
                      fontSize: 12,
                    ),
                  ),
                ],
              ),
              actions: <Widget>[
                IconButton(
                  key: Key('GoToInvitePage'),
                  icon: Icon(FamedlyIconsV2.add),
                  color: FamedlyColors.azul,
                  onPressed: () {
                    AdaptivePageLayout.of(context)
                        .pushNamed('/room/${widget.id}/invite');
                  },
                )
              ],
            ),
            body: FutureBuilder<List<User>>(
              future: loadParticipants(room),
              builder:
                  (BuildContext context, AsyncSnapshot<List<User>> snapshot) {
                if (snapshot.hasError) {
                  Logs().e('Error while loading participants', snapshot.error);
                }
                if (!snapshot.hasData) {
                  return Container(
                      color: Theme.of(context).backgroundColor,
                      child: Center(
                        child: CircularProgressIndicator(),
                      ));
                }

                final participants = snapshot.data;
                participants.removeWhere(
                    (User user) => user.membership == Membership.leave);
                participants.sort((User a, User b) {
                  if (a.powerLevel > b.powerLevel) {
                    return -1;
                  } else if (a.powerLevel == b.powerLevel) {
                    return 0;
                  } else {
                    return 1;
                  }
                });
                return SettingsListGroup(
                  children: [
                    Expanded(
                      child: ListView.separated(
                        separatorBuilder: (context, i) => Divider(
                          height: 1,
                          thickness: 1,
                          color: Theme.of(context).secondaryHeaderColor,
                        ),
                        itemCount: participants.length,
                        itemBuilder: (context, index) =>
                            MemberListItem(participants[index]),
                      ),
                    )
                  ],
                );
              },
            ),
          );
        });
  }
}
