/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'dart:async';

import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:famedly/components/settings_list_group.dart';
import 'package:famedly/models/plugins/voip_plugin.dart';
import 'package:famedly/styles/famedly_icons_v2_icons.dart';
import 'package:famedly/utils/famedly_file_picker.dart';
import 'package:famedly/utils/matrix_locals.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:file_picker_cross/file_picker_cross.dart';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:adaptive_page_layout/adaptive_page_layout.dart';
import 'package:future_loading_dialog/future_loading_dialog.dart';

import '../utils/room_archive_dialog_extension.dart';
import '../models/famedly.dart';
import '../components/profile_scaffold.dart';
import '../components/settings_menu_item.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import '../utils/room_status_extension.dart';
import '../models/room_type_extension.dart';
import '../styles/colors.dart';
import '../styles/famedly_icons_icons.dart';
import '../utils/matrix_file_thumbnail_extension.dart';

class RoomSettings extends StatefulWidget {
  final String id;

  RoomSettings({Key key, this.id}) : super(key: key);

  @override
  RoomSettingsState createState() => RoomSettingsState();
}

class RoomSettingsState extends State<RoomSettings> {
  bool remoteParticipants = false;

  Room room;

  void setChatNameDialog(BuildContext outerContext, Room room) async {
    final input = await showTextInputDialog(
      context: context,
      title: L10n.of(context).setChatName,
      useRootNavigator: false,
      textFields: [
        DialogTextField(
          initialText: room.name,
          minLines: 1,
          maxLines: 4,
          hintText: L10n.of(context).setChatName,
        )
      ],
      okLabel: L10n.of(context).set,
      cancelLabel: L10n.of(context).cancel,
    );
    if (input == null) return;
    await showFutureLoadingDialog(
        context: context, future: () => room.setName(input.single));
  }

  void setChatTopicDialog(BuildContext context, Room room) async {
    final input = await showTextInputDialog(
      context: context,
      title: L10n.of(context).setChatDescription,
      useRootNavigator: false,
      textFields: [
        DialogTextField(
          initialText: room.topic,
          minLines: 1,
          maxLines: 4,
          hintText: L10n.of(context).setChatDescription,
        )
      ],
      okLabel: L10n.of(context).set,
      cancelLabel: L10n.of(context).cancel,
    );
    if (input == null) return;
    await showFutureLoadingDialog(
        context: context, future: () => room.setDescription(input.single));
  }

  Future<List<User>> loadParticipants(Room room) async {
    if (remoteParticipants) {
      return await room.requestParticipants();
    } else {
      return room.getParticipants();
    }
  }

  @override
  Widget build(BuildContext context) {
    final famedly = Famedly.of(context);

    room ??= famedly.client.getRoomById(widget.id);

    if (room == null) {
      return Scaffold(
        appBar: AppBar(
          leading: BackButton(),
          title: Text(L10n.of(context).notConnected),
        ),
        body: Center(
          child: Icon(FamedlyIconsV2.info),
        ),
      );
    }

    return StreamBuilder<Object>(
        stream: room.onUpdate.stream,
        builder: (context, snapshot) {
          return ProfileScaffold(
            key: Key('RoomSettings'),
            title: Text(room.getLocalizedFamedlyDisplayname(
                MatrixLocals(L10n.of(context)))),
            profileContent: room.avatar,
            profileName: room
                .getLocalizedFamedlyDisplayname(MatrixLocals(L10n.of(context))),
            onAvatarTap: !room.canSendEvent('m.room.avatar')
                ? null
                : () async {
                    MatrixFile file;
                    final tempFile = await FamedlyFilePicker.pickFiles(
                      type: FileTypeCross.image,
                      context: context,
                    );
                    if (tempFile == null) return;
                    try {
                      file = await MatrixImageFile(
                        bytes: tempFile.toUint8List(),
                        name: tempFile.path,
                      ).createThumbnail();
                    } catch (e, s) {
                      Logs().d('Could not create thumbnail for avatar', e, s);
                      file = MatrixImageFile(
                        bytes: tempFile.toUint8List(),
                        name: tempFile.path,
                      );
                    }
                    await showFutureLoadingDialog(
                        context: context, future: () => room.setAvatar(file));
                  },
            actions: <Widget>[
              (!room.canSendEvent('m.room.name') ||
                      room.type == roomTypePatient)
                  ? Container()
                  : PopupMenuButton(
                      key: Key('PopupMenu'),
                      onSelected: (result) async {
                        switch (result) {
                          case 'editChatName':
                            setChatNameDialog(context, room);
                            break;
                          case 'editChatTopic':
                            setChatTopicDialog(context, room);
                            break;
                        }
                      },
                      icon: Icon(
                        FamedlyIconsV2.more,
                        color: Colors.black87,
                      ),
                      itemBuilder: (BuildContext context) => <PopupMenuEntry>[
                        PopupMenuItem(
                          key: Key('EditChatNamePopupMenuItem'),
                          value: 'editChatName',
                          child: Text(L10n.of(context).setChatName),
                        ),
                        PopupMenuItem(
                          key: Key('EditChatTopicPopupMenuItem'),
                          value: 'editChatTopic',
                          child: Text(L10n.of(context).setChatDescription),
                        ),
                      ],
                    ),
            ],
            buttons: [
              ProfileScaffoldButton(
                key: Key('ChatProfileScaffoldButton'),
                iconData: FamedlyIconsV2.chats,
                onTap: () {
                  AdaptivePageLayout.of(context).pop();
                },
              ),
              Builder(builder: (context) {
                return ProfileScaffoldButton(
                  key: Key('CallProfileScaffoldVideoButton'),
                  iconData: FamedlyIconsV2.phone,
                  onTap: () async {
                    final success = await showFutureLoadingDialog(
                        context: context,
                        future: () =>
                            Famedly.of(context).client.getTurnServer());
                    if (success.result != null) {
                      final voip = Famedly.of(context).voipPlugin;
                      await voip
                          .inviteToCall(room.id, CallType.kVideo)
                          .catchError((e) {
                        AdaptivePageLayout.of(context).showSnackBar(
                          SnackBar(
                            content: Text(e.toLocalizedString(context)),
                          ),
                        );
                      });
                    } else {
                      await showOkAlertDialog(
                        context: context,
                        title: L10n.of(context)
                            .thisFeatureHasNotBeenImplementedYet,
                        okLabel: L10n.of(context).next,
                        useRootNavigator: false,
                      );
                    }
                  },
                );
              }),
              Builder(builder: (context) {
                return ProfileScaffoldButton(
                    key: room.pushRuleState == PushRuleState.notify
                        ? Key('NotificationOnProfileScaffoldButton')
                        : Key('NotificationOffProfileScaffoldButton'),
                    iconData: room.pushRuleState == PushRuleState.notify
                        ? FamedlyIconsV2.on
                        : FamedlyIconsV2.off,
                    onTap: () async {
                      var newState = PushRuleState.notify;
                      if (room.pushRuleState == PushRuleState.notify) {
                        newState = PushRuleState.mentionsOnly;
                      }
                      final Future pushRuleUpdate = room
                          .client.onAccountData.stream
                          .where((u) => u.type == 'm.push_rules')
                          .first;
                      final success = await showFutureLoadingDialog(
                          context: context,
                          future: () => room.setPushRuleState(newState));
                      if (success.error != null) return;
                      await pushRuleUpdate;
                      setState(() {});
                    });
              })
            ],
            body: Column(
              children: <Widget>[
                SettingsListGroup(
                  children: [
                    SettingsMenuItem(
                      Icon(
                        FamedlyIcons.participants,
                        color: FamedlyColors.blueyGrey,
                      ),
                      L10n.of(context).members,
                      Text(
                        ((room?.mJoinedMemberCount ?? 0) +
                                (room?.mInvitedMemberCount ?? 0))
                            .toString(),
                        style: TextStyle(
                          color: FamedlyColors.blueyGrey,
                          fontSize: 15,
                          fontWeight: FontWeight.w400,
                          fontStyle: FontStyle.normal,
                          letterSpacing: 0,
                        ),
                      ),
                      () {
                        AdaptivePageLayout.of(context)
                            .pushNamed('/room/${widget.id}/members');
                      },
                      key: Key('GoToMembersPage'),
                    )
                  ],
                ),
                Builder(builder: (context) {
                  return SettingsListGroup(
                    title: L10n.of(context).media,
                    children: <Widget>[
                      SettingsMenuItem(
                        Icon(
                          FamedlyIcons.camera,
                          color: FamedlyColors.blueyGrey,
                        ),
                        L10n.of(context).images,
                        Container(),
                        () => AdaptivePageLayout.of(context)
                            .pushNamed('/room/${widget.id}/media/images'),
                      ),
                      SettingsMenuItem(
                        Icon(
                          FamedlyIcons.video,
                          color: FamedlyColors.blueyGrey,
                        ),
                        L10n.of(context).videos,
                        Container(),
                        () => AdaptivePageLayout.of(context)
                            .pushNamed('/room/${widget.id}/media/videos'),
                      ),
                      SettingsMenuItem(
                        Icon(
                          FamedlyIcons.document,
                          color: FamedlyColors.blueyGrey,
                        ),
                        L10n.of(context).files,
                        Container(),
                        () => AdaptivePageLayout.of(context)
                            .pushNamed('/room/${widget.id}/media/documents'),
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                            right: MediaQuery.of(context).size.width - 60),
                        child: Container(
                          height: 1,
                          color: Theme.of(context).backgroundColor,
                        ),
                      ),
                    ],
                  );
                }),
                Align(
                  alignment: Alignment.topRight,
                  child: Padding(
                    padding: EdgeInsets.only(right: 16, top: 20),
                    child: TextButton.icon(
                      onPressed: () {
                        room.showArchiveDialog(context);
                      },
                      icon: Icon(
                        FamedlyIconsV2.close,
                        color: FamedlyColors.grapefruit,
                      ),
                      label: Text(
                        L10n.of(context).leaveGroup,
                        style: TextStyle(
                          color: FamedlyColors.grapefruit,
                          fontSize: 15,
                          fontWeight: FontWeight.w500,
                          fontStyle: FontStyle.normal,
                          letterSpacing: 0,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          );
        });
  }
}
