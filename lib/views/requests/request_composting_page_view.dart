/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'dart:math';
import 'package:famedly/views/requests/request_composting_page.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:famedly/styles/colors.dart';
import 'package:famedly/styles/famedly_icons_v2_icons.dart';
import 'package:famedly/utils/error_reporter.dart';
import 'package:famedly/components/draggable_scrollbar.dart';
import 'package:famedly/components/questionnaire_items_buildert.dart';
import 'package:fhir/r4.dart' as fhir;
import 'package:flutter_gen/gen_l10n/l10n.dart';

// On this page we are currently just testing the UX of a specific use case. Thats
// why nothig here is localized. If the customers like this, we will specify a
// specific json format for this forms.
class RequestCompositingPageView extends StatelessWidget {
  final RequestCompositingPageController controller;

  const RequestCompositingPageView(this.controller, {Key key})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    controller.getTitle();
    return FutureBuilder<fhir.Questionnaire>(
        future: controller.formManager
            .loadForm(controller.widget.formName)
            .catchError((e, s) {
          ErrorReporter.reportError(e, s);
        }),
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return Center(child: CircularProgressIndicator());
          }
          final questionnaire = snapshot.data;
          return Scaffold(
            appBar: AppBar(
              leading: BackButton(),
              title: ListTile(
                contentPadding: EdgeInsets.zero,
                title: Text(
                  questionnaire.title ?? questionnaire.name,
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                ),
                subtitle: Text(
                  controller.titleText,
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
            ),
            body: FamedlyDraggableScrollbar(
              controller: controller.widget.scrollController,
              isAlwaysShown: kIsWeb,
              child: ListView(
                controller: controller.widget.scrollController,
                padding: EdgeInsets.symmetric(
                  vertical: 16.0,
                  horizontal:
                      max(16, (MediaQuery.of(context).size.width - 400) / 2),
                ),
                children: <Widget>[
                  if (questionnaire.description?.toString()?.isNotEmpty ??
                      false) ...{
                    Text(questionnaire.description.toString()),
                    Divider(height: 16, thickness: 1),
                  },
                  Form(
                      key: controller.form,
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      child: Column(
                        children: <Widget>[
                          controller.isExternal
                              ? Text(L10n.of(context).externalInquiryWarning,
                                  style: TextStyle(
                                    color: FamedlyColors.grapefruit,
                                    fontWeight: FontWeight.w600,
                                  ))
                              : Container(),
                          QuestionnaireItemsBuilder(
                            items: questionnaire.item,
                            onChanged: (k, v) => controller.inputs[k] = v,
                          ),
                          ListTile(
                            onTap: () => controller.addAttachmentOnTap(),
                            leading: controller.attachment == null
                                ? Icon(
                                    FamedlyIconsV2.attachment,
                                    color: FamedlyColors.slate,
                                  )
                                : Icon(
                                    FamedlyIconsV2.close,
                                    color: FamedlyColors.grapefruit,
                                  ),
                            title: Text(
                              controller.attachment == null
                                  ? L10n.of(context).addAttachment
                                  : L10n.of(context).attachmentAdded,
                            ),
                          ),
                          SizedBox(height: 16),
                          Container(
                            width: double.infinity,
                            child: ElevatedButton(
                              onPressed: () =>
                                  controller.onPressedRequests(questionnaire),
                              child: Text(L10n.of(context).requests),
                            ),
                          ),
                        ],
                      )),
                ],
              ),
            ),
          );
        });
  }
}
