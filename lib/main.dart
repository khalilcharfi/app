/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'dart:async';

import 'package:adaptive_theme/adaptive_theme.dart';
import 'package:famedly/components/famedly_top_bar.dart';
import 'package:famedly/utils/error_reporter.dart';
import 'package:famedly/utils/localized_exception_extension.dart';
import 'package:famedly/utils/platform_extension.dart';
import 'package:famedly/views/enter_pass_code_view.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:future_loading_dialog/future_loading_dialog.dart';
import 'package:provider/provider.dart';
import 'package:sentry/sentry.dart';
import 'package:universal_html/html.dart' as html;
import 'package:web_multiple_tab_detector/web_multiple_tab_detector.dart';
import 'package:flutter_app_lock/flutter_app_lock.dart';
import 'package:adaptive_page_layout/adaptive_page_layout.dart';

import 'config/chat_configs.dart';
import 'models/famedly.dart';
import 'config/routes.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'styles/colors.dart' as style;
import 'utils/famedlysdk_client.dart';
import 'models/plugins/local_notification_plugin.dart';
import 'models/plugins/background_push_plugin.dart';
import 'models/plugins/background_sync_plugin.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Sentry.init((options) => options.dsn = ChatConfigs.sentryDsn);

  final client = getClient();
  BackgroundSyncPlugin.clientOnly(client);
  if (kIsMobile) {
    LocalNotificationPlugin.clientOnly(client);
    BackgroundPushPlugin.clientOnly(client);
  }

  SystemChrome.setSystemUIOverlayStyle(
    SystemUiOverlayStyle(statusBarColor: Colors.transparent),
  );

  WebMultipleTabDetector.register();

  FlutterError.onError = (FlutterErrorDetails details) =>
      Zone.current.handleUncaughtError(details.exception, details.stack);

  runZonedGuarded(
      () => runApp(
            kIsWeb
                ? FamedlyApp()
                : AppLock(
                    builder: (args) => FamedlyApp(),
                    lockScreen: EnterPassCodeView(),
                    enabled: false,
                  ),
          ),
      ErrorReporter.reportError);
}

/// This is the entry class used to define Themes and the page to load on start.
class FamedlyApp extends StatelessWidget {
  static final aplKey = GlobalKey<AdaptivePageLayoutState>();

  @override
  Widget build(BuildContext context) {
    final locale =
        kIsWeb ? Locale(html.window.navigator.language.split('-').first) : null;
    final loadingMaterialApp = MaterialApp(
      title: 'Famedly',
      theme: style.Themes.famedlyLightTheme,
      home: Scaffold(
        body: Center(
          child: CircularProgressIndicator(),
        ),
      ),
    );
    return FutureBuilder<bool>(
        future: WebMultipleTabDetector.isSingleTab(),
        builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
          if (!snapshot.hasData) {
            return loadingMaterialApp;
          }
          if (snapshot.data) {
            // is single tab, start up normally
            return AdaptiveTheme(
              light: style.Themes.famedlyLightTheme,
              dark: style.Themes.famedlyDarkTheme,
              initial: AdaptiveThemeMode.system,
              builder: (theme, darkTheme) => MaterialApp(
                title: 'Famedly',
                theme: theme,
                darkTheme: darkTheme,
                localizationsDelegates: L10n.localizationsDelegates,
                supportedLocales: L10n.supportedLocales,
                locale: locale,
                home: Provider<Famedly>(
                  create: (context) => Famedly(
                    adaptivePageLayout: aplKey,
                    context: context,
                    clientName:
                        'Famedly', // Do never ever change this! It is used for indexing in the local storage.
                  ),
                  child: Builder(builder: (context) {
                    LoadingDialog.defaultBackLabel = L10n.of(context).back;
                    LoadingDialog.defaultOnError = (e) =>
                        LocalizedExceptionExtension.toLocalizedString(
                            context, e);
                    return AdaptivePageLayout(
                      key: aplKey,
                      onGenerateRoute: FamedlyRoutes(context).getRoute,
                      initialRoute: '/',
                      topBarBuilder: (_) => PreferredSize(
                        preferredSize: Size.fromHeight(FamedlyTopBar.height),
                        child: FamedlyTopBar(),
                      ),
                      safeAreaOnColumnView: false,
                      dividerWidth: 1,
                      dividerColor: Theme.of(context).dividerColor,
                      routeBuilder: (builder, settings) =>
                          FamedlyRoutes.isHardTransitionRoute(settings.name)
                              ? HardTransitionRoute(page: builder(context))
                              : MaterialPageRoute(builder: builder),
                    );
                  }),
                ),
              ),
            );
          }
          // is multi-tab, display warning
          return MaterialApp(
            title: 'Famedly',
            theme: style.Themes.famedlyLightTheme,
            localizationsDelegates: L10n.localizationsDelegates,
            supportedLocales: L10n.supportedLocales,
            locale: locale,
            home: Scaffold(
              body: Center(
                child: Builder(builder: (context) {
                  return Text(
                    L10n.of(context).onlyOneTabPossible,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 20,
                    ),
                  );
                }),
              ),
            ),
          );
        });
  }
}
