/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'package:famedly/components/search_bar.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import '../models/famedly.dart';
import '../components/matrix_event_list.dart';

class EventSearchPage extends StatefulWidget {
  final String id;

  EventSearchPage({Key key, this.id}) : super(key: key);

  @override
  _EventSearchPageState createState() => _EventSearchPageState();
}

class _EventSearchPageState extends State<EventSearchPage> {
  final TextEditingController _controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final famedly = Famedly.of(context);

    final room = famedly.client.getRoomById(widget.id) ??
        Room(
            id: widget.id,
            client: famedly.client,
            membership: Membership.leave);

    return Scaffold(
      key: Key('EventSearchPage'),
      appBar: AppBar(
        titleSpacing: 0,
        title: Padding(
          padding: const EdgeInsets.only(right: 4.0),
          child: SearchBar(
            key: Key('searchBar'),
            autofocus: true,
            filterTextController: _controller,
            onTextChanged: (_) => setState(() => null),
          ),
        ),
      ),
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Expanded(
              child: MatrixEventList(
                room,
                searchQuery: _controller.text,
                key: Key('results_list'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
