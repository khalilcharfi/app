import 'package:famedly/views/enter_pass_code_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_lock/flutter_app_lock.dart';
import 'package:flutter_test/flutter_test.dart';

import 'utils.dart';

void main() {
  /// All Tests related to the EnterPassCodeView
  group('EnterPassCodeView', () {
    NavigatorObserver mockObserver;

    setUp(() {
      mockObserver = MockNavigatorObserver();
    });

    testWidgets('should have all buttons', (WidgetTester tester) async {
      await tester.runAsync(() async {
        await tester.pumpWidget(
          await Utils.getWidgetWrapper(
            AppLock(
              lockScreen: EnterPassCodeView(),
              builder: (args) => EnterPassCodeView(),
            ),
            mockObserver,
          ),
        );

        await tester.pumpAndSettle();

        // TODO make possible with detection that we are in tests
        expect(find.byKey(Key('digit_0')), findsOneWidget);
        expect(find.byKey(Key('digit_1')), findsOneWidget);
        expect(find.byKey(Key('digit_2')), findsOneWidget);
        expect(find.byKey(Key('digit_3')), findsOneWidget);
        expect(find.byKey(Key('digit_4')), findsOneWidget);
        expect(find.byKey(Key('digit_5')), findsOneWidget);
        expect(find.byKey(Key('digit_6')), findsOneWidget);
        expect(find.byKey(Key('digit_7')), findsOneWidget);
        expect(find.byKey(Key('digit_8')), findsOneWidget);
        expect(find.byKey(Key('digit_9')), findsOneWidget);
      });
    });
  });
}
