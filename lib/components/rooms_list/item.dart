import 'package:adaptive_page_layout/adaptive_page_layout.dart';
import 'package:famedly/components/mouse_over_builder.dart';
import 'package:famedly/styles/famedly_icons_v2_icons.dart';
import 'package:famedly/utils/matrix_locals.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:future_loading_dialog/future_loading_dialog.dart';

import '../../models/famedly.dart';
import '../../styles/colors.dart';
import '../../utils/date_time_extension.dart';
import '../../utils/room_archive_dialog_extension.dart';
import '../../utils/room_status_extension.dart';
import '../avatar.dart';
import '../default_modal_bottom_sheet.dart';
import '../unread_bubble.dart';

enum RoomItemContextualAction {
  toggleUnread,
  togglePinned,
  toggleMuted,
  archive
}

/// Generates the widget for each room item.
class RoomItem extends StatelessWidget {
  final Room item;
  final bool activeChat;

  const RoomItem({Key key, @required this.item, this.activeChat = false})
      : super(key: key);

  List<PopupMenuItem<RoomItemContextualAction>> getPopupMenuItems(
          BuildContext context) =>
      [
        PopupMenuItem<RoomItemContextualAction>(
          value: RoomItemContextualAction.toggleUnread,
          child: Text(item.isUnread
              ? L10n.of(context).markRead
              : L10n.of(context).markUnread),
        ),
        PopupMenuItem<RoomItemContextualAction>(
          value: RoomItemContextualAction.togglePinned,
          child: Text(
              item.isFavourite ? L10n.of(context).unpin : L10n.of(context).pin),
        ),
        PopupMenuItem<RoomItemContextualAction>(
          value: RoomItemContextualAction.toggleMuted,
          child: Text(item.pushRuleState == PushRuleState.notify
              ? L10n.of(context).mute
              : L10n.of(context).unmute),
        ),
        PopupMenuItem<RoomItemContextualAction>(
          value: RoomItemContextualAction.archive,
          child: Text(
            L10n.of(context).archiveAction,
            style: TextStyle(color: FamedlyColors.grapefruit),
          ),
        ),
      ];

  void _showModalBottomSheetAction(BuildContext outerContext) {
    showModalBottomSheet(
      context: outerContext,
      backgroundColor: Colors.transparent,
      isScrollControlled: true,
      builder: (context) => DefaultModalBottomSheet(
        child: ListView(
          physics: NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          children: getPopupMenuItems(context)
              .map((item) => ListTile(
                    title: item.child,
                    onTap: () {
                      Navigator.of(context, rootNavigator: false).pop();
                      _contextualAction(outerContext, item.value);
                    },
                  ))
              .toList(),
        ),
      ),
    );
  }

  void _contextualAction(
      BuildContext context, RoomItemContextualAction action) async {
    switch (action) {
      case RoomItemContextualAction.togglePinned:
        await showFutureLoadingDialog(
          context: context,
          future: () => item.setFavourite(!item.isFavourite),
        );
        break;
      case RoomItemContextualAction.toggleMuted:
        var newState = PushRuleState.notify;
        if (item.pushRuleState == PushRuleState.notify) {
          newState = PushRuleState.mentionsOnly;
        }
        await showFutureLoadingDialog(
          context: context,
          future: () => item.setPushRuleState(newState),
        );
        break;
      case RoomItemContextualAction.archive:
        await item
            .showArchiveDialog(context, popAfter: false)
            .then((_) => AdaptivePageLayout.of(context).popUntilIsFirst());
        break;
      case RoomItemContextualAction.toggleUnread:
        await showFutureLoadingDialog(
          context: context,
          future: () => item.setUnread(!item.isUnread),
        );
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    final displayname =
        item.getLocalizedFamedlyDisplayname(MatrixLocals(L10n.of(context)));

    final badges = <Widget>[Container(width: 0.1, height: UnreadBubble.size)];
    final isTyping =
        item.typingUsers.where((u) => u.id != item.client.userID).isNotEmpty;
    final body = item.membership == Membership.invite
        ? L10n.of(context).youHaveBeenInvitedToChat
        : isTyping
            ? item.getLocalizedTypingText(context)
            : item.lastEvent != null
                ? item.lastEvent.getLocalizedBody(
                    MatrixLocals(L10n.of(context)),
                    withSenderNamePrefix: !item.isDirectChat ||
                        item.lastEvent.senderId == item.client.userID,
                    hideReply: true,
                  )
                : '';
    if (item.pushRuleState != PushRuleState.notify) {
      badges.add(Padding(
        padding: const EdgeInsets.only(left: 8.0),
        child:
            Icon(FamedlyIconsV2.off, size: 16, color: FamedlyColors.blueyGrey),
      ));
    }
    if (item.isFavourite) {
      badges.add(Padding(
        padding: const EdgeInsets.only(left: 8.0),
        child:
            Icon(FamedlyIconsV2.pin, size: 16, color: FamedlyColors.blueyGrey),
      ));
    }
    if (item.isUnread) {
      badges.add(Padding(
        padding: const EdgeInsets.only(left: 8.0),
        child: UnreadBubble(
          [item],
          showNumbers: item.notificationCount > 0,
          key: Key('notification_count'),
        ),
      ));
    }
    return MouseOverBuilder(
      builder: (context, hover) => ListTile(
        selected: activeChat,
        selectedTileColor: Theme.of(context).secondaryHeaderColor,
        key: Key('RoomListTile'),
        onLongPress: item.membership == Membership.leave
            ? null
            : () => _showModalBottomSheetAction(context),
        onTap: () async {
          if (activeChat) return;
          if (await Famedly.of(context)
              .roomsPlugin
              .joinRoomAndWait(context, item)) {
            await AdaptivePageLayout.of(context).pushNamedAndRemoveUntilIsFirst(
                '/room/${item.id}',
                arguments: item);
          }
        },
        leading: Avatar(
          item.avatar,
          size: 40,
          name: displayname,
        ),
        title: Row(
          children: <Widget>[
            Expanded(
              child: Text(
                displayname,
                // Fixme replace with actual data
                key: Key('room_name_${item.id}'),
                style: TextStyle(
                  color: Theme.of(context).textTheme.headline1.color,
                  fontSize: 16,
                  fontWeight: FontWeight.w500,
                  fontStyle: FontStyle.normal,
                  letterSpacing: 0,
                ),
                softWrap: false,
                overflow: TextOverflow.ellipsis,
              ),
            ),
            Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.end,
              children: badges,
            ),
            if (item.membership != Membership.leave)
              Opacity(
                // We need to hide it this way because otherwise the button would stop existing
                // when the user stop hovers it to select a PopupMenuButtonItem.
                opacity: hover ? 1 : 0,
                child: Container(
                  height: UnreadBubble.size,
                  margin: hover
                      ? const EdgeInsets.only(left: 8.0)
                      : EdgeInsets.zero,
                  width: hover ? 20 : 0,
                  child: PopupMenuButton<RoomItemContextualAction>(
                    padding: EdgeInsets.zero,
                    itemBuilder: (context) => getPopupMenuItems(context),
                    onSelected: (action) => _contextualAction(context, action),
                  ),
                ),
              ),
          ],
        ),
        subtitle: Row(
          children: <Widget>[
            Expanded(
              child: Text(
                body,
                key: Key('last_message'),
                maxLines: 1,
                style: TextStyle(
                  color: item.membership == Membership.invite || isTyping
                      ? FamedlyColors.aquaMarine
                      : FamedlyColors.blueyGrey,
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                  fontStyle: FontStyle.normal,
                  letterSpacing: 0,
                ),
                overflow: TextOverflow.ellipsis,
                softWrap: false,
              ),
            ),
            SizedBox(width: 8),
            Text(
              item.timeCreated.localizedTimeShort(context),
              key: Key('time_created'),
              style: TextStyle(
                color: FamedlyColors.blueyGrey,
                fontSize: 14,
                fontWeight: FontWeight.w400,
                fontStyle: FontStyle.normal,
                letterSpacing: 0,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
