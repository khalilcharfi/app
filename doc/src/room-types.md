This app utilizes room types as per [MSC1840](https://github.com/matrix-org/matrix-doc/pull/1840)

Here are the room types currently in use

| type | purpose |
|------|---------|
| `m.room_type.messaging` | normal text chat |
| `com.famedly.room_type.request` | request room |
| `com.famedly.room_type.patient` | patient room |
| `com.famedly.room_type.contact_discovery` | contact discovery room |
| `com.famedly.room_type.files` | File collection room, not in final app, subject to change |
