import 'dart:async';
import 'dart:io';

import 'package:audioplayers/audioplayers.dart';
import 'package:famedly/utils/downloader.dart';
import 'package:famedly/utils/error_reporter.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:adaptive_page_layout/adaptive_page_layout.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';

import '../../../styles/colors.dart';
import '../../../utils/format.dart';
import '../input_field.dart';

class AudioEventContent extends StatefulWidget {
  final Event audioEvent;

  final Uri file;

  final Event prevEvent;
  final Event nextEvent;

  AudioEventContent(
    this.audioEvent, {
    Key key,
    this.prevEvent,
    this.nextEvent,
  })  : file = Uri.parse(audioEvent.content['url'] ?? ''),
        super(key: key);

  @override
  State<StatefulWidget> createState() => AudioEventContentState();
}

class AudioEventContentState extends State<AudioEventContent> {
  final AudioPlayer _player = AudioPlayer();
  File _file;

  final bool _isLoadingFromCache = false;
  bool _isDownloading = false;

  bool get _loaded => _file != null;

  StreamSubscription _onAudioPositionChanged;
  StreamSubscription _onDurationChanged;
  StreamSubscription _onPlayerStateChanged;
  StreamSubscription _onPlayerError;
  Duration _position;
  Duration _max;
  bool get _isPlaying => _player.state == AudioPlayerState.PLAYING;

  Future<void> _download() async {
    if (kIsWeb) {
      await Downloader(context).start(
        widget.audioEvent,
        fileName: widget.audioEvent.body,
        mimeType: widget.audioEvent.content['info']['mimetype'],
      );
      return;
    }
    setState(() => _isDownloading = true);

    final matrixFile = await widget.audioEvent.downloadAndDecryptAttachment();
    final tempDir = await getTemporaryDirectory();
    final fileName = matrixFile.name.contains('.')
        ? matrixFile.name
        : '${matrixFile.name}.${InputField.defaultAudioFileType}';
    final file = File('${tempDir.path}/$fileName');
    await file.writeAsBytes(matrixFile.bytes);

    if (mounted) {
      setState(() {
        _file = file;
        _isDownloading = false;
      });
    }
    _play();
  }

  void _play() async {
    if (_player.state == AudioPlayerState.PAUSED) {
      await _player.resume();
    } else {
      _onAudioPositionChanged ??=
          _player.onAudioPositionChanged.listen((state) {
        setState(() => _position = state);
      });
      _onDurationChanged ??=
          _player.onDurationChanged.listen((max) => setState(() => _max = max));
      _onPlayerStateChanged ??=
          _player.onPlayerStateChanged.listen((_) => setState(() => null));
      _onPlayerError ??= _player.onPlayerError.listen((e) {
        AdaptivePageLayout.of(context).showSnackBar(
          SnackBar(
            content: Text(L10n.of(context).oopsSomethingWentWrong),
          ),
        );
        ErrorReporter.reportError(e, StackTrace.current);
      });

      await _player.play(_file.path, isLocal: true);
    }
  }

  void _pause() {
    if (!_isPlaying) return;
    _player.pause();
  }

  void _seek(double value) {
    if (!_isPlaying) return;
    _player.seek(
      Duration(milliseconds: value.round()),
    );
  }

  @override
  Widget build(BuildContext context) {
    IconData icon;

    if (_isPlaying) {
      icon = Icons.pause;
    } else {
      icon = Icons.play_arrow;
    }

    var onPressed;
    if (!_loaded) {
      onPressed = () => _download();
    } else if (!_isDownloading) {
      if (!_isPlaying) {
        onPressed = () => _play();
      } else {
        onPressed = () => _pause();
      }
    }

    return Row(
      //key: ValueKey(_player.playerId),
      children: <Widget>[
        Stack(
          alignment: Alignment.center,
          children: <Widget>[
            IconButton(
              key: _isPlaying ? Key('PauseButton') : Key('PlayButton'),
              icon: Icon(
                icon,
                color: _isLoadingFromCache || _isDownloading
                    ? FamedlyColors.cloudyBlue
                    : FamedlyColors.metallicBlue,
              ),
              onPressed: onPressed,
            ),
            if (_isDownloading)
              CircularProgressIndicator(
                strokeWidth: 3,
              ),
          ],
        ),
        Expanded(
          child: Slider(
            onChanged: (value) {
              _seek(value);
            },
            value: _position?.inMilliseconds?.toDouble() ?? 0,
            max: _max?.inMilliseconds?.toDouble() ?? 1,
          ),
        ),
        Text(Duration(milliseconds: _position?.inMilliseconds?.round() ?? 0)
            .format()),
        SizedBox(width: 16),
      ],
    );
  }

  @override
  void dispose() {
    if (_isPlaying) _player.stop();
    _onAudioPositionChanged?.cancel();
    _onDurationChanged?.cancel();
    _onPlayerStateChanged?.cancel();
    _onPlayerError?.cancel();
    super.dispose();
  }
}
