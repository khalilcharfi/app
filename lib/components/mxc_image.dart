/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'package:cached_network_image/cached_network_image.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class MxcImage extends StatelessWidget {
  final Uri mxContent;
  final double width;
  final double height;
  final BoxFit fit;
  final Function(BuildContext, String) placeholder;
  final Client matrixClient;

  const MxcImage(
    this.mxContent, {
    this.width,
    this.height,
    this.fit,
    this.placeholder,
    @required this.matrixClient,
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final url = mxContent == null
        ? Uri()
        : mxContent.getThumbnail(matrixClient,
            width: width * MediaQuery.of(context).devicePixelRatio,
            height: height * MediaQuery.of(context).devicePixelRatio,
            method: ThumbnailMethod.scale);
    return CachedNetworkImage(
      imageUrl: url.toString(),
      fit: BoxFit.cover,
      placeholder: placeholder,
    );
  }
}
