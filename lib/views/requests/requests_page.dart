/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'dart:async';
import 'package:famedly/models/famedly.dart';
import 'package:famedly/models/requests/filters.dart';
import 'package:famedly/models/requests/request.dart';
import 'package:famedly/models/roles/role_entity.dart';
import 'package:famedly/views/requests/requests_page_view.dart';
import 'package:famedlysdk/famedlysdk.dart' hide Tag;
import 'package:flutter/material.dart';
import 'package:famedly/models/roles/client_extension.dart';

typedef RoomFilter = bool Function(Room room);

class RequestsPageUnreadCount {
  int myRequests = 0;
  int myAssignedRequests = 0;
  int mySharedRequets = 0;
  int myBroadcasts = 0;
  int myIncomingBroadcasts = 0;
}

class RequestsPage extends StatefulWidget {
  final String currentType;

  const RequestsPage({Key key, this.currentType}) : super(key: key);

  @override
  RequestsPageController createState() => RequestsPageController();
}

class RequestsPageController extends State<RequestsPage> {
  final filterTextController = TextEditingController();
  bool searchActive = false;
  Famedly famedly;
  final int initialTabIndex = 0;

  @override
  void initState() {
    famedly = Famedly.of(context);
    roleFuture ??= famedly.client.getOwnRoles('role');
    super.initState();
  }

  @override
  void setState(fn) {
    if (!mounted) return;
    super.setState(fn);
  }

  Future<List<RoleEntity>> roleFuture;
  Future<bool> waitForFirstSync() async {
    if (famedly.client.prevBatch != null) return true;
    return famedly.client.onFirstSync.stream.first;
  }

  void searchBarTextChanged(String text) {
    if (searchActive != text.isNotEmpty) {
      setState(() {
        searchActive = text.isNotEmpty;
      });
    }
  }

  void searchBarOnCancel() {
    setState(() => searchActive = false);
  }

  RequestsPageUnreadCount countRooms(Client client, {bool countUnread = true}) {
    final unread = RequestsPageUnreadCount();
    Request.bundleFromRoomList(
        roomList: client.rooms,
        roomFilter: RequestFilters.isRequestRoomFilter,
        requestBundleFilter: (RequestBundle bundle) {
          // we highjack this method to populate our undread object
          // this is called once per bundle and we only want to calculate the undreads *per bundle*
          var isUnread = false;
          // we don't check the broadcast room here, as we don't want to receive
          // notifications from it
          if (bundle.rooms.isNotEmpty) {
            for (final room in bundle.rooms) {
              if (room.notificationCount > 0 ||
                  room.membership == Membership.invite ||
                  !countUnread) {
                isUnread = true;
                break;
              }
            }
          } else if (bundle.broadcastRoom != null && !countUnread ||
              bundle.broadcastRoom.notificationCount > 0 ||
              bundle.broadcastRoom.membership == Membership.invite) {
            isUnread = true;
          }
          if (!isUnread) {
            return false;
          }
          // okay, we have an unread bundle. Time to assign it to the correct categories!
          unread.myRequests++;
          if (!bundle.isOwn()) {
            unread.myIncomingBroadcasts++;
          }
          if (!RequestFilters.isOwnBroadcastBundleFilter(bundle)) {
            if (RequestFilters.isAdminBundleFilter(bundle)) {
              unread.myAssignedRequests++;
            } else {
              unread.mySharedRequets++;
            }
          } else {
            unread.myBroadcasts++;
          }
          return true;
        });
    return unread;
  }

  RequestsPageUnreadCount get unread => countRooms(famedly.client);
  RequestsPageUnreadCount get roomCounts =>
      countRooms(famedly.client, countUnread: false);
  Map<int, int> countRoleRooms(Client client, List<RoleEntity> roles,
      {bool countUnread = true}) {
    final unread = <int, int>{};
    for (final role in roles) {
      unread[role.id] = 0;
    }
    Request.bundleFromRoomList(
      roomList: client.rooms,
      roomFilter: RequestFilters.isRequestRoomFilter,
      requestBundleFilter: (RequestBundle bundle) {
        if (bundle.isOwn()) {
          return false;
        }
        // first we determine the role id
        int roleId;
        final room = bundle.broadcastRoom ?? bundle.rooms.first;
        for (final role in roles) {
          final user = room.getState('m.room.member', role.mxid)?.asUser;
          if (user == null || user.membership != Membership.join) {
            continue;
          }
          roleId = role.id;
        }
        if (roleId == null) {
          return false;
        }
        // we highjack this method to populate our undread object
        // this is called once per bundle and we only want to calculate the undreads *per bundle*
        var isUnread = false;
        // we don't check the broadcast room here, as we don't want to receive
        // notifications from it
        if (bundle.rooms.isNotEmpty) {
          for (final room in bundle.rooms) {
            if (room.notificationCount > 0 ||
                room.membership == Membership.invite ||
                !countUnread) {
              isUnread = true;
              break;
            }
          }
        } else if (bundle.broadcastRoom != null && !countUnread ||
            bundle.broadcastRoom.notificationCount > 0 ||
            bundle.broadcastRoom.membership == Membership.invite) {
          isUnread = true;
        }
        if (!isUnread) {
          return false;
        }
        unread[roleId]++;
        return true;
      },
    );
    return unread;
  }

  @override
  Widget build(BuildContext context) {
    return RequestsPageView(this);
  }
}
