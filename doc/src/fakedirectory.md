### Disable whitelist
To use the Directory even if you are not on a *.famedly.de homeserver, just change the line 49 in `/lib/views/RequestsPage.dart` from:
`if (!matrix.client.homeserver.toLowerCase().endsWith("famedly.de")) {`

To:

`if (false && !matrix.client.homeserver.toLowerCase().endsWith("famedly.de")) {`

### Use FakeDirectory

The FakeDirectory is a custom fake HTTPClient for testing. Just go to the directory.dart file and change this line:

`http.Client httpClient = http.Client();`

To:

`http.Client httpClient = FakeDirectory();`

Don't forget to import the FakeDirectory. Now you're app isn't communicating with the real directory but with a local one. Play around with it at `/utils/FakeDirectory.dart` but **NEVER EVER** commit the line httpClient = FakeDirectory() to the master or we will punish you by forcing you to use a Windows Phone as a daily driver for one month!
