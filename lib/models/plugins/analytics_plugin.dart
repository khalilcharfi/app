import 'dart:async';

import 'package:famedly/config/matomo_url.dart';
import 'package:famedly/models/plugins/famedly_plugin.dart';
import 'package:matomo/matomo.dart';
import 'package:localstorage/localstorage.dart';
import 'package:famedlysdk/famedlysdk.dart';

class AnalyticsPlugin extends FamedlyPlugin {
  AnalyticsPlugin(matrix) : super(matrix) {
    onLogin ??= client.onLoginStateChanged.stream.listen((_) => init());
  }

  StreamSubscription<LoginState> onLogin;

  void init() async {
    analyticsEnabled = false;
    final storage = LocalStorage('FamedlyLocalStorage');
    await storage.ready;
    if (storage.getItem('sentry') != true) return;

    initializeTracker(
        FamedlyMatomoInitializer.url, FamedlyMatomoInitializer.siteId);
    analyticsEnabled = true;
  }

  bool analyticsEnabled = false;

  void initializeTracker(URL, ID) => MatomoTracker().initialize(
        siteId: FamedlyMatomoInitializer.siteId,
        url: FamedlyMatomoInitializer.url,
        visitorId: famedly.client.userID?.localpart,
      );

  void trackScreen(context, eventName) =>
      !analyticsEnabled ? null : MatomoTracker.trackScreen(context, eventName);

  void trackScreenWithName(widgetName, eventName) => !analyticsEnabled
      ? null
      : MatomoTracker.trackScreenWithName(widgetName, eventName);

  void trackEvent(context, eventName, eventAction) async => !analyticsEnabled
      ? null
      : MatomoTracker.trackEvent(eventName, eventAction);

  void trackEventWithName(widgetName, eventName, eventAction) =>
      !analyticsEnabled
          ? null
          : MatomoTracker.trackEvent(eventName, eventAction,
              widgetName: widgetName);

  void trackGoal(goalId) async =>
      !analyticsEnabled ? null : MatomoTracker.trackGoal(goalId);
}
