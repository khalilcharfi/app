import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:math';

import 'package:famedly/utils/error_reporter.dart';
import 'package:famedly/utils/platform_extension.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:fcm_shared_isolate/fcm_shared_isolate.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:http/http.dart' as http;
import 'package:pedantic/pedantic.dart';

import '../famedly.dart';

class PreNotify {
  PreNotify(this.roomId, this.eventId);
  String roomId;
  String eventId;
}

class NoTokenException implements Exception {
  String get cause => 'Cannot get firebase token';
}

class BackgroundPushPlugin {
  static const String appId = 'com.famedly.app';
  static const String pusherFormat = 'event_id_only';
  static final Uri gatewayUrl =
      Uri.parse('https://push.famedly.de/_matrix/push/v1/notify');

  static String get suffixedAppId =>
      appId + (Platform.isAndroid ? '.data_message' : '');

  static BackgroundPushPlugin _instance;
  Client client;
  Famedly famedly;
  String _token;
  LoginState _loginState;

  final StreamController<PreNotify> onPreNotify = StreamController.broadcast();

  final pendingTests = <String, Completer<void>>{};

  DateTime lastReceivedPush;

  BackgroundPushPlugin._(this.client) {
    onLogin ??=
        client.onLoginStateChanged.stream.listen(handleLoginStateChanged);
    _firebaseMessaging.setListeners(
      onMessage: _onMessage,
      onNewToken: newToken,
    );
  }

  factory BackgroundPushPlugin.clientOnly(Client client) {
    _instance ??= BackgroundPushPlugin._(client);
    return _instance;
  }

  factory BackgroundPushPlugin(Famedly famedly) {
    final instance = BackgroundPushPlugin.clientOnly(famedly.client);
    unawaited(instance.initMatrix(famedly));
    return instance;
  }

  Future<void> initMatrix(Famedly famedly) async {
    this.famedly = famedly;
  }

  void handleLoginStateChanged(LoginState state) {
    // We need to disable it in debug mode to make sure integrationtests work
    _loginState = state;
    if (state == LoginState.logged && kIsMobile) {
      setupFirebase();
    }
  }

  void newToken(String token) {
    _token = token;
    if (_loginState == LoginState.logged && kIsMobile) {
      setupFirebase();
    }
  }

  final _firebaseMessaging = FcmSharedIsolate();

  StreamSubscription<LoginState> onLogin;

  Future<void> setupFirebase() async {
    await _firebaseMessaging.requestPermission();

    if (_token?.isEmpty ?? true) {
      try {
        _token = await _firebaseMessaging.getToken();
      } catch (_) {
        Logs().e('[Push] cannot get token');
        return;
      }
    }
    final pushers = await client.getPushers();
    final currentPushers = pushers.where((pusher) => pusher.pushkey == _token);
    if (currentPushers.length == 1 &&
        currentPushers.first.kind == 'http' &&
        currentPushers.first.appId == suffixedAppId &&
        currentPushers.first.appDisplayName == client.clientName &&
        currentPushers.first.deviceDisplayName == client.deviceName &&
        currentPushers.first.lang == 'en' &&
        currentPushers.first.data.url == gatewayUrl &&
        currentPushers.first.data.format == pusherFormat) {
      Logs().v('[Push] Pusher already set');
    } else {
      if (currentPushers.isNotEmpty) {
        for (final currentPusher in currentPushers) {
          if (!client.isLogged()) return;
          await client.postPusher(
            Pusher(
              _token,
              currentPusher.appId,
              currentPusher.appDisplayName,
              currentPusher.deviceDisplayName,
              currentPusher.lang,
              PusherData(
                url: currentPusher.data.url,
                format: pusherFormat,
              ),
              kind: null,
            ),
            append: true,
          );
          Logs().d('[Push] Remove legacy pusher for this device');
        }
      }
      if (!client.isLogged()) return;
      await client.postPusher(
        Pusher(
          _token,
          suffixedAppId,
          client.clientName,
          client.deviceName,
          'en',
          PusherData(
            url: gatewayUrl,
            format: pusherFormat,
          ),
          kind: 'http',
        ),
        append: true,
      );
    }
    Logs().v('[Push] Firebase initialized');
  }

  void _onMessage(Map<dynamic, dynamic> message) async {
    try {
      Logs().v('[Push] _onMessage');
      lastReceivedPush = DateTime.now();
      final Map<dynamic, dynamic> data = message['data'] ?? message;
      final roomId = data['room_id'];
      final eventId = data['event_id'];
      if (roomId == 'test') {
        Logs().v('[Push] Test $eventId was successful!');
        pendingTests.remove(eventId)?.complete();
        return;
      }
      if (roomId != null && eventId != null) {
        var giveUp = false;
        var loaded = false;
        final stopwatch = Stopwatch();
        stopwatch.start();
        final syncSubscription = client.onSync.stream.listen((r) {
          if (stopwatch.elapsed.inSeconds >= 30) {
            giveUp = true;
          }
        });
        final eventSubscription = client.onEvent.stream.listen((e) {
          if (e.content['event_id'] == eventId) {
            loaded = true;
          }
        });
        try {
          if (!(await eventExists(roomId, eventId)) && !loaded) {
            onPreNotify.add(PreNotify(roomId, eventId));
            do {
              Logs().v('[Push] getting ' + roomId + ', event ' + eventId);
              await client.oneShotSync();
              if (stopwatch.elapsed.inSeconds >= 60) {
                giveUp = true;
              }
            } while (!loaded && !giveUp);
          }
          Logs().v('[Push] ' +
              (giveUp ? 'gave up on ' : 'got ') +
              roomId +
              ', event ' +
              eventId);
        } finally {
          await syncSubscription.cancel();
          await eventSubscription.cancel();
        }
      } else {
        if (client.syncPending) {
          Logs().v('[Push] waiting for existing sync');
          await client.oneShotSync();
        }
        Logs().v('[Push] single oneShotSync');
        await client.oneShotSync();
      }
    } catch (e, s) {
      await ErrorReporter.reportError(e, s);
    }
  }

  Future<bool> eventExists(String roomId, String eventId) async {
    final room = client.getRoomById(roomId);
    if (room == null) return false;
    return (await client.database.getEventById(client.id, eventId, room)) !=
        null;
  }

  Future<bool> sendTestMessageGUI({bool verbose = false}) async {
    try {
      await sendTestMessage().timeout(Duration(seconds: 30));
      if (verbose) {
        famedly.adaptivePageLayout.currentState.showSnackBar(
          SnackBar(
            content: Text(L10n.of(famedly.context).pushTestSuccessful),
          ),
        );
      }
    } catch (e, s) {
      var msg;
      final l10n = L10n.of(famedly.context);
      if (e is SocketException) {
        msg = verbose ? l10n.pushServerUnreachable : null;
      } else if (e is NoTokenException) {
        msg = verbose ? l10n.pushTokenUnavailable : null;
      } else {
        msg = l10n.pushFail;
        await ErrorReporter.reportError(e, s);
      }
      if (msg != null) {
        famedly.adaptivePageLayout.currentState.showSnackBar(
          SnackBar(
            content: Text('$msg\n\n${e.toString()}'),
          ),
        );
      }
      return false;
    }
    return true;
  }

  Future<void> sendTestMessage() async {
    if (_token?.isEmpty ?? true) {
      throw NoTokenException();
    }

    final random = Random.secure();
    final randomId =
        base64.encode(List<int>.generate(12, (i) => random.nextInt(256)));
    final completer = Completer<void>();

    if (Platform.isIOS) {
      // don't expect a reply, because fcm_shared_isolate can't receive on iOS
      completer.complete();
    } else {
      pendingTests[randomId] = completer;
    }

    try {
      final resp = await http.post(
        gatewayUrl,
        headers: {'Content-Type': 'application/json'},
        body: jsonEncode(
          {
            'notification': {
              'event_id': randomId,
              'room_id': 'test',
              'counts': {
                'unread': 1,
              },
              'devices': [
                {
                  'app_id': suffixedAppId,
                  'pushkey': _token,
                  'pushkey_ts': 12345678,
                  'data': {},
                  'tweaks': {}
                }
              ]
            }
          },
        ),
      );
      if (resp.statusCode < 200 || resp.statusCode >= 299) {
        throw resp.body.isNotEmpty ? resp.body : resp.reasonPhrase;
      }
    } catch (_) {
      pendingTests.remove(randomId);
      rethrow;
    }

    return completer.future;
  }
}
