/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'package:famedlysdk/famedlysdk.dart';

import '../models/patients/client_patients_extension.dart';
import '../models/requests/request.dart';

const String roomTypeMessaging = 'm.room_type.messaging';
const String roomTypeRequest = 'com.famedly.room_type.request';
const String roomTypePatient = 'com.famedly.room_type.patient';
const String roomTypeContactDiscovery =
    'com.famedly.room_type.contact_discovery';

extension RoomTypeExtension on Room {
  String get type {
    final state = getState('m.room.type')?.content;
    if (state == null || state['type'] == null) {
      if (getState(Request.requestNameSpace) != null ||
          name == Request.requestNameSpace) {
        return roomTypeRequest;
      }
      if (getState(patientRoomNamespace) != null ||
          name == patientRoomNamespace) {
        return roomTypePatient;
      }
      if (canonicalAlias != null &&
          (canonicalAlias.startsWith('#famedlyContactDiscovery') ||
              canonicalAlias.startsWith('#famedly-contact-discovery'))) {
        return roomTypeContactDiscovery;
      }
      return roomTypeMessaging;
    }
    return state['type'];
  }
}
