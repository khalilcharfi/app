/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'dart:io';

import 'package:famedly/models/famedly.dart';
import 'package:famedly/components/settings_list_group.dart';
import 'package:famedly/models/plugins/local_notification_plugin.dart';
import 'package:famedly/styles/colors.dart';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:future_loading_dialog/future_loading_dialog.dart';
import 'package:open_noti_settings/open_noti_settings.dart';

class SettingsNotificationsPage extends StatelessWidget {
  void _openAndroidNotificationSettingsAction() async {
    await NotificationSetting.configureChannel(
      NotificationDetails(
        android: AndroidNotificationDetails(
          LocalNotificationPlugin.channelId,
          LocalNotificationPlugin.channelName,
          LocalNotificationPlugin.channelDescription,
        ),
      ),
    );
    return NotificationSetting.open();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: BackButton(),
        title: Text(L10n.of(context).notifications),
      ),
      backgroundColor: Theme.of(context).secondaryHeaderColor,
      body: StreamBuilder<Object>(
          stream: Famedly.of(context)
              .client
              .onAccountData
              .stream
              .where((event) => event.type == 'm.push_rules'),
          builder: (context, _) {
            return ListView(
              padding: FamedlyStyles.getTwoColumnListViewPadding(context),
              children: [
                SettingsListGroup(
                  children: [
                    SwitchListTile(
                      key: Key('pushMuteToggle'),
                      value:
                          !Famedly.of(context).client.allPushNotificationsMuted,
                      title: Text(
                          L10n.of(context).notificationsEnabledForThisAccount),
                      onChanged: (_) => showFutureLoadingDialog(
                        context: context,
                        future: () => Famedly.of(context)
                            .client
                            .setMuteAllPushNotifications(
                              !Famedly.of(context)
                                  .client
                                  .allPushNotificationsMuted,
                            ),
                      ),
                    ),
                    if (!kIsWeb && Platform.isAndroid)
                      ListTile(
                        title: Text(L10n.of(context).soundVibrationLedColor),
                        trailing: CircleAvatar(
                          backgroundColor:
                              Theme.of(context).scaffoldBackgroundColor,
                          foregroundColor: Colors.grey,
                          child: Icon(Icons.edit_outlined),
                        ),
                        onTap: () => _openAndroidNotificationSettingsAction(),
                      ),
                  ],
                ),
              ],
            );
          }),
    );
  }
}
