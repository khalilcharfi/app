/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'dart:async';
import 'dart:math';

import 'package:adaptive_page_layout/adaptive_page_layout.dart';
import 'package:famedly/components/chat_room/event_widgets/video_event_content.dart';
import 'package:famedly/utils/error_reporter.dart';
import 'package:famedly/utils/platform_extension.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';

import '../models/famedly.dart';
import '../models/requests/request.dart';
import '../models/room_type_extension.dart';
import '../styles/colors.dart';
import '../styles/famedly_icons_icons.dart';
import '../utils/downloader.dart';
import 'chat_room/event_widgets/audio_event_content.dart';
import 'chat_room/event_widgets/bad_encrypted_event_content.dart';
import 'chat_room/event_widgets/event_widget.dart';
import 'chat_room/event_widgets/file_event_content.dart';
import 'chat_room/event_widgets/image_event_content.dart';
import 'chat_room/event_widgets/state_event.dart';
import 'chat_room/event_widgets/text_event_content.dart';
import 'chat_room/event_widgets/typing_event.dart';
import 'chat_room/receipts_row.dart';

typedef OnReply = void Function(Event event);

class MatrixEventList extends StatefulWidget {
  final Room room;

  /// How much events should be requested at once, when the user scrolls to the
  /// top of the list? Defaults to 100.
  final int historyRequestCount;

  final String searchQuery;

  final bool Function(Event) filter;

  final OnReply onReply;

  final EdgeInsets padding;

  MatrixEventList(
    this.room, {
    this.onReply,
    this.historyRequestCount = 100,
    this.searchQuery,
    this.filter,
    this.padding,
    Key key,
  }) : super(key: key);

  @override
  MatrixEventListState createState() => MatrixEventListState();
}

class MatrixEventListState extends State<MatrixEventList>
    with WidgetsBindingObserver {
  MatrixEventListState() {
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    Famedly.of(context).activeRoomID =
        state == AppLifecycleState.resumed ? widget.room.id : null;
  }

  Timeline timeline;

  String lastSentReadReceipt;

  double showArrowDown = 0.0;

  final Set<String> _unfolded = {};

  void _unfold(String eventId) {
    final filteredEvents = filter(timeline.events);
    var i = filteredEvents.indexWhere((e) => e.eventId == eventId);
    setState(() {
      while (i < filteredEvents.length - 1 && filteredEvents[i].isState) {
        final id = filteredEvents[i].eventId;
        if (!_unfolded.add(id)) _unfolded.remove(id);
        i++;
      }
    });
  }

  final ScrollController _scrollController = ScrollController();

  void updateView() {
    if (!mounted) return;
    if (timeline != null) setState(() {});
  }

  Widget _buildEvent(
    BuildContext context,
    event,
    nextKey,
    prevKey,
  ) {
    final onReply = () => widget.onReply(event);

    if (event.redacted) {
      return EventWidget(
        event,
        room: widget.room,
        prevEvent: prevKey,
        nextEvent: nextKey,
        hasBubble: false,
        onReply: onReply,
        content: TextEventContent(
          event,
          prevEvent: prevKey,
          nextEvent: nextKey,
          timeline: timeline,
        ),
      );
    }

    switch (event.type) {
      case EventTypes.RoomMember:
      case EventTypes.Encryption:
      case EventTypes.RoomTopic:
      case EventTypes.RoomName:
      case EventTypes.CallHangup:
      case EventTypes.CallAnswer:
      case EventTypes.CallInvite:
      case EventTypes.RoomAvatar:
      case EventTypes.RoomCreate:
        return StateEvent(
          event,
          key: Key(event.eventId),
          prevEvent: prevKey,
          nextEvent: nextKey,
          unfold: _unfold,
          unfolded: _unfolded.contains(event.eventId),
        );
      case EventTypes.Sticker:
      case EventTypes.Message:
      case EventTypes.Encrypted:
        switch (event.messageType) {
          case MessageTypes.Audio:
            return EventWidget(
              event,
              onReply: onReply,
              key: Key(event.eventId),
              room: widget.room,
              prevEvent: prevKey,
              nextEvent: nextKey,
              padding: EdgeInsets.all(8),
              content: AudioEventContent(event),
            );
          case Downloader.fileDownloadMessageType:
            return StateEvent(
              event,
              prevEvent: prevKey,
              nextEvent: nextKey,
              key: Key(event.eventId),
              unfold: _unfold,
              unfolded: _unfolded.contains(event.eventId),
            );
          case Request.requestStateNameSpace:
            return StateEvent(
              event,
              prevEvent: prevKey,
              nextEvent: nextKey,
            );
          case MessageTypes.Text:
          case MessageTypes.None:
          case MessageTypes.Location:
          case MessageTypes.Notice:
          case MessageTypes.Emote:
            return EventWidget(
              event,
              room: widget.room,
              prevEvent: prevKey,
              nextEvent: nextKey,
              hasBubble: false,
              onReply: onReply,
              content: TextEventContent(
                event,
                prevEvent: prevKey,
                nextEvent: nextKey,
                timeline: timeline,
              ),
            );
          case MessageTypes.BadEncrypted:
            return EventWidget(
              event,
              room: widget.room,
              prevEvent: prevKey,
              nextEvent: nextKey,
              onReply: onReply,
              content: BadEncryptedEventContent(event),
            );
          case MessageTypes.Sticker:
          case MessageTypes.Image:
            return EventWidget(
              event,
              room: widget.room,
              prevEvent: prevKey,
              nextEvent: nextKey,
              hasBubble: false,
              onReply: onReply,
              content: ImageEventContent(event),
            );
          case MessageTypes.Video:
            if (kIsWeb || kIsMobile) {
              return EventWidget(
                event,
                room: widget.room,
                prevEvent: prevKey,
                nextEvent: nextKey,
                hasBubble: false,
                onReply: onReply,
                content: VideoEventContent(event),
              );
            }
            continue file;
          file:
          case MessageTypes.File:
            return EventWidget(
              event,
              room: widget.room,
              prevEvent: prevKey,
              nextEvent: nextKey,
              doubleBubble: false,
              onReply: onReply,
              content: FileEventContent(
                event,
                onDownload: () => Downloader(context).start(
                  event,
                  fileName: event.body,
                  mimeType: event.content['info']['mimetype'],
                ),
              ),
            );
        }
        Logs().w('The event type ${event.messageType} is unknown!');
        return Container();
      case Request.requestNameSpace:
        // we need to create a new dummy event, as the creator is actually someone else

        // fallback for missing msgtype
        event.content['msgtype'] ??= MessageTypes.Text;
        final evt = Event(
          status: event.status,
          content: event.content,
          type: EventTypes.Message,
          // we pretend that we are a normal message event now and try again
          eventId: event.eventId,
          roomId: event.roomId,
          senderId: event.content['creator'] ?? event.sender.id,
          originServerTs: event.time,
          unsigned: event.unsigned,
          prevContent: event.prevContent,
          stateKey: event.stateKey,
          room: event.room,
        );
        return _buildEvent(context, evt, nextKey, prevKey);
      default:
        Logs().w('The event type ${event.messageType} is unknown!');
        return Container();
    }
  }

  Widget _buildItem(
    BuildContext context,
    List<Event> events,
    int index,
  ) {
    if (index == -1) {
      final isSending = events.isNotEmpty &&
          events[0].senderId == widget.room.client.userID &&
          events[0].status == 0;
      return Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          AnimatedContainer(
            duration: Duration(milliseconds: 150),
            height: isSending ? 25 : 0,
            child: Padding(
              padding: EdgeInsets.symmetric(vertical: 5, horizontal: 0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: !isSending
                    ? <Widget>[]
                    : <Widget>[
                        Icon(
                          FamedlyIcons.clock,
                          color: FamedlyColors.slateGrey,
                          size: 14,
                        ),
                        Container(width: 5, height: 5),
                        Text(
                          L10n.of(context).willBeSent,
                          style: TextStyle(
                            color: FamedlyColors.slateGrey,
                            fontSize: 13,
                          ),
                        )
                      ],
              ),
            ),
          ),
          if (events.isNotEmpty) ReceiptsRow(events[0]),
          TypingEvent(widget.room),
        ],
      );
    } else if (index == events.length) {
      if (timeline.events.isNotEmpty &&
          timeline.events.last.type != EventTypes.RoomCreate) {
        return Center(
          child: timeline.isRequestingHistory
              ? Material(
                  elevation: 10,
                  borderRadius: BorderRadius.circular(90),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: CircularProgressIndicator(),
                  ),
                )
              : TextButton(
                  onPressed: () => timeline
                      .requestHistory(
                        historyCount: widget.historyRequestCount,
                      )
                      .catchError(
                        (e, s) => Logs().w('Request history failed', e, s),
                      ),
                  child: Text(
                    L10n.of(context).requestMoreMessages,
                    style: TextStyle(
                      color: FamedlyColors.azul,
                      decoration: TextDecoration.underline,
                    ),
                  ),
                ),
        );
      }
      return Container();
    } else if (index >= events.length) return Container();

    // Get keys from next and previous events
    Event nextKey;
    Event prevKey;
    if (index > 0) nextKey = events[index - 1];
    if (index < events.length - 1) prevKey = events[index + 1];

    final event = events[index];

    return _buildEvent(context, event, nextKey, prevKey);
  }

  static const Set<String> allowedStateEvents = {
    EventTypes.RoomCreate,
    EventTypes.RoomMember,
    EventTypes.Encryption,
    EventTypes.RoomTopic,
    EventTypes.RoomName,
    EventTypes.CallHangup,
    EventTypes.CallAnswer,
    EventTypes.CallInvite,
    EventTypes.RoomAvatar,
    EventTypes.Sticker,
    EventTypes.Message,
    EventTypes.Encrypted,
    Request.requestNameSpace,
  };

  List<Event> filter(List<Event> events) {
    // Filter out events we dont want to display
    events.removeWhere(
      (event) =>
          // Events we can display
          !allowedStateEvents.contains(event.type) ||
          // Custom filter
          (widget.filter?.call(event) ?? false) ||
          // Hide cryptic room name in patient rooms and requests
          event.type == EventTypes.RoomName &&
              ([roomTypePatient, roomTypeRequest].contains(widget.room.type)) ||
          // Hide botty state events
          (event.stateKey?.startsWith('@_botty_') ?? false) ||
          (event.isState && event.senderId.startsWith('@_botty_')),
    );

    // Fold state events
    var counter = 0;
    for (var i = events.length - 1; i >= 0; i--) {
      if (!events[i].isState) continue;
      if (i > 0 && events[i - 1].isState) {
        counter++;
        events[i].unsigned[StateEvent.collapsedKey] =
            !_unfolded.contains(events[i - 1].eventId);
      } else if (i == 0 || !events[i - 1].isState) {
        events[i].unsigned[StateEvent.collapsedKey] = false;
        events[i].unsigned[StateEvent.collapsedCounterKey] = counter;
        counter = 0;
      }
    }

    if (widget.searchQuery != null && widget.searchQuery.isNotEmpty) {
      return events
          .where((event) => event.text.toLowerCase().contains(
                widget.searchQuery.toLowerCase(),
              ))
          .toList(growable: false);
    } else {
      return events;
    }
  }

  void _requestHistoryIfScrolledTop() async {
    if (_scrollController.hasClients &&
        _scrollController.offset >=
            _scrollController.position.maxScrollExtent -
                MediaQuery.of(context).size.height &&
        !_scrollController.position.outOfRange) {
      if (timeline.events.isNotEmpty &&
          timeline.events[timeline.events.length - 1].type !=
              EventTypes.RoomCreate) {
        await timeline
            .requestHistory(
              historyCount: widget.historyRequestCount,
            )
            .catchError((e, s) => Logs().w('Request history failed', e, s));
      }
    }
  }

  Future<List<Event>> getList() async {
    if (timeline != null) return filter(timeline.events);
    try {
      timeline = await widget.room.getTimeline(onUpdate: updateView);
    } catch (e, s) {
      await ErrorReporter.reportError(e, s);
      rethrow;
    }

    if (timeline.room.markedUnread) {
      // ignore: unawaited_futures
      timeline.room
          .setUnread(false)
          .catchError((e, s) => Logs().v('Room.setUnread(false) failed', e, s));
    }
    // ignore: unawaited_futures
    Future.delayed(Duration(milliseconds: 500))
        .then((_) => _requestHistoryIfScrolledTop());

    return filter(timeline.events);
  }

  @override
  void initState() {
    _scrollController.addListener(() async {
      if (!mounted) return;
      if (timeline == null) return;
      _requestHistoryIfScrolledTop();

      if (_scrollController.position.pixels > 200 && showArrowDown != 1.0) {
        setState(() {
          showArrowDown = 1.0;
        });
      }
      if (_scrollController.position.pixels < 200 && showArrowDown != 0.0) {
        setState(() {
          showArrowDown = 0.0;
        });
      }
    });

    super.initState();
  }

  final Set<String> _alreadySentReadmarker = {};

  void _updateReadMarker() async {
    if (timeline.events.isEmpty) return;
    final eventId = timeline.events.first.eventId;
    if (!_alreadySentReadmarker.contains(eventId) &&
        timeline != null &&
        timeline.events.isNotEmpty &&
        widget.room.fullyRead != eventId &&
        Famedly.of(context).localNotificationPlugin.webHasFocus &&
        eventId.isValidMatrixId) {
      _alreadySentReadmarker.add(eventId);
      try {
        await widget.room.setReadMarker(
          eventId,
          readReceiptLocationEventId: eventId,
        );
      } catch (e, s) {
        _alreadySentReadmarker.remove(eventId);
        Logs().w('Could not send read marker', e, s);
      }
    }
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    timeline?.sub?.cancel();
    timeline = null;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: getList(),
        builder: (
          BuildContext context,
          AsyncSnapshot<List<Event>> snapshot,
        ) {
          if (snapshot.hasError) {
            return Center(
              child: Text(snapshot.error.toString()),
            );
          }
          if (timeline == null || !snapshot.hasData) {
            return Center(
              child: Material(
                elevation: 10,
                borderRadius: BorderRadius.circular(90),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: CircularProgressIndicator(),
                ),
              ),
            );
          }

          final events = snapshot.data;
          Famedly.of(context).activeRoomID = widget.room.id;

          // Send read marker
          _updateReadMarker();

          var padding = MediaQuery.of(context).size.width -
              (FamedlyStyles.columnWidth * 3.5);
          if (AdaptivePageLayout.of(context).threeColumnMode(context)) {
            padding -= FamedlyStyles.columnWidth;
          }

          if (events.isEmpty) {
            return Center(
              child: Text(
                L10n.of(context).noMessagesFoundYet,
                textAlign: TextAlign.center,
              ),
            );
          }

          return Stack(
            children: <Widget>[
              Scrollbar(
                isAlwaysShown: kIsWeb,
                controller: _scrollController,
                child: ListView.builder(
                  padding: widget.padding ??
                      EdgeInsets.symmetric(
                        horizontal: max(0, padding / 2),
                      ),
                  controller: _scrollController,
                  physics: const AlwaysScrollableScrollPhysics(),
                  reverse: true,
                  itemCount: events.length + 2,
                  itemBuilder: (context, index) => _buildItem(
                    context,
                    events,
                    index - 1,
                  ),
                ),
              ),
              AnimatedOpacity(
                duration: Duration(milliseconds: 250),
                opacity: showArrowDown,
                child: Align(
                  alignment: Alignment.bottomRight,
                  child: Padding(
                    padding: EdgeInsets.only(bottom: 108),
                    child: Container(
                      width: 63,
                      height: 60,
                      decoration: BoxDecoration(
                        color: Theme.of(context).backgroundColor,
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(22),
                          bottomLeft: Radius.circular(22),
                        ),
                        boxShadow: [
                          BoxShadow(
                            color: Theme.of(context)
                                .textTheme
                                .bodyText1
                                .color
                                .withOpacity(0.14),
                            offset: Offset(0, 2),
                            blurRadius: 5,
                            spreadRadius: 0,
                          )
                        ],
                      ),
                      child: Material(
                        borderRadius: BorderRadius.circular(22),
                        color: Theme.of(context).backgroundColor,
                        child: IconButton(
                            icon: Icon(
                              Icons.arrow_drop_down_circle,
                              size: 37.5,
                              color: FamedlyColors.azul,
                            ),
                            onPressed: () {
                              _scrollController.animateTo(
                                  _scrollController.position.minScrollExtent,
                                  duration: Duration(milliseconds: 300),
                                  curve: ElasticOutCurve());
                            }),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          );
        });
  }
}

extension on Event {
  bool get isState => !{
        EventTypes.Message,
        EventTypes.Sticker,
        EventTypes.Encrypted,
        Request.requestStateNameSpace,
      }.contains(type);
}
