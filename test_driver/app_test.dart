// Imports the Flutter Driver API
import 'dart:io';

import 'package:famedly/config/test_user.dart';
import 'package:flutter_driver/flutter_driver.dart';
import 'package:test/test.dart';

void main() {
  Future<void> waitHalfSecond() async {
    await Future.delayed(Duration(milliseconds: 500));
    return;
  }

  Future<void> waitFullSecond() async {
    await Future.delayed(Duration(milliseconds: 1000));
    return;
  }

  Future<bool> isPresent(
      SerializableFinder finder, FlutterDriver driver) async {
    try {
      await driver.waitFor(finder, timeout: const Duration(seconds: 1));
      return true;
    } catch (e) {
      return false;
    }
  }

  group('Famedly App', () {
    FlutterDriver driver;

    final homeserver = TestUser.get().homeserver;
    final username = TestUser.get().username;
    final fullname = TestUser.get().fullname;
    final password = TestUser.get().password;
    final backButtonToolTip = 'Back'; // Set to your preferred language
    final testMessage = '${DateTime.now().millisecondsSinceEpoch}';
    final testTask = '${DateTime.now().millisecondsSinceEpoch}';
    final groupName = '${DateTime.now().millisecondsSinceEpoch}';

    // Connect to the Flutter driver before running any tests
    setUpAll(() async {
      final envVars = Platform.environment;
      final adbPath =
          '${envVars['ANDROID_SDK_ROOT'] ?? '${envVars['HOME']}/Android/Sdk'}/platform-tools/adb';
      final permissionList = <String>[
        'android.permission.READ_EXTERNAL_STORAGE',
        'android.permission.WRITE_EXTERNAL_STORAGE',
        'android.permission.RECORD_AUDIO',
        'android.permission.CAMERA',
        'android.permission.READ_PHONE_STATE',
        'android.permission.CALL_PHONE',
      ];
      for (final permission in permissionList) {
        await Process.run(adbPath, [
          'shell',
          'pm',
          'grant',
          'com.famedly.talk.debug',
          permission,
        ]);
      }
      driver = await FlutterDriver.connect();
    });

    // Close the connection to the driver after the tests have completed
    tearDownAll(() async {
      if (driver != null) {
        await driver.close();
      }
    });

    var securityKeyInClipboard = false;

    final goBack = () async {
      await driver.waitFor(find.byTooltip(backButtonToolTip));
      await driver.tap(find.byTooltip(backButtonToolTip));
    };
    final waitFor = (String key) async {
      // ignore: avoid_print
      print('[Integration Test] Wait for $key');
      await driver.waitFor(find.byValueKey(key));
    };

    final waitForText = (String key) async {
      // ignore: avoid_print
      print('[Integration Test] Wait for text $key');
      await driver.waitFor(find.text(key));
    };

    final tap = (dynamic key) async {
      // ignore: avoid_print
      print('[Integration Test] Tap on $key');
      await driver.waitFor(find.byValueKey(key));
      await driver.tap(find.byValueKey(key));
    };

    final tapOnText = (String text) async {
      // ignore: avoid_print
      print('[Integration Test] Tap on $text');
      await driver.waitFor(find.text(text));
      await driver.tap(find.text(text));
    };

    final enterText = (String text) async {
      // ignore: avoid_print
      print('[Integration Test] Enter text: $text');
      await driver.enterText(text);
    };

    final login = () async {
      await waitFor('organisation_picker_page');
      await tap('organisation');
      await enterText(homeserver);
      await tap('login');
      await waitFor('LoginPage');
      await tap('username');
      await enterText(username);
      await tap('password');
      await enterText(password);
      await tap('login');
      await waitFor('IntroPage');
    };
    final bootstrap = () async {
      await tap('startBootstrapButton');
      await waitHalfSecond();
      if (await isPresent(
          find.byValueKey('copySecurityKeyToClipboardButton'), driver)) {
        await tap('copySecurityKeyToClipboardButton');
        await tapOnText('CONTINUE');
        securityKeyInClipboard = true;
      } else {
        if (securityKeyInClipboard) {
          await tap('pasteSecurityKeyFromClipboardButton');
          await waitHalfSecond();
          await tap('unlockBackupButton');
        } else {
          await tap('resetRecoveryKeyButton');
          await tapOnText('YES');
          await tap('copySecurityKeyToClipboardButton');
          await tapOnText('CONTINUE');
          securityKeyInClipboard = true;
        }
      }
      await tap('finishBootstrapButton');
    };
    final logout = () async {
      await waitFor('rooms_Page');
      await tap('more');
      await waitFor('SettingsPage');
      await tap('SignOutAction');
      await driver.waitFor(find.byType('AlertDialog'));
      await tapOnText('SIGN OUT');
      await waitFor('organisation_picker_page');
      await Future.delayed(Duration(seconds: 2));
    };
    final clearArchive = () async {
      await waitFor('rooms_Page');
      await waitFor('more');
      await tap('more');
      await waitFor('SettingsPage');
      await tap('goToArchive');
      await waitFor('ArchivesPage');
      await tap('PopupMenu');
      await waitFor('Clear');
      await tap('Clear');
      await tapOnText('CANCEL');
      await waitFor('PopupMenu');
      await tap('PopupMenu');
      await waitFor('Clear');
      await tap('Clear');
      await tapOnText('CLEAR');
      await tapOnText('NEXT');
      await goBack();
      await goBack();
    };
    final archiveChatInChatPage = () async {
      await tap('PopupMenu');
      await tap('ArchivePopupMenuItem');
      await tapOnText('YES');
    };
    /* see #607
    var checkSwitch = (String key, bool value) async {
      Map widgetDiagnostics = await driver.getWidgetDiagnostics(
        find.byValueKey(key), includeProperties: true
      );
      expect(widgetDiagnostics['properties']
        .firstWhere((property) => property['name'] == 'value')['value'], value);
    };*/
    // clear tasks and chats to avoid passing multiple elements to tap
    test('Prepare', () async {
      await tap('goToPrivacyPolicy');
      await goBack();
      await tap('goToTermsOfUse');
      await goBack();
      await login();
      await tap('NextButton');
      await goBack();
      await logout();
    });
    test('Login', () async {
      await login();
      await tap('NextButton');
      await bootstrap();
    });
    test('Logout', () async {
      await logout();
    });
    test('Login again', () async {
      await login();
    });
    test('Sentry Switch', () async {
      //await checkSwitch('SentrySwitch', false); see #607
      await tap('SentrySwitch');
      //await checkSwitch('SentrySwitch', true); see #607
      await tap('NextButton');
      await bootstrap();
      await tap('more');
      await waitHalfSecond();
      await driver.scrollIntoView(find.byValueKey('SentrySwitch'));
      //await checkSwitch('SentrySwitch', true); see #607
      await tap('SentrySwitch');
      //await checkSwitch('SentrySwitch', false); see #607
      await tap('SentrySwitch');
      //await checkSwitch('SentrySwitch', true); see #607
      await goBack();
      await logout();
      await login();
      //await checkSwitch('SentrySwitch', true); see #607
      await tap('NextButton');
      await bootstrap();
    });
    test('Push Notifications', () async {
      await tap('more');
      await tap('goToSettingsInfo');
      await driver.scroll(find.byValueKey('SettingsInfoListView'), 0, -1000,
          Duration(milliseconds: 500));
      await tap('inviteTest');
      await driver.tap(find.byType('TextFormField'));
      await enterText('inviter_bot');
      await tapOnText('OK');
      await driver.tap(find.byType('TextFormField'));
      await enterText(TestUser.get().password);
      await tapOnText('OK');
      await waitFor('SettingsPage');
      await goBack();

      await driver.scroll(find.text('You have been invited to chat'), 0, -1,
          Duration(milliseconds: 500));
      await tapOnText('Archive');
      await tapOnText('YES');

      await tap('more');
      await tap('goToSettingsInfo');
      await driver.scroll(find.byValueKey('SettingsInfoListView'), 0, -1000,
          Duration(milliseconds: 500));
      await waitForText('Last received push');
      await goBack();
      await goBack();
    });
    test('Push Gateway', () async {
      await tap('more');
      await tap('goToSettingsInfo');
      await tap('PushTest');
      await waitForText('Push notification test was successful.');
      await goBack();
      await goBack();
    });
    test('Style settings', () async {
      await tap('more');
      await tap('style');
      await tap('System');
      await tap('Light');
      await tap('Dark');
      await tap('background_0');
      await tap('background_1');
      await tap('background_2');
      await tap('background_3');
      await goBack();
      await goBack();
    });
    test('Device settings', () async {
      await tap('more');
      await tap('goToDevices');
      await goBack();
      await goBack();
    });
    test('Test all pages and tabs', () async {
      await waitHalfSecond();
      await tap('NavScaffoldTab-chats-integrations');
      await waitHalfSecond();
      await tap('NavScaffoldTab-chats-messages');
      await waitHalfSecond();
      await tap('requests');
      await waitHalfSecond();
      await tapOnText('My Conversations');
      await goBack();
      await tapOnText('My requests');
      await goBack();
      await tapOnText('Shared with me');
      await goBack();
      await tap('tasks');
      await waitHalfSecond();
      await tap('directory');
      await waitHalfSecond();
      await driver
          .tap(find.byValueKey('NavScaffoldTab-directory-integrations'));
      await waitHalfSecond();
      await driver.tap(find.byValueKey('NavScaffoldTab-directory-roles'));
      await waitHalfSecond();
      await tapOnText('Testrolle 1');
      await tapOnText('CANCEL');
      await tapOnText('Details');
      await waitHalfSecond();
      await driver.scroll(find.byType('ProfileScaffold'), 0, -1000,
          Duration(milliseconds: 500));
      await tapOnText('End shift for this role');
      await waitHalfSecond();
      await tapOnText('YES');
      await waitHalfSecond();
      await tapOnText('Start shift for this role');
      await waitHalfSecond();
      await tapOnText('YES');
      await goBack();
      await tap('more');
      await waitHalfSecond();
      await driver.tap(find.byTooltip(backButtonToolTip));
      await waitHalfSecond();
    });
    test('Test patient rooms', () async {
      final patientLastName = '${DateTime.now().millisecondsSinceEpoch}';
      await tap('NavScaffoldTab-chats-patients');
      await tap('default_floating_action_button');
      await tap('create_new_patient_button');
      await tap('caseId');
      await enterText('123456');
      await tap('firstName');
      await enterText('Patient');
      await tap('lastName');
      await enterText(patientLastName);
      await tap('setBirthDate');
      await tapOnText('OK');
      await driver.scroll(find.byValueKey('patientSettingsListView'), 0, -1000,
          Duration(milliseconds: 500));
      await tap('other');
      await tap('save');
      await tapOnText(TestUser.get().username2);
      await tapOnText('YES');
      await goBack();

      await driver.tap(find.text('Patient $patientLastName'));
      await tap('GoToPatientsSettings');
      await tap('caseId');
      await enterText('99999');
      await tap('lastName');
      await enterText('NineNine');
      await tap('save');
      await tap('GoToPatientsSettings');
      await driver.waitFor(find.text('NineNine'));
      await tap('cancelPatientSettingsButton');

      await tap('input');
      await enterText(testMessage);
      await tap('sendButton');

      await archiveChatInChatPage();
      await tap('NavScaffoldTab-chats-messages');
    });
    test('Create direct chat', () async {
      await waitFor('rooms_Page');
      await tap('default_floating_action_button');
      await tap('start_new_conversation_button');
      await waitFor('UsersPage');
      if (!(await isPresent(find.text(TestUser.get().username2), driver))) {
        await driver.scroll(find.byValueKey('UsersListView'), 0, -1000,
            Duration(milliseconds: 500));
      }
      await tapOnText(TestUser.get().username2);
      await tap('ChatProfileScaffoldButton');
      await waitFor('chat_room');
      await goBack();
      await waitFor('rooms_Page');
      await tapOnText('Messages');
      await tapOnText(TestUser.get().username2);
      await waitFor('chat_room');
      // send message
      await tap('input');
      await enterText(testMessage);
      await tap('sendButton');
      // forward
      await tap('MessageBubble');
      await waitFor('EventActionsForward');
      await tap('EventActionsForward');
      await waitFor('share_page');
      await tap('CloseForwardModeButton');
      await driver.tap(find.text(TestUser.get().username2));
      // search
      await tap('PopupMenu');
      await tap('SearchPopupMenuItem');
      await waitFor('EventSearchPage');
      await tap('searchBar');
      await enterText(testMessage);
      await driver.waitFor(find.text(testMessage));
      await goBack();
      //read receipts
      await tap('MessageBubble');
      await tap('readSoFarPopupEntry');
      await waitFor('readSoFarHeader');
      //our own read receipt takes a bit to show up
      await waitHalfSecond();
      await driver.waitFor(find.text(fullname));
      await tap('backButton');
      // send voice message
      // scroll with (0,0) offset is a longpress
      await driver.scroll(find.byValueKey('MicrophoneButton'), 0, 0,
          Duration(milliseconds: 1500));
      // wait for message to be sent... we should not have a play button until
      // it did but we currently do
      await waitFullSecond();
      await tap('PlayButton');
      await tap('PauseButton');
      await tap('PlayButton');
      await tap('PauseButton');
      // clear cache and reload
      await goBack();
      await tap('more');
      await waitFor('SettingsPage');
      await tap('goToSettingsInfo');
      await driver.scroll(find.byValueKey('SettingsInfoListView'), 0, -1000,
          Duration(milliseconds: 500));
      await tap('clearCacheAndReload');
      await tapOnText('YES');
      await waitFor('rooms_Page');
      await tapOnText('Messages');
      await tapOnText(TestUser.get().username2);
      await waitFor('chat_room');
      // archive chat
      await tap('PopupMenu');
      await tap('ArchivePopupMenuItem');
      await tapOnText('CANCEL');
      await waitFor('PopupMenu');
      await tap('ChatRoomAppBar');
      await waitFor('UserViewer');
      await tap('NotificationOnProfileScaffoldButton');
      await tap('NotificationOffProfileScaffoldButton');
      await tap('ChatProfileScaffoldButton');
      await tap('PopupMenu');
      await tap('ChatDetailsPopupMenuItem');
      await waitFor('UserViewer');
      await goBack();
      await archiveChatInChatPage();
    });
    test('Create group chat and test chat details', () async {
      await waitFor('rooms_Page');
      await tap('default_floating_action_button');
      await tap('start_new_conversation_button');
      await waitFor('NewGroupButton');
      await tap('NewGroupButton');
      await waitFor('NewGroupPage');
      // Should do nothing until at least one user is selected...
      await tap('StartGroupChat');
      await tap('newGroupTextField');
      await enterText(groupName);

      if (!(await isPresent(find.text(TestUser.get().username3), driver))) {
        await driver.scroll(find.byValueKey('UsersListView'), 0, -1000,
            Duration(milliseconds: 500));
      }
      await tapOnText(TestUser.get().username3);
      await tap('StartGroupChat');
      await waitFor('chat_room');
      await goBack();
      await waitFor('rooms_Page');
      await driver.tap(find.text(groupName));
      await waitFor('chat_room');
      await tap('input');
      await enterText(testMessage);
      await tap('sendButton');
      await tap('MessageBubble');
      await waitFor('EventActionsAddToTasks');
      await tap('EventActionsAddToTasks');
      await tap('MessageBubble');
      await waitFor('EventActionsForward');
      await tap('EventActionsForward');
      await waitFor('share_page');
      await driver.tap(find.text(groupName));
      await tap('ChatRoomAppBar');
      await waitFor('RoomSettings');
      await tap('NotificationOnProfileScaffoldButton');
      await tap('NotificationOffProfileScaffoldButton');
      await tap('ChatProfileScaffoldButton');
      await tap('PopupMenu');
      await tap('ChatDetailsPopupMenuItem');
      await waitFor('RoomSettings');
      await tap('GoToMembersPage');
      await driver.waitFor(find.text(TestUser.get().username3));
      await tap('GoToInvitePage');
      await waitFor('InvitePage');
      /*await tap('NavScaffoldTab-directory-roles');
      await waitHalfSecond();
      await tap('NavScaffoldTab-directory-organisations');
      await waitHalfSecond();
      await tap('NavScaffoldTab-directory-integrations');
      await waitHalfSecond();
      await tap('NavScaffoldTab-directory-people');*/
      await waitHalfSecond();
      if (!(await isPresent(find.text(TestUser.get().username2), driver))) {
        await driver.scroll(find.byValueKey('UsersListView'), 0, -1000,
            Duration(milliseconds: 500));
      }
      /*await tapOnText(TestUser.get().username2);
      await tapOnText('CANCEL');*/
      await tapOnText(TestUser.get().username2);
      await tapOnText('YES');
      await goBack();
      await tapOnText(TestUser.get().username2);
      await waitFor('UserViewer');
      await tap('userSettingsPopupMenu');
      await tap('UserSettingsPopupMenuKickButton');
      await tapOnText('YES');
      await waitFor('UserViewer');
      await goBack();
      await waitFor('MemberListPage');
      await goBack();
      await waitFor('RoomSettings');
      await tap('PopupMenu');
      await tap('EditChatNamePopupMenuItem');
      await driver.tap(find.byType('TextFormField'));
      await driver.enterText(groupName);
      await tapOnText('Set');
      await goBack();
      await archiveChatInChatPage();
    });
    test('Test tasks', () async {
      await tap('tasks');
      await waitFor('tasks_page');
      await driver.waitFor(find.text(testMessage));
      await tap('TagListCheckBox');
      await tap('default_floating_action_button');
      await tap('add_task_button');
      await waitFor('AddTaskModal');
      await tap('NewTaskTextField');
      await enterText(testTask);
      await tap('ShowDescTextFieldIconButton');
      await tap('DescriptionTextField');
      await enterText('Test description');
      await tap('CreateTextButton');
      await tap('TaskListTile');
      await waitFor('taskPage');
      await tap('DescriptionTextField');
      await enterText('Another Test description');
      await goBack();
      await driver.waitFor(find.text('Another Test description'));
      await tap('TaskListTile');
      await waitFor('taskPage');
      await tap('SubTaskTextField');
      await enterText('New Subtask');
      await goBack();
      await driver.waitFor(find.text('New Subtask'));
      await tap('TaskListTile');
      await waitFor('taskPage');
      await tap('DeleteIconButton');
      await tap('DoneListTile');
      await driver.waitFor(find.text(testMessage));
      await tap('DoneListTile');
      await driver.waitForAbsent(find.text(testMessage));
      await tap('PopupMenu');
      await tap('ClearPopupMenuItem');
      await tap('chats');
    });
    test('Test and clear archive', () async {
      await waitFor('rooms_Page');
      await waitFor('more');
      await tap('more');
      await waitFor('SettingsPage');
      await tap('goToArchive');
      await waitFor('ArchivesPage');
      // The group may already be purged by a parallel test
      if (await isPresent(find.text(groupName), driver)) {
        await driver.tap(find.text(groupName));
        await waitFor('chat_room');
        await driver.waitForAbsent(find.byValueKey('input'));
      }
      await goBack();
      await waitFor('rooms_Page');
      await clearArchive();
    });
    test('Logout', () async {
      await logout();
    });
  }, timeout: Timeout(Duration(minutes: 2)));
}
