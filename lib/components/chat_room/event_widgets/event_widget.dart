import 'dart:math';

import 'package:famedly/components/chat_room/event_widgets/event_avatar.dart';
import 'package:famedly/components/chat_room/event_widgets/event_title.dart';
import 'package:famedly/styles/famedly_icons_v2_icons.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';

import '../../../styles/colors.dart';
import '../../../utils/date_time_extension.dart';
import '../../../utils/event_forwarded_extension.dart';
import '../../../models/famedly.dart';
import 'message_bubble.dart';

/// The main Event Widget logic
class EventWidget extends StatelessWidget {
  /// The user who sent the message
  final Event event;

  final Event prevEvent;
  final Event nextEvent;
  final Room room;

  final EdgeInsets padding;

  final bool hasBubble;
  final bool doubleBubble;

  /// Builder that returns a content [Widget] for this [Event].
  final Widget content;

  /// Called when the user chooses to reply to this event
  final VoidCallback onReply;

  static const bubblePadding = EdgeInsets.only(
    top: 12,
    bottom: 12,
    left: 14,
    right: 14,
  );

  static const double defaultRadius = 18.0;
  static const double noneRadius = 1.0;

  static double getPageWidth(BuildContext context) =>
      MediaQuery.of(context).size.width > FamedlyStyles.columnWidth * 2
          ? MediaQuery.of(context).size.width - FamedlyStyles.columnWidth
          : MediaQuery.of(context).size.width;

  static double getMaxWidth(BuildContext context, bool columnMode,
          {bool maxMode = true}) =>
      min(
          columnMode
              ? FamedlyStyles.columnWidth + (maxMode ? 300 : 0)
              : FamedlyStyles.columnWidth,
          getPageWidth(context) - 95);

  static bool hasSameSender(Event a, Event b) {
    return a != null &&
        a.type == 'm.room.message' &&
        a.sender.id == b.sender.id;
  }

  EventWidget(
    this.event, {
    @required this.content,
    Key key,
    this.room,
    this.prevEvent,
    this.nextEvent,
    // TODO: CHECk bubble
    this.hasBubble = true,
    this.doubleBubble = false,
    this.onReply,
    this.padding,
  }) : super(key: key);

  static Color messageBubbleColor(
    BuildContext context,
    Event event,
    Famedly famedly,
  ) {
    if (event.isMine) {
      return FamedlyColors.darkGrey;
    }
    return Theme.of(context).backgroundColor;
  }

  static Border messageBubbleBorder(
      BuildContext context, Event event, bool isMe) {
    final defaultColor = Theme.of(context).brightness == Brightness.light
        ? FamedlyColors.lightBlueyGrey
        : FamedlyColors.slate;
    if (event.status == 0) {
      return Border.all(width: 1, color: defaultColor);
    } else if (event.status == -1) {
      return Border.all(width: 1, color: FamedlyColors.grapefruit);
    } else if (!isMe) {
      return Border.all(width: 0.5, color: defaultColor);
    } else {
      return Border.all(width: 0, color: Colors.transparent);
    }
  }

  @override
  Widget build(BuildContext context) {
    final prevEventSameSender = hasSameSender(prevEvent, event);
    final nextEventSameSender = hasSameSender(nextEvent, event);

    var topMargin = 10.0;
    var bottomMargin = 10.0;
    if (prevEventSameSender) {
      topMargin = 1;
    }
    if (nextEventSameSender || nextEvent == null) {
      bottomMargin = 1;
    }

    return Container(
      margin: EdgeInsets.only(
        top: topMargin,
        bottom: bottomMargin,
        left: 15.0,
        right: 15.0,
      ),
      child: Column(
        children: <Widget>[
          if (prevEvent == null ||
              !event.originServerTs.sameEnvironment(prevEvent.originServerTs))
            Container(
              padding: EdgeInsets.all(10.0),
              child: Text(
                event.originServerTs.localizedTime(context),
                style: TextStyle(
                  fontSize: 14.0,
                  color: FamedlyColors.slateGrey,
                ),
              ),
            ),
          EventTitle(
              event: event,
              display:
                  !(prevEventSameSender || event.isMine || room.isDirectChat)),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              EventAvatar(
                event: event,
                room: room,
                display: !prevEventSameSender && !event.isMine,
              ),
              Container(width: 10),
              Expanded(
                child: Container(
                  alignment: event.isMine
                      ? Alignment.centerRight
                      : Alignment.centerLeft,
                  child: MessageBubble(
                    event: event,
                    room: room,
                    onReply: onReply,
                    content: content,
                    hasBubble: hasBubble,
                    doubleBubble: doubleBubble,
                    padding: padding,
                  ),
                ),
              ),
            ],
          ),
          _Status(event: event),
        ],
      ),
    );
  }
}

class _Status extends StatelessWidget {
  final Event event;

  const _Status({Key key, @required this.event}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    if (event.status == -1) {
      return Padding(
        padding: EdgeInsets.symmetric(vertical: 5, horizontal: 0),
        child: Row(mainAxisAlignment: MainAxisAlignment.end, children: <Widget>[
          Icon(
            FamedlyIconsV2.info,
            color: FamedlyColors.grapefruit,
            size: 14,
          ),
          Container(width: 5, height: 5),
          Text(
            '${L10n.of(context).error}:',
            style: TextStyle(
              color: FamedlyColors.grapefruit,
              fontSize: 13,
            ),
          ),
          Container(width: 2, height: 5),
          InkWell(
            onTap: () {
              event.sendAgain();
            },
            child: Text(
              L10n.of(context).sendAgain,
              style: TextStyle(
                color: FamedlyColors.slateGrey,
                decoration: TextDecoration.underline,
                fontSize: 13,
              ),
            ),
          )
        ]),
      );
    }
    return Container();
  }
}
