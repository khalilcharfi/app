/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'package:flutter_gen/gen_l10n/l10n.dart';
import '../styles/colors.dart';

class DateDisplay extends StatelessWidget {
  final DateTime date;

  DateDisplay(this.date);

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(5)),
            border: Border.all(color: FamedlyColors.cloudyBlue)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.all(6),
              child: Icon(Icons.calendar_today,
                  color: FamedlyColors.azul, size: 12),
            ),
            Padding(
                padding: EdgeInsets.only(right: 6),
                child: Text(
                    DateFormat(L10n.of(context).localTimeFormat).format(date),
                    style: TextStyle(
                        color: FamedlyColors.cloudyBlue, fontSize: 13)))
          ],
        ));
  }
}
