/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'dart:async';
import 'dart:io';

import 'package:adaptive_page_layout/adaptive_page_layout.dart';
import 'package:famedly/components/chat_backup_header.dart';
import 'package:famedly/components/default_floating_action_button.dart';
import 'package:famedly/components/notifications_muted_header.dart';
import 'package:famedly/styles/famedly_icons_v2_icons.dart';
import 'package:famedly/utils/error_reporter.dart';
import 'package:famedly/utils/platform_extension.dart';
import 'package:famedly/utils/voip/callkeep_manager.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:future_loading_dialog/future_loading_dialog.dart';
import 'package:receive_sharing_intent/receive_sharing_intent.dart';
import 'package:uni_links/uni_links.dart';

import '../components/connection_status_header.dart';
import '../models/famedly.dart';
import '../components/matrix_chat_list.dart';
import '../components/nav_scaffold.dart';
import '../models/room_type_extension.dart';
import '../styles/colors.dart';

/// This wraps the Tab view into a view.
/// It includes the AppBar as well as the Tab View and the Fab Menu
class RoomsPage extends StatefulWidget {
  final String activeChatID;

  const RoomsPage({this.activeChatID});

  Future<bool> waitForFirstSync(Famedly famedly) async {
    if (famedly.client.prevBatch != null) return true;
    return famedly.client.onFirstSync.stream.first;
  }

  @override
  State<StatefulWidget> createState() => RoomsPageState();
}

class RoomsPageState extends State<RoomsPage> {
  final filterTextController = TextEditingController();

  StreamSubscription _intentDataStreamSubscription;
  StreamSubscription _intentFileStreamSubscription;
  StreamSubscription _intentUriStreamSubscription;

  void _processIncomingUris(String text) async {
    if (text == null || text.isEmpty) return;
    if (text.startsWith(Famedly.inviteLinkPrefix)) {
      final userId =
          Uri.decodeComponent(text.replaceAll(Famedly.inviteLinkPrefix, ''));
      if (userId.sigil == '@' &&
          userId.isValidMatrixId &&
          userId != Famedly.of(context).client.userID) {
        final response = await showFutureLoadingDialog(
            context: context,
            future: () => User(userId,
                    room: Room(id: '!', client: Famedly.of(context).client))
                .startDirectChat());
        if (response.error == null) {
          await AdaptivePageLayout.of(context)
              .pushNamedAndRemoveUntilIsFirst('/room/${response.result}');
        }
      }
    }
  }

  void processIncomingSharedText(String text) async {
    if (text == null || text.isEmpty) return;
    if (text.startsWith(Famedly.magicLinkPrefix) ||
        text.startsWith(Famedly.inviteLinkPrefix)) {
      // Uris are handled bei open_uri but may accidentally land here too
      return;
    }
    Famedly.of(context).forwardContent = {
      'msgtype': 'm.text',
      'body': text,
    };
    await AdaptivePageLayout.of(context)
        .pushNamedAndRemoveUntilIsFirst('/share');
  }

  void processIncomingSharedFiles(List<SharedMediaFile> value) async {
    if (value == null || value.isEmpty) return;

    // strip the 'file://' prefix, since that breaks sharing of files on iOS
    final file = File(value.first.path.replaceFirst('file://', ''));
    final matrixFile = MatrixFile(
      bytes: file.readAsBytesSync(),
      name: file.path,
    );
    Famedly.of(context).forwardFile = matrixFile.mimeType.startsWith('image')
        ? MatrixImageFile(
            bytes: file.readAsBytesSync(),
            name: file.path,
          )
        : matrixFile;
    await AdaptivePageLayout.of(context)
        .pushNamedAndRemoveUntilIsFirst('/share');
  }

  void _initReceiveSharingContent() {
    if (!kIsMobile) return;

    // For receiving shared Uris
    _intentUriStreamSubscription = linkStream.listen(_processIncomingUris,
        onError: ErrorReporter.reportError);
    getInitialLink().then(_processIncomingUris);

    // For sharing images coming from outside the app while the app is in the memory
    _intentFileStreamSubscription = ReceiveSharingIntent.getMediaStream()
        .listen(processIncomingSharedFiles, onError: ErrorReporter.reportError);

    // For sharing images coming from outside the app while the app is closed
    ReceiveSharingIntent.getInitialMedia().then(processIncomingSharedFiles);

    // For sharing or opening text coming from outside the app while the app is in the memory
    _intentDataStreamSubscription = ReceiveSharingIntent.getTextStream()
        .listen(processIncomingSharedText, onError: ErrorReporter.reportError);

    // For sharing or opening text coming from outside the app while the app is closed
    ReceiveSharingIntent.getInitialText().then(processIncomingSharedText);
  }

  @override
  void initState() {
    super.initState();
    _initReceiveSharingContent();
    if (!kIsWeb) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        CallKeepManager().setVoipPlugin(Famedly.of(context).voipPlugin);
        CallKeepManager().initialize().catchError((_) => true);
      });
    }
  }

  @override
  void dispose() {
    super.dispose();
    _intentDataStreamSubscription?.cancel();
    _intentFileStreamSubscription?.cancel();
    _intentUriStreamSubscription?.cancel();
  }

  static int currentTabIndex = 0;

  final ScrollController _scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    Famedly.of(context).localNotificationPlugin?.reinit();

    Famedly.of(context).appOutdatedPlugin.checkIfOutdated();

    final famedly = Famedly.of(context);

    return DefaultTabController(
      length: 3,
      initialIndex: currentTabIndex,
      child: NavScaffold(
        key: Key('rooms_Page'),
        floatingActionButton: DefaultFloatingActionButton(
          scrollController: _scrollController,
        ),
        leading: Padding(
          padding: EdgeInsets.only(
            left: 15,
            top: 15,
          ),
          child: IconButton(
            key: Key('SettingsButton'),
            icon: Icon(FamedlyIconsV2.settings),
            iconSize: 24,
            color: FamedlyColors.slate,
            onPressed: () {
              AdaptivePageLayout.of(context).pushNamed('/settings');
            },
          ),
        ),
        actions: <Widget>[
          Padding(
            padding: EdgeInsets.only(
              right: 15, top: 15, //bottom: 15
            ),
            child: IconButton(
              key: Key('NotificationsButton'),
              icon: Icon(FamedlyIconsV2.on),
              iconSize: 24,
              color: FamedlyColors.slate,
              onPressed: () {
                // FIXME Not implemented
              },
            ),
          ),
        ],
        activePage: MainPage.chats,
        filterTextController: filterTextController,
        body: StreamBuilder(
            stream: famedly.client.onCacheCleared.stream,
            builder: (context, snapshot) {
              return FutureBuilder<bool>(
                future: widget.waitForFirstSync(famedly),
                builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
                  if (snapshot.hasData) {
                    return Column(
                      children: [
                        ConnectionStatusHeader(),
                        NotificationsMutedHeader(),
                        ChatBackupHeader(),
                        Expanded(
                          child: TabBarView(
                            children: [
                              MatrixChatList(
                                filterTextController,
                                emptyListPlaceholder: Center(
                                    child: Text(L10n.of(context).noChats)),
                                activeChatID: widget.activeChatID,
                                scrollController: _scrollController,
                                filter: (Room room) =>
                                    room.type != roomTypeRequest &&
                                    room.membership != Membership.leave &&
                                    room.type == roomTypeMessaging &&
                                    room.directChatMatrixID !=
                                        Famedly.covidBotId,
                              ),
                              MatrixChatList(
                                filterTextController,
                                emptyListPlaceholder: Center(
                                    child: Text(L10n.of(context).noChats)),
                                activeChatID: widget.activeChatID,
                                scrollController: _scrollController,
                                filter: (Room room) =>
                                    room.type != roomTypeRequest &&
                                    room.membership != Membership.leave &&
                                    room.type == roomTypePatient,
                              ),
                              MatrixChatList(
                                filterTextController,
                                emptyListPlaceholder: Center(
                                    child: Text(L10n.of(context).noChats)),
                                activeChatID: widget.activeChatID,
                                scrollController: _scrollController,
                                filter: (Room room) =>
                                    room.type != roomTypeRequest &&
                                    room.membership != Membership.leave &&
                                    room.directChatMatrixID ==
                                        Famedly.covidBotId,
                              ),
                            ],
                          ),
                        ),
                      ],
                    );
                  } else {
                    return Container(
                      child: Center(
                        child: CircularProgressIndicator(),
                      ),
                    );
                  }
                },
              );
            }),
      ),
    );
  }
}
