/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:adaptive_page_layout/adaptive_page_layout.dart';
import 'package:famedly/components/chat_backup_header.dart';
import 'package:famedly/components/settings_list_group.dart';
import 'package:famedly/styles/famedly_icons_v2_icons.dart';
import 'package:famedly/utils/famedly_file_picker.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:file_picker_cross/file_picker_cross.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:future_loading_dialog/future_loading_dialog.dart';

import '../models/famedly.dart';
import '../components/profile_scaffold.dart';
import '../components/sentry_switch_list_tile.dart';
import '../components/settings_menu_item.dart';
import '../styles/colors.dart';
import '../styles/famedly_icons_icons.dart';
import '../utils/matrix_file_thumbnail_extension.dart';

class SettingsPage extends StatefulWidget {
  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  void logoutDialog(BuildContext context) async {
    if (OkCancelResult.ok ==
        await showOkCancelAlertDialog(
          context: context,
          title: L10n.of(context).areYouSureYouWantToSignOut,
          okLabel: L10n.of(context).signOut,
          cancelLabel: L10n.of(context).cancel,
          useRootNavigator: false,
        )) {
      Famedly.of(context).loginPlugin.logout();
    }
  }

  @override
  Widget build(BuildContext context) {
    final famedly = Famedly.of(context);
    return StreamBuilder<Object>(
        stream: famedly.client.onSync.stream,
        builder: (context, snapshot) {
          return FutureBuilder<Profile>(
              future: famedly.client.ownProfile,
              builder: (context, snapshot) {
                final displayname = snapshot.data?.displayname ??
                    famedly.client.userID?.localpart ??
                    L10n.of(context).yourSessionIsNoLongerValid;
                final avatarUrl =
                    snapshot.hasData ? snapshot.data.avatarUrl : Uri.parse('');
                return ProfileScaffold(
                  automaticallyImplyLeading:
                      !AdaptivePageLayout.of(context).columnMode(context),
                  key: Key('SettingsPage'),
                  title: Text(
                    L10n.of(context).settings,
                  ),
                  profileName: displayname,
                  profileContent: avatarUrl,
                  actions: <Widget>[
                    IconButton(
                      key: Key('SignOutAction'),
                      icon: Icon(
                        FamedlyIcons.logout,
                        color: FamedlyColors.blueyGrey,
                      ),
                      onPressed: () {
                        Famedly.of(context).analyticsPlugin.trackEventWithName(
                            'Settings', 'logout button', 'clicked');
                        logoutDialog(context);
                      },
                    ),
                  ],
                  onAvatarTap: () async {
                    MatrixFile file;
                    final tempFile = await FamedlyFilePicker.pickFiles(
                      type: FileTypeCross.image,
                      context: context,
                    );
                    if (tempFile == null) return;
                    try {
                      file = await MatrixImageFile(
                        bytes: tempFile.toUint8List(),
                        name: tempFile.path,
                      ).createThumbnail();
                    } catch (e, s) {
                      Logs().d('Could not create thumbnail for avatar', e, s);
                      file = MatrixImageFile(
                        bytes: tempFile.toUint8List(),
                        name: tempFile.path,
                      );
                    }

                    if (file != null) {
                      final success = await showFutureLoadingDialog(
                          context: context,
                          future: () => famedly.client.setAvatar(file));
                      if (success.error == null) {
                        AdaptivePageLayout.of(context).pop();
                      }
                    }
                  },
                  body: Column(
                    children: <Widget>[
                      SettingsListGroup(children: [
                        ChatBackupHeader(),
                        ListTile(
                          leading: Icon(
                            FamedlyIconsV2.profile,
                            key: Key('goToSettingsInfo'),
                            color: FamedlyColors.blueyGrey,
                          ),
                          title: Text(
                            L10n.of(context).loggedInAs(displayname),
                          ),
                          onTap: () => AdaptivePageLayout.of(context)
                              .pushNamed('/settings/info'),
                        ),
                      ]),
                      SettingsListGroup(
                        title: L10n.of(context).settings,
                        children: <Widget>[
                          SettingsMenuItem(
                            Icon(
                              FamedlyIconsV2.on,
                              key: Key('goToNotificationSettings'),
                              color: FamedlyColors.blueyGrey,
                            ),
                            L10n.of(context).notifications,
                            Container(),
                            () => AdaptivePageLayout.of(context)
                                .pushNamed('/settings/notifications'),
                          ),
                          SettingsMenuItem(
                            Icon(
                              FamedlyIconsV2.style,
                              key: Key('style'),
                              color: FamedlyColors.blueyGrey,
                            ),
                            'Style',
                            Container(),
                            () {
                              AdaptivePageLayout.of(context)
                                  .pushNamed('/settings/style');
                            },
                          ),
                          SettingsMenuItem(
                            Icon(
                              FamedlyIconsV2.devices,
                              key: Key('goToDevices'),
                              color: FamedlyColors.blueyGrey,
                            ),
                            L10n.of(context).devices,
                            Container(),
                            () {
                              AdaptivePageLayout.of(context)
                                  .pushNamed('/settings/devices');
                            },
                          ),
                          SettingsMenuItem(
                            Icon(
                              FamedlyIconsV2.archive,
                              key: Key('goToArchive'),
                              color: FamedlyColors.blueyGrey,
                            ),
                            L10n.of(context).archive,
                            Container(),
                            () {
                              AdaptivePageLayout.of(context)
                                  .pushNamed('/chatArchive');
                            },
                          ),
                          SettingsMenuItem(
                            Icon(
                              FamedlyIconsV2.search,
                              key: Key('goToUserDirectory'),
                              color: FamedlyColors.blueyGrey,
                            ),
                            L10n.of(context).userSearch,
                            Container(),
                            () {
                              AdaptivePageLayout.of(context)
                                  .pushNamed('/settings/userDirectory');
                            },
                          ),
                          SettingsMenuItem(
                            Icon(
                              FamedlyIconsV2.directory,
                              key: Key('goToUserDirectory'),
                              color: FamedlyColors.blueyGrey,
                            ),
                            L10n.of(context).myRoles,
                            Container(),
                            () {
                              AdaptivePageLayout.of(context)
                                  .popAndPushNamed('/directory/roles');
                            },
                          ),
                          SettingsMenuItem(
                            Icon(
                              FamedlyIconsV2.password,
                              key: Key('goToSecurityPage'),
                              color: FamedlyColors.blueyGrey,
                            ),
                            L10n.of(context).changePassword,
                            Container(),
                            () {
                              AdaptivePageLayout.of(context)
                                  .pushNamed('/settings/security');
                            },
                          ),
                          if (!kIsWeb)
                            SettingsMenuItem(
                              Icon(
                                FamedlyIconsV2.lock,
                                key: Key('goToAppLockPage'),
                                color: FamedlyColors.blueyGrey,
                              ),
                              L10n.of(context).setAppLock,
                              Container(),
                              () {
                                AdaptivePageLayout.of(context)
                                    .pushNamed('/settings/applock');
                              },
                            ),
                        ],
                      ),
                      SettingsListGroup(
                        title: L10n.of(context).aboutFamedly,
                        children: [
                          SettingsMenuItem(
                            Icon(
                              FamedlyIcons.security,
                              color: FamedlyColors.blueyGrey,
                            ),
                            L10n.of(context).privacyPolicy,
                            Container(),
                            () {
                              AdaptivePageLayout.of(context)
                                  .pushNamed('/documents/privacyPolicy');
                            },
                          ),
                          SettingsMenuItem(
                            Icon(
                              FamedlyIconsV2.info,
                              color: FamedlyColors.blueyGrey,
                            ),
                            L10n.of(context).termsOfUse,
                            Container(),
                            () {
                              AdaptivePageLayout.of(context)
                                  .pushNamed('/documents/termsOfUse');
                            },
                          ),
                          SettingsMenuItem(
                            Icon(
                              FamedlyIconsV2.licence,
                              color: FamedlyColors.blueyGrey,
                            ),
                            L10n.of(context).license,
                            Container(),
                            () => showLicensePage(
                              context: AdaptivePageLayout.of(context).context,
                              applicationIcon: Image.asset(
                                'assets/images/famedly_icon_transparent.png',
                                width: 32,
                                height: 32,
                              ),
                              applicationName: 'Famedly',
                            ),
                          ),
                          SentrySwitchListTile(),
                        ],
                      ),
                    ],
                  ),
                );
              });
        });
  }
}
