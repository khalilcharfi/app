/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'package:famedly/components/search_bar.dart';
import 'package:famedly/styles/famedly_icons_v2_icons.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:adaptive_page_layout/adaptive_page_layout.dart';

import '../components/user_list.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import '../styles/colors.dart';

class ContactsPage extends StatefulWidget {
  @override
  _ContactsPageState createState() => _ContactsPageState();
}

class _ContactsPageState extends State<ContactsPage> {
  final TextEditingController controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: Key('UsersPage'),
      appBar: AppBar(
        leading: BackButton(),
        titleSpacing: 0,
        title: Container(
          height: 40,
          padding: const EdgeInsets.only(
            right: 8.0,
          ),
          child: Hero(
            tag: 'user_search_view',
            child: SearchBar(
              labelText: L10n.of(context).search,
              filterTextController: controller,
            ),
          ),
        ),
      ),
      floatingActionButton: kIsWeb
          ? null
          : FloatingActionButton(
              onPressed: () =>
                  AdaptivePageLayout.of(context).pushNamed('/newContact'),
              key: Key('NewContactButton'),
              child: Icon(Icons.qr_code),
            ),
      body: UserList(
        searchController: controller,
        header: Column(
          children: <Widget>[
            ListTile(
              key: Key('NewGroupButton'),
              title: Text(L10n.of(context).newGroup),
              leading: Padding(
                padding: const EdgeInsets.all(2.0),
                child: CircleAvatar(
                  backgroundColor: Colors.white,
                  child: Icon(
                    Icons.group,
                    color: FamedlyColors.azul,
                  ),
                ),
              ),
              trailing: Icon(FamedlyIconsV2.right_1),
              onTap: () =>
                  AdaptivePageLayout.of(context).pushNamed('/newGroup'),
            ),
          ],
        ),
      ),
    );
  }
}
