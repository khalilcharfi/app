# Release Cycle

* Famedly has a two-week release cycle starting November 2nd, 2020
* Releases are always on monday 11am at the frontend sync meeting
* Feature freeze is at the friday before at 10pm
* Every release is a GitLab milestone with a set of issues which must be solved
before the release (blocking issues)
* When collecting issues for the next milestone, issues with the label
`Priority::High` should be preferred
* If there are still blocking issues, the release (milestone) must be postponed
to a new date - This should not affect the date of the next release

# Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, 
see the [tags on this repository](https://gitlab.com/famedly/app/tags).


# Critical Label
* If there is a bug which must be solved and released as soon as 
possible, it must be labeled as `Critical`
* A bugfix release should be provided in a new protected branch (`stable`)
created from the point of the last release tag
* The bug needs to be fixed and backported to the latest release on the stable
branch
* The team releases by hand from the stable branch

# Manual tests
Everything that can't be tested automatically should be tested by hand
before each release. Currently this includes:

### Android or iOS:
* [ ]  QR code login
* [ ]  User Directory
* [ ]  Start a new request
* [ ]  Broadcasts

### Android:
* [ ]  File picker and camera picker
* [ ]  Send and listen voice messages
* [ ]  Push notifications
* [ ]  QR code scanning
* [ ]  Receive sharing text/files
* [ ]  Download files
* [ ]  Test app lock

### iOS:
* [ ]  File picker and camera picker
* [ ]  Send and listen voice messages
* [ ]  Push notifications
* [ ]  QR code scanning
* [ ]  Receive sharing text/files
* [ ]  Download files
* [ ]  Test app lock

### Web:
* [ ]  Basic functionality
* [ ]  Drag&Drop
* [ ]  Notifications
* [ ]  Download files

# Steps to release

1.  Plan the release! Think about which version of which components are needed, which features needs to be implemented and create a milestone and open an issue [here](https://gitlab.com/famedly/ansible/collections/local/-/issues/new).
2.  Implement the new features and wait for the new versions of the components you need
3.  Run the `changelog` task on master manually to update the Changelog.md file
4.  Update the version in `pubspec.yaml` and `electron/package.json`
5.  Create a new tag (`v0.X.0`) and add into the lower text field the content 
of the CHANGELOG.md, that is corresponding to the current version.
6.  Wait until the pipeline finishes

# Manual release with Google Play
1.  Open the [Google Play Console](https://play.google.com/apps/publish/) and navigate to the alpha track
2.  Click change What new
3.  Add a summary of what changed between the language tags. The changelog can't be longer than 500 characters per language.

## Formatting help:
Bullet Points: `&#8226;&#8195;[ENTRY]`

Bold (Version Title): `<b>[0.1.0] - 2019-07-30</b>`

Italic (Type of changes): `<i>Added</i>` 


An example changelog would look like this for Google Play:
```
<de-DE>
<b>[0.1.0] - 2019-07-30</b>
<i>Hinzugefügt</i>

&#8226;&#8195;Wähle deinen Server
&#8226;&#8195;Melde dich mit deinem existierendem Account an und ab
&#8226;&#8195;Erstelle und betrete private Chats
&#8226;&#8195;Erstelle und betrete Gruppenchats
&#8226;&#8195;Verwalte Gruppenchats
&#8226;&#8195;Sende Text Nachrichten
&#8226;&#8195;Sende emojis
&#8226;&#8195;Zeige deine Chaträume an
&#8226;&#8195;Archiviere deine Chat Räume
&#8226;&#8195;Verwalte dein Archiv
&#8226;&#8195;Benachrichtigungen
</de-DE>

<en-US>
<b>[0.1.0] - 2019-07-30</b>
<i>Added</i>

&#8226;&#8195;Choose your server
&#8226;&#8195;Login to your existing account / Logout
&#8226;&#8195;Create and join private chats
&#8226;&#8195;Create and join group chats
&#8226;&#8195;Manage group chats
&#8226;&#8195;Send textmessages 
&#8226;&#8195;Send emojis
&#8226;&#8195;List your chat rooms
&#8226;&#8195;Archive your chat rooms
&#8226;&#8195;Manage your archive
&#8226;&#8195;Push notifications
</en-US>
```

# Manual release with iOS AppStore

1. Build the app locally with:
```
./scripts/build-ios.sh
```

2. Open xcode and archive the project
3. Upload the archive
4. Create a new release in the Apple Developer Account and provide a changelog
but make sure that you remove everything regarding to other platforms
