/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'package:famedly/components/role_list_tile.dart';
import 'package:famedly/models/roles/role_entity.dart';
import 'package:famedly/styles/colors.dart';
import 'package:famedly/views/invite_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';

class RoleList extends StatefulWidget {
  final List<RoleEntity> roles;
  final OnInviteCallback onInvite;
  final TextEditingController filterController;

  const RoleList({
    Key key,
    @required this.roles,
    this.onInvite,
    this.filterController,
  }) : super(key: key);
  @override
  _RoleListState createState() => _RoleListState();
}

class _RoleListState extends State<RoleList> {
  String _searchQuery = '';

  void _updateSearch() {
    if (!mounted) return;
    setState(() => _searchQuery = widget.filterController.text);
  }

  @override
  void initState() {
    super.initState();
    widget.filterController?.addListener(_updateSearch);
  }

  @override
  void dispose() {
    widget.filterController?.removeListener(_updateSearch);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final rolesToDisplay =
        widget.roles.where((r) => r.name.contains(_searchQuery)).toList();
    if (rolesToDisplay.isEmpty) {
      // return placeholder
      return Center(child: Text(L10n.of(context).noRoles));
    }
    return ListView.separated(
      shrinkWrap: true,
      itemCount: rolesToDisplay.length,
      separatorBuilder: (context, i) => Divider(
        indent: 70,
        height: 1,
        thickness: 1,
        color: Theme.of(context).brightness == Brightness.light
            ? FamedlyColors.paleGrey
            : FamedlyColors.slate,
      ),
      itemBuilder: (context, index) => RoleListTile(
        role: rolesToDisplay[index],
        allowOptOut: false,
        detailsButton: true,
        onInvite: widget.onInvite,
      ),
    );
  }
}
