import 'dart:convert';

import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:famedly_user_directory_client_sdk/famedly_user_directory_client_sdk.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';

extension UserDirectoryClientExtension on UserDirectoryClient {
  Future<UserData> requestUserDataFromUuid(BuildContext context, String uuid,
      {String secret}) async {
    try {
      return await requestUserDataByUuid(uuid, secret: secret);
    } catch (e) {
      String errorStr;
      try {
        errorStr = jsonDecode(
            e.toString().replaceFirst('Exception: ', '').trim())['msg'];
      } catch (_) {}
      errorStr ??= e.toString();
      if (errorStr != 'Secret needed') {
        throw errorStr;
      }
      final input = await showTextInputDialog(
        context: context,
        textFields: [
          DialogTextField(hintText: L10n.of(context).enterPassphrase)
        ],
        useRootNavigator: false,
        okLabel: L10n.of(context).next,
        cancelLabel: L10n.of(context).cancel,
      );
      if (input?.isEmpty ?? true) throw errorStr;
      return await requestUserDataFromUuid(context, uuid, secret: input.single);
    }
  }
}
