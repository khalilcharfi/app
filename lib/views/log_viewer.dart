/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/material.dart';

class LogViewer extends StatefulWidget {
  @override
  _LogViewerState createState() => _LogViewerState();
}

class _LogViewerState extends State<LogViewer> {
  Level logLevel = Level.debug;
  double fontSize = 14;
  @override
  Widget build(BuildContext context) {
    final outputEvents = Logs()
        .outputEvents
        .where((e) => e.level.index >= logLevel.index)
        .toList();
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        title: Text(logLevel.toString()),
        leading: BackButton(),
        actions: [
          IconButton(
            icon: Icon(Icons.zoom_in_outlined),
            onPressed: () => setState(() => fontSize++),
          ),
          IconButton(
            icon: Icon(Icons.zoom_out_outlined),
            onPressed: () => setState(() => fontSize--),
          ),
          PopupMenuButton<Level>(
            itemBuilder: (context) => Level.values
                .map((level) => PopupMenuItem(
                      value: level,
                      child: Text(level.toString()),
                    ))
                .toList(),
            onSelected: (Level level) => setState(() => logLevel = level),
          ),
        ],
      ),
      body: ListView.builder(
        itemCount: outputEvents.length,
        itemBuilder: (context, i) => SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Text(outputEvents[i].toDisplayString()),
        ),
      ),
    );
  }
}

extension on LogEvent {
  String toDisplayString() {
    var str = '# $title';
    if (exception != null) {
      str += ' - ${exception.toString()}';
    }
    if (stackTrace != null) {
      str += '\n${stackTrace.toString()}';
    }
    return str;
  }
}
