# Famedly

**This project is part of the source code of Famedly.**

We think that software for healthcare should be open source, so we publish most 
parts of our source code at [gitlab.com/famedly](https://gitlab.com/famedly).

Please read CONTRIBUTING.md for details on our code of
conduct, and the process for submitting pull requests to us.

For licensing information of this project, have a look at the LICENSE.md
file within the repository.

If you compile the open source software that we make available to develop your
own mobile, desktop or embeddable application, and cause that application to
connect to our servers for any purposes, you have to aggree to our Terms of
Service. In short, if you choose to connect to our servers, certain restrictions
apply as follows:  

* You agree not to change the way the open source software connects and
interacts with our servers
* You agree not to weaken any of the security features of the open source software
* You agree not to use the open source software to gather data
* You agree not to use our servers to store data for purposes other than
the intended and original functionality of the Software
* You acknowledge that you are solely responsible for any and all updates to
your software

No license is granted to the Famedly trademark and its associated logos, all of
which will continue to be owned exclusively by Famedly GmbH. Any use of the
Famedly trademark and/or its associated logos is expressly prohibited without
the express prior written consent of Famedly GmbH.

For more
information take a look at [Famedly.com](https://famedly.com) or contact
us by [info@famedly.com](mailto:info@famedly.com?subject=[GitLab]%20More%20Information%20)

---

# Famedly App

Flutter App for medical staff to connect to our decentralized care-team
collaboration platform.

- [Google Playstore](https://play.google.com/store/apps/details?id=com.famedly.talk)
- [iOS Appstore](https://apps.apple.com/de/app/famedly/id1459847644)
- [APK Download](https://gitlab.com/famedly/app/-/jobs/artifacts/main/browse?job=buildandroid:release)

## Getting Started

These instructions will help you to get a copy of the project up an running on
your local machine for development and testing purposes.

## Installing

You need to install the latest version of [Flutter](https://flutter.dev/docs/get-started/install).
We recommend MacOs as operating system, since all platforms can be developed
there. If you develop on Linux or Windows, you cannot build iOS of our app.

### Platform setup for Android:
1. Setup [Flutter for Android](https://flutter.dev/docs/get-started/install/macos#install-android-studio)
2. Start an emulator or connect your device
3. Install CMake from the SDK Manager -> Tools

### Platform setup for iOS:
1. Setup [Flutter for iOS](https://flutter.dev/docs/get-started/install/macos#ios-setup)
2. Launch the simulator from spotlight

### Platform setup for Web:
1. Enable flutter web:
```
flutter config --enable-web
```
2. Download web dependencies
```
./scritpts/prepare-web.sh
```

### Platform setup for Desktop:
1. Setup [Flutter for desktop](https://flutter.dev/desktop)

2. Install the required dependencies

#### Install dependencies on MacOS:
```
brew install cmake ninja libolm
```

#### Platform setup for Linux:

##### Debian/Ubuntu:
1. Install sqlite and other dependencies:
```
sudo apt install libsqlite3-dev
```

2. Install libolm 3.2.1
2.1 Package manager (if available)
```
sudo apt install libolm3
```

2.2 [Or alternatively compile from source](https://gitlab.matrix.org/matrix-org/olm)

## Building the Famedly App

#### Clone the repo with submodules:
```
git clone git@gitlab.com:famedly/app.git
cd app
```

#### Run the app:
Connected to a device you can now build and run the app with:
```
flutter run
```

## Running tests

### Widget Tests
```
flutter test test
```

### Instrumental Tests

Create a `test-credentials.json` file in the project root which contains the following:
```json
{
  "homeserver": "yourserver",
  "username": "youruser",
  "password": "yourpassword",
  "username2": "otheruser",
  "username3": "yetanotheruser"
}
```

Then run:
```
flutter drive --target=test_driver/app.dart
```

## Acknowledgments

See also the list of [contributors](https://gitlab.com/famedly/app/-/graphs/main)
who participated in this project.

