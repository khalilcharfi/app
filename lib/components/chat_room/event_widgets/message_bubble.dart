import 'dart:math';

import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:adaptive_page_layout/adaptive_page_layout.dart';
import 'package:famedly/components/FamedlyPopupMenuButton.dart';
import 'package:famedly/components/chat_room/event_widgets/event_avatar.dart';
import 'package:famedly/components/chat_room/event_widgets/event_title.dart';
import 'package:famedly/components/dialogs/read_receipt_dialog.dart';
import 'package:famedly/models/tasks/tasklist.dart';
import 'package:famedly/styles/colors.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:future_loading_dialog/future_loading_dialog.dart';
import 'package:keep_keyboard_popup_menu/keep_keyboard_popup_menu.dart';
import 'package:pedantic/pedantic.dart';

import '../../../models/famedly.dart';
import '../../../utils/date_time_extension.dart';
import '../../../utils/event_forwarded_extension.dart';
import 'event_widget.dart';

class MessageBubble extends StatelessWidget {
  final Event event;
  final Room room;
  final VoidCallback onReply;
  final bool isForward;

  final bool hasBubble;
  final bool doubleBubble;

  final EdgeInsets padding;
  final Widget content;

  const MessageBubble({
    Key key,
    @required this.event,
    this.room,
    this.onReply,
    this.isForward = false,
    this.hasBubble,
    this.doubleBubble,
    this.padding,
    this.content,
  })  : assert(event != null),
        super(key: key);

  Future<Profile> _forwardedFrom(Famedly famedly, Event event) {
    final String userId = event.content['com.famedly.app.forward']['sender'];
    return famedly.client.getProfileFromUserId(userId);
  }

  List<KeepKeyboardPopupMenuItem> getPopupList(
      BuildContext context, ClosePopupFn closePopup) {
    // move to SDK as soon as MSC will have been accepted
    Map<String, dynamic> forwardContent(Event event) {
      final content = Map<String, dynamic>.from(event.content);
      content['com.famedly.app.forward'] = {
        'sender': event.senderId,
        'event_id': event.eventId,
        'room_id': event.roomId,
        'origin_server_ts': event.originServerTs.millisecondsSinceEpoch,
      };
      return content;
    }

    final list = <KeepKeyboardPopupMenuItem>[
      KeepKeyboardPopupMenuItem(
        child: Text(
          '${L10n.of(context).receivedAt(event.originServerTs.localizedTime(context))}',
          style: TextStyle(
            fontStyle: FontStyle.italic,
            color: FamedlyColors.blueyGrey,
          ),
        ),
      ),
      KeepKeyboardPopupMenuItem(
        key: Key('readSoFarPopupEntry'),
        onTap: () async {
          unawaited(closePopup());
          await showDialog(
            context: context,
            useRootNavigator: false,
            builder: (c) => ReadReceiptDialog(event),
          );
        },
        child: Text(
          L10n.of(context).readSoFar,
        ),
      ),
    ];
    if (!event.redacted) {
      list.add(
        KeepKeyboardPopupMenuItem(
          key: Key('EventActionsForward'),
          onTap: () async {
            unawaited(closePopup());
            Famedly.of(context).forwardContent = forwardContent(event);
            await AdaptivePageLayout.of(context)
                .pushNamedAndRemoveUntilIsFirst('/share');
          },
          child: Text(L10n.of(context).forward),
        ),
      );
      if ([MessageTypes.Text, MessageTypes.Emote, MessageTypes.Notice]
          .contains(event.messageType)) {
        list.add(
          KeepKeyboardPopupMenuItem(
            key: Key('EventActionsCopy'),
            onTap: () async {
              unawaited(closePopup());
              await Clipboard.setData(ClipboardData(text: event.body));
            },
            child: Text(L10n.of(context).copy),
          ),
        );
        list.add(
          KeepKeyboardPopupMenuItem(
            key: Key('EventActionsAddToTasks'),
            onTap: () async {
              unawaited(closePopup());
              final title =
                  event.body.substring(0, min<int>(200, event.body.length));
              final newTask = await showFutureLoadingDialog(
                  context: context,
                  future: () => TaskList(Famedly.of(context).client).create(
                      notificationTitle: L10n.of(context).upcomingTODO(title),
                      title: title,
                      description:
                          '${event.sender.calcDisplayname(mxidLocalPartFallback: false)} - ${event.originServerTs.localizedTimeOfDay(context)}'));
              if (newTask.error == null) {
                AdaptivePageLayout.of(context).showSnackBar(
                  SnackBar(
                    content: Text(L10n.of(context).addedToTasks),
                  ),
                );
              }
            },
            child: Text(L10n.of(context).addToTasks),
          ),
        );
      }
      if (event.status == -1) {
        list.add(
          KeepKeyboardPopupMenuItem(
            key: Key('EventActionsDelete'),
            onTap: () async {
              unawaited(closePopup());
              await event.remove();
            },
            child: Text(L10n.of(context).deleteMessage),
          ),
        );
      }
      if (event.canRedact && event.status > 0) {
        list.add(
          KeepKeyboardPopupMenuItem(
            key: Key('EventActionsRedact'),
            onTap: () async {
              unawaited(closePopup());
              if (OkCancelResult.ok ==
                  await showOkCancelAlertDialog(
                    context: context,
                    title: L10n.of(context).deleteMessageForAllParticipants,
                    okLabel: L10n.of(context).yes,
                    cancelLabel: L10n.of(context).cancel,
                    useRootNavigator: false,
                  )) {
                await showFutureLoadingDialog(
                  context: context,
                  future: () => event.redactEvent(),
                );
              }
            },
            child: Text(L10n.of(context).deleteMessage),
          ),
        );
      }
      if (onReply != null &&
          room.canSendDefaultMessages &&
          room.membership == Membership.join) {
        list.add(
          KeepKeyboardPopupMenuItem(
            key: Key('EventActionsReply'),
            onTap: () async {
              unawaited(closePopup());
              if (onReply != null) {
                onReply();
              }
            },
            child: Text(L10n.of(context).reply),
          ),
        );
      }
    }
    return list;
  }

  @override
  Widget build(BuildContext context) {
    final famedly = Famedly.of(context);
    final isForward = this.isForward ?? event.isForward;
    final bubbleDecoration = BoxDecoration(
      color: EventWidget.messageBubbleColor(context, event, famedly),
      borderRadius: BorderRadius.circular(8),
      border: EventWidget.messageBubbleBorder(context, event, event.isMine),
    );

    return Opacity(
      opacity: event.status > 0 ? 1 : 0.75,
      child: FutureBuilder<Profile>(
          future: isForward ? _forwardedFrom(Famedly.of(context), event) : null,
          builder: (context, AsyncSnapshot<Profile> snapshot) {
            return WithKeepKeyboardPopupMenu(
              menuItemBuilder: (context, closePopup) =>
                  getPopupList(context, closePopup),
              backgroundBuilder: defaultPopupMenuBackgroundBuilder,
              childBuilder: (context, openPopup) => InkWell(
                splashColor: FamedlyColors.slate,
                borderRadius: BorderRadius.circular(8),
                key: Key('MessageBubble'),
                onTap: () {
                  if (event.messageType == MessageTypes.Image) {
                    AdaptivePageLayout.of(context).pushNamed(
                      '/room/imageEventView/${event.room.id}/${event.eventId}',
                    );
                  } else if (event.messageType == MessageTypes.Video) {
                    final video = Uri.parse(event.content['url'] ?? '');
                    AdaptivePageLayout.of(context).pushNamed(
                      "/room/videoView/${video == null ? "" : video.toString().replaceFirst("mxc://", "")}",
                    );
                  } else {
                    openPopup();
                  }
                },
                onLongPress: openPopup,
                child: hasBubble
                    ? Container(
                        constraints: BoxConstraints(
                            maxWidth: EventWidget.getMaxWidth(
                          context,
                          AdaptivePageLayout.of(context).columnMode(context),
                        )),
                        padding: padding ??
                            (!doubleBubble
                                ? EventWidget.bubblePadding
                                : const EdgeInsets.only(
                                    top: 2,
                                    bottom: 11,
                                    left: 2,
                                    right: 2,
                                  )),
                        decoration: !isForward ? bubbleDecoration : null,
                        child: isForward && snapshot.data != null
                            ? _ForwardedMessage(
                                event: event,
                                room: room,
                                originalSender: snapshot.data,
                              )
                            : content,
                      )
                    : Container(
                        decoration: !isForward ? bubbleDecoration : null,
                        constraints: BoxConstraints(
                            maxWidth: EventWidget.getMaxWidth(
                          context,
                          AdaptivePageLayout.of(context).columnMode(context),
                        )),
                        child: isForward && snapshot.data != null
                            ? _ForwardedMessage(
                                event: event,
                                room: room,
                                originalSender: snapshot.data,
                              )
                            : ClipRRect(
                                borderRadius: BorderRadius.circular(8),
                                child: content),
                      ),
              ),
            );
          }),
    );
  }
}

class _ForwardedMessage extends StatelessWidget {
  final Event event;
  final Profile originalSender;
  final Room room;

  const _ForwardedMessage({
    Key key,
    this.event,
    this.originalSender,
    this.room,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(children: <Widget>[
      EventTitle(
          event: event,
          display: true,
          displayname: originalSender.displayname,
          sentDateTime: DateTime.fromMillisecondsSinceEpoch(
              event.content['com.famedly.app.forward']['origin_server_ts'])),
      Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          EventAvatar(
              event: event,
              room: room,
              display: true,
              displayname: originalSender.displayname,
              avatarUrl: originalSender.avatarUrl),
          Container(width: 10),
          Expanded(
            child: Container(
              alignment: Alignment.centerRight,
              child: MessageBubble(
                isForward: false,
                event: event,
                room: event.room,
              ),
            ),
          ),
        ],
      ),
    ]);
  }
}
