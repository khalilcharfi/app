/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'package:famedly/styles/famedly_icons_v2_icons.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:adaptive_page_layout/adaptive_page_layout.dart';

import '../components/broadcast_list.dart';
import '../models/famedly.dart';
import '../components/nav_scaffold.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import '../styles/colors.dart';

/// This wraps the Tab view into a view.
/// It includes the AppBar as well as the Tab View and the Fab Menu
class BroadcastListPage extends StatefulWidget {
  final String requestId;
  final String activeChatId;

  BroadcastListPage({
    Key key,
    @required this.requestId,
    this.activeChatId,
  }) : super(key: key);

  @override
  BroadcastListPageState createState() => BroadcastListPageState();
}

class BroadcastListPageState extends State<BroadcastListPage> {
  Future<bool> waitForFirstSync(Famedly famedly) async {
    if (famedly.client.prevBatch != null) return true;
    return famedly.client.onFirstSync.stream.first;
  }

  final filterTextController = TextEditingController();
  String title;

  @override
  Widget build(BuildContext context) {
    title ??= L10n.of(context).broadcast;
    final famedly = Famedly.of(context);
    final chatList = BroadcastList(
      filterTextController,
      widget.requestId,
      activeChatId: widget.activeChatId,
      setTitle: (String newTitle) {
        if (newTitle != title) {
          SchedulerBinding.instance.addPostFrameCallback((_) => setState(() {
                title = newTitle;
              }));
        }
      },
      emptyListPlaceholder: Center(
        child: Text(L10n.of(context).noInquiries),
      ),
    );

    return NavScaffold(
      key: Key('BroadcastPage'),
      activePage: MainPage.requests,
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          AdaptivePageLayout.of(context).pushNamed('/directory');
        },
        backgroundColor: FamedlyColors.azul,
        foregroundColor: Colors.white,
        child: Icon(FamedlyIconsV2.add),
      ),
      appBar: AppBar(
        title: Text(title),
        leading: BackButton(),
      ),
      filterTextController: filterTextController,
      body: FutureBuilder<bool>(
        future: waitForFirstSync(famedly),
        builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
          if (snapshot.hasData) {
            return chatList;
          } else {
            return Container(
              color: Theme.of(context).backgroundColor,
              child: Center(
                child: CircularProgressIndicator(),
              ),
            );
          }
        },
      ),
    );
  }
}
