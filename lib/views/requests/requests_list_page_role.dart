/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'package:famedly/views/requests/requests_list_page_role_view.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/material.dart';
import 'package:famedly/models/famedly.dart';
import 'package:famedly/models/requests/request.dart';
import 'package:famedly/models/roles/client_extension.dart';
import 'package:famedly/models/roles/role_entity.dart';

/// This wraps the Tab view into a view.
/// It includes the AppBar as well as the Tab View and the Fab Menu
class RequestsListPageRole extends StatefulWidget {
  final int id;
  final RoleEntity role;
  final String activeChatId;

  const RequestsListPageRole(this.id, {Key key, this.role, this.activeChatId})
      : super(key: key);
  @override
  RequestsListPageRoleController createState() =>
      RequestsListPageRoleController();
}

class RequestsListPageRoleController extends State<RequestsListPageRole> {
  Future<bool> waitForFirstSync() async {
    final famedly = Famedly.of(context);
    if (famedly.client.prevBatch != null) return true;
    return famedly.client.onFirstSync.stream.first;
  }

  final filterTextController = TextEditingController();
  RoleEntity loadedFuture;
  Future<RoleEntity> getLoadedFuture() async {
    final famedly = Famedly.of(context);
    if (loadedFuture == null) {
      if (widget.role != null && widget.role.id == widget.id) {
        loadedFuture = await Future<RoleEntity>.value(widget.role);
        return loadedFuture;
      } else {
        loadedFuture = await famedly.client.getRole(widget.id);
        return loadedFuture;
      }
    }
    return loadedFuture;
  }

  bool requestBundleFilter(RequestBundle bundle, RoleEntity role) {
    if (bundle.isOwn()) {
      return false;
    }
    final room = bundle.broadcastRoom ?? bundle.rooms.first;
    if (room == null) {
      return false;
    }
    final user = room.getState('m.room.member', role.mxid)?.asUser;
    return user != null && user.membership == Membership.join;
  }

  @override
  Widget build(BuildContext context) {
    return RequestsListPageRoleView(this);
  }
}
