/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/material.dart';

import '../styles/colors.dart';

class UnreadBubble extends StatelessWidget {
  const UnreadBubble(
    this.rooms, {
    Key key,
    this.color = FamedlyColors.grapefruit,
    this.showNumbers = true,
  }) : super(key: key);

  final List<Room> rooms;
  final bool showNumbers;
  final Color color;

  static const double padding = 2.0;
  static const double size = 20.0;

  @override
  Widget build(BuildContext context) {
    var count = 0;
    for (final room in rooms) {
      count += room.notificationCount;
      // also count manually marked unread rooms
      if (room.notificationCount == 0 && room.isUnread) count++;
      if (room.membership == Membership.invite) count++;
    }
    if (count > 99) count = 99;

    if (count > 0) {
      return Container(
        margin: EdgeInsets.all(padding),
        decoration: BoxDecoration(
          color: color,
          borderRadius: BorderRadius.all(Radius.circular(size)),
        ),
        width: showNumbers ? size : size / 2,
        height: showNumbers ? size : size / 2,
        child: showNumbers
            ? Center(
                child: Text(
                  count.toString(),
                  key: key,
                  style: TextStyle(
                    color: Theme.of(context).backgroundColor,
                    fontSize: 10,
                    fontWeight: FontWeight.w600,
                    fontStyle: FontStyle.normal,
                    letterSpacing: 0,
                  ),
                ),
              )
            : Container(),
      );
    }
    return showNumbers
        ? Container(
            width: 1,
            height: size,
            margin: EdgeInsets.all(padding),
          )
        : Container(
            width: 0,
            height: 0,
          );
  }
}
