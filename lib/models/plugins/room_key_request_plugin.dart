import 'dart:async';

import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:famedly/components/dialogs/key_verification_dialog.dart';
import 'package:famedlysdk/encryption.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/material.dart';

import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'famedly_plugin.dart';
import '../../utils/beautify_string_extension.dart';

class RoomKeyRequestplugin extends FamedlyPlugin {
  RoomKeyRequestplugin(matrix) : super(matrix) {
    roomKeyRequestListener(famedly.client);
    keyVerificationRequestListener(famedly.client);
  }

  static bool locked = false;

  void roomKeyRequestListener(Client client) async {
    await for (final request in client.onRoomKeyRequest.stream) {
      while (locked) {
        await Future.delayed(Duration(seconds: 5));
      }
      await onRoomKeyRequestListener(request);
    }
  }

  void keyVerificationRequestListener(Client client) async {
    await for (final request in client.onKeyVerificationRequest.stream) {
      while (locked) {
        await Future.delayed(Duration(seconds: 5));
      }
      await onKeyVerificationRequest(request);
    }
  }

  Map<String, String> validatedDevices = {};

  Future<void> onRoomKeyRequestListener(RoomKeyRequest request) async {
    final room = request.room;
    final sender = room.getUserByMXIDSync(request.sender);
    var accepted = false;
    if (room == null ||
        sender == null ||
        request.requestingDevice == null ||
        (sender.displayName?.isEmpty ?? true)) {
      return;
    }
    if (request.requestingDevice?.deviceId != null &&
        validatedDevices[request.requestingDevice?.deviceId] ==
            request.requestingDevice?.curve25519Key) {
      accepted = true;
    } else {
      await showDialog(
        context: famedly.context,
        useRootNavigator: false,
        builder: (context) => AlertDialog(
          title: Text(L10n.of(famedly.context).requestToReadOlderMessages),
          content: Text(
              "${sender.displayName}\n\n${L10n.of(famedly.context).id}:\n${request.requestingDevice?.deviceId ?? request.content["requesting_device_id"]}\n\n${L10n.of(famedly.context).key}:\n${request.requestingDevice?.curve25519Key?.beautified ?? ""}"),
          actions: <Widget>[
            TextButton(
              onPressed: () =>
                  Navigator.of(context, rootNavigator: false).pop(),
              child: Text(L10n.of(famedly.context).cancel),
            ),
            TextButton(
              onPressed: () {
                accepted = true;
                if (request.requestingDevice?.deviceId != null) {
                  validatedDevices[request.requestingDevice?.deviceId] =
                      request.requestingDevice?.curve25519Key;
                }
                Navigator.of(context, rootNavigator: false).pop();
              },
              child: Text(L10n.of(famedly.context).verify),
            ),
          ],
        ),
      );
    }
    if (accepted) {
      await request.forwardKey();
    }
    return;
  }

  Future<void> onKeyVerificationRequest(KeyVerification request) async {
    var hidPopup = false;
    request.onUpdate = () {
      if (!hidPopup &&
          {KeyVerificationState.done, KeyVerificationState.error}
              .contains(request.state)) {
        Navigator.of(famedly.context, rootNavigator: true).pop('dialog');
      }
      hidPopup = true;
    };
    if (await showOkCancelAlertDialog(
          context: famedly.context,
          title: L10n.of(famedly.context).newVerificationRequest,
          message:
              L10n.of(famedly.context).askVerificationRequest(request.userId),
          useRootNavigator: false,
        ) ==
        OkCancelResult.ok) {
      request.onUpdate = null;
      hidPopup = true;
      await request.acceptVerification();
      await KeyVerificationDialog(
        request: request,
        context: famedly.context,
      ).show(famedly.context);
    } else {
      request.onUpdate = null;
      hidPopup = true;
      await request.rejectVerification();
    }
  }
}
