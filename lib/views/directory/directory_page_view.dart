/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import 'package:famedly/components/avatar.dart';
import 'package:famedly/components/facility_list.dart';
import 'package:famedly/components/nav_scaffold.dart';
import 'package:famedly/components/role_list.dart';
import 'package:famedly/components/settings_list_group.dart';
import 'package:famedly/components/user_list.dart';
import 'package:famedly/models/famedly.dart';
import 'package:famedly/models/roles/role_entity.dart';
import 'package:famedly/styles/colors.dart';
import 'package:famedly/views/directory/directory_page.dart';
import 'package:famedly_directory_client_sdk/famedly_directory_client_sdk.dart';
import 'package:famedlysdk/famedlysdk.dart' hide Tag;
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';

class DirectoryPageView extends StatelessWidget {
  final DirectoryPageController controller;

  const DirectoryPageView(this.controller, {Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      initialIndex: controller.widget.initialTabIndex,
      length: 4,
      child: NavScaffold(
          needsWebBack: true,
          filterTextController: controller.filterTextController,
          activePage: MainPage.directory,
          inlineView: controller.widget.inlineView,
          body: TabBarView(
            children: <Widget>[
              UserList(
                searchController: controller.filterTextController,
                roomId: Famedly.of(context).activeRoomID,
                // if null value is returned it is handled in user_list.dart
                // Don't try doing Clouse ()=> null here, that will give a
                // different result.
                onTap: controller.widget.onInvite != null
                    ? (Profile profile, {String organisationName}) =>
                        controller.userListOnTap(profile)
                    : null,
              ),
              FutureBuilder<List<RoleEntity>>(
                future: controller.getRoles,
                builder: (BuildContext context,
                    AsyncSnapshot<List<RoleEntity>> snapshot) {
                  if (snapshot.hasError) {
                    var errorMsg = snapshot.error.toString();
                    if (snapshot.error is MatrixException) {
                      final matrixError = snapshot.error as MatrixException;
                      switch (matrixError.error) {
                        case MatrixError.M_UNRECOGNIZED:
                          errorMsg = L10n.of(context).rolesNotConfigured;
                          break;
                        case MatrixError.M_FORBIDDEN:
                          errorMsg = L10n.of(context).rolesNotAllowed;
                          break;
                        default:
                          break; // ignore the default
                      }
                    }
                    if (snapshot.error is FormatException) {
                      errorMsg = L10n.of(context).rolesNotReachable;
                    }
                    return Center(
                      child: Text(
                        errorMsg,
                        textAlign: TextAlign.center,
                      ),
                    );
                  }
                  if (!snapshot.hasData) {
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  }
                  return RoleList(
                    filterController: controller.filterTextController,
                    roles: snapshot.data..removeWhere((r) => !r.privateInvite),
                    onInvite: controller.widget.onInvite,
                  );
                },
              ),
              Material(
                color: Theme.of(context).secondaryHeaderColor,
                child: Column(
                  children: <Widget>[
                    Container(
                      color: Theme.of(context).scaffoldBackgroundColor,
                      height: 46,
                      child: FutureBuilder<List<Subtype>>(
                        future: controller.getSubtypes(),
                        builder: (BuildContext context, snapshot) {
                          if (!snapshot.hasData) {
                            return Center(child: CircularProgressIndicator());
                          }
                          final subtypes = controller.selectedTag.isNotEmpty
                              ? controller.subtypeList
                                  .where((s) => s.id == controller.selectedTag)
                                  .toList()
                              : controller.subtypeList;

                          return Scrollbar(
                            isAlwaysShown: kIsWeb,
                            controller: controller.scrollController,
                            child: ListView.builder(
                                controller: controller.scrollController,
                                itemCount: subtypes.length,
                                scrollDirection: Axis.horizontal,
                                itemBuilder: (context, index) {
                                  return Container(
                                    margin: EdgeInsets.all(5),
                                    child: Material(
                                      color: controller.selectedTag ==
                                              subtypes[index].id
                                          ? FamedlyColors.azul
                                          : Theme.of(context).selectedRowColor,
                                      borderRadius: BorderRadius.circular(19),
                                      child: InkWell(
                                        borderRadius: BorderRadius.circular(19),
                                        onTap: () =>
                                            controller.selectSubtypeOnTap(
                                                subtypes[index]),
                                        child: Padding(
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 16, vertical: 10),
                                          child: Text(
                                            subtypes[index].description,
                                            style: TextStyle(
                                              color: controller.selectedTag ==
                                                      subtypes[index].id
                                                  ? Theme.of(context)
                                                      .backgroundColor
                                                  : Theme.of(context)
                                                      .textTheme
                                                      .bodyText1
                                                      .color,
                                              fontSize: 13,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  );
                                }),
                          );
                        },
                      ),
                    ),
                    Expanded(
                      child: SettingsListGroup(
                        children: <Widget>[
                          Expanded(
                            child: FacilityList(
                              controller.filterTextController,
                              subtypeList: controller.subtypeList,
                              tag: controller.selectedTag,
                              onInvite: controller.widget.onInvite,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              ListView(
                children: <Widget>[
                  ListTile(
                    leading: Avatar(
                        Uri.parse(
                            'mxc://shortestpath.dev/gKSerLYhLEsdOmcHGPrCxEar'),
                        name: 'CovBot'),
                    title: Text('CovBot'),
                    subtitle: Text(
                      controller.doesSupportCovid19Bot
                          ? L10n.of(context).currentFiguresForCovid19
                          : L10n.of(context).notAvailableOnAppleDevices,
                      style: TextStyle(
                        color: controller.doesSupportCovid19Bot
                            ? null
                            : FamedlyColors.grapefruit,
                      ),
                    ),
                    onTap: controller.doesSupportCovid19Bot
                        ? controller.covBotOnTap
                        : null,
                  ),
                ],
              ),
            ],
          )),
    );
  }
}
