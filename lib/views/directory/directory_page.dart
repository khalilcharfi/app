/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import 'dart:io';

import 'package:adaptive_page_layout/adaptive_page_layout.dart';
import 'package:famedly/models/famedly.dart';
import 'package:famedly/models/roles/role_entity.dart';
import 'package:famedly/views/directory/directory_page_view.dart';
import 'package:famedly/views/invite_page.dart';
import 'package:famedly_directory_client_sdk/famedly_directory_client_sdk.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:famedly/models/roles/client_extension.dart';
import 'package:future_loading_dialog/future_loading_dialog.dart';

class DirectoryPage extends StatefulWidget {
  final int initialTabIndex;
  final OnInviteCallback onInvite;
  final bool inlineView;

  DirectoryPage(
      {this.initialTabIndex = 2,
      this.onInvite,
      this.inlineView = false,
      Key key})
      : super(key: key);

  @override
  DirectoryPageController createState() => DirectoryPageController();
}

class DirectoryPageController extends State<DirectoryPage> {
  final TextEditingController filterTextController = TextEditingController();
  String selectedTag = '';
  List<Subtype> subtypeList;
  Famedly famedly;
  final scrollController = ScrollController();

  @override
  void setState(fn) {
    if (!mounted) return;
    super.setState(fn);
  }

  @override
  void initState() {
    famedly = Famedly.of(context);
    getRoles ??= famedly.client.getAllRoles('role');
    getRoles.catchError((_) => null);
    super.initState();
  }

  Future<List<RoleEntity>> getRoles;
  bool get doesSupportCovid19Bot => (kIsWeb || !Platform.isIOS);
  Future<List<Subtype>> getSubtypes() async {
    if (subtypeList != null) {
      return subtypeList;
    }
    try {
      final directory =
          await Famedly.of(context).getOrganizationDirectoryClient();
      final newList = await directory.getSubtypes();

      if (mounted) {
        subtypeList = newList;
      }

      return newList;
    } catch (_) {
      subtypeList = [];
      return [];
    }
  }

  void userListOnTap(Profile profile, {String organisationName}) {
    widget.onInvite(InviteType.person, profile.userId);
  }

  void covBotOnTap() async {
    if (widget.onInvite != null) {
      widget.onInvite(InviteType.integration, Famedly.covidBotId);
    }
    final roomID = await showFutureLoadingDialog(
      context: context,
      future: () => User(
        Famedly.covidBotId,
        room: Room(
          id: '',
          client: famedly.client,
        ),
      ).startDirectChat(),
    );
    await AdaptivePageLayout.of(context).pushNamedAndRemoveUntilIsFirst(
      '/room/${roomID.result}',
    );
  }

  void selectSubtypeOnTap(Subtype subType) {
    setState(() {
      if (selectedTag == subType.id) {
        selectedTag = '';
      } else {
        selectedTag = subType.id;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return DirectoryPageView(this);
  }
}
