class UserAvailability {
  final bool available;
  final List<int> roles;

  UserAvailability({
    this.available,
    this.roles,
  });

  UserAvailability.fromJson(Map<String, dynamic> json)
      : available = json['available'],
        roles = json['roles'].cast<int>();
}
