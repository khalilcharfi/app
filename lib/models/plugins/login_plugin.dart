import 'dart:async';

import 'package:adaptive_page_layout/adaptive_page_layout.dart';
import 'package:famedly/utils/famedlysdk_store.dart';
import 'package:famedly/views/enter_pass_code_view.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_lock/flutter_app_lock.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:future_loading_dialog/future_loading_dialog.dart';

import '../../config/test_user.dart';
import '../room_type_extension.dart';
import 'famedly_plugin.dart';

class LoginPlugin extends FamedlyPlugin {
  final String clientName;
  LoginState currentLoginState;

  bool get isIntegrationTest =>
      !kIsWeb &&
      (client.homeserver?.toString()?.contains(TestUser.get().homeserver) ??
          false);

  LoginPlugin(matrix, this.clientName) : super(matrix) {
    onLogin ??=
        client.onLoginStateChanged.stream.listen(handleLoginStateChanged);
  }

  /// Use with caution! Passwords should not be cached in RAM for a long time. If
  /// you set this, then it will be removed after 10 minutes. Also you can only
  /// get a cachedPassword once! It will be removed after getting it.
  String get cachedPassword {
    final passwordReturn = _cachedPassword;
    _cachedPassword = null;
    return passwordReturn;
  }

  set cachedPassword(String password) {
    _cachedPassword = password;
    _resetCachedPasswordTimer?.cancel();
    _resetCachedPasswordTimer =
        Timer(Duration(minutes: 10), () => _cachedPassword = null);
  }

  String _cachedPassword;

  Timer _resetCachedPasswordTimer;

  void handleLoginStateChanged(LoginState state) {
    // User has been logged out externally
    if (state == LoginState.loggedOut &&
        currentLoginState == LoginState.logged) {
      AppLock.of(famedly.context).disable();
      Store().setItem(EnterPassCodeView.appLockNamespace, null);
      if (!_isLoggingOut) {
        famedly.adaptivePageLayout.currentState
            .pushNamedAndRemoveAllOthers('/');
        AdaptivePageLayout.of(famedly.context).showSnackBar(
          SnackBar(
            content: Text(L10n.of(famedly.context).yourSessionIsNoLongerValid),
          ),
        );
      }
      final storage = Store();
      storage.setItem(EnterPassCodeView.appLockNamespace, null);
      storage.setItem(EnterPassCodeView.appLockBiometricsNamespace, 'false');
      AppLock.of(famedly.context).disable();
    }
    // User has been logged in and may need to reset the user account
    if (state == LoginState.logged) {
      resetUser();
    }
    currentLoginState = state;
  }

  StreamSubscription<LoginState> onLogin;

  Future<void> resetUser() async {
    // Mute all contact discovery rooms and mark them as read
    for (final room
        in client.rooms.where((r) => r.type == roomTypeContactDiscovery)) {
      try {
        if (room.pushRuleState != PushRuleState.dontNotify) {
          await room.setPushRuleState(PushRuleState.dontNotify);
        }
        if (room.notificationCount != 0 &&
            room.lastEvent.eventId.isValidMatrixId) {
          await room.setReadMarker(
            room.lastEvent.eventId,
            readReceiptLocationEventId: room.lastEvent.eventId,
          );
        }
      } catch (e, s) {
        Logs().v('Problem while resetting contact discovery room', e, s);
      }
    }
  }

  bool _isLoggingOut = false;

  void logout() async {
    (await famedly.getOrganizationDirectoryClient())
        .invalidateToken(); // to not bleed into future sessions
    _isLoggingOut = true;
    try {
      await showFutureLoadingDialog(
        context: famedly.context,
        future: () => famedly.client.logout(),
      );
      await famedly.adaptivePageLayout.currentState
          .pushNamedAndRemoveAllOthers('/');
    } finally {
      _isLoggingOut = false;
    }
  }
}
