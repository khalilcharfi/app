More information over at: https://pub.dev/packages/golden_toolkit

# Generating new goldens

`flutter test --update-goldens`

# Testing

This requires just the regular test command `flutter test` after the goldens have been generated

# Test Template

```dart
import 'package:famedly/components/avatar.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:golden_toolkit/golden_toolkit.dart';

void main() {
  /// All Tests related to the RoomsPage
  group("Avatar", () {
    /// Check Avatar Fallback
    testGoldens("Check Avatar Fallback", (WidgetTester tester) async {
      final widget = Avatar(
        MxContent(""),
      );
      final builder = GoldenBuilder.column()
        ..addScenario('Default font size', widget);
      await tester.pumpWidgetBuilder(builder.build());
      await screenMatchesGolden(tester, 'avatar_fallback');
    });

    testGoldens('Avatar Fallback - Accessibility', (tester) async {
      final widget = Avatar(
        MxContent(""),
        name: "Alexa",
      );
      final builder = GoldenBuilder.column()
        ..addScenario('Default font size', widget)
        ..addTextScaleScenario('Large font size', widget, textScaleFactor: 2.0)
        ..addTextScaleScenario('Largest font', widget, textScaleFactor: 3.2);
      await tester.pumpWidgetBuilder(builder.build());
      await screenMatchesGolden(tester, 'avatar_fallback_accessibility');
    });
  });
}

```
