//import 'package:famedly/views/chat_room.dart';
import 'package:famedly/views/rooms_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
//import 'package:mockito/mockito.dart';

import 'utils.dart';

void main() {
  /// All Tests related to the RoomsPage
  group('RoomsPage', () {
    NavigatorObserver mockObserver;

    setUp(() {
      mockObserver = MockNavigatorObserver();
    });

    /// Check if Bottom TabBar buttons are present
    testWidgets('should have Bottom TabBar buttons',
        (WidgetTester tester) async {
      await tester.runAsync(() async {
        await tester.pumpWidget(
          await Utils.getWidgetWrapper(
            RoomsPage(),
            mockObserver,
          ),
        );

        await tester.pump();

        expect(find.byKey(Key('chats')), findsOneWidget);
        expect(find.byKey(Key('requests')), findsOneWidget);
        expect(find.byKey(Key('tasks')), findsOneWidget);
        expect(find.byKey(Key('directory')), findsOneWidget);
        expect(find.byKey(Key('more')), findsOneWidget);
      });
    });

    /// Check if Top TabBar buttons are present
    testWidgets('should have Top TabBar buttons', (WidgetTester tester) async {
      await tester.runAsync(() async {
        await tester.pumpWidget(
          await Utils.getWidgetWrapper(
            RoomsPage(),
            mockObserver,
          ),
        );

        await tester.pump();

        expect(
            find.byKey(Key('NavScaffoldTab-chats-messages')), findsOneWidget);
        expect(
            find.byKey(Key('NavScaffoldTab-chats-patients')), findsOneWidget);
        expect(find.byKey(Key('NavScaffoldTab-chats-integrations')),
            findsOneWidget);
      });
    });

    /// Check if Room tapping works
    /*testWidgets('should open a room', (WidgetTester tester) async {
      await tester.runAsync(() async {
        await tester.pumpWidget(
          await Utils.getWidgetWrapper(
            RoomsPage(),
            mockObserver,
          ),
        );

        await tester.pumpAndSettle();

        await Utils.tapItem(tester, Key('room_1'));
        verify(mockObserver.didPush(any, any));

        expect(find.byType(ChatRoom), findsOneWidget);
      });
    });*/
  });
}
