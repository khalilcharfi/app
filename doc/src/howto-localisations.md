Localizations are now manageg following the scheme described here: https://docs.google.com/document/d/10e0saTfAv32OZLRmONy866vnaw0I2jwL8zukykpgWBc/edit

The means only the `lib/l10n/*.arb` files are checking into the VCS and the dart class files are generated before the build. The `intl_en.arb` file is the translation source.

# Update translations

Just add a new entry to the `intl_en.arb` file like this:

```
  "translationKey": "English Message String",
  "@translationKey": {
    "type": "text",
    "placeholders": {}
  },
```

The translationKey is used as a dart method name by the generator, so it must not contain spaces, etc. and should conform to camelCase.

Copy the new string over to `intl_de.arb` and translate it, the German translations always need to be complete. If you don't speak German, don't hesitate to ask us for help. 👍

# Add a new Language
If you want to add for example the Klingon language, create a `intl_kl.arb` file in `lib/l10n` with a `"@@locale": "kl"` entry. Add the desired translations there.

## Add language to XCode

You need to add the language to XCode. Open Runner.xcworkspace, select Runner Project and in the Info tab add the Localizations that you want to support.
