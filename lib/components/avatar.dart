/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'package:famedly/models/famedly.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import '../styles/colors.dart';
import '../utils/string_extensions.dart';
import 'mxc_image.dart';

/// Generates the widget for each room item.
class Avatar extends StatelessWidget {
  final String name;
  final Uri mxContent;
  final double size;
  final double textSize;
  final Client matrixClient;

  Avatar(
    this.mxContent, {
    this.size = 45,
    this.name = '',
    this.textSize = 14,
    this.matrixClient,
    Key key,
  }) : super(key: key);

  String get twoLetters {
    final words = name.split(' ');
    if (words.length > 1 &&
        words[0].runes.toList().isNotEmpty &&
        words[1].runes.toList().isNotEmpty) {
      return words[0].runeSubstring(0, 1) + words[1].runeSubstring(0, 1);
    } else if (words[0].runes.toList().length > 1 &&
        words[0].runes.toList().isNotEmpty) {
      return words[0].runeSubstring(0, 2);
    } else if (words[0].runes.toList().length == 1) {
      return words[0];
    } else {
      return '@';
    }
  }

  @override
  Widget build(BuildContext context) {
    return (mxContent != null && mxContent.toString() != '')
        ? Container(
            width: size,
            height: size,
            child: ClipOval(
              child: MxcImage(
                mxContent,
                fit: BoxFit.cover,
                width: size,
                height: size,
                matrixClient: matrixClient ?? Famedly.of(context).client,
              ),
            ),
          )
        : Container(
            width: size,
            height: size,
            decoration: BoxDecoration(
              color: Theme.of(context).brightness == Brightness.light
                  ? FamedlyColors.cloudyBlue
                  : FamedlyColors.cloudyBlue.withAlpha(80),
              shape: BoxShape.circle,
            ),
            child: Center(
              child: Text(
                twoLetters.toUpperCase(),
                style: TextStyle(
                  fontSize: textSize,
                  fontWeight: FontWeight.w500,
                  fontStyle: FontStyle.normal,
                  letterSpacing: 0,
                ),
              ),
            ),
          );
  }
}
