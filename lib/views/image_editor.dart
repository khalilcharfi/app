/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'dart:ui';

import 'package:famedly/styles/colors.dart';
import 'package:famedly/styles/famedly_icons_v2_icons.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:painter/painter.dart';

import 'package:flutter_gen/gen_l10n/l10n.dart';

class ImageEditor extends StatefulWidget {
  final MatrixFile file;

  const ImageEditor({Key key, @required this.file}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _ImageEditorState();
}

class _ImageEditorState extends State<ImageEditor> {
  final _key = GlobalKey();

  final _controller = PainterController()
    ..thickness = 10
    ..backgroundColor = Colors.transparent
    ..drawColor = Colors.black;

  Future<void> _finish() async {
    _controller.finish();
    MatrixFile outputFile = widget.file; // ignore: omit_local_variable_types

    if (!_controller.isEmpty) {
      final RenderRepaintBoundary boundary =
          _key.currentContext.findRenderObject();

      final image = await boundary.toImage();
      final byteData = await image.toByteData(format: ImageByteFormat.png);

      // Edit path so extension is correct

      final splitFileName = widget.file.name.split('.');
      final fileNameWithoutExtension =
          splitFileName.sublist(0, splitFileName.length - 1).join();

      final newFileName = fileNameWithoutExtension + '.png';

      outputFile = MatrixImageFile(
        bytes: byteData.buffer.asUint8List(),
        name: newFileName,
        mimeType: 'image/png',
        width: image.width,
        height: image.height,
      );
    }

    Navigator.pop(context, outputFile);
  }

  void _switchColor() {
    setState(() {
      if (_controller.drawColor == Colors.black) {
        _controller.drawColor = Theme.of(context).backgroundColor;
      } else {
        _controller.drawColor = Colors.black;
      }
    });
  }

  void _undo() {
    if (!_controller.isEmpty) {
      _controller.undo();
    }
  }

  @override
  Widget build(BuildContext context) {
    final appBarColor = Colors.black.withOpacity(0.60);

    return IconTheme(
      data: IconThemeData(
        color: Colors.white,
      ),
      child: Scaffold(
        extendBodyBehindAppBar: true,
        extendBody: true,
        backgroundColor: Colors.black,
        appBar: AppBar(
          brightness: Brightness.dark,
          backgroundColor: appBarColor,
          elevation: 0,
          actions: [
            IconButton(
              icon: Icon(Icons.check, color: FamedlyColors.aquaMarine),
              onPressed: _finish,
            ),
          ],
          leading: IconButton(
            icon: Icon(FamedlyIconsV2.close, color: Colors.white),
            onPressed: Navigator.of(context).pop,
          ),
        ),
        bottomNavigationBar: BottomAppBar(
          color: appBarColor,
          elevation: 0,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              IconButton(
                icon: Icon(Icons.undo),
                onPressed: _undo,
              ),
              Row(
                children: [
                  Text(
                    _controller.drawColor == Colors.black
                        ? L10n.of(context).black
                        : L10n.of(context).white,
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ),
                  IconButton(
                    icon: Icon(FamedlyIconsV2.style),
                    onPressed: _switchColor,
                  ),
                ],
              )
            ],
          ),
        ),
        body: RepaintBoundary(
          key: _key,
          child: Stack(
            children: [
              Center(child: Image.memory(widget.file.bytes)),
              Center(child: Painter(_controller)),
            ],
          ),
        ),
      ),
    );
  }
}
