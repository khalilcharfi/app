/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'package:famedly/models/famedly.dart';
import 'package:famedly/views/bootstrap_page.dart';
import 'package:famedly/views/directory/directory_page.dart';
import 'package:famedly/views/directory/facility_page.dart';
import 'package:famedly/views/log_viewer.dart';
import 'package:famedly/views/not_found_page.dart';
import 'package:famedly/views/organisation_picker_page.dart';
import 'package:famedly/views/requests/request_composting_page.dart';
import 'package:famedly/views/requests/requests_list_page.dart';
import 'package:famedly/views/requests/requests_list_page_role.dart';
import 'package:famedly/views/requests/requests_page.dart';
import 'package:famedly/views/settings_notifications_page.dart';
import 'package:famedly/views/tasks/task_details_page.dart';
import 'package:famedly/views/tasks/tasks_page.dart';
import 'package:famedly/views/user_directory_settings_page.dart';
import 'package:famedlysdk/famedlysdk.dart' as sdk;
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:adaptive_page_layout/adaptive_page_layout.dart';

import '../components/document_viewer.dart';
import '../views/home_picker.dart';
import '../views/archives_pages.dart';
import '../views/broadcast_list_page.dart';
import '../views/calling_page.dart';
import '../views/chat_room.dart';
import '../views/contact_page.dart';
import '../views/device_settings_page.dart';
import '../views/empty_page.dart';
import '../views/event_search_page.dart';
import '../views/image_view.dart';
import '../views/intro.dart';
import '../views/invite_page.dart';
import '../views/login_page.dart';
import '../views/member_list_page.dart';
import '../views/new_broadcast_page.dart';
import '../views/new_contact_page.dart';
import '../views/new_group_page.dart';
import '../views/patient_settings_page.dart';
import '../views/role_page.dart';
import '../views/room_encryption_settings_page.dart';
import '../views/room_media_page.dart';
import '../views/room_settings.dart';
import '../views/rooms_page.dart';
import '../views/security_settings_page.dart';
import '../views/settings_app_lock_view.dart';
import '../views/settings_info_page.dart';
import '../views/settings_page.dart';
import '../views/settings_style_page.dart';
import '../views/share_page.dart';
import '../views/user_viewer.dart';

class FamedlyRoutes {
  final BuildContext context;

  const FamedlyRoutes(this.context);

  static bool isHardTransitionRoute(String route) =>
      _hardTransitionRoutes.contains(route) ||
      route.startsWith('/requests') ||
      route.startsWith('/broadcast');

  static const Set<String> _hardTransitionRoutes = {
    '/',
    '/home',
    '/chats',
    '/tasks',
    '/requests',
    '/directory',
  };

  ViewData getRoute(RouteSettings settings) {
    final route = settings.name;
    final censoredRoute = route.matchesRoute('/intro/:username')
        ? '/intro/username'
        : route
            .split('/')
            .map((part) => part.isValidMatrixId ? part.localpart : part)
            .toList()
            .join('/');
    Famedly.of(context)
        ?.analyticsPlugin
        ?.trackScreenWithName(censoredRoute, null);
    final isLogged = Famedly.of(context).client.isLogged();

    // === Always available routes
    if (route.matchesRoute('/documents/:document')) {
      return ViewData(
        routeName: route,
        displayTopBar: false,
        mainView: (_) => DocumentViewer(
          route.routeParams('/documents/:document')['document'],
        ),
      );
    }
    if (route.matchesRoute('/logs')) {
      return ViewData(
        routeName: route,
        displayTopBar: false,
        mainView: (_) => LogViewer(),
      );
    }

    // Only available if user is NOT logged in
    if (route.matchesRoute('/')) {
      if (Famedly.of(context).client.isLogged()) {
        return ViewData(
          routeName: route,
          mainView: (_) => RoomsPage(),
          emptyView: (_) => EmptyPage(),
        );
      }
      return ViewData(
        routeName: route,
        mainView: (_) => HomePicker(),
        displayTopBar: false,
      );
    }
    if (!isLogged) {
      if (route.matchesRoute('/login')) {
        return ViewData(
          routeName: route,
          mainView: (_) => LoginPage(),
          displayTopBar: false,
        );
      }
      return ViewData(
        routeName: route,
        mainView: (_) => OrganisationPickerPage(),
        displayTopBar: false,
      );
    }

    // Only available if user is logged in
    if (route.matchesRoute('/intro/:username')) {
      return ViewData(
        routeName: route,
        mainView: (_) => Intro(
          username: route.routeParams('/intro/:username')['username'],
          welcomeMessage: settings.arguments,
        ),
        displayTopBar: false,
      );
    }
    if (route.matchesRoute('/share')) {
      return ViewData(
        routeName: route,
        mainView: (_) => SharePage(),
        displayTopBar: false,
      );
    }
    if (route.matchesRoute('/contacts')) {
      return ViewData(
        routeName: route,
        leftView: (_) => RoomsPage(),
        mainView: (_) => ContactsPage(),
      );
    }
    if (route.matchesRoute('/contacts/:userid')) {
      final params = route.routeParams('/profile/:userid');
      return ViewData(
        routeName: route,
        leftView: (_) => RoomsPage(),
        mainView: (_) => ContactsPage(),
        rightView: (_) => UserViewer(
          userId: params['userid'],
          profile: settings.arguments,
        ),
      );
    }
    if (route
        .matchesRoute('/organisation/:organisationname/contacts/:userid')) {
      final params =
          route.routeParams('/organisation/:organisationname/contacts/:userid');
      return ViewData(
        routeName: route,
        leftView: (_) => RoomsPage(),
        mainView: (_) => ContactsPage(),
        rightView: (_) => UserViewer(
          userId: params['userid'],
          profile: settings.arguments,
          organisationName: params['organisationname'],
        ),
      );
    }
    if (route.matchesRoute('/newGroup')) {
      return ViewData(
        routeName: route,
        leftView: (_) => RoomsPage(),
        mainView: (_) => NewGroupPage(),
      );
    }
    if (route.matchesRoute('/newContact')) {
      return ViewData(
        routeName: route,
        leftView: (_) => RoomsPage(),
        mainView: (_) => NewContactPage(),
      );
    }
    if (route.matchesRoute('/broadcast/:id')) {
      return ViewData(
        routeName: route,
        leftView: (_) => RequestsPage(
          currentType: 'requests/broadcasts',
        ),
        mainView: (_) => BroadcastListPage(
          requestId: route.routeParams('/broadcast/:id')['id'],
        ),
        emptyView: (_) => EmptyPage(),
        expandRightView: true,
        columnWidth: 270,
      );
    }
    if (route.matchesRoute('/requests')) {
      return ViewData(
        routeName: route,
        mainView: (_) => RequestsPage(),
        emptyView: (_) => EmptyPage(),
        columnWidth: 270,
      );
    }
    if (route.matchesRoute('/requests/:type')) {
      final params = route.routeParams('/requests/:type');
      return ViewData(
        routeName: route,
        leftView: (_) => RequestsPage(
          currentType: 'requests/${params['type']}',
        ),
        mainView: (_) => RequestsListPage(
          type: params['type'],
        ),
        emptyView: (_) => EmptyPage(),
        expandRightView: true,
        columnWidth: 270,
      );
    }
    if (route.matchesRoute('/requests/role/:id')) {
      final roleId = route.routeParams('/requests/role/:id')['id'];
      return ViewData(
        routeName: route,
        leftView: (_) => RequestsPage(
          currentType: 'requests/role/$roleId',
        ),
        mainView: (_) => RequestsListPageRole(
            int.parse(
              roleId,
            ),
            role: settings.arguments),
        emptyView: (_) => EmptyPage(),
        expandRightView: true,
        columnWidth: 270,
      );
    }
    if (route.matchesRoute('/backup')) {
      return ViewData(
        routeName: route,
        displayTopBar: false,
        mainView: (_) => BootstrapPage(wipe: settings.arguments ?? false),
      );
    }
    if (route.matchesRoute('/chatArchive')) {
      return ViewData(
        routeName: route,
        leftView: (_) => SettingsPage(),
        mainView: (_) => ArchivesPage(),
      );
    }
    if (route.matchesRoute('/settings')) {
      return ViewData(
        routeName: route,
        mainView: (_) => SettingsPage(),
        emptyView: (_) => EmptyPage(),
      );
    }
    if (route.matchesRoute('/settings/info')) {
      return ViewData(
        routeName: route,
        leftView: (_) => SettingsPage(),
        mainView: (_) => SettingsInfoPage(),
      );
    }
    if (route.matchesRoute('/settings/style')) {
      return ViewData(
        routeName: route,
        leftView: (_) => SettingsPage(),
        mainView: (_) => SettingsStylePage(),
      );
    }
    if (route.matchesRoute('/settings/userDirectory')) {
      return ViewData(
        routeName: route,
        leftView: (_) => SettingsPage(),
        mainView: (_) => UserDirectorySettingsPage(),
      );
    }
    if (route.matchesRoute('/settings/notifications')) {
      return ViewData(
        routeName: route,
        leftView: (_) => SettingsPage(),
        mainView: (_) => SettingsNotificationsPage(),
      );
    }
    if (route.matchesRoute('/settings/applock')) {
      return ViewData(
        routeName: route,
        leftView: (_) => SettingsPage(),
        mainView: (_) => SettingsAppLockView(),
      );
    }
    if (route.matchesRoute('/settings/devices')) {
      return ViewData(
        routeName: route,
        leftView: (_) => SettingsPage(),
        mainView: (_) => DevicesSettingsPage(),
      );
    }
    if (route.matchesRoute('/settings/security')) {
      return ViewData(
        routeName: route,
        leftView: (_) => SettingsPage(),
        mainView: (_) => SecuritySettingsPage(),
      );
    }
    if (route.matchesRoute('/call/:room/:type/:dir')) {
      return ViewData(
        routeName: route,
        leftView: (_) => RoomsPage(),
        mainView: (_) => Calling(context),
      );
    }
    if (route.matchesRoute('/tasks')) {
      return ViewData(
        routeName: route,
        mainView: (_) => TasksPage(),
        emptyView: (_) => EmptyPage(),
      );
    }
    if (route.matchesRoute('/tasks/:task')) {
      return ViewData(
        routeName: route,
        mainView: (_) => TaskDetailsPage(
          id: route.routeParams('/tasks/:task')['task'],
        ),
        leftView: (_) => TasksPage(),
      );
    }
    if (route.matchesRoute('/role/:id')) {
      return ViewData(
        routeName: route,
        leftView: (_) => RequestsPage(),
        mainView: (_) => RolePage(
          int.parse(
            route.routeParams('/role/:id')['id'],
          ),
          role: settings.arguments,
        ),
      );
    }
    if (route.matchesRoute('/newBroadcast/verify')) {
      return ViewData(
        routeName: route,
        mainView: (_) => NewBroadcastPage(),
      );
    }
    if (route.matchesRoute('/requestCompositing/:formname')) {
      return ViewData(
        routeName: route,
        mainView: (_) => RequestCompositingPage(
            route.routeParams('/requestCompositing/:formname')['formname'],
            settings.arguments),
      );
    }
    if (route.matchesRoute('/directory')) {
      return ViewData(
        routeName: route,
        mainView: (_) => DirectoryPage(initialTabIndex: 2),
        emptyView: (_) => EmptyPage(),
      );
    }
    if (route.matchesRoute('/directory/roles')) {
      return ViewData(
        routeName: route,
        mainView: (_) => DirectoryPage(initialTabIndex: 1),
        emptyView: (_) => EmptyPage(),
      );
    }
    if (route.matchesRoute('/directory/integrations')) {
      return ViewData(
        routeName: route,
        mainView: (_) => DirectoryPage(initialTabIndex: 3),
        emptyView: (_) => EmptyPage(),
      );
    }
    if (route.matchesRoute('/directory/:organisation')) {
      return ViewData(
        routeName: route,
        leftView: (_) => DirectoryPage(initialTabIndex: 3),
        mainView: (_) => FacilityPage(
          route.routeParams('/directory/:organisation')['organisation'],
          onInvite: settings.arguments,
        ),
      );
    }
    if (route.matchesRoute('/room/:id')) {
      final id = route.routeParams('/room/:id')['id'];
      return ViewData(
        routeName: route,
        leftView: (_) => RoomsPage(activeChatID: id),
        mainView: (_) => ChatRoom(
          id: id,
          room: settings.arguments,
          routeName: '/room/$id',
        ),
      );
    }
    if (route.matchesRoute('/room/:id/media/images')) {
      final id = route.routeParams('/room/:id/media/images')['id'];
      return ViewData(
        routeName: route,
        leftView: (_) => RoomsPage(activeChatID: id),
        mainView: (_) => ChatRoom(
          id: id,
          routeName: '/room/$id',
        ),
        rightView: (_) => RoomMediaPage(id, [
          sdk.MessageTypes.Image,
          sdk.MessageTypes.Sticker,
        ]),
      );
    }
    if (route.matchesRoute('/room/:id/media/videos')) {
      final id = route.routeParams('/room/:id/media/videos')['id'];
      return ViewData(
        routeName: route,
        leftView: (_) => RoomsPage(activeChatID: id),
        mainView: (_) => ChatRoom(
          id: id,
          routeName: '/room/$id',
        ),
        rightView: (_) => RoomMediaPage(id, [
          sdk.MessageTypes.Video,
        ]),
      );
    }
    if (route.matchesRoute('/room/:id/media/documents')) {
      final id = route.routeParams('/room/:id/media/documents')['id'];
      return ViewData(
        routeName: route,
        leftView: (_) => RoomsPage(activeChatID: id),
        mainView: (_) => ChatRoom(
          id: id,
          routeName: '/room/$id',
        ),
        rightView: (_) => RoomMediaPage(id, [
          sdk.MessageTypes.File,
          sdk.MessageTypes.Audio,
        ]),
      );
    }
    if (route.matchesRoute('/room/:id/members')) {
      final id = route.routeParams('/room/:id/members')['id'];
      return ViewData(
        routeName: route,
        leftView: (_) => RoomsPage(activeChatID: id),
        mainView: (_) => ChatRoom(
          id: id,
          routeName: '/room/$id',
        ),
        rightView: (_) => MemberListPage(id),
      );
    }
    if (route.matchesRoute('/room/broadcast/:requestid/:id')) {
      final params = route.routeParams('/room/broadcast/:requestid/:id');
      return ViewData(
        routeName: route,
        leftView: (_) => RequestsPage(
          currentType: 'requests/${params['requestid']}',
        ),
        mainView: (_) => BroadcastListPage(
          requestId: params['requestid'],
          activeChatId: params['id'],
        ),
        rightView: (_) => ChatRoom(
          id: params['id'],
          routeName: '/room/broadcast/${params['requestid']}/${params['id']}',
        ),
        expandRightView: true,
        columnWidth: 270,
      );
    }
    if (route.matchesRoute('/room/broadcast/:requestid/:id/invite')) {
      final params = route.routeParams('/room/broadcast/:requestid/:id/invite');
      return ViewData(
        routeName: route,
        leftView: (_) => RequestsPage(
          currentType: 'requests/${params['requestid']}',
        ),
        mainView: (_) => BroadcastListPage(
          requestId: params['requestid'],
          activeChatId: params['id'],
        ),
        rightView: (_) => InvitePage(roomId: params['id']),
        expandRightView: true,
        columnWidth: 270,
      );
    }
    if (route.matchesRoute('/room/broadcast/:requestid/:id/encryption')) {
      final params =
          route.routeParams('/room/broadcast/:requestid/:id/encryption');
      return ViewData(
        routeName: route,
        leftView: (_) => RequestsPage(
          currentType: 'requests/${params['requestid']}',
        ),
        mainView: (_) => BroadcastListPage(
          requestId: params['requestid'],
          activeChatId: params['id'],
        ),
        rightView: (_) => RoomEncryptionSettingsPage(params['id']),
        expandRightView: true,
        columnWidth: 270,
      );
    }
    if (route.matchesRoute('/room/requests/role/:roleid/:id')) {
      final params = route.routeParams('/room/requests/role/:roleid/:id');
      return ViewData(
        routeName: route,
        leftView: (_) => RequestsPage(
          currentType: 'requests/role${params['roleid']}',
        ),
        mainView: (_) => RequestsListPageRole(
          int.parse(params['roleid']),
          activeChatId: params['id'],
        ),
        rightView: (_) => ChatRoom(
          id: params['id'],
          routeName: '/room/requests/role/${params['roleid']}/${params['id']}',
        ),
        expandRightView: true,
        columnWidth: 270,
      );
    }
    if (route.matchesRoute('/room/requests/role/:roleid/:id/invite')) {
      final params =
          route.routeParams('/room/requests/role/:roleid/:id/invite');
      return ViewData(
        routeName: route,
        leftView: (_) => RequestsPage(
          currentType: 'requests/role${params['roleid']}',
        ),
        mainView: (_) => RequestsListPageRole(
          int.parse(params['roleid']),
          activeChatId: params['id'],
        ),
        rightView: (_) => InvitePage(roomId: params['id']),
        expandRightView: true,
        columnWidth: 270,
      );
    }
    if (route.matchesRoute('/room/requests/role/:roleid/:id/encryption')) {
      final params =
          route.routeParams('/room/requests/role/:roleid/:id/encryption');
      return ViewData(
        routeName: route,
        leftView: (_) => RequestsPage(
          currentType: 'requests/role${params['roleid']}',
        ),
        mainView: (_) => RequestsListPageRole(
          int.parse(params['roleid']),
          activeChatId: params['id'],
        ),
        rightView: (_) => RoomEncryptionSettingsPage(params['id']),
        expandRightView: true,
        columnWidth: 270,
      );
    }
    if (route.matchesRoute('/room/requests/:requestid/:id')) {
      final params = route.routeParams('/room/requests/:requestid/:id');
      return ViewData(
        routeName: route,
        leftView: (_) => RequestsPage(
          currentType: 'requests/${params['requestid']}',
        ),
        mainView: (_) => RequestsListPage(
          type: params['requestid'],
          activeChatId: params['id'],
        ),
        rightView: (_) => ChatRoom(
          id: params['id'],
          routeName: '/room/requests/${params['requestid']}/${params['id']}',
        ),
        expandRightView: true,
        columnWidth: 270,
      );
    }
    if (route.matchesRoute('/room/requests/:requestid/:id/invite')) {
      final params = route.routeParams('/room/requests/:requestid/:id/invite');
      return ViewData(
        routeName: route,
        leftView: (_) => RequestsPage(
          currentType: 'requests/${params['requestid']}',
        ),
        mainView: (_) => RequestsListPage(
          type: params['requestid'],
          activeChatId: params['id'],
        ),
        rightView: (_) => InvitePage(roomId: params['id']),
        expandRightView: true,
        columnWidth: 270,
      );
    }
    if (route.matchesRoute('/room/requests/:requestid/:id/encryption')) {
      final params =
          route.routeParams('/room/requests/:requestid/:id/encryption');
      return ViewData(
        routeName: route,
        leftView: (_) => RequestsPage(
          currentType: 'requests/${params['requestid']}',
        ),
        mainView: (_) => RequestsListPage(
          type: params['requestid'],
          activeChatId: params['id'],
        ),
        rightView: (_) => RoomEncryptionSettingsPage(params['id']),
        expandRightView: true,
        columnWidth: 270,
      );
    }
    if (route.matchesRoute('/room/:id/search')) {
      final params = route.routeParams('/room/:id/search');
      return ViewData(
        routeName: route,
        leftView: (_) => RoomsPage(activeChatID: params['id']),
        mainView: (_) => EventSearchPage(id: params['id']),
      );
    }
    if (route.matchesRoute('/room/:id/invite')) {
      final id = route.routeParams('/room/:id/invite')['id'];
      return ViewData(
        routeName: route,
        leftView: (_) => RoomsPage(),
        mainView: (_) => ChatRoom(
          id: id,
          routeName: '/room/$id',
        ),
        rightView: (_) => InvitePage(roomId: id),
      );
    }
    if (route.matchesRoute('/room/:id/member/:userid')) {
      final params = route.routeParams('/room/:id/member/:userid');
      return ViewData(
        routeName: route,
        leftView: (_) => RoomsPage(),
        mainView: (_) => ChatRoom(
          id: params['id'],
          routeName: '/room/${params['id']}',
        ),
        rightView: (_) => UserViewer(
          roomId: params['id'],
          userId: params['userid'],
        ),
      );
    }
    if (route.matchesRoute('/room/imageEventView/:roomid/:eventid')) {
      final params = route.routeParams('/room/imageEventView/:roomid/:eventid');
      return ViewData(
        routeName: route,
        displayTopBar: false,
        mainView: (_) => ImageView(
          roomId: params['roomid'],
          eventId: params['eventid'],
        ),
      );
    }
    if (route.matchesRoute('/room/:roomid/settings')) {
      final params = route.routeParams('/room/:roomid/settings');
      return ViewData(
        routeName: route,
        leftView: (_) => RoomsPage(),
        mainView: (_) => ChatRoom(
          id: params['roomid'],
          routeName: '/room/${params['roomid']}',
        ),
        rightView: (_) => RoomSettings(id: params['roomid']),
      );
    }
    if (route.matchesRoute('/room/:roomid/encryption')) {
      final params = route.routeParams('/room/:roomid/settings');
      return ViewData(
        routeName: route,
        leftView: (_) => RoomsPage(),
        mainView: (_) => ChatRoom(
          id: params['roomid'],
          routeName: '/room/${params['roomid']}',
        ),
        rightView: (_) => RoomEncryptionSettingsPage(params['roomid']),
      );
    }
    if (route.matchesRoute('/patients/new')) {
      return ViewData(
        routeName: route,
        leftView: (_) => RoomsPage(),
        mainView: (_) => PatientSettingsPage(),
      );
    }
    if (route.matchesRoute('/patients/:roomid')) {
      final params = route.routeParams('/patients/:roomid');
      return ViewData(
        routeName: route,
        leftView: (_) => RoomsPage(activeChatID: params['roomid']),
        mainView: (_) => ChatRoom(
          id: params['roomid'],
          routeName: '/room/${params['roomid']}',
        ),
        rightView: (_) => PatientSettingsPage(roomId: params['roomid']),
      );
    }
    if (route.matchesRoute('/chats/integrations')) {
      return ViewData(
        routeName: route,
        emptyView: (_) => EmptyPage(),
        mainView: (_) => RoomsPage(),
      );
    }
    if (route.matchesRoute('/chats')) {
      return ViewData(
        routeName: route,
        emptyView: (_) => EmptyPage(),
        mainView: (_) => RoomsPage(),
      );
    }
    Logs().w('Route not found!', route);
    return ViewData(
      routeName: route,
      displayTopBar: false,
      mainView: (_) => NotFoundPage(),
    );
  }
}

extension on String {
  bool matchesRoute(String route) {
    final parts = split('/');
    final routeParts = route.split('/');
    if (parts.length != routeParts.length) {
      return false;
    }
    for (var i = 0; i < parts.length; i++) {
      if (routeParts[i].startsWith(':')) {
        continue;
      }
      if (routeParts[i].toLowerCase() != parts[i].toLowerCase()) {
        return false;
      }
    }
    return true;
  }

  Map<String, String> routeParams(String route) {
    final params = <String, String>{};
    final parts = split('/');
    final routeParts = route.split('/');
    for (var i = 0; i < parts.length; i++) {
      if (routeParts[i].startsWith(':')) {
        params[routeParts[i].substring(1)] = parts[i];
      }
    }
    return params;
  }
}

class HardTransitionRoute extends PageRouteBuilder {
  final Widget page;

  HardTransitionRoute({this.page})
      : super(
          pageBuilder: (
            BuildContext context,
            Animation<double> animation,
            Animation<double> secondaryAnimation,
          ) =>
              page,
        );
}
