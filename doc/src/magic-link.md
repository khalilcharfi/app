## Magic link format
We have three strings to encode:
*  username
*  password
*  domain

They will be encoded in this format:
```
com.famedly://username:password@domain
```

The QR Code should just contain this link and would look like this:
