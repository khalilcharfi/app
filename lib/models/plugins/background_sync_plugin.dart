import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import '../famedly.dart';

class BackgroundSyncPlugin with WidgetsBindingObserver {
  final Client client;

  BackgroundSyncPlugin._(this.client) {
    if (kIsWeb) return;
    final wb = WidgetsBinding.instance;
    wb.addObserver(this);
    didChangeAppLifecycleState(wb.lifecycleState);
  }

  static BackgroundSyncPlugin _instance;

  factory BackgroundSyncPlugin.clientOnly(Client client) {
    _instance ??= BackgroundSyncPlugin._(client);
    return _instance;
  }

  factory BackgroundSyncPlugin(Famedly famedly) =>
      BackgroundSyncPlugin.clientOnly(famedly.client);

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    Logs().v('AppLifecycleState = $state');
    final foreground = state != AppLifecycleState.detached &&
        state != AppLifecycleState.paused;
    client.backgroundSync = foreground;
    client.syncPresence = foreground ? null : PresenceType.unavailable;
    client.requestHistoryOnLimitedTimeline = !foreground;
  }
}
