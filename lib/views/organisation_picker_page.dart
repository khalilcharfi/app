/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'dart:async';
import 'dart:convert';
import 'dart:math';

import 'package:adaptive_page_layout/adaptive_page_layout.dart';
import 'package:adaptive_theme/adaptive_theme.dart';
import 'package:famedly/config/chat_configs.dart';
import 'package:famedly/styles/famedly_icons_v2_icons.dart';
import 'package:famedly/utils/error_reporter.dart';
import 'package:famedly/utils/famedlysdk_store.dart';
import 'package:famedly/utils/platform_extension.dart';
import 'package:famedly_user_directory_client_sdk/famedly_user_directory_client_sdk.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:http/http.dart' as http;
import 'package:uni_links/uni_links.dart';
import 'package:universal_html/html.dart' as html;
import 'package:url_launcher/url_launcher.dart';

import '../components/web_enter_listener.dart';
import '../models/famedly.dart';
import '../models/famedly_well_known.dart';
import '../models/roles/client_extension.dart';
import '../styles/colors.dart';
import '../utils/magic_link.dart';
import '../views/qr_scanner_page.dart';

class HomeserverNotInDirectoryException implements Exception {}

class OrganisationPickerPage extends StatefulWidget {
  @override
  _OrganisationPickerPageState createState() => _OrganisationPickerPageState();
}

class _OrganisationPickerPageState extends State<OrganisationPickerPage> {
  final _organisationController = TextEditingController();
  final _textFieldFocusNode = FocusNode();
  String errorText;
  StreamSubscription _intentUriStreamSubscription;
  String urlSuffix = '.famedly.care';
  bool _suffixVisible = true;
  bool _isLoading = false;

  void _openMagicLink(String text) async {
    try {
      Famedly.of(context).magicLink = MagicLink(text);
      _organisationController.text = Famedly.of(context).magicLink.domain;
      setState(() => _suffixVisible = false);
      submit(context);
    } catch (e, s) {
      AdaptivePageLayout.of(context).showSnackBar(
        SnackBar(
          content: Text(L10n.of(context).oopsSomethingWentWrong),
        ),
      );
      await ErrorReporter.reportError(e, s);
    }
  }

  void _processIncomingUris(String text) async {
    if (text == null || !text.startsWith(Famedly.magicLinkPrefix)) return;
    AdaptivePageLayout.of(context).popUntilIsFirst();
    _openMagicLink(text);
  }

  /// getInitialLink may rereturn the value multiple times if this view is
  /// opened multiple times for example if the user logs out after they logged
  /// in with qr code or magic link.
  static bool _gotInitialLink = false;

  void _initReceiveUris() {
    if (!kIsMobile) return;

    // For receiving shared Uris
    _intentUriStreamSubscription = linkStream.listen(_processIncomingUris,
        onError: ErrorReporter.reportError);
    if (_gotInitialLink == false) {
      _gotInitialLink = true;
      getInitialLink().then(_processIncomingUris);
    }
  }

  @override
  void initState() {
    super.initState();
    _initReceiveUris();
  }

  @override
  void dispose() {
    super.dispose();
    _intentUriStreamSubscription?.cancel();
  }

  void submit(BuildContext context,
      {String organisation, bool showError = true}) async {
    if (organisation == null && _organisationController.text.isEmpty) {
      setState(() => errorText = L10n.of(context).pleaseFillOutTextField);
      return;
    }
    setState(() => errorText = null);

    _organisationController.text = _organisationController.text.trim();

    var server = organisation ??
        (_organisationController.text + (_suffixVisible ? urlSuffix : ''));
    if (!server.startsWith('https://')) server = 'https://' + server;
    final famedly = Famedly.of(context);
    setState(() => _isLoading = true);
    try {
      final wellKnown = await famedly.client.checkHomeserver(server);
      final famedlyWellKnown =
          FamedlyWellKnownInformation.fromJson(wellKnown?.content ?? {});
      await Store().setItem('famedlyOrganizationDirectoryBaseUrl',
          famedlyWellKnown.famedlyOrganizationDirectory?.baseUrl);
      await Store().setItem('famedlyUserDirectoryBaseUrl',
          famedlyWellKnown.famedlyUserDirectory?.baseUrl);
      // The following is mainly done as a safeguard for the someone
      // initializing the directory before we ever did a well-known request.
      // It's also potentially faster to do it here directly instead of
      // roundtripping the baseUrls to the Store.
      famedly.initOrganizationDirectory(
          famedlyWellKnown.famedlyOrganizationDirectory?.baseUrl ??
              ChatConfigs.fallbackOrganizationDirectoryBaseUrl);
      famedly.setUserDirectoryClient(UserDirectoryClient(
          baseUrl: famedlyWellKnown.famedlyUserDirectory?.baseUrl ??
              ChatConfigs.fallbackUserDirectoryBaseUrl));
      try {
        // Check if the homeserver is in the Famedly Organisations Directory
        if (!ChatConfigs.serverWhitelist
            .contains(famedly.client.homeserver.origin)) {
          await famedly.client
              .getOwnOrganisation(famedly.getOrganizationDirectoryClient());
        }
      } on MatrixException catch (e) {
        if (e.error == MatrixError.M_UNRECOGNIZED) {
          throw HomeserverNotInDirectoryException();
        }
      }
      setState(() => _isLoading = false);
    } on HomeserverNotInDirectoryException catch (_) {
      Logs().d('$server is not in the Famedly Directory!');
      if (showError) {
        setState(() =>
            errorText = L10n.of(context).organisationServerIsNotSupported);
      }
      setState(() => _isLoading = false);
      return;
    } on MatrixException catch (exception) {
      if (showError) {
        setState(() => errorText = exception.errorMessage);
      }
      setState(() => _isLoading = false);
      return;
    } catch (exception, stacktrace) {
      Logs().e(exception.toString());
      Logs().e(stacktrace.toString());
      if (showError) {
        setState(() =>
            errorText = L10n.of(context).organisationServerIsNotResponding);
      }
      setState(() => _isLoading = false);
      return;
    } finally {
      setState(() => _isLoading = false);
    }
    await AdaptivePageLayout.of(context)
        .pushNamedAndRemoveUntilIsFirst('/login');
  }

  static bool _localUrlChecked = false;

  void _checkDefaultHomeserver(BuildContext context) async {
    var defaultHomeserver = html.window.location.origin;
    try {
      final config = jsonDecode(
        utf8.decode((await http.get(Uri(path: 'config.json'))).bodyBytes),
      ) as Map<String, dynamic>;
      defaultHomeserver = config['homeserver'] ?? defaultHomeserver;
    } catch (e, s) {
      Logs().v('Unable to load config.json', e, s);
    }

    submit(
      context,
      organisation: defaultHomeserver,
      showError: false,
    );
  }

  @override
  Widget build(BuildContext context) {
    Famedly.of(context).appOutdatedPlugin.checkIfOutdated();
    WidgetsBinding.instance
        .addPostFrameCallback((_) => AdaptiveTheme.of(context).reset());

    return Scaffold(
      key: Key('organisation_picker_page'),
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        leading: ClipOval(
          child: Image.asset(
            'assets/images/famedly_icon_transparent.png',
            width: 56,
            height: 56,
            fit: BoxFit.cover,
          ),
        ),
        actions: [
          IconButton(
            icon: Icon(FamedlyIconsV2.info, color: FamedlyColors.blueyGrey),
            onPressed: () => showLicensePage(
                context: AdaptivePageLayout.of(context).context),
          ),
        ],
      ),
      body: Builder(builder: (context) {
        if (kReleaseMode && kIsWeb && !_localUrlChecked) {
          WidgetsBinding.instance
              .addPostFrameCallback((_) => _checkDefaultHomeserver(context));
          _localUrlChecked = true;
        }
        return SafeArea(
          child: ListView(
            padding: EdgeInsets.symmetric(
                horizontal:
                    max(0, (MediaQuery.of(context).size.width - 400) / 2)),
            children: <Widget>[
              if (!!kIsMobile) SizedBox(height: 16),
              if (!!kIsMobile)
                Center(
                  child: InkWell(
                    onTap:
                        _isLoading ? null : () => QrScannerPage.scan(context),
                    child: Material(
                      elevation: 5,
                      borderRadius: BorderRadius.all(Radius.circular(16.0)),
                      color: FamedlyColors.azul,
                      child: Container(
                        width: 144,
                        height: 144,
                        child: Center(
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Image.asset(
                                'assets/images/qr.png',
                                width: 100,
                                height: 100,
                              ),
                              Text(
                                L10n.of(context).scanTheQrCode,
                                style: TextStyle(color: Colors.black),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              SizedBox(height: 16),
              Padding(
                padding: EdgeInsets.only(
                    left: 24.0, top: 8.0, bottom: 8.0, right: 24.0),
                child: Text(
                  (!kIsMobile
                      ? L10n.of(context).enterYourOrganisationSLoginUrl
                      : L10n.of(context).orEnterYourOrganisationSUrl),
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 16.0,
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.all(16),
                child: WebEnterListener(
                  focusNode: _textFieldFocusNode,
                  onEnter: () => submit(context),
                  child: TextField(
                    onEditingComplete: () => null,
                    keyboardType: TextInputType.url,
                    readOnly: _isLoading,
                    key: Key('organisation'),
                    autocorrect: false,
                    onSubmitted: kIsWeb ? null : (s) => submit(context),
                    onChanged: (String url) => url.contains('.')
                        ? _suffixVisible != false
                            ? setState(() => _suffixVisible = false)
                            : null
                        : _suffixVisible != true
                            ? setState(() => _suffixVisible = true)
                            : null,
                    autofocus: !kIsMobile,
                    focusNode: _textFieldFocusNode,
                    controller: _organisationController,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      prefixText: 'https://',
                      suffixText: _suffixVisible ? urlSuffix : null,
                      labelText: L10n.of(context).loginUrl,
                      hintText: L10n.of(context).myOrganisation,
                      errorText: errorText,
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: ElevatedButton(
                  key: Key('login'),
                  onPressed: _isLoading ? null : () => submit(context),
                  child: _isLoading
                      ? LinearProgressIndicator()
                      : Text(
                          L10n.of(context).connect,
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                ),
              ),
              SizedBox(height: 16),
              Padding(
                padding: EdgeInsets.only(
                    left: 24.0, top: 8.0, bottom: 8.0, right: 24.0),
                child: Text(
                  L10n.of(context).famedlyBenefit,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 16.0,
                  ),
                ),
              ),
              Center(
                child: TextButton(
                  onPressed: () => launch('https://famedly.com'),
                  child: Text(
                    L10n.of(context).registerPersonally,
                    style: TextStyle(
                      color: Colors.blue,
                      fontWeight: FontWeight.bold,
                      decoration: TextDecoration.underline,
                    ),
                  ),
                ),
              ),
              SizedBox(height: 16),
            ],
          ),
        );
      }),
      bottomNavigationBar: SafeArea(
        child: Wrap(
          alignment: WrapAlignment.center,
          children: [
            TextButton(
              key: Key('goToPrivacyPolicy'),
              onPressed: () => AdaptivePageLayout.of(context)
                  .pushNamed('/documents/privacyPolicy'),
              child: Text(
                L10n.of(context).privacyPolicy,
                style: TextStyle(
                  fontSize: 14,
                  decoration: TextDecoration.underline,
                ),
              ),
            ),
            TextButton(
              key: Key('goToTermsOfUse'),
              onPressed: () => AdaptivePageLayout.of(context)
                  .pushNamed('/documents/termsOfUse'),
              child: Text(
                L10n.of(context).termsOfUse,
                style: TextStyle(
                  fontSize: 14,
                  decoration: TextDecoration.underline,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
