/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'package:famedly/models/famedly.dart';
import 'package:famedly/models/tasks/task.dart';
import 'package:famedly/models/tasks/tasklist.dart';
import 'package:famedly/views/tasks/tasks_page_view.dart';
import 'package:flutter/material.dart';
import 'package:future_loading_dialog/future_loading_dialog.dart';

class TasksPage extends StatefulWidget {
  @override
  TasksPageController createState() => TasksPageController();
}

class TasksPageController extends State<TasksPage> {
  TaskList taskList;
  bool showDone = false;
  var doneCount;
  List<Task> tasks;
  final ScrollController scrollController = ScrollController();

  @override
  void setState(fn) {
    if (!mounted) return;
    super.setState(fn);
  }

  @override
  void initState() {
    if (taskList == null) {
      taskList = TaskList(Famedly.of(context).client);
      taskList.onUpdate = () {
        setState(() {
          sortTasks();
        });
      };
    }

    tasks = List.from(taskList.tasks);
    super.initState();
  }

  void clearCompletedTasks() async {
    final newList = List<Task>.from(taskList.tasks);
    newList.removeWhere((Task t) => t.done);
    await showFutureLoadingDialog(
        context: context, future: () => taskList.updateTaskList(newList));
  }

  void showDoneTasks() {
    setState(() {
      showDone = !showDone;
    });
  }

  /// for "Done (n)" listtile
  void sortTasks() {
    tasks = List.from(taskList.tasks);
    doneCount = 0;
    for (var n = 0; n < tasks.length; n++) {
      if (tasks[n].done) doneCount++;
    }
    tasks.sort((t1, t2) => (t1.done ? 1 : 0).compareTo((t2.done ? 1 : 0)));
  }

  @override
  Widget build(BuildContext context) {
    return TasksPageView(this);
  }
}
