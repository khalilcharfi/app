/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'dart:convert';
import 'dart:io';

import 'package:famedly/config/forms.dart';
import 'package:fhir/r4.dart' as fhir;
import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;

class FormManager {
  List<fhir.Questionnaire> _cache;

  Future<fhir.Questionnaire> loadForm(String name) async {
    final found = _cache?.where((form) => form.name == name);
    if (found?.isNotEmpty ?? false) return found.single;

    var configJsonString = '';
    if (kIsWeb) {
      configJsonString = utf8.decode(
          (await http.get(Uri(path: 'assets/assets/questionnaires/$name.json')))
              .bodyBytes);
    } else if (Platform.isWindows || Platform.isLinux) {
      configJsonString =
          await File('assets/questionnaires/$name.json').readAsString();
    } else {
      configJsonString =
          await rootBundle.loadString('assets/questionnaires/$name.json');
    }
    return fhir.Questionnaire.fromJson(jsonDecode(configJsonString));
  }

  Future<List<fhir.Questionnaire>> getAllForms() async {
    if (_cache == null) {
      _cache = <fhir.Questionnaire>[];
      for (final QuestionnaireName in Forms.names) {
        _cache.add(await loadForm(QuestionnaireName));
      }
    }
    return _cache;
  }
}

extension FormManagerExtension on fhir.Questionnaire {
  String get tagDescription => 'questionnaire::$name';
}
