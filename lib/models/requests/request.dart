import 'package:famedly_directory_client_sdk/famedly_directory_client_sdk.dart';
import 'package:famedlysdk/famedlysdk.dart' hide Visibility;
import 'package:famedlysdk/famedlysdk.dart' as sdk show Visibility;
import 'package:uuid/uuid.dart';

import '../room_type_extension.dart';
import 'broadcast_content.dart';
import 'request_content.dart';
import 'request_state_content.dart';

class RequestBundle {
  String id;
  List<Room> rooms;
  Room broadcastRoom;

  RequestBundle({this.id, this.rooms}) {
    rooms ??= [];
  }

  bool isOwn() {
    if (broadcastRoom != null) {
      final req = Request(broadcastRoom);
      return req.isAuthor();
    } else if (rooms.isNotEmpty) {
      final req = Request(rooms[0]);
      return req.isAuthor();
    }
    return false;
  }

  bool isOwnBroadcast() {
    if (broadcastRoom != null) {
      final req = Request(broadcastRoom);
      return req.isAuthor();
    } else if (rooms.length > 1) {
      // greater one, because if it is only one room in the bundle then it is a normal request
      final req = Request(rooms[0]);
      return req.isAuthor();
    }
    return false;
  }

  bool isAdmin() {
    if (broadcastRoom != null) {
      return broadcastRoom.ownPowerLevel >= 99 ||
          broadcastRoom.membership == Membership.invite;
    } else if (rooms.isNotEmpty) {
      return rooms[0].ownPowerLevel >= 99 ||
          rooms[0].membership == Membership.invite;
    }
    return false;
  }
}

typedef RoomFilter = bool Function(Room room);
typedef RequestBundleFilter = bool Function(RequestBundle requestBundle);

class Request {
  static const String requestNameSpace = 'com.famedly.app.request';
  static const String broadcastNameSpace = 'com.famedly.app.broadcast';
  static const String requestStateNameSpace = 'com.famedly.app.request_state';
  static const String usersNameSpace = 'com.famedly.app.users';

  final Room room;
  bool isRequest;
  bool requesting;
  RequestContent content;
  BroadcastContent broadcast;
  BroadcastContactsContent contact;
  RequestStateContent state;

  Request(this.room, {this.contact, String contactId}) {
    final contentState = room.getState(Request.requestNameSpace);
    if (contentState == null) {
      // we are likely an invite, so let's assume that as good as possible
      state = RequestStateContent.newBlank();
      requesting = false;
      isRequest = false;
      return;
    }
    isRequest = true;
    content = RequestContent.fromJson(contentState.content);
    requesting = isAuthor();

    final broadcastState = room.getState(Request.broadcastNameSpace);
    if (broadcastState != null) {
      broadcast = BroadcastContent.fromJson(broadcastState.content);
      if (contact == null && contactId != null) {
        for (final c in broadcast.contacts) {
          if (c.contactId == contactId) {
            contact = c;
            break;
          }
        }
      }
    }

    final stateState = room.getState(Request.requestStateNameSpace,
        broadcast != null && contact != null ? contact.contactId : '');
    if (stateState == null) {
      state = RequestStateContent.newBlank();
    } else {
      state = RequestStateContent.fromJson(stateState.content);
    }
  }

  String get authorId {
    if (!isRequest) {
      return room.getState(EventTypes.RoomMember, room.client.userID) != null
          ? room.getState(EventTypes.RoomMember, room.client.userID).senderId
          : '';
    }
    if (content.creator != null && content.creator != '') {
      return content.creator;
    }
    return room.getState(Request.requestNameSpace).senderId;
  }

  User get author {
    return room.getState(EventTypes.RoomMember, authorId)?.asUser ??
        User(authorId);
  }

  bool isSubUser(String authorId, String userId, [Set<String> iterated]) {
    if (authorId == userId) {
      return true;
    }
    iterated ??= <String>{};
    if (iterated.contains(authorId)) {
      return false; // prevent recursing into the same object
    }
    iterated.add(authorId);
    final usersState = room.getState(Request.usersNameSpace, authorId);
    if (usersState == null) {
      return false;
    }
    if (usersState.content['users'].contains(userId)) {
      return true;
    }
    for (final user in usersState.content['users']) {
      if (isSubUser(user, userId, iterated)) {
        return true;
      }
    }
    return false;
  }

  bool isAuthor([String userId]) {
    userId ??= room.client.userID;
    return isSubUser(authorId, userId);
  }

  String get title {
    if (!isRequest ||
        content.requestTitle == null ||
        content.requestTitle == '') {
      return author.calcDisplayname(mxidLocalPartFallback: false);
    }
    return content.requestTitle;
  }

  String get organisation {
    if (!isRequest) {
      return null;
    }
    return contact == null
        ? content.organisationName
        : contact.organisationName;
  }

  String get description {
    if (!isRequest) {
      return null;
    }
    return contact == null
        ? content.contactDescription
        : contact.contactDescription;
  }

  Future<String> accept() async {
    if (requesting) {
      state.requesting.accept = true;
      state.requesting.reject = false;
      state.requesting.needInfo = false;
    } else {
      state.requested.accept = true;
      state.requested.reject = false;
      state.requested.needInfo = false;
    }
    final roomId = await sendState();
    if (contact == null) {
      await room.sendEvent({
        'msgtype': Request.requestStateNameSpace,
        'accept': true,
        'body':
            "The ${requesting ? 'requesting' : 'requested'} party has accepted the request",
      });
    }
    return roomId;
  }

  Future<String> reject() async {
    state.resolved = true;
    if (requesting) {
      state.requesting.accept = false;
      state.requesting.reject = true;
      state.requesting.needInfo = false;
    } else {
      state.requested.accept = false;
      state.requested.reject = true;
      state.requested.needInfo = false;
    }
    final roomId = await sendState(false);
    if (contact == null) {
      await room.sendEvent({
        'msgtype': Request.requestStateNameSpace,
        'reject': true,
        'body':
            "The ${requesting ? 'requesting' : 'requested'} party has rejected the request",
      });
    }
    return roomId;
  }

  Future<String> needInfo() async {
    if (requesting) {
      state.requesting.accept = false;
      state.requesting.reject = false;
      state.requesting.needInfo = true;
    } else {
      state.requested.accept = false;
      state.requested.reject = false;
      state.requested.needInfo = true;
    }
    final roomId = await sendState();
    // for need more information we do *not* send a message down the timeline
    return roomId;
  }

  Future<String> resolve() async {
    state.resolved = true;
    return await sendState();
  }

  Future<String> roomHandoff() async {
    if (contact == null) {
      return room.id;
    }
    // first we set the room handoff on the broadcast state
    state.roomHandoff = true;
    // we need to create a new room and stuffs
    final invites = <String>{};
    final userPermissions = <String, int>{
      room.client.userID: 100,
    };
    invites.add(content.creator);
    userPermissions[content.creator] = 100;
    if (contact.matrixId != null) {
      invites.add(contact.matrixId);
      userPermissions[contact.matrixId] = 100;
    }
    invites.remove(room.client.userID);
    final roomId = await room.client.createRoom(
      invite: invites.toList(),
      visibility: sdk.Visibility.private,
      preset: CreateRoomPreset.private_chat,
      name: Request.requestNameSpace,
      initialState: [
        {
          'type': Request.requestNameSpace,
          'content': RequestContent(
            creator: content.creator,
            requestedOrganisation: contact.requestedOrganisation,
            requestingOrganisation: content.requestingOrganisation,
            requestTitle: broadcast.title,
            requestId: content.requestId,
            msgtype: 'm.text',
            body: content.body,
            organisationId: contact.organisationId,
            organisationName: contact.organisationName,
            contactId: contact.contactId,
            contactDescription: contact.contactDescription,
            requestType: content.requestType,
            isBroadcast: content.isBroadcast,
          ).toJson(),
        },
        {
          'type': 'm.room.history_visibility',
          'content': {'history_visibility': 'shared'}
        },
        {
          'type': 'm.room.guest_access',
          'content': {'guest_access': 'forbidden'}
        },
        {
          'type': Request.requestStateNameSpace,
          'content': state.toJson(),
        },
        {
          'type': 'm.room.type',
          'content': {'type': roomTypeRequest},
        },
      ],
      powerLevelContentOverride: {
        'events': {
          Request.requestStateNameSpace: 0,
        },
        'users': userPermissions,
      },
    );
    if (roomId == null) {
      throw ('Room Handoff failed.');
    }
    await sendState(false); // send once the state in the broadcast room
    return roomId;
  }

  Future<String> sendState([bool createRoom = true]) async {
    if (contact == null || !createRoom) {
      var path = '/client/r0/rooms/' +
          room.id +
          '/state/' +
          Request.requestStateNameSpace;
      if (contact != null) {
        path += '/${contact.contactId}';
      }
      await room.client.request(RequestType.PUT, path, data: state.toJson());
      return room.id;
    } else {
      return await roomHandoff();
    }
  }

  bool remoteAccepted() {
    return requesting ? state.requested.accept : state.requesting.accept;
  }

  bool remoteRejected() {
    return requesting ? state.requested.reject : state.requesting.reject;
  }

  bool remoteNeedInfo() {
    return requesting ? state.requested.needInfo : state.requesting.needInfo;
  }

  bool youAccepted() {
    return requesting ? state.requesting.accept : state.requested.accept;
  }

  bool youRejected() {
    return requesting ? state.requesting.reject : state.requested.reject;
  }

  bool youNeedInfo() {
    return requesting ? state.requesting.needInfo : state.requested.needInfo;
  }

  static List<BroadcastContactsContent> getBroadcastContacts(
      Room room, String userId) {
    final req = Request(room);
    if (req.isAuthor()) return null;
    final ret = <BroadcastContactsContent>[];
    for (final contact in req.broadcast.contacts) {
      // check the URI
      if (contact.matrixId == userId ||
          (room.getState(Request.usersNameSpace, contact.matrixId) != null &&
              room
                  .getState(Request.usersNameSpace, contact.matrixId)
                  .content['users']
                  .contains(userId))) {
        final requestState =
            room.getState(Request.requestStateNameSpace, contact.contactId);
        final state = requestState == null
            ? RequestStateContent.newBlank()
            : RequestStateContent.fromJson(requestState.content);
        if (!state.roomHandoff && !state.requested.reject) {
          ret.add(contact);
        }
        continue;
      }
    }
    return ret;
  }

  static RequestBundle getRequestBundle(Client client, String requestId) {
    final ret = RequestBundle();
    for (final room in client.rooms) {
      if (room.type != roomTypeRequest) continue;
      final req = Request(room);
      if (!req.isRequest) continue;
      if (req.content.requestId != requestId) continue;
      if (req.broadcast != null) {
        ret.broadcastRoom = room;
      } else {
        ret.rooms.add(room);
      }
    }
    return ret;
  }

  /// Starts an official request to this organisation at this contact point. Returns
  /// the request ID if successfull.
  static Future<String> startBroadcast({
    String requestTitle,
    String requestBody,
    String requestType,
    List<Contact> contactList,
    Client client,
  }) async {
    final requestContacts = <Map<String, dynamic>>[];
    for (final contact in contactList) {
      requestContacts.add({
        'requested_organisation':
            contact.matrixId?.domain ?? contact.email?.split('@')[1],
        'organisation_id': contact.organisation?.id,
        'organisation_name': contact.organisation?.name,
        'contact_id': contact.id,
        'contact_description': contact.description,
        'contact_uri': contact.uri,
        'passport': contact.passport,
      });
    }
    final requestData = <String, dynamic>{
      'request_title': requestTitle,
      'content': {
        'msgtype': 'm.text',
        'body': requestBody,
      },
      'requesting_organisation': client.userID.domain,
      'request_type': requestType,
      'contacts': requestContacts,
      'invite': [], // this can be an array of additional MXIDs to invite
    };
    final res = await client.request(
      RequestType.POST,
      '/client/unstable/com.famedly/lisa/r0/createBroadcast',
      data: requestData,
    );

    return res['request_id'];
  }

  static Future<void> closeBroadcast(RequestBundle bundle,
      [Room acceptRoom]) async {
    if (bundle.broadcastRoom != null) {
      // lisa doesn't handle a broadcast room, so we'll keep doing these ourself
      Request req;
      if (acceptRoom != null) {
        req = Request(acceptRoom);
      }
      for (final contact in Request(bundle.broadcastRoom).broadcast.contacts) {
        if (req != null && contact.contactId == req.content.contactId) continue;
        var leftRoom = false;
        for (final r in bundle.rooms) {
          if (acceptRoom != null && r.id == acceptRoom.id) continue;
          final reqq = Request(r);
          if (reqq.content.contactId != contact.contactId) continue;
          await reqq.reject();
          await r.leave();
          leftRoom = true;
          break;
        }
        if (!leftRoom) {
          await Request(bundle.broadcastRoom, contact: contact).reject();
        }
      }
      await bundle.broadcastRoom.leave();
      if (req != null) {
        await req.accept();
      }
    } else if (bundle.rooms.isNotEmpty) {
      // we have lisa to our availability!
      final allRoomIds = <String>[];
      for (final r in bundle.rooms) {
        allRoomIds.add(r.id);
      }
      final requestData = <String, dynamic>{
        'rooms': allRoomIds,
      };
      if (acceptRoom != null) {
        requestData['accept'] = acceptRoom.id;
      }
      await bundle.rooms[0].client.request(
        RequestType.POST,
        '/client/unstable/com.famedly/lisa/r0/closeBroadcast',
        data: requestData,
      );
    }
  }

  static List<RequestBundle> bundleFromRoomList({
    List<Room> roomList,
    RoomFilter roomFilter,
    RequestBundleFilter requestBundleFilter,
  }) {
    final requests = <RequestBundle>[];
    // first we iterate over all the rooms
    for (final room in roomList) {
      if (room.type != roomTypeRequest) {
        continue;
      } // ignore it, if it isn't a request
      if (roomFilter != null && !roomFilter(room)) {
        continue;
      } // optionally apply the room filter
      if (room.states[Request.requestNameSpace] == null) {
        // alright, the room doesn't have a request state but it is a request
        // meaning we were just invited and it has the magic room name
        // generate a random place holder ID
        final uuid = Uuid();
        requests.add(RequestBundle(id: uuid.v4(), rooms: [room]));
        continue;
      }
      final req = Request(room);
      if (req.content.requestId == null) {
        // we are an old request without an ID.
        // let's just assign a random one for displaying and continue as normal
        final uuid = Uuid();
        requests.add(RequestBundle(id: uuid.v4(), rooms: [room]));
        continue;
      }
      if (!req.isAuthor()) {
        // we aren't the author so we don't bundle this with other reqeusts
        final uuid = Uuid();
        if (req.broadcast != null) {
          final bundle = RequestBundle(id: uuid.v4(), rooms: []);
          bundle.broadcastRoom = room;
          requests.add(bundle);
        } else {
          requests.add(RequestBundle(id: uuid.v4(), rooms: [room]));
        }
        continue;
      }
      // okay, now we can normally process the room
      // first we process if there is already a request bundle for this room
      RequestBundle r;
      for (final rr in requests) {
        if (rr.id == req.content.requestId) {
          r = rr;
          break;
        }
      }
      if (r == null) {
        // create new request bundle object
        if (req.broadcast != null) {
          final bundle = RequestBundle(id: req.content.requestId, rooms: []);
          bundle.broadcastRoom = room;
          requests.add(bundle);
        } else {
          requests.add(RequestBundle(id: req.content.requestId, rooms: [room]));
        }
      } else {
        // add room to existing request bundle object
        if (req.broadcast != null) {
          r.broadcastRoom = room;
        } else {
          r.rooms.add(room);
        }
      }
    }
    // alright, we have all the requests bundled into the `request` variable,
    // only thing left is to apply those last filters and return it
    return requests
        ?.where((bundle) =>
            !(requestBundleFilter != null && !requestBundleFilter(bundle)))
        ?.toList();
  }
}
