import 'dart:math';

import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:famedly/components/dialogs/key_verification_dialog.dart';
import 'package:famedly/config/tutorial_video_links.dart';
import 'package:famedly/models/famedly.dart';
import 'package:famedly/styles/famedly_icons_v2_icons.dart';
import 'package:famedly/utils/error_reporter.dart';
import 'package:famedlysdk/encryption.dart';
import 'package:famedlysdk/encryption/utils/bootstrap.dart';
import 'package:famedlysdk/famedlysdk.dart';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:adaptive_page_layout/adaptive_page_layout.dart';
import 'package:future_loading_dialog/future_loading_dialog.dart';
import 'package:url_launcher/url_launcher.dart';

class BootstrapPage extends StatefulWidget {
  final bool wipe;

  BootstrapPage({
    Key key,
    this.wipe = false,
  }) : super(key: key ?? Key('wipe:$wipe'));
  @override
  _BootstrapPageState createState() => _BootstrapPageState();
}

class _BootstrapPageState extends State<BootstrapPage> {
  static const int recoveryKeyFieldCount = 8;

  final List<TextEditingController> _recoveryKeyTextEditingController =
      List<TextEditingController>.generate(
          recoveryKeyFieldCount, (i) => TextEditingController());

  final List<FocusNode> _recoveryKeyTextFocusNode =
      List<FocusNode>.generate(recoveryKeyFieldCount, (i) => FocusNode());

  Bootstrap bootstrap;

  bool _recoveryKeyInputLoading = false;

  String stateText;

  bool _recoveryKeyStored = false;

  Future<bool> waitForFirstSync(BuildContext context) async {
    if (Famedly.of(context).client.prevBatch != null) return true;
    return Famedly.of(context).client.onFirstSync.stream.first;
  }

  void _createBootstrap(BuildContext context) {
    setState(() {
      stateText = null;
      bootstrap ??=
          Famedly.of(context).client.encryption.bootstrap(onUpdate: () {
        if (mounted) setState(() => null);
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: waitForFirstSync(context),
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
          if (widget.wipe && bootstrap == null) {
            WidgetsBinding.instance.addPostFrameCallback((_) {
              _createBootstrap(context);
            });
          }
          final hasBackup = Famedly.of(context).client.encryptionEnabled &&
              Famedly.of(context).client.encryption.crossSigning.enabled &&
              Famedly.of(context).client.encryption.keyManager.enabled;
          List<Widget> body;
          if (bootstrap == null) {
            stateText = L10n.of(context).configureChatBackup;
            body = [
              Text(hasBackup
                  ? L10n.of(context).openYourBackupDescription
                  : L10n.of(context).securingYourKeysDescription),
              SizedBox(height: 36),
              InkWell(
                borderRadius: BorderRadius.circular(12),
                onTap: () => launch(TutorialVideoLinks.bootstrap),
                child: Container(
                  decoration: BoxDecoration(
                    color: Theme.of(context).secondaryHeaderColor,
                    borderRadius: BorderRadius.circular(12),
                    border: Border.all(
                      width: 1,
                      color: Theme.of(context).dividerColor,
                    ),
                  ),
                  child: Padding(
                    padding: EdgeInsets.symmetric(vertical: 36),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(Icons.play_circle_fill_outlined, size: 40),
                        SizedBox(height: 4),
                        Text(L10n.of(context).whatIsThis,
                            textAlign: TextAlign.center),
                      ],
                    ),
                  ),
                ),
              ),
              SizedBox(height: 36),
              if (!hasBackup)
                ElevatedButton(
                  key: Key('startBootstrapButton'),
                  onPressed: () => _createBootstrap(context),
                  child: Text(L10n.of(context).continueText),
                ),
              if (hasBackup) ...{
                _BootstrapButton(
                  title: L10n.of(context).requestFromOtherDevice,
                  description:
                      L10n.of(context).requestFromOtherDeviceDescription,
                  onPressed: () async {
                    final client = Famedly.of(context).client;
                    final req = await client.userDeviceKeys[client.userID]
                        .startVerification();
                    final success = await KeyVerificationDialog(
                      context: context,
                      request: req,
                    ).show(context);
                    if (success == true) {
                      final isCached = await showFutureLoadingDialog<bool>(
                        context: context,
                        future: () async {
                          for (var i = 0; i < 10; i++) {
                            await Future.delayed(Duration(seconds: 2));
                            if (await Famedly.of(context)
                                .client
                                .encryption
                                .keyManager
                                .isCached()) {
                              return true;
                            }
                          }
                          return false;
                        },
                        title: L10n.of(context).waitingPartnerAcceptRequest,
                      );
                      if (isCached.result == true) {
                        await AdaptivePageLayout.of(context)
                            .pushNamedAndRemoveAllOthers('/');
                      } else {
                        await showOkAlertDialog(
                          context: context,
                          title: L10n.of(context).didNotWork,
                          message: L10n.of(context)
                              .pleaseTryAgainLaterOrWithAnotherDevice,
                          okLabel: L10n.of(context).close,
                        );
                      }
                    }
                  },
                ),
                SizedBox(height: 8),
                _BootstrapButton(
                  key: Key('startBootstrapButton'),
                  title: L10n.of(context).enterSecurityKey,
                  description: L10n.of(context).enterSecurityKeyDescription,
                  onPressed: () => _createBootstrap(context),
                ),
              }
            ];
          } else if (bootstrap.newSsssKey?.recoveryKey != null &&
              _recoveryKeyStored == false) {
            // We are reformatting the recovery key in groups of 6 characters. The spec
            // is recommending 4 characters but for our use case this might increase
            // the readability.
            final key = bootstrap.newSsssKey.recoveryKey
                .replaceAll(' ', '')
                .replaceAllMapped(RegExp(r'.{6}'), (s) => '${s.group(0)} ')
                .trim();
            final keyParts = key.split(' ');
            stateText = L10n.of(context).securityKey;
            body = [
              Text(L10n.of(context).securityKeyDescription),
              SizedBox(height: 46),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 32),
                alignment: Alignment.center,
                child: GridView.count(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  crossAxisCount: 4,
                  mainAxisSpacing: 16,
                  crossAxisSpacing: 8,
                  childAspectRatio: 2,
                  children: keyParts
                      .map(
                        (string) => Text(
                          string,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontFamily: 'monospace',
                            color: Theme.of(context).textTheme.bodyText1.color,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      )
                      .toList(),
                ),
              ),
              SizedBox(height: 36),
              _BootstrapButton(
                key: Key('copySecurityKeyToClipboardButton'),
                title: L10n.of(context).copyToClipboard,
                description: L10n.of(context).copyToClipboardDescription,
                onPressed: () async {
                  await Clipboard.setData(ClipboardData(text: key));
                  if (OkCancelResult.ok ==
                      await showOkCancelAlertDialog(
                        context: context,
                        title: L10n.of(context).copiedToClipboard,
                        message: L10n.of(context).pleaseStoreKey,
                        okLabel: L10n.of(context).continueText,
                        cancelLabel: L10n.of(context).cancel,
                        useRootNavigator: false,
                      )) {
                    setState(() => _recoveryKeyStored = true);
                  } else {
                    await Clipboard.setData(ClipboardData(text: ''));
                  }
                },
              ),
              SizedBox(height: 10),
              _BootstrapButton(
                title: L10n.of(context).writeDownSecurityKey,
                description: L10n.of(context).writeDownSecurityKeyDescription,
                onPressed: () {
                  setState(() => _recoveryKeyStored = true);
                },
              ),
            ];
          } else {
            switch (bootstrap.state) {
              case BootstrapState.askWipeSsss:
                stateText = L10n.of(context).onlineBackupHasBeenFound;
                WidgetsBinding.instance.addPostFrameCallback(
                  (_) => bootstrap.wipeSsss(widget.wipe),
                );
                break;
              case BootstrapState.askUseExistingSsss:
                stateText = L10n.of(context).onlineBackupHasBeenFound;
                WidgetsBinding.instance.addPostFrameCallback(
                  (_) => bootstrap.useExistingSsss(!widget.wipe),
                );
                break;
              case BootstrapState.askUnlockSsss:
                throw Exception('This state is not supposed to be implemented');
              case BootstrapState.askNewSsss:
                stateText = L10n.of(context).creatingSecurityKey;
                WidgetsBinding.instance.addPostFrameCallback(
                  (_) => bootstrap.newSsss(),
                );
                break;
              case BootstrapState.openExistingSsss:
                _recoveryKeyStored = true;
                stateText = L10n.of(context).enterSecurityKey;
                body = [
                  TextButton.icon(
                    key: Key('pasteSecurityKeyFromClipboardButton'),
                    onPressed: _recoveryKeyInputLoading
                        ? null
                        : () => Clipboard.getData(Clipboard.kTextPlain)
                                .then((data) {
                              var parts = data.text
                                  .replaceAll(' ', '')
                                  .replaceAllMapped(
                                      RegExp(r'.{6}'), (s) => '${s.group(0)} ')
                                  .trim()
                                  .split(' ');

                              if (parts.length !=
                                  _recoveryKeyTextEditingController.length) {
                                parts = data.text.split(' ');
                                if (parts.length !=
                                    _recoveryKeyTextEditingController.length) {
                                  AdaptivePageLayout.of(context)
                                      .showSnackBar(SnackBar(
                                    content: Text(L10n.of(context).didNotWork),
                                  ));
                                  return;
                                }
                              }
                              for (var i = 0;
                                  i < _recoveryKeyTextEditingController.length;
                                  i++) {
                                _recoveryKeyTextEditingController[i].text =
                                    parts[i];
                              }
                            }),
                    icon: Icon(Icons.paste_outlined),
                    label: Text(L10n.of(context).pasteFromClipboard),
                  ),
                  GridView.count(
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    crossAxisCount: 4,
                    mainAxisSpacing: 32,
                    crossAxisSpacing: 8,
                    childAspectRatio: 2,
                    children: List<Widget>.generate(
                      recoveryKeyFieldCount,
                      (i) => Center(
                        child: TextField(
                          maxLength: 6,
                          autocorrect: false,
                          readOnly: _recoveryKeyInputLoading,
                          autofillHints: _recoveryKeyInputLoading
                              ? null
                              : [AutofillHints.password],
                          autofocus: i == 0,
                          style: TextStyle(fontFamily: 'monospace'),
                          onChanged: (text) {
                            if (text.length == 6 &&
                                i < _recoveryKeyTextFocusNode.length - 1) {
                              _recoveryKeyTextFocusNode[i + 1].requestFocus();
                            } else if (text.isEmpty && i > 0) {
                              _recoveryKeyTextFocusNode[i - 1].requestFocus();
                            }
                          },
                          keyboardType: TextInputType.visiblePassword,
                          focusNode: _recoveryKeyTextFocusNode[i],
                          controller: _recoveryKeyTextEditingController[i],
                          decoration: InputDecoration(
                            contentPadding: EdgeInsets.symmetric(
                                horizontal: 10, vertical: 0),
                            counterText: '',
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 32),
                  ElevatedButton(
                    key: Key('unlockBackupButton'),
                    onPressed: _recoveryKeyInputLoading
                        ? null
                        : () async {
                            setState(() => _recoveryKeyInputLoading = true);
                            try {
                              var input = '';
                              _recoveryKeyTextEditingController.forEach(
                                  (controller) => input += controller.text);
                              await bootstrap.newSsssKey.unlock(
                                keyOrPassphrase: input.trim(),
                              );
                              await bootstrap.openExistingSsss();
                              if (Famedly.of(context)
                                  .client
                                  .encryption
                                  .crossSigning
                                  .enabled) {
                                Logs().v(
                                    'Cross signing is already enabled. Try to self-sign');
                                try {
                                  await Famedly.of(context)
                                      .client
                                      .encryption
                                      .crossSigning
                                      .selfSign(recoveryKey: input);
                                } catch (e, s) {
                                  // ignore: unawaited_futures
                                  ErrorReporter.reportError(
                                      'Unable to self sign with recovery key after successfully open existing SSSS: ${e.toString()}',
                                      s);
                                }
                              }
                            } catch (e, s) {
                              Logs().w('Unable to unlock SSSS', e, s);
                              AdaptivePageLayout.of(context)
                                  .showSnackBar(SnackBar(
                                content: Text(L10n.of(context).didNotWork),
                              ));
                            } finally {
                              setState(() => _recoveryKeyInputLoading = false);
                            }
                          },
                    child: _recoveryKeyInputLoading
                        ? LinearProgressIndicator()
                        : Text(L10n.of(context).continueText),
                  ),
                  TextButton(
                    key: Key('resetRecoveryKeyButton'),
                    onPressed: () async {
                      if (OkCancelResult.ok ==
                          await showOkCancelAlertDialog(
                            context: context,
                            title: L10n.of(context).areYouSure,
                            message: L10n.of(context)
                                .deleteAndCreateBackupNewDescription,
                            okLabel: L10n.of(context).yes,
                            cancelLabel: L10n.of(context).cancel,
                            isDestructiveAction: true,
                            useRootNavigator: false,
                          )) {
                        return AdaptivePageLayout.of(context)
                            .popAndPushNamed('/backup', arguments: true);
                      }
                    },
                    child: Text(
                      L10n.of(context).deleteAndCreateBackupNew,
                      style: TextStyle(color: Colors.red),
                    ),
                  ),
                ];
                break;
              case BootstrapState.askWipeCrossSigning:
                stateText = L10n.of(context).wipingDeviceVerification;
                WidgetsBinding.instance.addPostFrameCallback(
                  (_) => bootstrap.wipeCrossSigning(widget.wipe),
                );
                break;
              case BootstrapState.askSetupCrossSigning:
                stateText = L10n.of(context).createDeviceVerification;
                WidgetsBinding.instance.addPostFrameCallback(
                  (_) => bootstrap.askSetupCrossSigning(
                    setupMasterKey: true,
                    setupSelfSigningKey: true,
                    setupUserSigningKey: true,
                  ),
                );

                break;
              case BootstrapState.askWipeOnlineKeyBackup:
                stateText = L10n.of(context).wipingBackup;
                WidgetsBinding.instance.addPostFrameCallback(
                  (_) => bootstrap.wipeOnlineKeyBackup(widget.wipe),
                );
                break;
              case BootstrapState.askSetupOnlineKeyBackup:
                stateText = L10n.of(context).setupBackup;
                WidgetsBinding.instance.addPostFrameCallback(
                  (_) => bootstrap.askSetupOnlineKeyBackup(true),
                );
                break;
              case BootstrapState.loading:
                break;
              case BootstrapState.askBadSsss:
              case BootstrapState.error:
                stateText = L10n.of(context).oopsSomethingWentWrong;
                body = [
                  Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.all(16),
                    child: Icon(
                      Icons.error_rounded,
                      size: 40,
                      color: Colors.red,
                    ),
                  ),
                  SizedBox(height: 36),
                  ElevatedButton(
                    onPressed: () => AdaptivePageLayout.of(context)
                        .pushNamedAndRemoveAllOthers('/'),
                    child: Text(L10n.of(context).back),
                  ),
                ];
                break;
              case BootstrapState.done:
                Famedly.of(context).loginPlugin.cachedPassword = null;
                stateText = L10n.of(context).backupSynchronized;
                body = [
                  Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.all(16),
                    child: Icon(
                      Icons.check_circle_outlined,
                      size: 40,
                      color: Colors.green,
                    ),
                  ),
                  SizedBox(height: 36),
                  ElevatedButton(
                    key: Key('finishBootstrapButton'),
                    onPressed: () => AdaptivePageLayout.of(context)
                        .pushNamedAndRemoveAllOthers('/'),
                    child: Text(L10n.of(context).continueText),
                  ),
                ];
                break;
            }
            stateText ??= L10n.of(context).loading;
            body ??= [
              Container(
                alignment: Alignment.center,
                padding: EdgeInsets.all(16),
                child: CircularProgressIndicator(),
              ),
            ];
          }

          return WillPopScope(
            onWillPop: () async {
              await AdaptivePageLayout.of(context)
                  .pushNamedAndRemoveAllOthers('/');
              return Future.value(false);
            },
            child: Scaffold(
              appBar: AppBar(
                leading: BackButton(),
                elevation: 0,
              ),
              body: ListView(
                padding: EdgeInsets.symmetric(
                  horizontal:
                      max(32, (MediaQuery.of(context).size.width - 400) / 2),
                  vertical: 32,
                ),
                children: [
                  Text(
                    stateText,
                    style: TextStyle(
                      fontSize: 22,
                      fontWeight: FontWeight.bold,
                      color: Theme.of(context).textTheme.bodyText1.color,
                    ),
                  ),
                  SizedBox(height: 36),
                  ...body,
                ],
              ),
            ),
          );
        });
  }
}

class _BootstrapButton extends StatelessWidget {
  final String title;
  final String description;
  final void Function() onPressed;

  const _BootstrapButton({
    Key key,
    @required this.title,
    @required this.description,
    this.onPressed,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      style: ElevatedButton.styleFrom(
        elevation: 0,
        padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 10),
      ),
      onPressed: onPressed,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Row(
            children: [
              Text(title),
              Spacer(),
              Icon(FamedlyIconsV2.right_1),
            ],
          ),
          SizedBox(height: 6),
          Opacity(
            opacity: 0.8,
            child: Text(
              description,
              style: TextStyle(
                fontSize: 12,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
