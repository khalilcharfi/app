#!/bin/bash

set -e
# get access token
token=$(curl -S -f -H "Content-Type: application/json" -d "{\"username\":\"$INTEGRATION_SERVER_ADMIN_USERNAME\",\"password\":\"$INTEGRATION_SERVER_ADMIN_PASSWORD\"}" $INTEGRATION_SERVER_ADMIN_API/api/login | jq -r '.token')
# Get an access token for the newly created user so we can call _matrix/client/r0/account/whoami
usertoken=$(curl -S  https://${INTEGRATION_SERVER_URL}/_matrix/client/r0/login -H "Content-Type: application/json" -d "{\"type\": \"m.login.password\", \"identifier\": {\"type\": \"m.id.user\",\"user\": \"${INTEGRATION_TEST_DYNAMIC_USERNAME}\"},\"password\":\"$INTEGRATION_TESTUSER_PASSWORD\"}" | jq -r '.access_token')
# get our mxid
encoded_mxid=$(curl -S https://${INTEGRATION_SERVER_URL}/_matrix/client/r0/account/whoami -H "Authorization: Bearer $usertoken" | jq -r ".user_id | @uri")
# delete mxid from test role
curl -f -S "https://$INTEGRATION_SERVER_URL/_matrix/client/unstable/com.famedly/role/r0/role/${INTEGRATION_TEST_ROLEID}/user/$encoded_mxid" -X DELETE -H 'Content-Length: 0' -H "Authorization: Bearer $token"
# delete mxid from test group
curl -f -S "https://$INTEGRATION_SERVER_URL/_matrix/client/unstable/com.famedly/role/r0/role/${INTEGRATION_TEST_GROUPID}/user/$encoded_mxid" -X DELETE -H 'Content-Length: 0' -H "Authorization: Bearer $token"
# deactivate the user
curl -f -S "$INTEGRATION_SERVER_ADMIN_API/api/user/$INTEGRATION_TEST_DYNAMIC_USERNAME" -X PUT -H 'Content-Type: application/json' -H "Authorization: Bearer $token" --data-raw '{"deactivated":true}'
