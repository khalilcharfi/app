import 'dart:async';
import 'dart:io';
import 'dart:ui';

import 'package:adaptive_page_layout/adaptive_page_layout.dart';
import 'package:famedly/components/FamedlyPopupMenuButton.dart';
import 'package:famedly/styles/famedly_icons_v2_icons.dart';
import 'package:famedly/utils/famedly_file_picker.dart';
import 'package:famedly/utils/platform_extension.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:file_picker_cross/file_picker_cross.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:future_loading_dialog/future_loading_dialog.dart';
import 'package:image_picker/image_picker.dart';
import 'package:keep_keyboard_popup_menu/keep_keyboard_popup_menu.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:record/record.dart';
import 'package:vibration/vibration.dart';

import '../../styles/colors.dart';
import '../../styles/famedly_icons_icons.dart';
import '../../utils/format.dart';
import '../../utils/room_send_file_extension.dart';
import '../../views/image_editor.dart';
import '../avatar.dart';
import '../custom_popup_menu_item.dart';
import '../web_enter_listener.dart';
import 'event_widgets/text_event_content.dart';

class InputField extends StatefulWidget {
  final ValueChanged<String> onSendText;
  final ValueChanged<String> onSendAudio;
  final Room room;

  final Event replyTo;

  final VoidCallback onCloseReply;

  static const height = 78.0;

  static const String defaultAudioFileType = 'aac';

  InputField(
    this.room, {
    Key key,
    this.replyTo,
    this.onCloseReply,
    this.onSendText,
    this.onSendAudio,
  }) : super(key: key);

  @override
  InputFieldState createState() => InputFieldState();
}

class InputFieldState extends State<InputField> {
  String text;
  final TextEditingController _inputController = TextEditingController();
  bool isTyping = false;
  static Duration typingCoolDownDuration = Duration(seconds: 2);
  Timer isTypingCoolDown;
  static Duration typingTimeoutDuration = Duration(seconds: 30);
  Timer isTypingTimeout;

  BorderSide defaultBorderSide() => BorderSide(width: 1.0, color: Colors.grey);

  OutlineInputBorder defaultOutlineInputBorder() => OutlineInputBorder(
        borderRadius: BorderRadius.circular(20),
        borderSide: defaultBorderSide(),
      );

  bool micOrSend = true;
  FocusNode inputFocus = FocusNode();

  bool get _hasTyped =>
      _inputController.text != null && _inputController.text.isNotEmpty;

  StreamSubscription _recorderSubscription;
  bool _isRecording = false;

  Duration _recordingDuration = Duration.zero;
  Timer _recordingTimer;

  String _recordedPath;

  Future<void> _startRecording() async {
    if (await Permission.microphone.isGranted) {
      final tempDir = await getTemporaryDirectory();
      _recordedPath =
          '${tempDir.path}/recording_${DateTime.now().microsecondsSinceEpoch}.${InputField.defaultAudioFileType}';

      // delete any existing file
      final outputFile = File(_recordedPath);
      if (outputFile.existsSync()) {
        await outputFile.delete();
      }
      // Check and request permission
      final result = await Record.hasPermission();
      if (result) {
        await Record.start(path: _recordedPath);
        setState(() => _isRecording = true);
        _recordingTimer?.cancel();
        _recordingDuration = Duration.zero;
        _recordingTimer = Timer.periodic(
          Duration(milliseconds: 100),
          (_) => setState(
            () => _recordingDuration += Duration(milliseconds: 100),
          ),
        );
      }
    } else {
      final status = await Permission.microphone.request();
      if (status != PermissionStatus.granted) {
        throw Exception('Microphone permission not granted');
      }
    }
  }

  Future<void> _stopRecording() async {
    await Record.stop();
    _recordingTimer?.cancel();
    _recordingDuration = Duration.zero;
    setState(() => _isRecording = false);
  }

  void _sendRecording() => widget.onSendAudio(_recordedPath);

  void submit() {
    if (text != null && text.trim() != '') {
      _inputController.clear();
      widget.onSendText(text);
      text = '';

      if (widget.replyTo != null) {
        widget.onCloseReply();
      }
    }
  }

  void pickAndSendFile(
    BuildContext context,
    bool pickImage, {
    ImageSource imageSource,
  }) async {
    MatrixFile file;
    FocusScope.of(context).requestFocus(FocusNode());
    if (pickImage && imageSource == ImageSource.camera && kIsMobile) {
      final tempFile = await ImagePicker().getImage(source: imageSource);
      if (tempFile == null) return;
      file = MatrixImageFile(
        bytes: await tempFile.readAsBytes(),
        name: tempFile.path,
      );
    } else {
      final tempFile = await FamedlyFilePicker.pickFiles(
        type: pickImage ? FileTypeCross.image : FileTypeCross.any,
        context: context,
      );

      if (tempFile == null) return;

      file = pickImage
          ? MatrixImageFile(
              bytes: tempFile.toUint8List(),
              name: tempFile.path,
            )
          : MatrixFile(
              bytes: tempFile.toUint8List(),
              name: tempFile.path,
            );
    }

    if (kIsMobile && file is MatrixImageFile) {
      file = await Navigator.push(
        context,
        MaterialPageRoute(builder: (_) => ImageEditor(file: file)),
      );
      if (file == null) return;
    }
    await showFutureLoadingDialog(
      context: context,
      future: () => widget.room.sendFileEventWithThumbnail(file),
    );
  }

  List<Map<String, String>> getSuggestions(String text) {
    if (_inputController.selection.baseOffset !=
            _inputController.selection.extentOffset ||
        _inputController.selection.baseOffset < 0) {
      return []; // no entries if there is selected text
    }
    final searchText = _inputController.text
        .substring(0, _inputController.selection.baseOffset);
    final ret = <Map<String, String>>[];
    final MAX_RESULTS = 10;
    final userMatch = RegExp(r'(?:\s|^)@([-\w]+)$').firstMatch(searchText);
    if (userMatch != null) {
      final userSearch = userMatch[1].toLowerCase();
      for (final user in widget.room.getParticipants()) {
        if ((user.displayName != null &&
                user.displayName.toLowerCase().contains(userSearch)) ||
            // famedly mxids are cryptic, so don't match single characters.
            (userSearch.length > 1 &&
                user.id.split(':')[0].toLowerCase().contains(userSearch))) {
          ret.add({
            'type': 'user',
            'mxid': user.id,
            'displayname': user.displayName,
            'avatar_url': user.avatarUrl?.toString(),
          });
        }
        if (ret.length > MAX_RESULTS) {
          break;
        }
      }
    }
    return ret;
  }

  Widget buildSuggestion(BuildContext context, Map<String, String> suggestion) {
    if (suggestion['type'] == 'user') {
      final size = 30.0;
      final url = Uri.parse(suggestion['avatar_url'] ?? '');
      return Container(
        padding: EdgeInsets.all(4.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Avatar(
              url,
              name: suggestion['displayname'] ?? suggestion['mxid'],
              size: size,
              matrixClient: widget.room.client,
            ),
            SizedBox(width: 6),
            Text(suggestion['displayname'] ?? suggestion['mxid']),
          ],
        ),
      );
    }
    return Container();
  }

  void insertSuggestion(BuildContext context, Map<String, String> suggestion) {
    final replaceText = _inputController.text
        .substring(0, _inputController.selection.baseOffset);
    var startText = '';
    final afterText = replaceText == _inputController.text
        ? ''
        : _inputController.text
            .substring(_inputController.selection.baseOffset + 1);
    var insertText = '';
    if (suggestion['type'] == 'user') {
      insertText = '@' + suggestion['displayname'] + ' ';
      startText = replaceText.replaceAllMapped(
        RegExp(r'(\s|^)(@[-\w]+)$'),
        (Match m) => '${m[1]}$insertText',
      );
    }
    if (insertText.isNotEmpty && startText.isNotEmpty) {
      text = _inputController.text = startText + afterText;
      _inputController.selection = TextSelection(
        baseOffset: startText.length,
        extentOffset: startText.length,
      );
    }
  }

  bool keyboardVisible = false;
  String _lastRoomId;

  StreamSubscription _onKeyboardVisibilityChangedSub;

  @override
  void initState() {
    if (kIsMobile) {
      _onKeyboardVisibilityChangedSub =
          KeyboardVisibilityController().onChange.listen(
                (bool visible) => setState(() => keyboardVisible = visible),
              );
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (_lastRoomId != widget.room.id) {
      _lastRoomId = widget.room.id;
      _inputController.clear();
    }
    final canRecord = kIsMobile && !_hasTyped && widget.replyTo == null;
    micOrSend = canRecord;

    return Container(
      decoration: BoxDecoration(
        color: Theme.of(context).appBarTheme.backgroundColor,
        boxShadow: [
          BoxShadow(
            color: FamedlyColors.slate.withOpacity(0.05),
            blurRadius: 1.0,
            spreadRadius: 0.0,
            offset: Offset(
              0.0,
              -2.0,
            ),
          )
        ],
      ),
      child: Padding(
        padding: EdgeInsets.all(15).copyWith(
          top: widget.replyTo != null ? 0 : null,
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Builder(builder: (BuildContext context) {
              if (widget.replyTo != null) {
                return Padding(
                  padding: EdgeInsets.only(
                    top: 12,
                    bottom: 16,
                  ),
                  child: TextReplyEventContent(
                    textColor: FamedlyColors.blueyGrey,
                    repliedEvent: widget.replyTo,
                    elevation: 4,
                    onTapClose: widget.onCloseReply,
                  ),
                );
              } else {
                return Container();
              }
            }),
            if (_isRecording)
              _RecordingStatus(recordingDuration: _recordingDuration),
            Offstage(
              offstage: _isRecording,
              child: Builder(builder: (BuildContext context) {
                final button = !canRecord
                    ? _SendButton(
                        onPressed: () {
                          if (_hasTyped && !_isRecording) {
                            submit();
                          }
                        },
                      )
                    : _MicrophoneButton(
                        key: Key('MicrophoneButton'),
                        onPressed: () {
                          AdaptivePageLayout.of(context).showSnackBar(
                            SnackBar(
                              content: Text(L10n.of(context).holdToRecord),
                            ),
                          );
                        },
                      );
                return Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    Container(
                      height: 40,
                      width: 40,
                      child: FamedlyPopupMenuButton(
                        menuItemBuilder: (context, closePopup) {
                          final entries = <KeepKeyboardPopupMenuItem>[
                            KeepKeyboardPopupMenuItem(
                              onTap: () {
                                pickAndSendFile(context, true,
                                    imageSource: ImageSource.gallery);
                                closePopup();
                              },
                              child: CustomPopupMenuItem(
                                  title: L10n.of(context).gallery,
                                  iconData: FamedlyIcons.image),
                            ),
                            KeepKeyboardPopupMenuItem(
                              onTap: () {
                                pickAndSendFile(context, false);
                                closePopup();
                              },
                              child: CustomPopupMenuItem(
                                  title: L10n.of(context).document,
                                  iconData: FamedlyIcons.document),
                            ),
                          ];
                          if (kIsMobile) {
                            entries.add(KeepKeyboardPopupMenuItem(
                              onTap: () {
                                pickAndSendFile(context, true,
                                    imageSource: ImageSource.camera);
                                closePopup();
                              },
                              child: CustomPopupMenuItem(
                                  title: L10n.of(context).camera,
                                  iconData: FamedlyIcons.camera),
                            ));
                          }
                          return entries;
                        },
                        icon: Icon(
                          FamedlyIconsV2.attachment,
                          color: Theme.of(context).appBarTheme.iconTheme.color,
                          size: 24,
                        ),
                      ),
                    ),
                    SizedBox(width: 12, height: 40),
                    Expanded(
                      child: WebEnterListener(
                        key: Key('input'),
                        focusNode: inputFocus,
                        onEnter: submit,
                        child: TypeAheadField(
                          textFieldConfiguration: TextFieldConfiguration(
                            onChanged: (text) {
                              isTypingCoolDown?.cancel();
                              isTypingCoolDown =
                                  Timer(typingCoolDownDuration, () {
                                isTypingCoolDown = null;
                                isTyping = false;
                                try {
                                  widget.room.setTyping(false);
                                } catch (_) {}
                              });
                              isTypingTimeout ??=
                                  Timer(typingTimeoutDuration, () {
                                isTypingTimeout = null;
                                isTyping = false;
                              });
                              if (!isTyping) {
                                isTyping = true;
                                try {
                                  widget.room.setTyping(true,
                                      timeout:
                                          typingTimeoutDuration.inMilliseconds);
                                } catch (_) {}
                              }
                              setState(() => this.text = text);
                            },
                            onSubmitted: (text) {
                              if (kIsWeb) return;
                              submit();
                              FocusScope.of(context).requestFocus(inputFocus);
                            },
                            maxLines: 7,
                            minLines: 1,
                            keyboardType: !kIsMobile
                                ? TextInputType.text
                                : TextInputType.multiline,
                            controller: _inputController,
                            style: TextStyle(
                              color:
                                  Theme.of(context).textTheme.bodyText1.color,
                              fontSize: 15,
                            ),
                            focusNode: inputFocus,
                            decoration: InputDecoration(
                              filled: false,
                              fillColor: Theme.of(context).backgroundColor,
                              contentPadding: EdgeInsets.all(11),
                              isCollapsed: true,
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8),
                                borderSide: BorderSide(
                                    style: BorderStyle.solid,
                                    color: FamedlyColors.paleGreyLight),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8),
                                borderSide: BorderSide(
                                    width: 2,
                                    style: BorderStyle.solid,
                                    color: FamedlyColors.azul),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8),
                                borderSide: BorderSide(
                                    style: BorderStyle.solid,
                                    color: FamedlyColors.paleGreyLight),
                              ),
                              hintText: L10n.of(context).message,
                              hintStyle: TextStyle(
                                color: FamedlyColors.cloudyBlue,
                                fontSize: 15,
                              ),
                            ),
                            textCapitalization: TextCapitalization.sentences,
                            autofocus: !kIsMobile &&
                                AdaptivePageLayout.of(context)
                                    .columnMode(context),
                            onEditingComplete: !kIsMobile ? () => null : null,
                            textInputAction: !kIsMobile
                                ? TextInputAction.done
                                : TextInputAction.newline,
                          ),
                          direction: AxisDirection.up,
                          hideOnEmpty: true,
                          hideOnLoading: true,
                          keepSuggestionsOnSuggestionSelected: true,
                          debounceDuration: Duration(milliseconds: 50),
                          suggestionsCallback: getSuggestions,
                          onSuggestionSelected:
                              (Map<String, String> suggestion) =>
                                  insertSuggestion(context, suggestion),
                          itemBuilder: buildSuggestion,
                        ),
                      ),
                    ),
                    SizedBox(width: 16.0, height: 40.0),
                    LongPressDraggable(
                      maxSimultaneousDrags: !canRecord || _isRecording ? 0 : 1,
                      hapticFeedbackOnStart: true,
                      axis: Axis.horizontal,
                      feedback: SizedBox(
                        height: 200,
                        width: 200,
                        child: Stack(
                          clipBehavior: Clip.none,
                          children: <Widget>[
                            Positioned(
                              left: -_Button.largeSize.width / 3,
                              top: -_Button.largeSize.height / 3,
                              child: _LargeMicrophoneButton(),
                            )
                          ],
                        ),
                      ),
                      childWhenDragging: Container(),
                      onDragStarted: () async {
                        await _startRecording();
                      },
                      onDraggableCanceled: (_, __) async {
                        if (!_isRecording) return;
                        await _stopRecording();
                        _sendRecording();
                      },
                      onDragCompleted: () async {
                        if (!_isRecording) return;
                        await _stopRecording();
                        await Vibration.vibrate(duration: 100);
                      },
                      child: button,
                    ),
                  ],
                );
              }),
            ),
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
    _recorderSubscription?.cancel();
    _onKeyboardVisibilityChangedSub?.cancel();
  }
}

class _Button extends StatelessWidget {
  static const size = Size(40, 40);
  static const largeSize = Size(115, 115);

  final VoidCallback onPressed;
  final IconData icon;
  final bool large;

  const _Button({
    Key key,
    this.onPressed,
    @required this.icon,
    this.large = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox.fromSize(
      size: large ? largeSize : size,
      child: RawMaterialButton(
        key: Key('sendButton'),
        onPressed: onPressed,
        shape: CircleBorder(),
        elevation: 0,
        fillColor: FamedlyColors.azul,
        child: Icon(
          icon,
          color: Colors.white,
          size: large ? 47 : 24,
        ),
      ),
    );
  }
}

class _MicrophoneButton extends StatelessWidget {
  final VoidCallback onPressed;

  const _MicrophoneButton({Key key, this.onPressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return _Button(
      icon: FamedlyIconsV2.microphone,
      onPressed: onPressed,
    );
  }
}

class _LargeMicrophoneButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return _Button(
      icon: FamedlyIconsV2.microphone,
      large: true,
    );
  }
}

class _SendButton extends StatelessWidget {
  final VoidCallback onPressed;

  const _SendButton({
    Key key,
    @required this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return _Button(
      icon: FamedlyIcons.send,
      onPressed: onPressed,
    );
  }
}

class _RecordingStatus extends StatelessWidget {
  final Duration recordingDuration;

  const _RecordingStatus({Key key, @required this.recordingDuration})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Icon(
          FamedlyIconsV2.microphone,
          color: Colors.red,
          size: 32,
        ),
        SizedBox(
          width: 12,
          height: 40, // this is the height of the input bar we emulate here
        ),
        Expanded(
          child: Text(recordingDuration.recordingFormat()),
        ),
        DragTarget(builder: (BuildContext context, _, __) {
          return Row(
            children: <Widget>[
              Icon(
                FamedlyIconsV2.close,
                color: Colors.red,
                size: 22,
              ),
              Text(
                L10n.of(context).cancel,
                style: TextStyle(
                  color: Colors.red,
                ),
              ),
              SizedBox(
                width: _Button.largeSize.width / 2,
              ),
            ],
          );
        }),
        SizedBox(
          width: _Button.largeSize.width / 2,
        ),
      ],
    );
  }
}
