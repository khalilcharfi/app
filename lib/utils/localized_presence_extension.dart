/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/cupertino.dart';
import 'date_time_extension.dart';

extension LocalizedPresenceExtension on Presence {
  String getLocalized(BuildContext context) {
    if (presence.currentlyActive == true) {
      return L10n.of(context).currentlyActive;
    }
    if (presence.lastActiveAgo == null) {
      return L10n.of(context).lastSeenLongTimeAgo;
    }
    final time = DateTime.fromMillisecondsSinceEpoch(
        DateTime.now().millisecondsSinceEpoch - presence.lastActiveAgo);
    return L10n.of(context).lastActiveAgo(time.localizedTimeShort(context));
  }
}
