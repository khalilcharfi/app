/*
 *   Famedly
 *   Copyright (C) 2019, 2020, 2021 Famedly GmbH
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as
 *   published by the Free Software Foundation, either version 3 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import 'package:famedly/styles/famedly_icons_v2_icons.dart';
import 'package:famedlysdk/famedlysdk.dart' hide Visibility;
import 'package:flutter/material.dart';
import 'package:adaptive_page_layout/adaptive_page_layout.dart';

import 'package:flutter_gen/gen_l10n/l10n.dart';
import '../styles/colors.dart';
import 'avatar.dart';
import '../models/famedly.dart';
import 'user_settings_popup_menu.dart';

class MemberListItem extends StatelessWidget {
  final User contact;

  const MemberListItem(this.contact);

  String _getMembershipString(Membership membership, BuildContext context) {
    switch (membership) {
      case Membership.join:
        return L10n.of(context).connected;
      case Membership.ban:
        return L10n.of(context).banned;
      case Membership.invite:
        return L10n.of(context).invited;
      case Membership.leave:
        return L10n.of(context).leftTheRoom;
    }
    return L10n.of(context).connected;
  }

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onLongPress: () => UserSettingsPopupMenu(context, contact).show(),
      leading: Avatar(
        contact.avatarUrl,
        size: 44,
        name: contact.calcDisplayname(mxidLocalPartFallback: false),
      ),
      title: Wrap(
        children: <Widget>[
          Text(
            contact.calcDisplayname(mxidLocalPartFallback: false),
            style: TextStyle(fontSize: 15),
          ),
          Visibility(
            visible: contact != null &&
                contact.powerLevel != null &&
                contact.powerLevel >= 100,
            child: Padding(
              padding: EdgeInsets.only(left: 10),
              child: Container(
                width: 59,
                height: 18,
                decoration: BoxDecoration(
                  color: FamedlyColors.azul,
                  borderRadius: BorderRadius.circular(3),
                ),
                child: Padding(
                  padding:
                      EdgeInsets.only(left: 13, right: 13, top: 3, bottom: 2),
                  child: Text(
                    L10n.of(context).admin,
                    style: TextStyle(
                      color: Theme.of(context).backgroundColor,
                      fontSize: 11,
                      fontWeight: FontWeight.w600,
                      fontStyle: FontStyle.normal,
                      letterSpacing: 0,
                    ),
                  ),
                ),
              ),
            ),
          ),
          Visibility(
            visible: contact != null &&
                contact.powerLevel != null &&
                contact.powerLevel >= 50 &&
                contact.powerLevel < 100,
            child: Padding(
              padding: EdgeInsets.only(left: 10),
              child: Container(
                height: 18,
                decoration: BoxDecoration(
                  color: FamedlyColors.azul,
                  borderRadius: BorderRadius.circular(3),
                ),
                child: Padding(
                  padding:
                      EdgeInsets.only(left: 13, right: 13, top: 3, bottom: 2),
                  child: Text(
                    L10n.of(context).moderator,
                    style: TextStyle(
                      color: Theme.of(context).backgroundColor,
                      fontSize: 11,
                      fontWeight: FontWeight.w600,
                      fontStyle: FontStyle.normal,
                      letterSpacing: 0,
                    ),
                  ),
                ),
              ),
            ),
          )
        ],
      ),
      subtitle: Text(
        _getMembershipString(contact.membership, context),
        style: TextStyle(
          color: FamedlyColors.blueyGrey,
          fontSize: 13,
          fontWeight: FontWeight.w400,
          fontStyle: FontStyle.normal,
          letterSpacing: 0,
        ),
      ),
      trailing: Icon(
        FamedlyIconsV2.right_1,
        color: FamedlyColors.blueyGrey,
      ),
      onTap: () {
        if (contact.id != Famedly.of(context).client.userID) {
          AdaptivePageLayout.of(context)
              .pushNamed('/room/${contact.room.id}/member/${contact.id}');
        } else {
          AdaptivePageLayout.of(context).pushNamed(routeName());
        }
      },
    );
  }

  String routeName() => '/settings';
}
