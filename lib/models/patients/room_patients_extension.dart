import 'package:famedlysdk/famedlysdk.dart';
import 'client_patients_extension.dart';
import 'patient_data.dart';

extension RoomPatientsExtension on Room {
  /// Returns the [PatientData] of this patient room. If this room isn't
  /// a patient room then this returns null.
  PatientData get patientData => getState(patientRoomNamespace) != null
      ? PatientData.fromJson(getState(patientRoomNamespace).content)
      : null;

  Future<void> setPatientData(PatientData patientData) async {
    await client.request(
      RequestType.PUT,
      '/client/r0/rooms/$id/state/$patientRoomNamespace/',
      data: patientData.toJson(),
    );
    return;
  }
}
